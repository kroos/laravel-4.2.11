<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 *
 */

class OrderApi {

    public function generateID($uid, $orderType) {
        $id = '';

        if($orderType == Order::ORDER_PACKAGE_JOIN_NETWORK || $orderType == Order::ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER) {
            $id .= 'JN';    // Join network
        } elseif($orderType == Order::ORDER_PACKAGE_OF_CREATE_NETWORK) {
            $id .= 'CN';    // Create network
        } else {
            $id .= 'UV';    // Upgrade VIP level
        }

        $id .= '-';
        $id .= $uid;

        $id .= '-';

        $salt = time() . $uid . rand();
        $md5OfSalt = md5($salt);

        $id .= substr($md5OfSalt, 11, 12);

        return $id;
    }


    public function saveNetworkOrder() {

    }

    public function saveGeneralOrder() {

    }

    public function findNetworkOrder($id) {

    }

    public function findGeneralOrder($id) {

    }

    public function updateNetworkOrder() {

    }

    public function updateGeneralOrder() {
    }


    public function getOrder($uid, $id) {
        return Order::where('uid', '=', $uid)
            ->where('id', '=', $id)
            ->first();
    }

    public function getOrderByOrderId($uid, $orderId) {
        return Order::where('uid', '=', $uid)
            ->where('orderid', '=', $orderId)
            ->first();
    }

    public function getOrders($uid, $status) {
        return Order::where('uid', '=', $uid)
            ->where('status', '=', $status)
            ->orderBy('creattime', 'DESC')
            ->get();
    }

    public function afterPayment(Order $order, $txnId) {

    }

    public function calculateOverage($uid, $packageId, $isAutoRenew, $orderStatus) {

        if($packageId == Order::ORDER_PACKAGE_JOIN_NETWORK
            || $packageId == Order::ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER
            || $packageId == Order::ORDER_PACKAGE_OF_CREATE_NETWORK) {
            return 0;
        }

        $userApi = new UsersApi;
        $currentUser = $userApi->getCurrentUser();

        $vipLevel = $currentUser->vip_level;

        $orders = Order::where('uid', '=', $uid)
            //->where('status', '=', Order::ORDER_STATUS_PAID)
            ->where('package', '=', $vipLevel)
            ->orderBy('id', 'DESC')
            ->get();

        if(null == $orders || count($orders) == 0) {
            return 0;
        }

        $lastOrderHistory = $this->getLastUpgradeOrderOrRecurringOrderHistory($uid);
        if(empty($lastOrderHistory)){
            $orderHistories = array();
        }else{
            $orderHistories = $this->getAllOrderHistoriesOfSamePackageLevel($lastOrderHistory->function_related_id);
        }

        Log::info("calculateOverage orderHistories:". json_encode($orderHistories));
        $overage = 0;
        foreach($orderHistories as $orderHistory){
            $periodTotalPaid = $orderHistory->overage + $orderHistory->payments;
            $now = strtotime(date("Y-m-d"));
            $fromTime = strtotime(explode(' ', $orderHistory->actual_start_date)[0]);
            $endTime = strtotime(explode(' ', $orderHistory->expiry_date)[0]);
            if($now >= $fromTime && $now <= $endTime){
                $periodOverage = ($endTime - $now) / ($endTime - $fromTime) * $periodTotalPaid;
            }else if($now < $fromTime){
                $periodOverage = $periodTotalPaid;
            }else if($now > $endTime){
                $periodOverage = 0;
            }
            Log::info("calculateOverage periodOverage: ".json_encode($periodOverage));
            $overage += $periodOverage;
        }

        return $overage;

    }

    public function judgeIfOverageOverNextLevelExpense($overage, $nextLevelSinglePrice, $purchaseType){
        $data = new stdClass();
        // 升级的情况计算新等级最少买几个月
        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade')){
            $data->quantity = ceil($overage / $nextLevelSinglePrice);
            $data->quantity = $data->quantity == 0 ? $data->quantity + 1 : $data->quantity;
            $data->restFee = $overage % $nextLevelSinglePrice;
        }else{
            // 原等级续费情况，默认至少买一个月
            $data->quantity = 1;
        }
        return $data;
    }

    public function getValidOrders($uid, $status, $timeFrom) {
        return Order::where('uid', '=', $uid)
            ->where('status', '=', $status)
            ->where('creattime', '>', $timeFrom)
            ->orderBy('creattime', 'DESC')
            ->get();
    }

    public function deleteOrder($id) {

        $uid = Session::get('uid');

        return Order::where('id', '=', $id)->where('uid', '=', $uid)->delete();

    }


    public function afterPaid($orderId, $amount, $txnId) {

        /*
        $orderId = $myPaypal->ipnData['item_number'];
        $amount =  $myPaypal->ipnData['mc_gross'];
        $txnId = $myPaypal->ipnData['txn_id'];
        */

        $uid = Session::get('uid');

        $order = self::getOrderByOrderId($uid, $orderId);

        if(null == $order || empty($order)) {
            return false;
        }

        $overage = 0.0;

        // if($order->package != 0) {
            //$overage = self::calculateOverage($uid, $order->package);
        $overage = self::calculateOverage($uid, $order->package, false, Order::ORDER_STATUS_PAID);
        // }

        $shouldPaid = $order->price - $overage;
        if( ($shouldPaid - $amount) > $shouldPaid * 0.01) {
            return false;
        }

        $nextYear = mktime(0,0,0,date("m"),date("d"),date("Y")+1);

        $order->status = Order::ORDER_STATUS_PAID;
        $order->fromtime = time();
        $order->endtime = $nextYear;
        $order->paidtime = time();
        $order->overage = $overage;
        $order->payments = $amount;
        $order->txnid = $txnId;

        $order->save();

        // Join Network
        if($order->package == Order::ORDER_PACKAGE_JOIN_NETWORK) {
            $networkApi = new NetworkApi;
            $networkApi->joinNetwork($order->networkid, $uid);
        } elseif($order->package == Order::ORDER_PACKAGE_OF_CREATE_NETWORK) {   // Update status of this network
            $networkApi = new NetworkApi;
            $parameters = array("is_activate" => 1);
            $networkApi->update($order->networkid, $parameters);
        } else { // Upgrade VIP Level
            $userApi = new UsersApi;
            $user = $userApi->getCurrentUser();
            $user->vip_level = $order->package; // package = vip level

            $user->save();
        }

    }


    function sendNotifyMail($order) {

/*        $SUBJECT = 'TraDove Payment Invoice';

        if ( !empty($order) ) {

            $user_id = $order['user_id'];

            $pre_level = $order['pre_level'];

            $annual_or_month = $order['pay_period'];
            $to_level = $order['to_level'];
            $quantity = $order['quantity'];

            $prices = Order::getSettingPrices();

            $prices_annual = $prices["annual"];
            $prices_month = $prices["month"];

            if( $annual_or_month == PAY_PERIOD_ANNUAL ) {
                $current_price = $prices_annual[$to_level];
            } else {
                $current_price = $prices_month[$to_level];

            }

            $current_level = $order['to_level'];

            $current_level_note = Order::$_LEVEL_TITLE[$current_level];

            if( $order['pay_period'] == PAY_PERIOD_ANNUAL ) {
                $current_level_note .= '(Annual)';
                $current_price_note = '$' . $current_price . '/Annual';
            } else {
                $current_level_note .= '(Month)';
                $current_price_note = '$' . $current_price . '/Month';
            }

            $current_start_date = getDateByTimestamp( $order['from_date'] );
            $current_end_date = getDateByTimestamp(  $order['end_date'] );
            $current_span = 'From ' . $current_start_date . ' To ' . $current_end_date;


            $user = User::getUserById( $user_id );

            $mailaddress = $user['email'];
            $contactname = $user['first_name'] . ' ' . $user['last_name'];

            $BODY = "Dear $contactname:\r\n\r\n";

            $BODY .= "Thank you for subscribing to TraDove paid services. The following are your payment details:\r\n\r\n";

            $BODY .= "----------------------------------------------------------------------------------\r\n";

            $BODY .= "Current Subscription: " . Order::$_LEVEL_TITLE[$pre_level] . "\r\n";

            $BODY .= "----------------------------------------------------------------------------------\r\n";

            $BODY .= "New Subscription: " . $current_level_note . "\t\t" . $current_price_note . "\r\n";
            $BODY .= "Quantity: " . $order['quantity'] . "\r\n";
            $BODY .= $current_span . "\r\n\r\n";

            $BODY .= "Total Purchase:\t\t\t\t$" . $order['amount'] . "\r\n";
            $BODY .= "Discount/Credits:\t\t\t($" . $order['balance'] . ")\r\n";
            $BODY .= "Estimated Sales Tax:\t\t$0.00\r\n";

            $BODY .= "----------------------------------------------------------------------------------\r\n";
            $BODY .= "Total Paid\t\t\t\t\t$" . $order['actually_pay'] . "\r\n\r\n";


            $BODY .= "Please contact our customer service at customer.service@tradove.com with any questions or problems.\r\n\r\n";

            $BODY .= "TraDove Customer Service\r\n";
            $BODY .= "http://www.tradove.com";

            $from = MANAGER_EMAIL_ADDRESS;
            $to = $mailaddress;

            send_mail_by_yahoo( $from, $to, $SUBJECT, $BODY );*/

        }


    /**
     * Validate the IPN notification
     *
     * @param none
     * @return boolean
     */
    public function validateIpn()
    {
        // parse the paypal URL
        $urlParsed = parse_url($this->gatewayUrl);

        // generate the post string from the _POST vars
        $postString = '';

        foreach ($_POST as $field=>$value)
        {
            $this->ipnData["$field"] = $value;
            $postString .= $field .'=' . urlencode(stripslashes($value)) . '&';
            $_SESSION["$field"] = $value;
        }

        $postString .="cmd=_notify-validate"; // append ipn command

        // open the connection to paypal
        // $fp = fsockopen( $urlParsed[host], "80", $errNum, $errStr, 30);
        $fp = fsockopen( 'ssl://' . $urlParsed[host], "443", $errNum, $errStr, 30);
        if(!$fp)
        {
            // Could not open the connection, log error if enabled
            $this->lastError = "fsockopen error no. $errNum: $errStr";
            $this->logResults(false);

            return false;
        }
        else
        {
            // Post the data back to paypal

            fputs($fp, "POST $urlParsed[path] HTTP/1.1\r\n");
            fputs($fp, "Host: $urlParsed[host]\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($postString) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $postString . "\r\n\r\n");

            // loop through the response from the server and append to variable
            while(!feof($fp))
            {
                $this->ipnResponse .= fgets($fp, 1024);
            }

            fclose($fp); // close connection
        }

        if (eregi("VERIFIED", $this->ipnResponse))
        {
            // Valid IPN transaction.
            $this->logResults(true);
            return true;
        }
        else
        {
            // Invalid IPN transaction. Check the log for details.
            $this->lastError = "IPN Validation Failed . $urlParsed[path] : $urlParsed[host]";
            $this->logResults(false);
            return false;
        }
    }

    public function getByOrderId($orderId){
        return Order::where("orderid", $orderId)->first();
    }

    public function getByUserIdAndType($uid, $type){
        return Order::where("uid", $uid)->where("type", $type)->orderBy("id", "desc")->first();
    }

    public function getByUserIdAndTypeAndRenewType($uid, $type, $isAutoRenew){
        $query = Order::where("uid", $uid)->where("type", $type);
        if($isAutoRenew){
           $query = $query-> where("is_auto_renew", 1);
        }else{
            $query =$query->where(function($innerQuery){
                $innerQuery->whereNull("is_auto_renew")
                    ->orWhere("is_auto_renew", 0);
            });
        }
        $query = $query->orderBy("id", "desc")->first();
        return $query;
    }

    public function getByUserIdAndTypeAndStatus($uid, $type, $status){
        return Order::where("uid", $uid)->where("type", $type)
            ->where("status", $status)
            ->orderBy("id", "desc")->first();
    }

    public function getByUserIdAndTypeAndRenewTypeAndStatus($uid, $type, $isAutoRenew, $status){
        $query = Order::where("uid", $uid)->where("type", $type);
        if($isAutoRenew){
            $query = $query-> where("is_auto_renew", 1);
        }else{
            $query =$query->where(function($innerQuery){
                $innerQuery->whereNull("is_auto_renew")
                    ->orWhere("is_auto_renew", 0);
            });
        }
        $query = $query->where("status", $status);
        $query = $query->orderBy("id", "desc")->first();
        return $query;
    }

    public function getOrderHistoryByOrderId($orderId){
        return OrderHistory::where("orderid", $orderId)->first();
    }

    public function getLastUpgradeOrderOrRecurringOrderHistory($uid){
        return OrderHistory::where("uid", $uid)
        ->whereIn("status", array(Order::ORDER_STATUS_PAID, Order::ORDER_STATUS_RECURRING_AUTO_PAID, Order::ORDER_STATUS_CANCELED))
        ->orderBy("id", "desc")->first();
    }

    public function getLastUpgradeOrderOrRecurringOrder($uid){
        $query = Order::where("uid", $uid);
        $query = $query->where(function($innerQuery){
            $innerQuery->where("status", Order::ORDER_STATUS_PAID)
                ->orWhere("status", Order::ORDER_STATUS_RECURRING_AUTO_PAID);
        });
        $query = $query->orderBy("id", "desc")->first();
        return $query;
    }

    public function getLastUpgradeRecurringOrder($uid){
        $query = Order::where("uid", $uid);
        $query = $query->where(function($innerQuery){
            $innerQuery->where("status", Order::ORDER_STATUS_RECURRING_AUTO_PAID)
                ->orWhere("status", Order::ORDER_STATUS_CANCELED);
        });
        $query = $query->orderBy("id", "desc")->first();
        return $query;
    }

    public function getLastUpgradeOrderHistory($uid, $isAutoRenew, $orderStatus){
        $query = OrderHistory::where("uid", $uid)
            ->where("function_type", PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE)
            ->where("status", 1);
        if($isAutoRenew){
            $query = $query-> where("is_auto_renew", 1);
        }else{
            $query = $query->where(function($innerQuery){
                $innerQuery->whereNull("is_auto_renew")
                    ->orWhere("is_auto_renew", 0);
            });
        }
        $query = $query->where("status", $orderStatus);
        $query = $query->orderBy("id", "desc")->first();

        return $query;
    }

    public function getAllOrderHistoriesOfSamePackageLevel($functionRelatedId){
        return OrderHistory::where("function_related_id", $functionRelatedId)
            ->whereIn("status", array(Order::ORDER_STATUS_PAID, Order::ORDER_STATUS_RECURRING_AUTO_PAID, Order::ORDER_STATUS_CANCELED))
            ->orderBy("id", "asc")->get();
    }

    public function getOrderHistoriesOfSamePackageLevel($functionRelatedId, $isAutoRenew){
        return OrderHistory::where("function_related_id", $functionRelatedId)
            ->where(function($innerQuery){
            $innerQuery->where("status", Order::ORDER_STATUS_PAID)
                ->orWhere("status", Order::ORDER_STATUS_RECURRING_AUTO_PAID);
            })
            ->where("is_auto_renew", $isAutoRenew)
            ->orderBy("id", "asc")->get();
    }

    public function getInstantOrderHistories($functionRelatedId){
        return OrderHistory::where("function_related_id", $functionRelatedId)
            ->where("status", Order::ORDER_STATUS_PAID)
            ->where("is_auto_renew", 0)
            ->orderBy("id", "asc")->get();
    }

    /**
     * 更新支付后的订单
     *
     * @param $order
     */
    public function updatePaidOrder($order, $orderHistory, $callbackType){

        Log::info("updatePaidOrder order status: ". $order->status);

        // 自动购买的情况， 需要更新时间
        if($callbackType == PAYMENT_SUBSCRIPTION_PAYMENT_TXN_TYPE
            && $orderHistory->status == Order::ORDER_STATUS_RECURRING_AUTO_PAID
            && $order->status == Order::ORDER_STATUS_RECURRING_AUTO_PAID){
            $orderHistory->endtime = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->endtime)));

            $originalExpiryDate = $orderHistory->expiry_date;
            $orderHistory->actual_start_date = date('Y-m-d', strtotime('+1 day', strtotime($originalExpiryDate)));
            $orderHistory->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->actual_start_date)));

            // 修改即时订单时间
            $instantOrder = $this->getInstantOrder($orderHistory->uid);
            if(!empty($instantOrder)){
                $instantOrder->fromtime = date('Y-m-d', strtotime('+1 month', strtotime($instantOrder->fromtime)));
                $instantOrder->endtime = date('Y-m-d', strtotime('+1 month', strtotime($instantOrder->endtime)));
                $instantOrder->save();
            }
        }

        if($callbackType == PAYMENT_INSTANT_TXN_TYPE){
            $order->status = Order::ORDER_STATUS_PAID;
            $orderHistory->status = Order::ORDER_STATUS_PAID;
        }else if($callbackType == PAYMENT_SUBSCRIPTION_SIGN_UP_TXN_TYPE){
            if($order->status != Order::ORDER_STATUS_RECURRING_AUTO_PAID){
                $order->status = Order::ORDER_STATUS_RECURRING_SIGNED_UP;
                $orderHistory->status = Order::ORDER_STATUS_RECURRING_SIGNED_UP;
            }
        }else{
            $order->status = Order::ORDER_STATUS_RECURRING_AUTO_PAID;
            $orderHistory->status = Order::ORDER_STATUS_RECURRING_AUTO_PAID;
        }
        // 防止自动购买ipn连发两次消息引起的脏读
        $order->save();
        Log::info("updatePaidOrder order after status: ". $order->status);
        $orderHistory->save();
        // 获取当前等级所有的renew的订单历史记录方便计算总价格和时间范围
        $samePackageOrderHistories = $this->getOrderHistoriesOfSamePackageLevel((int)$orderHistory->function_related_id, $orderHistory->is_auto_renew);


        Log::info("updatePaidOrder order: ".json_encode($order));
        Log::info("updatePaidOrder orderHistory: ".json_encode($orderHistory));
        $order->orderid = $orderHistory->orderid;
        $order->nid = $orderHistory->nid;
        $order->price = $orderHistory->price;
        $order->period = $orderHistory->period;
        $order->month_count = $orderHistory->month_count;
        $order->quantity = 0;
        $order->description = $orderHistory->description;
        $order->creattime = $orderHistory->creattime;
        $order->fromtime = $orderHistory->actual_start_date;
        $order->endtime = $orderHistory->expiry_date;
        $order->paidtime = time();
        $order->discount = 0;
        $order->overage = $orderHistory->overage;
        $order->tax = $orderHistory->discount;
        $order->payments = 0;
        $order->txnid = $orderHistory->txnid;
        $order->is_auto_renew = $orderHistory->is_auto_renew;

        $order->package = $orderHistory->package;

        // 计算所有同一package的总费用，时间范围，购买数量
        Log::info("updatePaidOrder samePackageOrderHistory started. ");
        $instantOrderHistories = array();
        foreach($samePackageOrderHistories as $index => $samePackageOrderHistory){
            Log::info("updatePaidOrder samePackageOrderHistory payments: ". $samePackageOrderHistory->payments);
            // 总费用
            $order->payments += $samePackageOrderHistory->payments;
            // 购买数量
            $order->quantity += $samePackageOrderHistory->quantity;
            // 打折
            $order->discount += $samePackageOrderHistory->discount;

            array_push($instantOrderHistories, $samePackageOrderHistory);
        }
        if(count($instantOrderHistories) > 0){
            $order->fromtime = $instantOrderHistories[0]->actual_start_date;
            $order->endtime = $instantOrderHistories[count($instantOrderHistories) - 1]->expiry_date;
        }
        Log::info("updatePaidOrder instantOrderHistories". json_encode($instantOrderHistories));

        $order->save();

        // 如果是升级的情况，更新上个等级的order history（paid或者auto_paid）状态为canceled
//        $instantOrder = $this->getInstantOrder($orderHistory->uid);
//        $recurringOrder = $this->getRecurringOrder($orderHistory->uid);
//        if($orderHistory->operationType == PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE && (!empty($instantOrder) || !empty($recurringOrder))){
//            $this->cancelOrderHistories($orderHistory->uid);
//        }
    }

    public function getNearExpiryOrder($uid, $expiryDate, $currentDate){
        return Order::join('users', 'users.id', '=', 'orders.uid')
            ->where('orders.uid', $uid)
            ->where(function($query){
                $query->whereNull('orders.is_auto_renew')
                    ->orWhere('orders.is_auto_renew', 0);
            })->where('orders.endtime', '<=', $expiryDate)
            ->where('orders.endtime', '>', $currentDate)
            ->where('users.vip_level', '<>', 1)
            ->first();
    }

    public function disableRecurringOrder($uid){
        // cancel orders
        Order::where('uid', $uid)
            ->where('is_auto_renew', 1)
            ->where('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID)
            ->update(array('status'=>Order::ORDER_STATUS_DISABLED));
        OrderHistory::where('uid', $uid)
            ->where('is_auto_renew', 1)
            ->update(array('status'=>Order::ORDER_STATUS_DISABLED));
    }
    public function disableOrders($uid){
        $query = Order::where('uid', $uid);
        $query = $query -> where(function($innerQuery){
            $innerQuery->where('status', Order::ORDER_STATUS_PAID)
                ->orWhere('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID);
        });
        return $query->update(array('status', Order::ORDER_STATUS_DISABLED));
    }

    public function cancelRecurringOrder($uid){
        // cancel orders
        Order::where('uid', $uid)
            ->where('is_auto_renew', 1)
            ->where('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID)
            ->update(array('status'=>Order::ORDER_STATUS_CANCELED));
        OrderHistory::where('uid', $uid)
            ->where('is_auto_renew', 1)
            ->update(array('status'=>Order::ORDER_STATUS_CANCELED));
    }

    public function cancelOrders($uid){
        $query = Order::where('uid', $uid);
        $query = $query -> where(function($innerQuery){
            $innerQuery->where('status', Order::ORDER_STATUS_PAID)
                ->orWhere('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID);
        });
        return $query->update(array('status', Order::ORDER_STATUS_CANCELED));
    }
    public function cancelOrderHistories($uid){
        $query = OrderHistory::where('uid', $uid);
        $query = $query -> where(function($innerQuery){
            $innerQuery->where('status', Order::ORDER_STATUS_PAID)
                ->orWhere('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID);
        });
        return $query->update(array('status', Order::ORDER_STATUS_CANCELED));
    }

    public function postponeInstantOrderOneMonth($uid){
        $orderHistory = OrderHistory::where("uid", $uid)
            ->where(function($query){
            $query->whereNull('is_auto_renew')
                ->orWhere('is_auto_renew', 0);
            })
            ->orderBy("id", "desc")
            ->first();
        // 先执行自动续费，不执行instant order延时的逻辑，因为instant order不存在
        if(empty($orderHistory)){
            return;
        }
        if(empty($orderHistory->expiry_date)){
            $orderHistory->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->endtime)));
        }else{
            $orderHistory->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->expiry_date)));
        }
        $orderHistory->save();

        $order = Order::where("uid", $uid)
            ->where(function($query){
            $query->whereNull('is_auto_renew')
                ->orWhere('is_auto_renew', 0);
            })
            ->orderBy("id", "desc")
            ->first();
        if(empty($order->expiry_date)){
            $order->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->endtime)));
        }else{
            $order->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($order->expiry_date)));
        }

        $order->save();
    }

    public function getRecurringOrder($uid){
        return Order::where('uid', $uid)
            ->where('is_auto_renew', 1)
            ->where('type', PAYMENT_ORDER_VIP_TYPE)
            ->where(function($innerQuery){
                $innerQuery->where('status', Order::ORDER_STATUS_RECURRING_AUTO_PAID)
                    ->orWhere('status', Order::ORDER_STATUS_CANCELED);
            })
            ->orderBy('id', 'desc')->first();
    }

    public function getInstantOrder($uid){
        return Order::where('uid', $uid)
            ->where('is_auto_renew', 0)
            ->where('type', PAYMENT_ORDER_VIP_TYPE)
            ->where('status', Order::ORDER_STATUS_PAID)
            ->orderBy('id', 'desc')->first();
    }
}