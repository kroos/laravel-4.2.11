<?php

define("MD5_OF_TRADOVE", "8c261dd6468ae95b33cf948804e46ff2");

define('COL_MEMBER_TYPE', 0);
define('COL_FNAME', 1);
define('COL_LNAME', 2);
define('COL_CURRENT_JOB', 3);
define('COL_PHONE', 4);
define('COL_EMAIL', 5);
define('COL_CURRENT_COMPANY', 6);
define('COL_CITY', 7);
define('COL_STATE', 8);
define('COL_ZIPCODE', 9);
define('COL_COUNTRY', 10);
define('COL_WEBLINK', 12);
define('COL_CURRENT_INDUSTRY', 18);
define('COL_DIVERSITY', 19);

define('COL_IS_WOMAN', 14);
define('COL_IS_MINORITY', 15);

define('COL_COM_NAME', 6);
define('COL_COM_CITY', 7);
define('COL_COM_STATE', 8);
define('COL_COM_ZIPCODE', 9);
define('COL_COM_COUNTRY', 10);
define('COL_COM_PHONE', 11);
define('COL_COM_WEBLINK', 12);
define('COL_COM_DESCRIPTION', 13);
define('COL_COM_SALE', 16);
define('COL_COM_EMPLOYEES', 17);
define('COL_COM_INDUSTRY', 18);
define('COL_COM_DIVERSITY', 19);
define('COL_COM_USER_BELONGS_NETWORK', 20);


class UsersApi
{

    function __construct()
    {
    }


    public function getCurrentUser()
    {

        $userId = Session::get("uid");

        //$user = CacheApi::getUser($userId);

        //if (empty($user)) {
            $user = Users::find($userId);
        //}

        return $user;
    }


    /**
     * 通过ID查找用户,先查找缓存,在查找数据库。
     * 如果缓存中没有该用户,数据库中存在,那么将该用户信息写入缓存
     * @param $id
     * @return null|Users
     */
    public function getUserById($id)
    {
        /*$user = CacheApi::getUser($id);
        if (is_null($user)) {
            $user = Users::find($id);
            if (!empty($user)) {
                CacheApi::setUser($user);
                CacheApi::setUserProperty($user->id, 'password', $user->password);
                if ($user->exists == false) {
                    CacheApi::setAllUserCountInc();
                }
            }
        }*/
        $user = Users::find($id);
        return $user;
    }

    public function getCurrentVipLevel() {
        $user = $this->getCurrentUser();

        return $user->vip_level;
    }

    public function upgradeVipLevel($uid, $vipLevel){
        $user = Users::find($uid);
        $user->vip_level = $vipLevel;
        $this->save($user);
    }

    public function save($user)
    {
        $user->save();
    }
}