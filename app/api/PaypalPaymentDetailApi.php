<?php
/**
 * Created by JetBrains PhpStorm.
 * User: long
 * Date: 16-6-18
 * Time: 上午11:05
 * To change this template use File | Settings | File Templates.
 */
class PaypalPaymentDetailApi
{
    public function savePaymentDetail($paymentDetail){
        PaypalPaymentDetail::insert($paymentDetail);
    }

    public function getPaymentDetail($orderId){
        return PaypalPaymentDetail::where("item_number", $orderId)->first();
    }

    public function getLatestPaymentDetail($uid){
        return PaypalPaymentDetail::where("uid", $uid)->orderBy("id", "desc")->first();
    }

    public function getPaymentDetailWithTypeAndOrderId($txnType, $itemNumber){
        return PaypalPaymentDetail::where("item_number", $itemNumber)
            ->where("txn_type", $txnType)->first();
    }

    public function getPaymentDetailsBySubscrId($subscrId){
        return PaypalPaymentDetail::where("subscr_id", $subscrId)->get();
    }
}
