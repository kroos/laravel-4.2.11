<?php

/**
 * Paypal Class
 */

class Paypal extends PaymentGateway
{

    /**
     * Initialize the Paypal gateway
     *
     * @param none
     * @return void
     */
    public function __construct() {
        // parent::__construct();
        // Some default values of the class
        $this->gatewayUrl = 'https://www.paypal.com/cgi-bin/webscr';
        $this->ipnLogFile = '/var/www/laravel-4.2.11/app/storage/logs/paypal.ipn.log';
    }

    /**
     * Enables the test mode
     *
     * @param none
     * @return none
     */
    public function enableTestMode()
    {
        $this->testMode = TRUE;
        $this->gatewayUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    }

    /**
     * Validate the IPN notification
     *
     * @param none
     * @return boolean
     */
    public function validateIpn()
    {
        // parse the paypal URL
        $urlParsed = parse_url($this->gatewayUrl);

        // generate the post string from the _POST vars
        $postString = '';

        foreach ($_POST as $field=>$value)
        {
            $this->ipnData["$field"] = $value;
            $postString .= $field .'=' . urlencode(stripslashes($value)) . '&';
            $_SESSION["$field"] = $value;
        }

        $postString .="cmd=_notify-validate"; // append ipn command
        Log::info("validate post string: ". $postString);

        // open the connection to paypal
        // $fp = fsockopen( $urlParsed[host], "80", $errNum, $errStr, 30);
        $fp = fsockopen( 'ssl://' . $urlParsed['host'], "443", $errNum, $errStr, 30);
        if(!$fp)
        {
            // Could not open the connection, log error if enabled
            $this->lastError = "fsockopen error no. $errNum: $errStr";
            $this->logResults(false);

            return false;
        }
        else
        {
            // Post the data back to paypal

            fputs($fp, "POST $urlParsed[path] HTTP/1.1\r\n");
            fputs($fp, "Host: $urlParsed[host]\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($postString) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $postString . "\r\n\r\n");

            // loop through the response from the server and append to variable
            while(!feof($fp))
            {
                $this->ipnResponse .= fgets($fp, 1024);
            }

            fclose($fp); // close connection
        }

        if (strcmp("VERIFIED", $this->ipnResponse))
        {
            // Valid IPN transaction.
            $this->logResults(true);
            return true;
        }
        else
        {            // Invalid IPN transaction. Check the log for details.
            $this->lastError = "IPN Validation Failed . $urlParsed[path] : $urlParsed[host]";
            $this->logResults(false);

            return false;
        }
    }
}
