<?php

class EmailTemplateManager {

    const EMAIL_RECENT_UPDATES = 1;
    const EMAIL_ADMIN2ALL = 2;
    const EMAIL_MESSAGE_NOTIFICATION = 3;
    const EMAIL_PROPOSAL_NOTIFICATION = 4;
    const EMAIL_DISCUSSION_NOTIFICATION = 5;

    const EMAIL_GRP_POSTING_NOTIFICATION = 6;
    const EMAIL_GRP_DISCUSSION_NOTIFICATION = 7;
    const EMAIL_LEAD_INDUSTRY_NOTIFICATION = 8;

    const EMAIL_PROPOSAL_REPLY_NOTIFICATION = 9;
    const EMAIL_DISCUSSION_REPLY_NOTIFICATION = 10;

    const EMAIL_GRP_POSTING_REPLY_NOTIFICATION = 11;
    const EMAIL_GRP_DISCUSSION_REPLY_NOTIFICATION = 12;

    const EMAIL_LEAD_REPLY_NOTIFICATION = 13;
    const EMAIL_BLOG_REPLY_NOTIFICATION = 14;
    const EMAIL_LOOP_INVITE_NOTIFICATION = 15;

    const EMAIL_PRODUCT_CONNECT_NOTIFICATION = 16;
    const EMAIL_COMPANY_CONNECT_NOTIFICATION = 17;

    const EMAIL_GRP_INVITE_NOTIFICATION = 18;
    const EMAIL_NETWORK_INVITE_NOTIFICATION = 19;

    const EMAIL_GRP_JOIN_REQUEST = 20;
    const EMAIL_LOOP_INVITE_ACCEPTED_NOTIFICATION = 21;

    const EMAIL_PRODUCT_CONNECT_ACCEPTED_NOTIFICATION = 22;
    const EMAIL_COMPANY_CONNECT_ACCEPTED_NOTIFICATION = 23;

    const EMAIL_GRP_INVITE_ACCEPTED_NOTIFICATION = 24;
    const EMAIL_NETWORK_INVITE_ACCEPTED_NOTIFICATION = 25;
    const EMAIL_REGISTER_NOTIFICATION = 26;
    const EMAIL_ACTIVE_NOTIFICATION = 27;
    const EMAIL_REJECT_NOTIFICATION = 28;

    const EMAIL_LOOP_INVITE_OUTSIDE_NOTIFICATION = 29;
    const EMAIL_NETWORK_INVITE_OUTSIDE_NOTIFICATION = 30;

    const EMAIL_LOOP_INVITE_INACTIVE_NOTIFICATION = 31;

    const EMAIL_PRODUCT_CONNECT_INACTIVE_NOTIFICATION = 32;
    const EMAIL_COMPANY_CONNECT_INACTIVE_NOTIFICATION = 33;
    const EMAIL_GRP_INVITE_INACTIVE_NOTIFICATION = 34;
    const EMAIL_NETWORK_INVITE_INACTIVE_NOTIFICATION = 35;

    const EMAIL_CHANGE_EMAIL_NOTIFICATION = 36;
    const EMAIL_FORGET_PASSWORD_NOTIFICATION = 37;

    const EMAIL_REGISTER_ACTIVE_SMALLBIZ_NOTIFICATION = 38;

    const EMAIL_EVERYDAY_MESSAGE_NOTIFICATION = 39;

    // Send the email when one modify the email and approved by our administrator
    const EMAIL_CHANGE_APPROVED_NOTIFICATION = 40;
    // Send the email when one modify the email, but rejected by our administrator
    const EMAIL_CHANGE_REJECT_NOTIFICATION = 41;
    // payment e billing
    const EMAIL_PAYMENT_E_BILLING = 42;
    // payment auto billing failed
    const EMAIL_AUTO_BILLING_FAILED = 43;


    public static function getTemplateNameById($emailTempId) {
        return PREFIX_OF_EMAIL_TEMPLATE_NAME . Config::get("email_templates")[$emailTempId];
    }


    public static function getEmailSubjectById($emailTempId, $parameters=array()) {

        $key = 'email.subject_' . $emailTempId;

        if(empty($parameters) || count($parameters) == 0) {
            return Lang::get($key);
        } else {
            return Lang::get($key, $parameters);
        }

    }

}