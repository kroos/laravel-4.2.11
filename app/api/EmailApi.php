<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tian Wei
 * Date: 13-09-23
 * Time: 下午4:30
 * To change this template use File | Settings | File Templates.
 */

class EmailApi{

    // public $isSendMail;

    public function __construct(){
        // $this->isSendMail = Config::get("database.isSendMail");
    }

    public function sendMailOfTemplate($from, $to, $template_name, $params) {

        $template = EmailTemplate::byName($template_name);

        if(empty($template)) {
            return false;
        }

        $body = self::constructBody($template->content, $params);

        return self::sendMail($from, $to, $template->subject, $body, $template->content_type);
    }

    public function constructBody($content, $params) {

        foreach($params as $key => $value) {
            $find = '{{$' . $key . '}}';
            $content = str_replace($find, $value, $content);
        }

        return $content;
    }

    public function sendMail($from, $to, $subject, $body, $content_type='text/html') {

        if(!SEND_MAIL_FLAG){
            // Log::info("sendMail return");
            return ;
        }

        // Log::info("sendMail continue");
        // $to  = 'aidan@example.com,wez@example.com'// note the comma

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: ' . $content_type . '; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'From: ' . $from . "\r\n";
        $parameters = '-fcustomer.service@tradove.com';
        // $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
        // $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

        // Mail it
        // Log::info("sendMail to: ". $to);
        return mail($to, $subject, $body, $headers, $parameters);

    }

    public function addEmailTemplate($template_name, $subject, $content, $content_type='html') {

        $template = new EmailTemplate();

        $template->name = $template_name;
        $template->subject = $subject;
        $template->content = $content;
        $template->content_type = $content_type;

        return $template->save();

    }

    public function deleteEmailTemplate($tempate_name) {
        $template = EmailTemplate::byName($tempate_name);

        if(!empty($template)) {
            return $template->delete();
        }

        return 0;
    }

    public function updateEmailTemplate($old_name, $new_name, $subject, $content, $content_type='html') {
        $template = EmailTemplate::byName($old_name);

        $template->name = $new_name;
        $template->subject = $subject;
        $template->content = $content;
        $template->content_type = $content_type;

        return $template->save();
    }

    // Sending email whose body is rendered with the blade template
    public function sendMailByTemplate($from, $to, $subject, $tempName, $data) {
        Log::info("sendMailByTemplate start");
        $body = View::make($tempName)->with($data)->render();

        // Not pass the last parameter, the default content is html format
        return self::sendMail($from, $to, $subject, $body);

    }

    // Send email to users who are not active
    /**
    public function sendEmail2Inactive($senderId, $receiverId) {

    $nid = Session::get('network_id');

    $userApi = new UsersApi;

    $senderUser = $userApi->getUserById($senderId);
    $senderFullName = $senderUser->first_name . ' ' . $senderUser->last_name;
    $receiverUser = $userApi->getUserById($receiverId);
    $receiverName = $receiverUser->first_name;

    $templateName = 'emails.email2inactive';
    $subject = Lang::get('email.email2inactive.subject', array('name'=> $senderFullName));
    $content = Lang::get('email.email2inactive.content');

    $senderInfo = $senderFullName . ' - ' . getUserInfoById($senderId);

    $data = array(
    'receiverName' => $receiverName,
    'sender' => $senderFullName,
    'senderInfo' => $senderInfo,
    'imageUrl'  =>  BASE_URL . "imgs/email/btn_click_go_to_tradove.gif",
    'updates'   =>  $content,
    'visitUrl' => BASE_URL . 'inactive?aid=' . $receiverUser->activeid . '&uid=' . $receiverId
    );

    $this->sendMailByTemplate(MANAGER_EMAIL_ADDRESS_FROM, $receiverUser->email, $subject, $templateName, $data);

    } */

    // Send email to users who are not active
    public function sendEmail2Inactive($senderId, $receiverId, $subject, $content, $fromAdmin = false) {
        // $nid = Session::get('network_id');
        $userApi = new UsersApi;

        $receiverUser = $userApi->getUserById($receiverId);
        $receiverName = $receiverUser->first_name;

        $templateName = 'emails.email2inactive';
        // $subject = Lang::get('email.email2inactive.subject', array('name'=> $senderFullName));
        // $content = Lang::get('email.email2inactive.content');

        if($fromAdmin){
            $senderFullName = "TraDove member";
            $senderInfo = "";
        }else{
            $senderUser = $userApi->getUserById($senderId);
            $senderFullName = $senderUser->first_name . ' ' . $senderUser->last_name;
            $senderInfo = $senderFullName . ' - ' . getUserInfoById($senderId);
        }

        /**
         * 根据是否导入来确定
         * 是则到需要选择类型流程
         * 否则到只需要输入密码
         */
        if($receiverUser->is_import == 0){
            $visitUrl = API_URL ."redirect/urls?url=".urlencode("active?aid={$receiverUser->activeid}&uid=$receiverId");
//            if(!NetworkUserApi::isInGeneral($receiverId))
//                $visitUrl .= "&private=true";
        }else{
            $visitUrl = BASE_URL . "inactive?aid={$receiverUser->activeid}&uid=$receiverId";
        }

        $data = array(
            'receiverName' => $receiverName,
            'sender' => $senderFullName,
            'senderInfo' => $senderInfo,
            'imageUrl'  =>  BASE_URL . "imgs/email/btn_click_to_join_tradove.gif",
            'updates'   =>  $content,
            'visitUrl' => $visitUrl
        );

        if($receiverUser->state == USER_STATE_OF_SENT_ACTIVE) {
            $data['imageUrl'] = BASE_URL . "imgs/email/btn_click_go_to_tradove.gif";
            $data['visitUrl'] = BASE_URL;
        }

        $this->sendMailByTemplate(MANAGER_EMAIL_ADDRESS_FROM, $receiverUser->email, $subject, $templateName, $data);

    }

    public function recordUnsubscribe($email) {

        $unsubscribeFile = Config::get("uploadfile.unsubscribe_emails");
        $fh = fopen($unsubscribeFile, "a");
        fwrite($fh, $email . "\n");
        fclose($fh);

        return true;

    }

    // queue
    public function sendWeeklyEmail($job, $data){
        $this->sendMail($data['from'], $data['to'], $data['subject'], $data['body']);

        $job->delete();
    }


    // All the emails send out from here
    public function sendMailByTemplateId( $emailTemplateId, $to, $subjectData, $bodyData, $from = MANAGER_EMAIL_ADDRESS_FROM ) {

        $templateName = EmailTemplateManager::getTemplateNameById($emailTemplateId);
        $subject = EmailTemplateManager::getEmailSubjectById($emailTemplateId, $subjectData);

        self::sendMailByTemplate($from, $to, $subject, $templateName, $bodyData);

//        if(!LoginLogApi::loginedIn60Days($to)) {
//            $secondEmail = UsersApi::getSecondEmailByFirstEmail($to);
//
//            if(!empty($secondEmail)) {
//                self::sendMailByTemplate($from, $secondEmail, $subject, $templateName, $bodyData);
//            }
//        }

    }

}
