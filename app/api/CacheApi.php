<?php

class CacheApi {

    /**
     * key:
     *
     * 用户: USER_用户ID
     * loop: loop:networkID:用户ID
     * loop pending : loop:pending:networkID:用户ID
     *
     *
     * 公司: COMPANY_公司ID
     * connet的公司 : companies:conn:networkID:用户ID
     * connet pending的公司：companies:pending:networkID:用户ID
     *
     *
     * connet的产品： products:conn:$networkID:用户ID
     * connet pending的产品：products:pending:$networkID:用户ID
     *
     *
     * network
     * nid:$networkID:name
     * nid:$networkId:number
     * vid:$vipId:name
     *
     *
     * Group  g:$networkID:$groupId
     * group加入： g:pending:$networkID:$groupId
     * group discussion统计： discussion:hot:gid:$groupId
     *
     *
     */

    public static function getWriteConnection(){
        return Redis::connection('write');
    }

    /**
     * 设置key的过期时间
     * @param $keyreturn Redis::connection('write');
    }

     * @param $ttl 单位：秒
     * @return bool
     */
    public static function setKeyExpire($key, $ttl){
        //return Redis::expire($key, $ttl);
        return CacheApi::getWriteConnection()->expire($key, $ttl);
    }

    public static function setUser($user) {

        $key = "USER_" . $user->id;
        $user = $user->toArray();

        if(!empty($user) && count($user) > 0) {
            //Redis::hmset($key, $user);
            CacheApi::getWriteConnection()->hmset($key, $user);
        }
    }

    /**
     * @param $uid 用户ID
     * @param $property string USER对象的属性，如first_name
     * @return string USER对象属性的值
     */
    public static function getUserProperty($uid, $property) {
        $key = "USER_" . $uid;
        return Redis::hget($key, $property);
    }


    public static function setUserProperty($uid, $property, $value){
        $key = "USER_" . $uid;
        //return Redis::hset($key, $property, $value);
        return CacheApi::getWriteConnection()->hset($key, $property, $value);
    }

    /**
     * 读取缓存的USER对象，转换成框架USER对象
     *
     * @param   $uid userid 用户ID
     * @return Users|null
     */
    public static function getUser($uid) {
        $key = "USER_" . $uid;
        $userArray = Redis::hgetall($key);

        if (count($userArray) > 0) {
            $user = new Users();
            $user->exists = true;
            foreach ($userArray as $key => $value) {
                $user->$key = $value;
            }
            return $user;
        }

        return null;
    }

    /**
     * 返回phpRedis的USER对象，一个简单数组
     *
     * @param $uid userid
     * @return An array that contains user property
     */
    public static function getSimpleUser($uid) {
        $key = "USER_" . $uid;
        return Redis::hgetall($key);
    }

    public static function updateUser($uid, array $fields) {
        $key = "USER_" . $uid;
        /*foreach ($fields as $field => $value) {
            Redis::hset($key, $field, $value);
        }*/
        //Redis::hmset($key, $fields);
        CacheApi::getWriteConnection()->hmset($key, $fields);
    }


    public static function removeUser($uid) {
        $key = "USER_" . $uid;
        //return Redis::del($key);
        return CacheApi::getWriteConnection()->del($key);
    }

    /**
     *
     * 删除缓存中USER对象的某一个属性，比如USER对象的first_name属性，删除后该对象不存在first_name属性
     * 但是，有可能其它方法会重新创建该属性
     *
     *
     * @param $uid
     * @param array $fields 要删除的对象字段
     */
    public static function removeUserField($uid, array $fields) {
        $key = "USER_" . $uid;
        foreach ($fields as $field) {
            //Redis::hdel($key, $field);
            CacheApi::getWriteConnection()->hdel($key, $field);
        }
    }

    /**
     * 用于Redis USER对象排序，该方法配合下面的sortUser方法和destroySortUserList方法使用
     * 1.首先创建一个只包含ID的用户列表
     * 2.根据用户排序规则排序， sortUser方法是按照last_name进行排序的。
     * 3.从内存中依次指定key的排序对象，避免占用内存。如果该排序是频繁进行的，可以忽略该步
     *
     * @param $key 用户设定的key
     * @param array $sortList 待排序的用户ID列表
     * @deprecated 方法已不再使用
     */
    public static function setSortUserList($key, array $sortList){
        foreach($sortList as $id){
            Redis::lPush($key, $id);
        }
    }

    /**
     * @param $key
     * @param string $storeKey
     * @return array
     *
     * @deprecated 方法已不再使用
     */
    public static function sortUser($key, $storeKey = ''){

        if(empty($storeKey)){
            return Redis::sort($key, array('by'=>'USER_*->last_name', 'alpha'=>true, 'sort'=>'asc'));
        }else{
            return Redis::sort($key, array('by'=>'USER_*->last_name', 'alpha'=>true, 'sort'=>'asc', 'store'=>$storeKey));
        }

    }

    /**
     * @param $key
     * @return int
     * @deprecated 方法已不在使用
     */
    public static function destroySortUserList($key){
        return Redis::del($key);
    }


    /**
     * 向一个无序集合中添加用户ID，保存用户的好友关系
     *
     * @param $nid networkID
     * @param $uid 当前用户的ID
     * @param $loopList array|string|int 用户的好友ID
     */
    public static function setLoopSet($nid, $uid, $loopList) {

        $key = 'loop:' . $nid . ':' . $uid;

        if (is_array($loopList)) {
            foreach ($loopList as $val) {
                //Redis::sadd($key, $val);
                CacheApi::getWriteConnection()->sadd($key, $val);
            }
        } else {
            //Redis::sadd($key, $loopList);
            CacheApi::getWriteConnection()->sadd($key, $loopList);
        }

    }

    public static function getLoopSet($nid, $uid) {
        $key = 'loop:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    /**
     * 分别删除双方的loop好友
     * 为了代码更加清晰，分成了两个循环
     * @param $nid
     * @param $uid
     * @param $loopIds
     */
    public static function removeLoop($nid, $uid, $loopIds) {
        $key = 'loop:' . $nid . ':' . $uid;

        //将loopIds从uid的loop中删除，如下
        //nid=0 loopIds=[1 3 11 22] uid=19
        //删除loop:0:1 loop:0:3 loop:0:11 loop:0:22
        if(is_array($loopIds)){
            foreach($loopIds as $loopId){
                //Redis::sRem($key, $loopId);
                CacheApi::getWriteConnection()->sRem($key, $loopId);
            }
        }else{
            //Redis::sRem($key, $loopIds);
            CacheApi::getWriteConnection()->sRem($key, $loopIds);
        }

        //将uid从loopIds中删除,如下
        //nid=0 loopIds=[1 3 11 22] uid=19
        //删除loop:0:1中删除uid为19的好友， loop:0:3删除uid为19的好友 loop:0:11中删除uid为19的loop loop:0:22中删除uid为19的loop
        if(is_array($loopIds)){
            foreach($loopIds as $loopId){
                $key2 = 'loop:'.$nid.':'.$loopId;
                //Redis::sRem($key2, $uid);
                CacheApi::getWriteConnection()->sRem($key2, $uid);
            }
        }else{
            $key2 = 'loop:'.$nid.':'.$loopIds;
            //Redis::sRem($loopIds, $uid);
            CacheApi::getWriteConnection()->sRem($key2, $uid);
        }

    }

    /**
     * 判断用户是否是当前用户的好友
     *
     * @param $nid networkId
     * @param $uid 当前用户ID
     * @param $anotherId 用户ID
     * @return  bool  TRUE if value is a member of the set at key key, FALSE otherwise.
     * @link    http://redis.io/commands/sismember
     *
     */
    public static function isInLoop($nid, $uid, $anotherId) {
        $key = 'loop:' . $nid . ':' . $uid;
        return Redis::sismember($key, $anotherId);
    }


    public static function setLoopPendingSet($nid, $uid, $inviteId) {
        $key = 'loop:pending:' . $nid . ':' . $uid;
        //return Redis::sadd($key, $inviteId);
        return CacheApi::getWriteConnection()->sadd($key, $inviteId);
    }

    /**
     * 返回当前用户所有的pending请求的用户ID
     *
     * @param $nid networkID
     * @param $uid 当前用户ID
     * @return  array   An array of elements, the contents of the set.
     * @link    http://redis.io/commands/smembers
     *
     */
    public static function getLoopPendingSet($nid, $uid) {
        $key = 'loop:pending:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    /**
     * 返回多个集合的并集
     * @param string $key1 集合1的key
     * @param string $key2 集合2的key
     * @return array loop & loop pending
     */
    public static function getUnionSet($key1, $key2){
        return Redis::sUnion($key1, $key2);
    }

    /**
     * 返回多个集合的交集
     * @param $key1
     * @param $key2
     * @return array
     */
    public static function getInterSet($key1, $key2){
        return Redis::sInter($key1, $key2);
    }

    /**
     * 返回多个集合的交集并存储该结果
     * @param $storeKey
     * @param $key1
     * @param $key2
     * @return int
     */
    public static function getInterSetStore($storeKey, $key1, $key2){
        return Redis::sInterStore($storeKey, $key1, $key2);
    }
    /**
     * 返回第一个集合和其它集合比较后的差集,并将结果存储在$storeKey上
     * @param $storeKey
     * @param $key1
     * @param $key2
     * @param null $key3
     * @return array
     */
    public static function getDiffSetStore($storeKey, $key1, $key2, $key3 = null){
        return Redis::sDiffStore($storeKey, $key1, $key2, $key3);
    }

    /**
     * 返回第一个集合和其它集合比较后的差集,但是不存在结果到指定的key上
     * @param $key1
     * @param $key2
     * @param null $key3
     * @return array
     */
    public static function getDiffSet($key1, $key2, $key3 = null){
        return Redis::sDiff($key1, $key2, $key3);
    }

    /**
     * 从一个集合中随机返回count个数的元素
     * @param $key
     * @param int $count 默认为1
     * @return array 包含0个或多个的元素数组
     */
    public static function getRandomElementsFromSet($key, $count = 1){
        return Redis::sRandMember($key, $count);
    }

    /**
     * 获取指定key集合的总数量
     * @param $key
     * @return int
     */
    public static function getSetNumber($key){
        return Redis::sCard($key);
    }

    public static function isInPendingLoop($nid, $uid, $anotherId) {
        $key = 'loop:pending:' . $nid . ':' . $uid;
        return Redis::sIsMember($key, $anotherId);
    }

    public static function removeLoopPending($nid, $uid, array $inviteIds) {
        $key = 'loop:pending:' . $nid . ':' . $uid;

        foreach($inviteIds as $loopId){
            //Redis::sRem($key, $loopId);
            CacheApi::getWriteConnection()->sRem($key, $loopId);
        }

    }

    public static function setCompany($company) {
        $key = "COMPANY_" . $company->id;
        $company = $company->toArray();
        //Redis::hmset($key, $company);
        CacheApi::getWriteConnection()->hmset($key, $company);
    }

    public static function getCompany($cid) {
        $key = "COMPANY_" . $cid;
        $companyArray = Redis::hgetall($key);
        if (count($companyArray) > 0) {
            $user = new Company();
            $user->exists = true;
            foreach ($companyArray as $key => $value) {
                $user->$key = $value;
            }
            return $user;
        }
        return null;
    }

    public static function showPending($type, $anotherId) {

        $nid = Session::get('network_id');
        $uid = Session::get('uid');

        if ($type == INVITE_TYPE_LOOP) {

            $isLoopPending = CacheApi::isInPendingLoop($nid, $uid, $anotherId);

            if ($isLoopPending) {
                echo HTML::image(asset('imgs/icon/icon_relation_pending.png'), null, array('align' => 'absmiddle'));
                //return true;
            }

        } //group
        else if ($type == INVITE_TYPE_GROUP) {
        } //product
        else if ($type == INVITE_TYPE_PRODUCT) {
            $isLoopPending = CacheApi::isPendingProduct($nid, $uid, $anotherId);

            if ($isLoopPending) {
                echo HTML::image(asset('imgs/icon/icon_relation_pending.png'), null, array('align' => 'absmiddle'));
                return true;
            }
        } //company
        else if ($type == INVITE_TYPE_COMPANY) {
            $isLoopPending = CacheApi::isPendingCompany($nid, $uid, $anotherId);

            if ($isLoopPending) {
                echo HTML::image(asset('imgs/icon/icon_relation_pending.png'), null, array('align' => 'absmiddle'));
                return true;
            }
        }

        return false;
    }

    /**
     *
     * show pending relationship
     *
     * type = 1 INVITE_TYPE_LOOP loop
     * type = 2 INVITE_TYPE_GROUP group
     * type = 3 INVITE_TYPE_PRODUCT
     * type = 4 INVITE_TYPE_COMPANY
     *
     */
    public static function showRelationship($type, $anotherId) {

        $nid = Session::get('network_id');
        $uid = Session::get('uid');

        //loop/loop pending info
        if ($type == INVITE_TYPE_LOOP) {

            $isLoop = CacheApi::isInLoop($nid, $uid, $anotherId);
            if ($isLoop) {
                echo HTML::image(asset('imgs/icon/icon_relation_loop.png'), null, array('align' => 'absmiddle'));
            } else {
                CacheApi::showPending($type, $anotherId);
            }
            return true;
        } //group
        else if ($type == INVITE_TYPE_GROUP) {
            CacheApi::showMyGroupInfo($anotherId);
        } //product
        else if ($type == INVITE_TYPE_PRODUCT) {
        } //company
        else if ($type == INVITE_TYPE_COMPANY) {
        }

    }

    public static function showRelationshipOfMemberType($uid) {

        $memberType = CacheApi::getUserProperty($uid, 'member_type');
        switch ($memberType) {
            case MEMBER_TYPE_BUYER_ID:
                echo HTML::image(asset('imgs/icon/icon_relation_buyer.png'), null, array('align' => 'absmiddle'));
                break;

            case MEMBER_TYPE_SELLER_ID:
                echo HTML::image(asset('imgs/icon/icon_relation_seller.png'), null, array('align' => 'absmiddle'));
                break;

            case MEMBER_TYPE_OTHER_ID:
                echo HTML::image(asset('imgs/icon/icon_relation_other.png'), null, array('align' => 'absmiddle'));
                break;

            default:
                break;
        }
    }



    public static function showRelationshipOfActivation($uid) {

        $memberStatus = CacheApi::getUserProperty($uid, 'state');
        if ($memberStatus == USER_STATE_OF_SENT_ACTIVE ) {
                echo HTML::image(asset('imgs/icon/icon_relation_active.png'), null, array('align' => 'absmiddle'));
        }

    }


    /**
     *
     * show pending/connect/membertype relationship
     *
     * type = 1 INVITE_TYPE_LOOP loop
     * type = 2 INVITE_TYPE_GROUP group
     * type = 3 INVITE_TYPE_PRODUCT
     * type = 4 INVITE_TYPE_COMPANY
     *
     */
    public static function showMyRelationInfo($type, $anotherId) {
        $uid = Session::get('uid');
        if ($uid != $anotherId) {
            CacheApi::showRelationshipOfMemberType($anotherId);
            CacheApi::showRelationship($type, $anotherId);
            CacheApi::showMyGroupInfo($anotherId);
            CacheApi::showRelationshipOfActivation($anotherId);
        }
    }

    public static function showRelationInfoOfOneWithAnother($oneId, $anotherId) {
        if ($oneId != $anotherId) {
            CacheApi::showRelationshipOfMemberType($oneId);

            $isLoop = CacheApi::isInLoop(0, $oneId, $anotherId);
            if ($isLoop) {
                echo HTML::image(asset('imgs/icon/icon_relation_loop.png'), null, array('align' => 'absmiddle'));
            }

            CacheApi::showRelationshipOfActivation($oneId);
        }
    }

    public static function showMyGroupInfo($userId){

        $key = 'g:'.Session::get('network_id').':*';

        $allGroupKeys = Redis::keys($key);

        foreach($allGroupKeys as $key){

            if(strcmp(Redis::type($key), 'set') == 0){
                $otherInGroup = Redis::sIsMember($key, $userId);
                $myInGroup = Redis::sIsMember($key, Session::get('uid'));
                if($otherInGroup && $myInGroup){
                    echo HTML::image(asset('imgs/icon/icon_relation_group.png'), null, array('align' => 'absmiddle'));
                    break;
                }
            }
        }
    }

    public static function handleInviteIcon($type, $anotherId, $gid = null) {

        $nid = Session::get('network_id');
        $uid = Session::get('uid');

        if ($type == INVITE_TYPE_LOOP) {
            $isLoop = CacheApi::isInLoop($nid, $uid, $anotherId);
            $isPending = CacheApi::isInPendingLoop($nid, $uid, $anotherId);
            if(!$isLoop && !$isPending && ($uid != $anotherId)){
                if((FiltersApi::isNetworkMember($nid,$anotherId))){
                    //show invite
                    echo '<div class="btn">';
                    echo HTML::image(asset('imgs/icon/icon_add.png'), null, array('align' => 'absmiddle'));
                    $userName = CacheApi::getUserProperty($anotherId, "first_name") . " " . CacheApi::getUserProperty($anotherId, "last_name");
                    echo '&nbsp<a href="javascript:;" onclick="tradove.invitation(' . INVITE_TYPE_LOOP . ',' . $anotherId . ',this)">';
                    echo Lang::get("tip.invite");
                    echo '</a></div>';
                }
            }else if(!$isLoop && $isPending && ($uid != $anotherId)){
                //show pending
                //echo HTML::image(asset('imgs/icon/icon_relation_pending.png'), null, array('align' => 'absmiddle'));
            }
        }
        elseif($type == INVITE_TYPE_GROUP){
            $isPending = CacheApi::isInJoinPendingGroup($nid, $gid, $anotherId);
            if($isPending){
                echo HTML::image(asset('imgs/icon/icon_relation_pending.png'), null, array('align' => 'absmiddle'));
            }else{
                if((FiltersApi::isNetworkMember($nid,$anotherId))){
                    echo HTML::image("imgs/icon/icon_add.png",null,array("align"=>"absmiddle"));
                    echo '<a href="javascript:void(0)" onclick="joinGroup('.$gid.');">';
                    echo Lang::get("industry.lnk.group_list_join");
                    echo '</a>';
                }
            }
        }

    }

    //calc buyer/seller/other number in loop
    public static function calcLoopNumByMemberType($nid, $uid, $memberType) {

        $loopList = CacheApi::getLoopSet($nid, $uid);

        if (empty($loopList) || count($loopList) == 0) {
            return 0;
        }

        $count = 0;
        foreach ($loopList as $loopId) {

            $mbType = CacheApi::getUserProperty($loopId, 'member_type');

            if ($mbType == $memberType) {
                $count++;
            }
        }

        return $count;

    }

    //get some loop id, then calc buyer/seller/other number in loop 
    //note that : we do not check the list id whether is a loop
    //you must make sure , all the id in list must loop
    public static function calcSpecifyLoopNumByMemberType($specifyLoopList, $memberType) {

        if (empty($specifyLoopList) || count($specifyLoopList) == 0) {
            return 0;
        }

        $count = 0;
        foreach ($specifyLoopList as $loopId) {
            $mbType = CacheApi::getUserProperty($loopId, 'member_type');
            if ($mbType == $memberType) {
                $count++;
            }
        }
        return $count;

    }


    /**
     *
     * 保存已经关联的公司到缓存
     * 如：
     * companies:conn:0:43333
     * companies:conn:3:1456
     *
     * @param $nid network ID
     * @param $uid 用户ID
     * @param $companyList company的ID|包含多个company ID的数组
     *
     */
    public static function setConnectCompanySet($nid, $uid, $companyList) {
        $key = 'companies:conn:' . $nid . ':' . $uid;

        if (is_array($companyList)) {
            foreach ($companyList as $val) {
                //Redis::sadd($key, $val);
                CacheApi::getWriteConnection()->sadd($key, $val);
            }
        } else {
            //Redis::sadd($key, $companyList);
            CacheApi::getWriteConnection()->sadd($key, $companyList);
        }

    }

    // get connecting companies
    public static function getConnectCompanySet($nid, $uid) {
        $key = 'companies:conn:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    // remove connected companies
    public static function removeConnectCompanySet($nid, $uid, array $companyList) {
        $key = 'companies:conn:' . $nid . ':' . $uid;
        foreach($companyList as $company){
            //Redis::srem($key, $company);
            CacheApi::getWriteConnection()->srem($key, $company);
        }
    }

    // judge whether a company is connected
    public static function isConnectedCompany($nid, $uid, $companyId) {
        $key = 'companies:conn:' . $nid . ':' . $uid;
        return Redis::sismember($key, $companyId);
    }

    // add a pending company to set
    public static function setPendingCompanySet($nid, $uid, $companyId) {
        $key = 'companies:pending:' . $nid . ':' . $uid;
        //return Redis::sadd($key, $companyId);
        return CacheApi::getWriteConnection()->sadd($key, $companyId);
    }

    // get set of pending companies
    public static function getPendingCompanySet($nid, $uid) {
        $key = 'companies:pending:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    // remove pending company from set
    public static function removePendingCompanySet($nid, $uid, $companyIds) {
        $key = 'companies:pending:' . $nid . ':' . $uid;
        if (is_array($companyIds)) {
            foreach ($companyIds as $val) {
                //Redis::sRem($key, $val);
                CacheApi::getWriteConnection()->sRem($key, $val);
            }
        } else {
            //Redis::sRem($key, $companyIds);
            CacheApi::getWriteConnection()->sRem($key, $companyIds);
        }
    }

    // judge whether a company is pending
    public static function isPendingCompany($nid, $uid, $companyId) {
        $key = 'companies:pending:' . $nid . ':' . $uid;
        return Redis::sismember($key, $companyId);
    }


    /**
     *
     * 保存已经关联的产品到缓存
     * 如：
     * products:conn:0:111
     * products:conn:3:22
     *
     * @param $nid network ID
     * @param $uid 用户ID
     * @param $productList 产品的ID|包含多个产品 ID的数组
     *
     */
    public static function setConnectProductSet($nid, $uid, $productList) {
        $key = 'products:conn:' . $nid . ':' . $uid;
        if (is_array($productList)) {
            foreach ($productList as $val) {
                //Redis::sadd($key, $val);
                CacheApi::getWriteConnection()->sadd($key, $val);
            }
        } else {
            //Redis::sadd($key, $productList);
            CacheApi::getWriteConnection()->sadd($key, $productList);
        }
    }

    // get connecting products
    public static function getConnectProductSet($nid, $uid) {
        $key = 'products:conn:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    // remove connected products
    public static function removeConnectProductSet($nid, $uid, array $productList) {
        $key = 'products:conn:' . $nid . ':' . $uid;
        foreach($productList as $product){
            //Redis::srem($key, $product);
            CacheApi::getWriteConnection()->srem($key, $product);
        }
    }

    // judge whether a product is connected
    public static function isConnectedProduct($nid, $uid, $productId) {
        $key = 'products:conn:' . $nid . ':' . $uid;
        return Redis::sismember($key, $productId);
    }

    // add a pending product to set
    public static function setPendingProductSet($nid, $uid, $productId) {
        $key = 'products:pending:' . $nid . ':' . $uid;
        //return Redis::sadd($key, $productId);
        return CacheApi::getWriteConnection()->sadd($key, $productId);
    }

    // get set of pending products
    public static function getPendingProductSet($nid, $uid) {
        $key = 'products:pending:' . $nid . ':' . $uid;
        return Redis::smembers($key);
    }

    // remove pending product from set
    public static function removePendingProductSet($nid, $uid, array $productIds) {
        $key = 'products:pending:' . $nid . ':' . $uid;
        if (is_array($productIds)) {
            foreach ($productIds as $val) {
                //Redis::sRem($key, $val);
                CacheApi::getWriteConnection()->sRem($key, $val);
            }
        } else {
            //Redis::sRem($key, $productIds);
            CacheApi::getWriteConnection()->sRem($key, $productIds);
        }
    }

    // judge whether a product is pending
    public static function isPendingProduct($nid, $uid, $productId) {
        $key = 'products:pending:' . $nid . ':' . $uid;
        return Redis::sismember($key, $productId);
    }


    // nid:network id value:name => Network名称
    public static function setNetworkName($nid, $name) {
        $key = 'nid:' . $nid . ':name';

        //return Redis::set($key, $name);
        return CacheApi::getWriteConnection()->set($key, $name);
    }

    public static function getNetworkName($nid) {
        $key = 'nid:' . $nid . ':name';

        return Redis::get($key);
    }

    // nid:network id value:number => 加入　Network 的人数量
    public static function setNetworkNum($nid, $number) {
        $key = 'nid:' . $nid . ':number';

        //return Redis::set($key, $number);
        return CacheApi::getWriteConnection()->set($key, $number);
    }

    public static function getNetworkNum($nid) {
        $key = 'nid:' . $nid . ':number';

        return Redis::get($key);
    }


    // vid:vip level id:name => VIP LEVEL 名称
    // VIP LEVEL ID => VIP LEVEL 名称
    public static function setVipName($vipId, $number) {
        $key = 'vid:' . $vipId . ':name';

        //return Redis::set($key, $number);
        return CacheApi::getWriteConnection()->set($key, $number);
    }

    public static function getVipName($vipId) {
        $key = 'vid:' . $vipId . ':name';

        return Redis::get($key);
    }

    //the industry id of current login user
    public static function setIndustryId($industryId) {
        $userId = Session::get("uid");
        $key = 'uid:' . $userId . ":industryId";
        //return Redis::set($key, $industryId);
        return CacheApi::getWriteConnection()->set($key, $industryId);
    }

    public static function getIndustryId() {
        $userId = Session::get("uid");
        $key = 'uid:' . $userId . ":industryId";
        return Redis::get($key);
    }

    /**
     * hot discussion的key如下
     * @param $groupId
     * @param $discussionId
     */
    public static function removeGroupHot($groupId, $discussionId){
        $key = "discussion:hot:gid:".$groupId;
        //Redis::hDel($key, $discussionId);
        CacheApi::getWriteConnection()->hDel($key, $discussionId);
    }

    /**
     * increase discussion hot by one
     * @param int $groupId
     * @param int $discussionId
     * @param int $increaseBy
     * @return bool
     */
    public static function incrDiscussionHot($groupId, $discussionId, $increaseBy = 1){
        $key = "discussion:hot:gid:".$groupId;
        //return Redis::hincrby($key, $discussionId, $increaseBy);
        return CacheApi::getWriteConnection()->hincrby($key, $discussionId, $increaseBy);
    }

    /**
     * get discussion host
     *
     * @param int $groupId
     * @param int $discussionId
     * @return int
     */
    public static function getDiscussionHot($groupId, $discussionId){
        $key = "discussion:hot:gid:".$groupId;
        $hot = Redis::hget($key, $discussionId);

        return $hot;
    }
    /**
     * get discussion hosts array in reverse order
     * @param int $groupId
     * @return array
     */
    public static function getDiscussionHots($groupId){
        $key = "discussion:hot:gid:".$groupId;
        $hots = Redis::hgetall($key);

        arsort($hots);

        return $hots;
    }

    /**
     *
     * in redis, key like this
     * g:0:26
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userId  user id
     * @return int The number of elements added to the set
     */
    public static function setGroupSet($nid, $gid, $userId){
        $key = "g:".$nid.":".$gid;
        if(is_array($userId)){
            foreach($userId as $uid){
                //Redis::sAdd($key, $uid);
                CacheApi::getWriteConnection()->sAdd($key, $uid);
            }
        }
        //return Redis::sAdd($key, $userId);
        return CacheApi::getWriteConnection()->sAdd($key, $userId);
    }

    /**
     *
     * whether a user in a group
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userId  user id
     * @return bool  bool   TRUE if value is a member of the set at key key, FALSE otherwise.
     */
    public static function isInGroup($nid, $gid, $userId){
        $key = "g:".$nid.":".$gid;
        return Redis::sIsMember($key, $userId);
    }

    /**
     *
     * get all the group members
     * note that:
     *      we do not check group member status, if one member's status is block
     *      we do not do any operation, you get all members list, and recheck it by yourself
     *
     * @param $nid     network id
     * @param $gid     group id
     * @return array   An array of elements, all the group members of the set.
     */
    public static function getGroupMembers($nid, $gid){
        $key = "g:".$nid.":".$gid;
        return Redis::sMembers($key);
    }

    /**
     *
     * remove group member from group
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userIds array|string user(s) id
     * @return int Removes the specified members from the set value stored at key.
     */
    public static function removeGroupMember($nid, $gid, $userIds) {
        $key = "g:".$nid.":".$gid;

        if(is_array($userIds)){

            foreach($userIds as $uid){
                Redis::srem($key, $uid);
            }
        }

        return Redis::sRem($key, $userIds);

    }

    /**
     *
     * in redis, key like this
     * g:pending:0:26
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userId  user id
     * @return int The number of elements added to the set
     */
    public static function setGroupJoinPendingSet($nid, $gid, $userId){
        $key = "g:pending:".$nid.":".$gid;
        //return Redis::sAdd($key, $userId);
        return CacheApi::getWriteConnection()->sAdd($key, $userId);
    }

    /**
     *
     * whether a user in a pending group
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userId  user id
     * @return bool  bool   TRUE if value is a member of the set at key key, FALSE otherwise.
     */
    public static function isInJoinPendingGroup($nid, $gid, $userId){
        $key = "g:pending:".$nid.":".$gid;
        return Redis::sIsMember($key, $userId);
    }

    /**
     *
     * remove pending request from group
     *
     * @param $nid     network id
     * @param $gid     group id
     * @param $userIds array|string user(s) id
     * @return int Removes the specified members from the set value stored at key.
     */
    public static function removeGroupJoinPending($nid, $gid, $userIds) {
        $key = "g:pending:".$nid.":".$gid;

        if(is_array($userIds)){

            foreach($userIds as $uid){
                //Redis::srem($key, $uid);
                CacheApi::getWriteConnection()->srem($key, $uid);
            }
        }else{
            //return Redis::sRem($key, $userIds);
            return CacheApi::getWriteConnection()->sRem($key, $userIds);
        }

    }

    public static function removeData($key){
        //return Redis::del($key);
        return CacheApi::getWriteConnection()->del($key);
    }

    //for Login page
    public static function getAllUserCount(){
        $key="user_member_count";
        return Redis::get($key);
    }

    public static function setAllUserCountInc(){
        $key="user_member_count";
        //return Redis::incr($key);
        return CacheApi::getWriteConnection()->incr($key);
    }

    public static function setAllUserCountDec(){
        $key="user_member_count";
        //return Redis::incrBy($key,-1);
        return CacheApi::getWriteConnection()->incrBy($key,-1);
    }

    public static function getAllCompanyCount(){
        $key="company_number_count";
        return Redis::get($key);
    }

    public static function setAllCompanyInc(){
        $key="company_number_count";
        //return Redis::incr($key);
        return CacheApi::getWriteConnection()->incr($key);
    }

    public static function setAllCompanyDec(){
        $key="company_number_count";
        //return Redis::incrBy($key,-1);
        return CacheApi::getWriteConnection()->incrBy($key,-1);
    }

    public static function getAllCountriesCount(){
        $key="all_countries_count";
        return Redis::get($key);
    }

    public static function getAllIndustriesCount(){
        $key="all_industries_count";
        return Redis::get($key);
    }

    //删除缓存用户信息
    /**
     * @param $networkId
     * @param $userId
     * @param $targetId
     * @deprecated 方法不在被使用
     */
    public static function removeUserInfo($networkId, $userId, $targetId){
        //1.delete loop
        CacheApi::removeLoop($networkId, $userId, $targetId);
        //2.delete loop pending
        CacheApi::removeLoopPending($networkId, $userId, $targetId);
        //3.delete network
        CacheApi::removeUserFromNetworkUserSet($networkId, $targetId);
        //4.delete member type
        if(is_array($targetId)){
            foreach($targetId as $uid){
                CacheApi::removeMemberTypeFromSet($networkId, CacheApi::getUserProperty($uid, 'member_type'), $uid);
            }
        }else{
            CacheApi::removeMemberTypeFromSet($networkId, CacheApi::getUserProperty($targetId, 'member_type'), $targetId);
        }
        //5.delete user
        if(is_array($targetId)){
            foreach($targetId as $uid){
                CacheApi::removeUser($uid);
            }
        }
    }

    /**
     * 保存network user id到缓存中, key为 n:networkId
     * @param $networkId
     * @param $userId
     */
    public static function setNetworkUserSet($networkId, $userId){
        $key = "n:".$networkId;

        if(is_array($userId)){
            foreach($userId as $uid){
                //Redis::sAdd($key, $uid);
                CacheApi::getWriteConnection()->sAdd($key, $uid);
            }
        }

        //Redis::sAdd($key, $userId);
        CacheApi::getWriteConnection()->sAdd($key, $userId);
    }

    /**
     * 根据networkId获取该network下的用户id
     * @param $networkId
     * @return array
     */
    public static function getNetworkUserSet($networkId){
        $key = "n:".$networkId;
        return Redis::sMembers($key);
    }

    /**
     * 判断一个用户是否在network中
     * @param $networkId
     * @param $userId
     * @return bool
     */
    public static function isNetworkUser($networkId, $userId){
        $key = "n:".$networkId;
        return Redis::sIsMember($key, $userId);
    }

    /**
     * 删除network下的用户id
     * @param $networkId
     * @param $userId
     * @return int
     */
    public static function removeUserFromNetworkUserSet($networkId, $userId){
        $key = "n:".$networkId;
        if(is_array($userId)){
            foreach($userId as $uid){
                //Redis::sRem($key, $uid);
                CacheApi::getWriteConnection()->sRem($key, $uid);
            }
        }
        //return Redis::sRem($key, $userId);
        return CacheApi::getWriteConnection()->sRem($key, $userId);
    }

    /**
     * 根据用户的member type来保存用户
     * @param $networkId
     * @param $memberType 1 buyer 2 seller 3 other
     * @param $ids
     * @return int
     */
    public static function setMemberTypeSet($networkId, $memberType, $ids){
        $key = "u:member:".$networkId.":".$memberType;
        if(is_array($ids)){
            foreach($ids as $uid){
                //Redis::sAdd($key, $uid);
                CacheApi::getWriteConnection()->sAdd($key, $uid);
            }
        }

        //return Redis::sAdd($key, $ids);
        return CacheApi::getWriteConnection()->sAdd($key, $ids);
    }

    /**
     * 返回指定member type的所有用户ID
     * @param $networkId
     * @param $memberType
     * @return array
     */
    public static function getMemberTypeSet($networkId, $memberType){
        $key = "u:member:".$networkId.":".$memberType;
        return Redis::sMembers($key);
    }

    /**
     * 从member type集合中删除一个用户ID
     * @param $networkId
     * @param $memberType
     * @param $userId
     * @return int
     */
    public static function removeMemberTypeFromSet($networkId,$memberType, $userId){
        $key = "u:member:".$networkId.":".$memberType;
        //return Redis::sRem($key, $userId);
        return CacheApi::getWriteConnection()->sRem($key, $userId);
    }

    /**
     * 设置被block的新闻或评论
     * 如果是新闻那么key是 news_reported; 如果是新闻的评论，那么key是news_commented_reported
     *
     * @param array $attr
     * $attr['type'] 设置的类型(新闻/新闻地评论)
     * $attr['id'] 新闻或评论的id
     *
     */
    public static function setReportedNewsOrComment(array $attr){
        $key = ($attr['type'] == NEWS_ATTITUDE_TYPE_NEWS ? "news_reported" : "news_commented_reported");
        //Redis::sAdd($key, $attr['id']);
        CacheApi::getWriteConnection()->sAdd($key, $attr['id']);
    }

    /**
     * 返回新闻或评论是否被block
     * @param $type
     * @param $id
     * @return bool
     */
    public static function getReportedNewsOrComment($type, $id){
        $key = ($type == NEWS_ATTITUDE_TYPE_NEWS ? "news_reported" : "news_commented_reported");
        return Redis::sIsMember($key, $id);
    }

    /**
     * 移除一个被block的新闻或评论
     *
     * @param $key
     * @param $ids
     */
    public static function removeReportedNewsOrComment($key, $ids){

        if(is_array($ids)){
            foreach($ids as $id){
                //Redis::sRem($key, $id);
                CacheApi::getWriteConnection()->sRem($key, $id);
            }
        }else{
            //Redis::sRem($key, $ids);
            CacheApi::getWriteConnection()->sRem($key, $ids);
        }

    }

    /**
     * 维护所有新闻的ID的集合
     * @param $newId
     */
    public static function setNews($newId){
        $key = "news";
        //Redis::sAdd($key, $newId);
        CacheApi::getWriteConnection()->sAdd($key, $newId);
    }

    /**
     * @return array 包含所有的新闻ID的数组
     */
    public static function getNews(){
        $key = "news";
        return Redis::sMembers($key);
    }

    /**
     *
     * 设置新闻点击数量.每当不同用户点击1次，点击数加1. key: news+新闻ID
     *
     * @param $newId 新闻ID
     */
    public static function setNewsClickNumber($newId){
        $key = "news:click:".$newId;
        //Redis::setBit($key, Session::get('uid'), 1);
        CacheApi::getWriteConnection()->setBit($key, Session::get('uid'), 1);
    }

    /**
     * 获取新闻点击数量
     * @param $newId 新闻ID
     * @return int 新闻点击数
     */
    public static function getNewsClickNumber($newId){
        $key = "news:click:".$newId;
        return Redis::bitCount($key);
    }

    /**
     * 获取新闻的评论数
     * @param $newId 新闻ID
     * @return string 新闻评论数量
     */
    public static function getNewsCommentNumber($newId){
        $key = "news:comment:number";
        return Redis::hGet($key, $newId);
    }

    /**
     * 设置新闻的评论数
     * @param $newId 新闻ID
     * @param int $number 设置的评论数,默认是0
     */
    public static function setNewsCommentNumber($newId, $number=0){
        $key = "news:comment:number";
        $number = CacheApi::getNewsCommentNumber($newId);
        $number = $number + 1;
        //Redis::hSet($key, $newId, $number);
        CacheApi::getWriteConnection()->hSet($key, $newId, $number);
    }

    /**
     * 按照新闻点击数量为score设置一个可排序的集合
     * @param $newId
     */
    public static function setTopNews($newId){
        $key = "top_news";
        $commentNumber = CacheApi::getNewsCommentNumber($newId);
        $newsClickNumber = CacheApi::getNewsClickNumber($newId);
        $score = $commentNumber + $newsClickNumber;
        //Redis::zAdd($key, $score, $newId);
        CacheApi::getWriteConnection()->zAdd($key, $score, $newId);
    }

    /**
     * 根据news点击数和评论数，设置一个可排序集合
     */
    public static function setUpTopNews(){
        $newsId = CacheApi::getNews();
        if(count($newsId) > 0){
            foreach($newsId as $id){
                //CacheApi::setTopNews($id);
                CacheApi::getWriteConnection()->setTopNews($id);
            }
        }
    }

    /**
     * 按照点击数获取新闻排名
     * @param bool $isGetScore
     * @return array  如果设置了包含score，则返回内容包含score
     */
    public static function getTopNews($isGetScore = false){
        $key = "top_news";

        if($isGetScore){
            //return Redis::zRevRange($key, 0, -1, 'withscores');
            return CacheApi::getWriteConnection()->zRevRange($key, 0, -1, 'withscores');
        }

        //return Redis::zRevRange($key, 0, -1);
        return CacheApi::getWriteConnection()->zRevRange($key, 0, -1);
    }

    //stats group # of members,discussions,buyer/seller postings
    public static function incrGroupMembersCount($gid, $value = 1){
        $key = "g:".$gid.":members_count";
        //return Redis::incrBy($key, $value);
        return CacheApi::getWriteConnection()->incrBy($key, $value);
    }

    public static function getGroupMembersCount($gid){
        $key = "g:".$gid.":members_count";
        return Redis::get($key);
    }

    public static function incrGroupDiscussionsCount($gid, $value = 1){
        $key = "g:".$gid.":discussions_count";
        //return Redis::incrBy($key, $value);
        return CacheApi::getWriteConnection()->incrBy($key, $value);
    }

    public static function getGroupDiscussionsCount($gid){
        $key = "g:".$gid.":discussions_count";
        $count = Redis::get($key);
        if($count<0){
            $count = 0;
        }
        return $count;
    }

    public static function incrGroupBuyerPostingsCount($gid, $value = 1){
        $key = "g:".$gid.":buyer_postings_count";
        //return Redis::incrBy($key, $value);
        return CacheApi::getWriteConnection()->incrBy($key, $value);
    }

    public static function getGroupBuyerPostingsCount($gid){
        $key = "g:".$gid.":buyer_postings_count";
        $count = Redis::get($key);
        if($count<0){
            $count = 0;
        }
        return $count;
    }

    public static function incrGroupSellerPostingsCount($gid, $value = 1){
        $key = "g:".$gid.":seller_postings_count";
        //return Redis::incrBy($key, $value);
        return CacheApi::getWriteConnection()->incrBy($key, $value);
    }

    public static function getGroupSellerPostingsCount($gid){
        $key = "g:".$gid.":seller_postings_count";
        $count = Redis::get($key);
        if($count<0){
            $count = 0;
        }
        return $count;
    }

    /**
     *
     * each click on q&a increase 1
     *
     * @param $qaId q&a id
     */
    public static function setQAClickCount($qaId){
        $key = "qa:click_count:".$qaId;
        //Redis::setBit($key, Session::get('uid'), 1);
        CacheApi::getWriteConnection()->setBit($key, Session::get('uid'), 1);
    }

    /**
     * get click total count for q&a
     *
     * @param $qaId q&a id
     * @return int
     */
    public static function getQAClickCount($qaId){
        $key = "qa:click_count:".$qaId;
        return Redis::bitCount($key);
    }

    /**
     * add q&a id to the popular_qa list
     *
     * @param $qaId
     */
    public static function addToPopularQA($qaId){
        $key = "popular_qa";
        //Redis::zRem($key,$qaId);
        CacheApi::getWriteConnection()->zRem($key,$qaId);
        $score = CacheApi::getQAClickCount($qaId);
        //Redis::zAdd($key, $score, $qaId);
        CacheApi::getWriteConnection()->zAdd($key, $score, $qaId);
    }

    /**
     * delete cache for q&a when delete
     *
     * @param int $qaId
     */
    public static function delQA($qaId){
        $key = "qa:click_count:".$qaId;
        //Redis::del($key);
        CacheApi::getWriteConnection()->del($key);
        $key = "popular_qa";
        //Redis::zRem($key,$qaId);
        CacheApi::getWriteConnection()->zRem($key,$qaId);
    }

    /**
     * get popular q&a ids
     *
     * @param int $total
     * @param bool $withScore
     * @return array
     */
    public static function getPopularQA($total = POPULAR_QA_COUNT, $withScore = false){
        $key = "popular_qa";

        if($withScore){
            return Redis::zRevRange($key, 0, $total-1, 'withscores');
        }

        return Redis::zRevRange($key, 0, $total-1);
    }

    /**
     * 将所有的network logo存成一个hash对象，同时存储network的名称和logo
     * @param int $nid
     * @param string $logo
     * @param string $networkName
     * @param int $activate
     */
    public static function addNetworkInfo($nid, $logo, $networkName = '', $activate = 0){
        $key = "network_info";
        $logoField = $nid."_logo";
        $nameField = $nid."_name";
        $activateField = $nid."_activate";

        //Redis::hSet($key, $logoField, $logo);
        CacheApi::getWriteConnection()->hSet($key, $logoField, $logo);
        //Redis::hSet($key, $nameField, $networkName);
        CacheApi::getWriteConnection()->hSet($key, $nameField, $networkName);
        //Redis::hSet($key, $activateField, $activate);
        CacheApi::getWriteConnection()->hSet($key, $activateField, $activate);
    }

    /**
     * 返回network对应的logo/name信息
     * @param $nid
     * @param $field string  如果是选取名称为(name), 如果是选择logo则是(logo)
     * @return string
     */
    public static function getNetworkInfo($nid, $field = 'name'){
        $key = "network_info";
        $selectField = $nid."_".$field;

        return Redis::hGet($key, $selectField);
    }

    /**
     * 添加loop的relationship 集合列表
     *
     * 其中，关系为同事的类型，应该查询同事的集合。不建议保存这个集合当中
     *
     * @param $nid network id
     * @param $uid 主用户id
     * @param $rType [LOOP_R_OTHER 0  other/LOOP_R_BUYING_SELLING 1/LOOP_R_PARTNERS 2/LOOP_R_COLLEAGUE 3]
     * @param $loopId 和主用户有$rType关系的用户id
     *
     * @return int 添加成功的个数
     */
    public static function setLoopRelationship($nid, $uid, $rType, $loopId){
        $key = 'r:'.$nid.':'.$uid.':'.$rType;
        //return Redis::sAdd($key, $loopId);
        return CacheApi::getWriteConnection()->sAdd($key, $loopId);
    }

    /**
     * 从已有的关系列表中删除对应关系的id
     *
     * @param $nid
     * @param $uid
     * @param $rType
     * @param $loopId
     * @return int
     */
    public static function removeLoopRelationship($nid, $uid, $rType, $loopId){

        $key = 'r:'.$nid.':'.$uid.':'.$rType;

        if( is_array($loopId) ){
            //return Redis::sRem($key, $loopId);
            return CacheApi::getWriteConnection()->sRem($key, $loopId);
        }

        //return Redis::sRem($key, $loopId);
        return CacheApi::getWriteConnection()->sRem($key, $loopId);

    }

    /**
     * 返回用户有具体关系的用户id列表
     * @param $nid
     * @param $uid
     * @param $rType
     * @return array］
     * ＼
     */
    public static function getLoopRelationshipSet($nid, $uid, $rType ){
        $key = 'r:'.$nid.':'.$uid.':'.$rType;

        return Redis::sMembers($key);
    }

    /**
     * 将同一公司的人员放到一个set中
     * @param int $nid network id
     * @param int $companyId
     * @param array| int $uid
     */
    public static function setColleagueSet($nid, $companyId, $uid){

        $key = 'colleague:'.$nid.':'.$companyId;

        if(is_array($uid)){

            foreach($uid as $anotherId){
                //Redis::sAdd($key, $anotherId);
                CacheApi::getWriteConnection()->sAdd($key, $anotherId);
            }

        }

        //Redis::sAdd($key, $uid);
        CacheApi::getWriteConnection()->sAdd($key, $uid);

    }

    /**
     * 返回公司所有的同事
     * @param int $nid
     * @param int $companyId
     * @return array
     */
    public static function getColleagueSet($nid, $companyId){

        $key = 'colleague:'.$nid.':'.$companyId;
        return Redis::sMembers($key);
    }

    /**
     * 删除一个或多个同事
     * @param int $nid
     * @param int $companyId
     * @param int $uid
     * @return int|void
     */
    public static function removeFromColleagueSet($nid, $companyId, $uid){
        $key = 'colleague:'.$nid.':'.$companyId;

        if( is_array($uid) ){
            foreach( $uid as $userId ){
                //Redis::sRem($key, $userId);
                CacheApi::getWriteConnection()->sRem($key, $userId);
            }
        }

        //return Redis::sRem($key, $uid);
        return CacheApi::getWriteConnection()->sRem($key, $uid);
    }

    /**
     * 判断两个用户是否是同事
     *
     * @param int $nid
     * @param int $userId
     * @param int $anotherId
     * @return bool
     */
    public static function isColleague($nid, $userId, $anotherId){

        $companyUserApi = new CompanyUserApi();
        $companyId = $companyUserApi->getCompany($userId)->id;

        if(is_null($companyId)){
            return false;
        }

        $key = 'colleague:'.$nid.':'.$companyId;

        return Redis::sIsMember($key, $userId) && Redis::sIsMember($key, $anotherId);

    }

    /**
     * 保存用户对网站的评价
     * @param $reputationId
     * @return int
     */
    public static function setReputation($reputationId){
        $key = 'rep';
        //return Redis::sAdd($key, $reputationId);
        return CacheApi::getWriteConnection()->sAdd($key, $reputationId);
    }

    /**
     * 删除用户对网站的评价
     * @param $reputationId
     * @return int
     */
    public static function delReputation($reputationId){
        $key = 'rep';
        //return Redis::sRem($key, $reputationId);
        return CacheApi::getWriteConnection()->sRem($key, $reputationId);
    }

    /**
     * 随机返回count个用户评价
     * @param int $count
     * @return string
     */
    public static function getRandReputation($count = 2){
        $key = 'rep';
        return Redis::sRandMember($key, $count);
    }

    /**
     * 映射blogger用户的ID和tradove用户的ID
     * @param $bloggerId
     * @param $tradoveId
     * @return int
     */
    public static function setBloggerIdToTradoveId($bloggerId, $tradoveId){

        $key = 'blog:tradove';

        //return Redis::hSet($key,$bloggerId, $tradoveId);
        return CacheApi::getWriteConnection()->hSet($key,$bloggerId, $tradoveId);

    }

    /**
     * 根据blogger的ID返回tradove的用户ID
     * @param $bloggerId
     * @return string
     */
    public static function getTradoveIdFromBlog($bloggerId){
        $key = 'blog:tradove';
        return Redis::hGet($key, $bloggerId);
    }

    /**
     * 返回所有的blog和tradove对应的ID列表
     * @return array
     */
    public static function getAllTradoveIdFromBlog(){
        $key = 'blog:tradove';
        return Redis::hVals($key);
    }

    /**
     * 将用户blog信息添加到用户hash对象中
     *
     * 获取用户的blog缓存信息,调用获取用户的属性方法
     *
     * @param $blogger
     * @param int $uid tradove user id
     * @return bool
     */
    public static function setBlogger($blogger, $uid = null){

        $key = "USER_" . $uid;

        $properties = array(
            'bid' => $blogger->ID, //blog用户ID
            'user_login' => $blogger->user_login,
            'user_pass' => $blogger->user_pass,
        );

        //return Redis::hmset($key, $properties);
        return CacheApi::getWriteConnection()->hmset($key, $properties);
    }

    /**
     * 获取缓存中，blog用户对象
     *
     * @param int $userId tradove用户ID
     * @param array $properties 需要获取的对象属性,如果属性为空,返回整个blogger对象
     * @return array
     */
    public static function getBlogger($userId, array $properties = null){

        $key = 'USER_'.$userId;

        if (!empty($properties) && count($properties) > 0 ) {
            return Redis::hMGet($key, $properties);
        }

        return Redis::hMGet($key, array('id', 'first_name','last_name', 'bid', 'user_login', 'user_pass'));

    }


    public static function suggestUser($nid, $uid, array $info){

        $key = 'r:'.$nid.':'.$uid;

        //return Redis::hMset($key, $info);
        return CacheApi::getWriteConnection()->hMset($key, $info);

    }

    public static function getSuggestUser($nid, $uid, array $fields){
        $key = 'r:'.$nid.':'.$uid;

        return Redis::hMget($key, $fields);
    }

    //***************** reference cache start *******************//

    /**
     * 获取评价的数量
     *
     * @param $targetId 被评价目标id
     * @param $networkId
     * @param $refType  REFERENCE_TYPE_USER|REFERENCE_TYPE_PRODUCT|REFERENCE_TYPE_COMPANY
     * @return int
     */
    public static function getReferenceCount($targetId, $networkId, $refType){
        switch($refType){
            case REFERENCE_TYPE_USER:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $pattern = "user:*:product:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $pattern = "user:*:company:".$targetId.":network:".$networkId.":ref_score";
                break;
            default:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
        }

        $keys = Redis::keys($pattern);
        $count = count($keys);

        return $count;
    }

    /**
     * 获取发送评价的数量
     *
     * @param $userId
     * @param $networkId
     * @param $refType
     * @return int
     */
    public static function getSentReferenceCount($userId, $networkId, $refType){
        switch($refType){
            case REFERENCE_TYPE_USER:
                $pattern = "user:".$userId.":target:*:network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $pattern = "user:".$userId.":product:*:network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $pattern = "user:".$userId.":company:*:network:".$networkId.":ref_score";
                break;
            default:
                $pattern = "user:".$userId.":target:*:network:".$networkId.":ref_score";
                break;
        }

        $keys = Redis::keys($pattern);
        $count = count($keys);

        return $count;
    }

    /**
     * 设置评价分数
     *
     * @param $userId 发起者id
     * @param $targetId 被评价目标id
     * @param $networkId
     * @param $refType REFERENCE_TYPE_USER|REFERENCE_TYPE_PRODUCT|REFERENCE_TYPE_COMPANY
     * @param $score 评价分数
     * @return int
     */
    public static function addReferenceScore($userId, $targetId, $networkId, $refType, $score){
        switch($refType){
            case REFERENCE_TYPE_USER:
                $key = "user:".$userId.":target:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $key = "user:".$userId.":product:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $key = "user:".$userId.":company:".$targetId.":network:".$networkId.":ref_score";
                break;
            default:
                $key = "user:".$userId.":target:".$targetId.":network:".$networkId.":ref_score";
                break;
        }

        //return Redis::set($key, $score);
        return CacheApi::getWriteConnection()->set($key, $score);
    }

    /**
     * 获取评价分数
     *
     * @param $userId 发起者id
     * @param $targetId 被评价目标id
     * @param $networkId
     * @param $refType REFERENCE_TYPE_USER|REFERENCE_TYPE_PRODUCT|REFERENCE_TYPE_COMPANY
     * @return int
     */
    public static function getReferenceScore($userId, $targetId, $networkId, $refType){
        switch($refType){
            case REFERENCE_TYPE_USER:
                $key = "user:".$userId.":target:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $key = "user:".$userId.":product:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $key = "user:".$userId.":company:".$targetId.":network:".$networkId.":ref_score";
                break;
            default:
                $key = "user:".$userId.":target:".$targetId.":network:".$networkId.":ref_score";
                break;
        }

        return Redis::get($key);
    }

    /**
     * 获取评价平均分
     *
     * @param $targetId 被评价目标id
     * @param $networkId
     * @param $refType REFERENCE_TYPE_USER|REFERENCE_TYPE_PRODUCT|REFERENCE_TYPE_COMPANY
     * @return float|int
     */
    public static function getReferenceAvgScore($targetId, $networkId, $refType){
        switch($refType){
            case REFERENCE_TYPE_USER:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $pattern = "user:*:product:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $pattern = "user:*:company:".$targetId.":network:".$networkId.":ref_score";
                break;
            default:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
        }

        $keys = Redis::keys($pattern);
        $count = count($keys);
        if($count <= 0){
            return 0;
        }
        $sum = 0;

        foreach($keys as $key){
            $sum += Redis::get($key);
        }

        $avgScore = round( $sum / $count );

        return $avgScore;
    }

    /**
     * 获取评价的统计
     *
     * @param $targetId 被评价目标id
     * @param $networkId
     * @param $refType REFERENCE_TYPE_USER|REFERENCE_TYPE_PRODUCT|REFERENCE_TYPE_COMPANY
     * @return float|int
     */
    public static function getReceivedReferenceStats($targetId, $networkId, $refType){

        switch($refType){
            case REFERENCE_TYPE_USER:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_PRODUCT:
                $pattern = "user:*:product:".$targetId.":network:".$networkId.":ref_score";
                break;
            case REFERENCE_TYPE_COMPANY:
                $pattern = "user:*:company:".$targetId.":network:".$networkId.":ref_score";
                break;
            default:
                $pattern = "user:*:target:".$targetId.":network:".$networkId.":ref_score";
                break;
        }

        $keys = Redis::keys($pattern);

        $count = count($keys);

        $score1 = 0;
        $score2 = 0;
        $score3 = 0;
        $score4 = 0;
        $score5 = 0;

        if($count>0){
            foreach($keys as $key){
                $score = Redis::get($key);
                switch($score){
                    case REF_SCORE_ONE:
                        $score1++;
                        break;
                    case REF_SCORE_TOW:
                        $score2++;
                        break;
                    case REF_SCORE_THREE:
                        $score3++;
                        break;
                    case REF_SCORE_FOUR:
                        $score4++;
                        break;
                    case REF_SCORE_FIVE:
                        $score5++;
                        break;
                    default;
                        break;
                }
            }
        }

        $result = array(
            REF_SCORE_ONE => $score1,
            REF_SCORE_TOW => $score2,
            REF_SCORE_THREE => $score3,
            REF_SCORE_FOUR => $score4,
            REF_SCORE_FIVE => $score5,
        );

        return $result;
    }
    //***************** reference cache end *******************//


    /**
     * @param $nid
     * @param $uid
     * @param $type 1:message 2:invitation 3:proposal 4:private discussion
     * @param $number
     * @return int
     */
    public static function addMsgNoneReadNumber($nid, $uid, $type, $number = 1){
        $key = 'n:'.$nid. ':u:' . $uid . ':msg:'.$type;
        //return Redis::incrBy($key, $number);
        return CacheApi::getWriteConnection()->incrBy($key, $number);
    }

    /**
     * 如果type == 0 返回所有的未读消息
     * @param $nid
     * @param $uid
     * @param $type
     * @return bool|int|string
     */
    public static function getMsgNoneReadNumber($nid, $uid, $type){
        
        $count = 0;

        $key = 'n:'.$nid. ':u:' . $uid . ':msg:'.$type;
        if($type == 0){

            for($i=1; $i<=4; $i++){
                $k = 'n:'. $nid. ':u:' . $uid .':msg:'.$i;
                $n = Redis::get($k);

                if(empty($n) || $n < 0) {
                    $n = 0;
                    Redis::set($k, $n);
                }

                $count += $n;
            }
        } else {
            $count = Redis::get($key);
            if(empty($count) || $count < 0) {
                $count = 0;
                Redis::set($key, $count);
            }
        }

        return $count;

    }

    public static function initIndustry(){
        $ind = Lang::get('category');
        foreach($ind as $k => $v){
            if(strlen($k) == 3){
                $key = 'industry:1';
                //Redis::hSet($key, $k, $v);
                CacheApi::getWriteConnection()->hSet($key, $k, $v);

                $listKey = 'industry:1:list';
                //Redis::rPush($listKey, $k);
                CacheApi::getWriteConnection()->rPush($listKey, $k);
            }

            if(strlen($k) == 5){
                $key = 'industry:2:'.substr($k, 0, 3);
                //Redis::hSet($key, $k, $v);
                CacheApi::getWriteConnection()->hSet($key, $k, $v);
            }

            if(strlen($k) == 7){
                $key = 'industry:3:'.substr($k, 0, 5);
                //Redis::hSet($key, $k, $v);
                CacheApi::getWriteConnection()->hSet($key, $k, $v);
            }
        }
    }

    public static function getIndustryInfo($key, $field = null){
        if(is_null($field)){
            return Redis::hGetAll($key);
        }

        return Redis::hGet($key, $field);
    }

}