<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('test/ipn', 'TestController@testIpn');
Route::post('test/ipn', 'TestController@testIpn');

Route::get('test/session/init', 'TestController@setTestSession');
Route::get('test/subscription/status/init', 'TestController@initSubscriptionStatusChange');
Route::post('test/subscription/status/change', 'TestController@changeSubscriptionStatus');
Route::get('test/block/logs', 'TestController@testLogBlocks');
Route::get('test/billing/mail', 'TestController@testBillingMail');
Route::get('test/auto/billing/failed', 'TestController@sendAutoBillingFailedMail');

Route::get('people/upgrade', 'PaymentController@upgrade');
Route::post('people/upgrade', 'PaymentController@upgrade');

Route::get('payment/order_list', 'PaymentController@orderList');

Route::get('payment/order_detail/{id}', 'PaymentController@orderDetail');

// Generate & View package info
Route::post('payment/step1', 'PaymentController@step1');
Route::get('payment/step1', 'PaymentController@step1');

// Generate & View order info
Route::post('payment/step2', 'PaymentController@step2');
Route::get('payment/step2', 'PaymentController@step2');

Route::post('payment/order/update', 'PaymentController@saveOrder');

// Delete order
Route::post('payment/delete_order', 'PaymentController@deleteOrder');

Route::get('payment/order/upgrade/unpass/paypal', 'PaymentController@updateUpgradeInfosWithZeroFee');

// IPN from paypal
Route::post('payment/ipn', 'PaymentController@ipn');
//Route::get('payment/ipn', 'PaymentController@ipn');

Route::get('payment/success', 'PaymentController@success');
Route::post('payment/success', 'PaymentController@success');
Route::get('payment/failed', 'PaymentController@failed');
Route::post('payment/failed', 'PaymentController@failed');

Route::post('payment/auto_billing/success', 'PaymentController@autoBillingSuccess');

// payment setting
Route::get('payment/setting', 'PaymentController@settingPayment');

// cancel auto billing function
Route::get('payment/auto_billing/cancel', 'PaymentController@cancelAutoBilling');