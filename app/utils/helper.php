<?php

if (!function_exists('array_check')) {

    /**
     * 为了解决数组根据Key取得value, 如果key为空报错,
     * 如GroupApi query方法有很多个参数要判断重新赋值
     *
     * 避免
     * <pre>
     *        if(isset($param['nid'])){
     *            $nid = $param['nid']
     *        }else{
     *            $nid = null;
     *        }
     *
     *
     *        if(isset($param['user_id'])){
     *            $uid = $param['user_id']
     *        }else{
     *            $uid = null;
     *        }
     *
     *      //more
     *
     *       Group::ById($id) -> ByNid($nid) -> ByType($type) -> .....;
     * </pre>
     *
     * 修改之后，只需要传参数的时候
     * <pre>
     *        Group::ById(array_check($param, 'id'))
     *            -> ByNid(array_check($param, 'nid'))
     *            -> ByType(array_check($param, 'type'))
     *            ->
     *            .....;
     *
     * </pre>
     *
     * @param  [array] $array  [an array contains different attributes]
     * @param  [string] $key
     * @return [null || mix]
     */
    function array_check($array, $key) {

        if (isset($array[$key])) {
            return $array[$key];
        } else {
            return null;
        }

    }
}

if (!function_exists("validate_data")) {
    /**
     * validate source data_array
     *
     * @param array $data_array source array
     * @param array $check_fields contains all the field needs check isset and not empty
     * @return bool
     */
    function validate_data(array $data_array, array $check_fields = array()) {
        if (empty($data_array)) {
            return false;
        }

        foreach ($check_fields as $field) {
            if (!isset($data_array[$field]) || empty($data_array[$field])) {
                return false;
            }
        }

        return true;
    }


}

if (!function_exists("array_union")) {
    /**
     * fix array_merge bug, if one of array is null , will get the null value
     *
     * @param  array $first_array [description]
     * @param  array $another_array [description]
     * @return [type]                [description]
     */
    function array_union(array $first_array, array $another_array) {

        //first check is empty
        $first_array = empty($first_array) ? array() : $first_array;
        $another_array = empty($another_array) ? array() : $another_array;

        //seconde check is an array
        $first_array = is_array($first_array) ? $first_array : array($first_array);
        $another_array = is_array($another_array) ? $another_array : array($another_array);

        return array_merge($first_array, $another_array);
    }

}

function getExtensionFileName($extension) {
    $fileInfo = pathinfo($extension);
    $extension = strtolower($fileInfo["extension"]);
    return $extension;
}

function getDomainFromEmail($email) {
    return strstr($email, '@');
}

// 'Y-m-d H:i'
function getDateByTimestamp($stamp, $format = 'Y-m-d') {
    if (is_null($stamp) || $stamp < 1000) {
        return "";
    }
    return date($format, $stamp);
}

function getLoginUser() {
    $currentUserId = Session::get('uid');
        $currentUser = Users::find($currentUserId);
    return $currentUser;
}

if (!function_exists("blog_news_format")) {
    /**
     * get abstract blog and news
     *
     * @param string $html
     * @param int $length
     * @param string $pad
     * @param bool $img
     * @return string
     */
    function blog_news_format($html, $length = 100, $pad = "...", $img = true) {
        $text = strip_tags($html);
        $text = mb_strimwidth($text, 0, $length, $pad);

        if ($img) {
            preg_match('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i', $html, $match);

            if (!empty($match)) {
                $text = $text . "<br />" . $match[0];
            }
        }

        return $text;
        //return $html;
    }
}


function removePrefixOfWeblink($weblink) {
    $for_weblink = trim($weblink);

    $index = strpos($for_weblink, "www . ");

    if ($index > 0 || $index === 0) {
        $for_weblink = substr($for_weblink, $index + 4);
    } else {
        $index = strpos($for_weblink, "http://");
        if ($index > 0 || $index === 0) {
            $for_weblink = substr($for_weblink, $index + 7);
        } else {
            $index = strpos($for_weblink, "https://");
            if ($index > 0 || $index === 0) {
                $for_weblink = substr($for_weblink, $index + 8);
            }
        }
    }
    return trim($for_weblink);
}


if (!function_exists("formatUserInformation")) {
    /**
     *
     * the Object contains Users data, but not a Users Model
     * so cannot invoke Users->formUserInformation() method
     *
     *
     * @param  [type] $Object stand PHP Object contains User's data
     * @return string
     */
    function formatUserInformation($object) {

        $info = "";

        if (!empty($object->current_job_title)) {
            $info .= $object->current_job_title . ' - ';
        }

        if (!empty($object->company)) {
            $info .= $object->company . ' - ';
        } else {
            $companyApi = new CompanyApi();
            $userCompany = $companyApi->getCompanyByUser($object->id);
            if (!empty($userCompany)) {
                $comapnyName = $userCompany->name;
                if (!empty($comapnyName)) {
                    $info .= $comapnyName . ' - ';
                }
            }
        }

        if($info){
            $info = rtrim($info, ' - ');
            $info .= '<br/>';
        }

        if (!empty($object->city)) {
            $info .= $object->city . ' - ';
        }

        if (!empty($object->province)) {
            $province = Lang::get('province')[$object->province];
            if(empty($province)){
                $info .=  $object->province. ' - ';
            }else{
                $info .=  $province. ' - ';
            }

        }

        /*if(!empty($object -> country)){
            $country = Lang::get('country.'.strtolower($object -> country));
            $info .= $country;
        }else{
            $country = CacheApi::getUserProperty($object->id, 'country');
            if(!empty($country)){
                $country = trans('country.'.strtolower($country));
                $info .= $country;
            }
        }*/

        $country = CacheApi::getUserProperty($object->id, 'country');
        if (!empty($country)) {
            $country = trans('country.' . strtolower($country));
            $info .= $country;
        }

        if ($info) {
            $info = rtrim($info, ' - ');
        }
        return $info;

    }
}

if (!function_exists("anotherFormatUserInformation")) {
    /**
     *
     * the Object contains Users data, but not a Users Model
     * so cannot invoke Users->formUserInformation() method
     *
     *
     * @param  [type] $Object stand PHP Object contains User's data
     * @return string
     */
    function anotherFormatUserInformation($object) {

        $info = "";

        if (!empty($object->current_job_title)) {
            $info .= $object->current_job_title . ', ';
        }

        if (!empty($object->company)) {
            $info .= $object->company . ', ';
        } else {
            $companyApi = new CompanyApi();
            $userCompany = $companyApi->getCompanyByUser($object->id);
            if (!empty($userCompany)) {
                $companyName = $userCompany->name;
                if (!empty($companyName)) {
                    $info .= $companyName . ', ';
                }
            }
        }

        $country = CacheApi::getUserProperty($object->id, 'country');
        if (!empty($country)) {
            $country = trans('country.' . strtolower($country));
            $info .= $country;
        }

        if ($info) {
            $info = rtrim($info, ', ');
        }

        if(!empty($info)) {
            return '(' . $info . ')';
        }

        return '';

    }

}

if (!function_exists('format_user_info_with_loop_relationship')) {

    function transRelationTypeToText($type) {

        $text = 'Other Relationships';
        switch ($type) {

            case LOOP_R_OTHER:
                break;
            case LOOP_R_BUYING_SELLING:
                $text = 'Buying/Selling';
                break;
            case LOOP_R_PARTNERS:
                $text = 'Partner';
                break;
            case LOOP_R_COLLEAGUE:
                $text = 'Colleague';
                break;
            default:
        }

        return $text;
    }

    function format_user_info_with_loop_relationship($object) {

        $info = formatUserInformation($object);

        $info = $info . '</br>';

        $loopUserApi = new LoopUserApi();
        $lu = $loopUserApi->getLoopUser($object->id);

        if (!empty($lu)) {

            $relationText = transRelationTypeToText($lu->relationship);
            $info = $info . 'Loop Type:' . $relationText;

        }

        return $info;
    }

}


/**
 * cut a string by limit number letters and replace with the $pad string
 *
 * @param string $source
 * @param int $limit
 * @param string $pad
 * @return string
 */
function cut_str_by_letters($source, $limit, $pad = '...') {
    $source = trim($source);

    if (strlen($source) > $limit) {
        return substr($source, 0, $limit) . $pad;
    }

    return $source;
}

/**
 * cut string by limit number words and replace with the $pad string
 *
 * @param string $source
 * @param int $limit
 * @param string $pad
 * @return string
 */
function cut_str_by_words($source, $limit, $pad = '...') {
    $source = trim($source);

    $words = explode(' ', $source);

    if (count($words) > $limit) {
        return implode(" ", array_slice($words, 0, $limit)) . $pad;
    }

    return $source;

}

if (!function_exists("displayCurrPageNum")) {
    /**
     * @param object $models pagination object
     * @return string
     */
    function displayCurrPageNum($models) {
        if (empty($models)) {
            return "0 - 0 / 0";
        }
        $totalNum = $models->getTotal();
        $currPage = $models->getCurrentPage();
        $perPage = $models->getPerPage();

        $startNum = ($currPage - 1) * $perPage + 1;
        if ($currPage * $perPage >= $totalNum) {
            $endNum = $totalNum;
        } else {
            $endNum = $currPage * $perPage;
        }
        if ($totalNum == 0) {
            $startNum = 0;
            $endNum = 0;
        }
        $str = $startNum . " - " . $endNum . " / " . $totalNum;
        return $str;
    }
}

if (!function_exists("showUserName")) {

    function showUserName($id, $isFull = true, $isFirst = false, $isLast = false) {

        if (empty($id)) {
            return null;
        }

        $firstName = CacheApi::getUserProperty($id, 'first_name');
        $lastName = CacheApi::getUserProperty($id, 'last_name');

        if ($isFull) {
            return $firstName . ' ' . $lastName;
        }

        if ($isFirst) {
            return $firstName;
        }

        if ($isLast) {
            return $lastName;
        }
    }
}


if (!function_exists("getUserInfoById")) {

    function getUserInfoById($id) {

        $info = "";

        $jobTitle = CacheApi::getUserProperty($id, 'current_job_title');
        if (!empty($jobTitle)) {
            $info .= $jobTitle . ' - ';
        }

        $companyApi = new CompanyApi();
        $userCompany = $companyApi->getCompanyByUser($id);
        if (!empty($userCompany)) {
            $comapnyName = $userCompany->name;
            if (!empty($comapnyName)) {
                $info .= $comapnyName . ' - ';
            }
        }

        /*$companyId = CacheApi::getUserProperty($id, 'company');
        if(!empty($companyId)){
            $api = new CompanyApi;
            $company = $api->getCompanyById($companyId);
            if(!empty($company)) {
                $info .= $company->name .' - ';
            }
        }*/

        $city = CacheApi::getUserProperty($id, 'city');
        if (!empty($city)) {
            $info .= $city . ' - ';
        }

        $province = CacheApi::getUserProperty($id, 'province');
        if (!empty($province)) {
            $temp = Lang::get('province')[$province];
            if(empty($temp)){
                $info .= $province . ' - ';
            }else{
                $info .= $temp . ' - ';
            }

        }

        $country = CacheApi::getUserProperty($id, 'country');
        if (!empty($country)) {
            $country = trans('country.' . strtolower($country));
            $info .= $country;
        }

        if ($info) {
            $info = rtrim($info, ' - ');
        }

        return $info;

    }

}

if (!function_exists("formatCompanyInformation")) {
    function formatCompanyInformation($object, $isFormat = true, $fontNumber = 120) {

        $info = "";
        $lineChangeMark = '<br/>';

        if (!$isFormat) {
            $info = "";
            $lineChangeMark = ' - ';
        }

        if (!empty($object->industry)) {
            $info .= Lang::get('category')[$object->industry];
            if (!empty($object->sec_industry)) {
                $info .= ' & ' . Lang::get('category')[$object->sec_industry];
            }
            $info .= $lineChangeMark;
        }

        if (!empty($object->description)) {
            $info .= transfer($object->description, 150) . $lineChangeMark;
        }

        if (!empty($object->city)) {
            $info .= $object->city . ' - ';
        }

        if (!empty($object->province)) {
            $province = Lang::get('province')[$object->province];
            if(empty($province)){
                $info .= $object->province . ' - ';
            }else{
                $info .= $province . ' - ';
            }

        }

        if (!empty($object->country)) {
            $country = Lang::get('country.' . strtolower($object->country));
            $info .= $country;
        } else {
            $country = CacheApi::getUserProperty($object->id, 'country');
            if (!empty($country)) {
                $country = trans('country.' . strtolower($country));
                $info .= $country;
            }
        }

        if ($info) {
            $info = rtrim($info, ' - ');
        }

        return $info;
    }
}

if (!function_exists("formatProductInformation")) {
    function formatProductInformation($object, $isFormat = true) {

        $info = "<br/>";
        $lineChangeMark = '<br/>';

        if (!$isFormat) {
            $info = "";
            $lineChangeMark = '';
        }

        if (!empty($object->spec)) {
            $info .= transfer($object->spec, 168) . $lineChangeMark;
        }

        return $info;
    }
}

if (!function_exists("transfer")) {
    function transfer($s, $length = 50) {
        if (mb_strlen($s, "utf-8") > $length) {
            return mb_substr($s, 0, $length, "utf-8") . '...';
        }
        return $s;
    }
}


function displayMemberCompanyId($companyId) {
    if (!is_null($companyId)) {
        echo "/" . $companyId;
    }
}

function isValidEmail($email) {
    $email = trim($email);

    return eregi("^[a-zA-Z0-9]+[_a-zA-Z0-9-]*(\.[_a-z0-9-]+)*@[a-z??????0-9]+"
        . "(-[a-z??????0-9]+)*(\.[a-z??????0-9-]+)*(\.[a-z]{2,6})$", $email);
}

function createURL($url, $user_id, $network_id = 0) {

    if (strpos($url, '?') > 0) {

        if (!empty($network_id))
            $new_url = $url . '&ntid=' . $network_id . '&uid=' . $user_id . '&to=' . urlencode(str_replace('&', '+', $url));
        else
            $new_url = $url . '&uid=' . $user_id . '&to=' . urlencode(str_replace('&', '+', $url));

    } else {

        if (!empty($network_id))
            $new_url = $url . '?ntid=' . $network_id . '&uid=' . $user_id . '&to=' . urlencode(str_replace('&', '+', $url));
        else
            $new_url = $url . '?uid=' . $user_id . '&to=' . urlencode(str_replace('&', '+', $url));
    }

    return $new_url;
}

function calcPercetage($num, $totalNum) {

    if (empty($num) || empty($totalNum)) {
        return 0;
    }

    if($num == 0 || $totalNum == 0) {
        return 0;
    }

    return round($num / $totalNum * 100, 1);
}

function limit_text($text, $limit = 20) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function showCompanyDiversityFlags($diversity) {

    if (($diversity & DIVERSITY_OF_WOMAN) == DIVERSITY_OF_WOMAN) {
        echo HTML::image(asset('imgs/icon/icon_company_w.gif'), null, array('align' => 'absmiddle'));
    }

    if (($diversity & DIVERSITY_OF_MINORITY) == DIVERSITY_OF_MINORITY) {
        echo HTML::image(asset('imgs/icon/icon_company_m.gif'), null, array('align' => 'absmiddle'));
    }

    if (($diversity & DIVERSITY_OF_VETERAN) == DIVERSITY_OF_VETERAN) {
        echo HTML::image(asset('imgs/icon/icon_company_v.gif'), null, array('align' => 'absmiddle'));
    }

}

if (!function_exists('generate_password')) {

    function generate_password($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $password;
    }

}

if (!function_exists('str_urlToA')) {

    function str_urlToA($text) {
        //$pattern =  "/(?<=[^\]\)a-z0-9-=\"'\\/])((https?|ftp|callto|mailto):\/\/|www\.)([a-z0-9\/\-_+=.~!%@?#%&;:$\\()|]+)/i";
        $pattern = "/((https?|ftp|callto|mailto):\/\/|www\.)([a-z0-9\/\-_+=.~!%@?#%&;:$\\()|]+)/i";
        $replacement = "<a href=\"\\1\\3\" target=\"_blank\">\\1\\3</a>";
        $result = preg_replace($pattern, $replacement, $text);

        $pattern = "/href=\"(www.)([a-z0-9\/\-_+=.~!%@?#%&;:$\\|]+)\"/i";
        $replacement = "href=\"http://\\1\\2\"";

        $result = preg_replace($pattern, $replacement, $result);

        return $result;
    }

}

if (!function_exists('getUrlQueryString')) {
    /**
     * get url query string
     * @param string url if url not set,get current request url query string
     * @return array
     */
    function getUrlQueryString($url = "") {
        $query = "";
        if (empty($url)) {
            $query = $_SERVER["QUERY_STRING"];
        } else {
            $arr = parse_url($url);
            if (isset($arr['query'])) {
                $query = $arr['query'];
            }
        }
        $retArr = array();

        if (!empty($query)) {
            $arr = explode('&', $query);
            foreach ($arr as $str) {
                $temp = explode("=", $str);
                $retArr[$temp[0]] = $temp[1];
            }
        }
        return $retArr;
    }
}

if (!function_exists("rangeOfEmployees")) {
    function rangeOfEmployees() {
        return array(
            '1-10' => '1-10',
            '11-50' => '11-50',
            '51-200' => '51-200',
            '201-500' => '201-500',
            '501-1000' => '501-1,000',
            '1001-5000' => '1,001-5,000',
            '5001-10000' => '5,001-10,000',
            '10001-0' => '10,001+'
        );
    }
}

if (!function_exists("rangeOfAnnualSales")) {
    function rangeOfAnnualSales() {
//        return array(
//            '1-999999' => 'Under $1 Mil',
//            '1000000-4999999' => '$1 - 4.9 Mil',
//            '5000000-9999999' => '$5 - 9.9 Mil ',
//            '10000000-24900000' => '$10 - 24.9 Mil',
//            '25000000-49900000' => '$25 - 49.9 Mil',
//            '50000000-99900000' => '$50 - 99.9 Mil',
//            '100000000-249900000' => '$100 - 249.9 Mil',
//            '250000000-0' => '$250 Mil. and over',
//        );

        return array(
            '1-999999' => 'Under $1 M',
            '1000000-4999999' => '$1 M - 5 M',
            '5000000-9999999' => '$5 M - 10 M',
            '10000000-49999999' => '$10 M - 50 M',
            '50000000-99999999' => '$50 M - 100 M',
            '100000000-499999999' => '$100 M - 500 M',
            '500000000-999999999' => '$500 M - 1 B',
            '1000000000-4999999999' => '$1 B - 5 B',
            '5000000000-0' => '$5 B and over',
        );
    }
}

if (!function_exists("getMinAndMaxValue")) {
    /**
     * @param string $range the range string
     * example:
     * 1-10
     * 1,000-10,000
     * 10,000+
     * @return array the array contain min and max value,if min or max not a numeric,set it to 0
     * example
     * array(1,10)
     * array(1000,10000)
     * array(10000,0)
     */
    function getMinAndMaxValue($range) {
        $temp = explode("-", $range);
        /*
        if( count($temp) == 2){
            $min = preg_replace("/,/", "", $temp[0]);
            $max = preg_replace("/,/", "", $temp[1]);
        }else{
            $range = substr($range, 0, strlen($range)-1);
            $min = preg_replace("/,/", "", $range);
            $max = 0;
        }
        */
        if (count($temp) == 2) {
            $min = $temp[0];
            $max = $temp[1];
        } else {
            $min = $range;
            $max = 0;
        }

        if (!is_numeric($min)) {
            $min = 0;
        }

        if (!is_numeric($max)) {
            $max = 0;
        }
        return array(
            $min,
            $max
        );
    }
}

if (!function_exists("oldCompanyEmployeesAndSales2NewFormat")) {
    /**
     * @param object $company
     * @param bool $isSave true write to the database, false return new object
     * @return mixed
     */
    function oldCompanyEmployeesAndSales2NewFormat($company, $isSave = false) {
        $employees = $company->employees;
        $sales = $company->sales;

        $minEmployees = $company->minEmployees;
        $minSales = $company->minSales;

        if ($minEmployees > 0 || $minSales > 0) {
            if ($isSave) {
                return;
            }
            return $company;
        }
        //for employees
        $rangeOfEmployees = rangeOfEmployees();
        $max = 0;
        $flag = false;
        foreach ($rangeOfEmployees as $key => $value) {
            $temp = getMinAndMaxValue($key);
            if (is_null($employees) || $employees <= 0) {
                $company->minEmployees = $temp[0];
                $company->maxEmployees = $temp[1];
                $flag = true;
                break;
            }
            if ($employees >= $temp[0] && $employees <= $temp[1]) {
                $company->minEmployees = $temp[0];
                $company->maxEmployees = $temp[1];
                $flag = true;
                break;
            }
            $max = $temp[0];
        }
        if (!$flag) {
            if ($employees >= $max) {
                $company->minEmployees = $max;
                $company->maxEmployees = 0;
            }
        }
        $flag = false;
        $max = 0;
        //for sales
        $rangeOfSales = rangeOfAnnualSales();
        foreach ($rangeOfSales as $key => $value) {
            $temp = getMinAndMaxValue($key);
            if (is_null($sales) || $sales <= 0) {
                $company->minSales = $temp[0];
                $company->maxSales = $temp[1];
                $flag = true;
                break;
            }
            if ($sales >= $temp[0] && $sales <= $temp[1]) {
                $company->minSales = $temp[0];
                $company->maxSales = $temp[1];
                $flag = true;
                break;
            }
            $max = $temp[0];
        }
        if (!$flag) {
            if ($sales >= $max) {
                $company->minSales = $max;
                $company->maxSales = 0;
            }
        }
        if ($isSave) {
            $company->save();
        } else {
            return $company;
        }

    }
}

if (!function_exists("addHttpPrefix")) {
    function addHttpPrefix($url) {
        if (empty($url)) {
            return "";
        }
        if (substr($url, 0, 7) != 'http://' && substr($url, 0, 8) != 'https://') {
            $url = "http://" . $url;
        }
        return $url;
    }
}

if (!function_exists("transferEmbedVideoCode")) {
    /**
     * @param $code
     * example:
     *     <iframe width="1280" height="720" src="//www.youtube.com/embed/o3mP3mJDL2k" frameborder="0" allowfullscreen></iframe>
     * @return string
     */
    function transferEmbedVideoCode($code) {
        if (empty($code)) {
            return "";
        } else {
            $arr = $keywords = preg_split("/\s+/", $code);
            $src = "";
            foreach ($arr as $val) {
                if (substr($val, 0, 4) == 'src=') {
                    $src = $val;
                    break;
                }
            }
            return '<iframe width="550" height="450" ' . $src . ' frameborder="0" allowfullscreen></iframe>';
        }

    }
}


if (!function_exists("stripTags")) {
    /**
     * 去掉html里面的标签
     *
     * @param $text  待处理的内容
     * @param string $allowable_tags 保留的标签
     * @param bool $isTransBr 是否将br转换为html的换行符
     * @return string 处理过后的内容，如果为空，返回空的字符串
     */
    function stripTags($text, $allowable_tags = "", $isTransBr = false) {

        $text = !empty($text) ? strip_tags($text, $allowable_tags) : $text;

        $text = (!empty($text) && $isTransBr) ? nl2br($text) : $text;

        return $text;
    }
}


if (!function_exists("getLoopRelationship")) {

    function getLoopRelationship($userAId, $userBId = null) {
        $loopUserApi = new LoopUserApi();
        $lu = $loopUserApi->getLoopUser($userAId, $userBId);

        return $lu->relationship;
    }

}

//通过network的类型获取到network类型的名称
if (!function_exists("getNetWorkType")) {

    function getNetWorkType($networkType = 0) {

        $ntTypeName = '';

        switch ($networkType) {

            case NETWORK_TYPE_WOMAN_MINORITY:
                $ntTypeName = 'Women/Minority';
                break;
            case NETWORK_TYPE_BUY_SIDE:
                $ntTypeName = 'Buy Side';
                break;
            case NETWORK_TYPE_INDUSTRY:
                $ntTypeName = 'Industry';
                break;
            case NETWORK_TYPE_SELL_SIDE:
                $ntTypeName = 'Sell Side';
                break;
            case NETWORK_TYPE_TRADESHOW:
                $ntTypeName = 'Tradeshow';
                break;
            case NETWORK_TYPE_ASSOCIATION:
                $ntTypeName = 'Association';
                break;
            case NETWORK_TYPE_LOYALTY:
                $ntTypeName = 'Loyalty';
                break;
            case NETWORK_TYPE_OTHER:
                $ntTypeName = 'Other';
                break;

            default:
                break;
        }

        return $ntTypeName;

    }

}

//对单引号或者双引号加双斜杠
if (!function_exists("my_escape")) {

    function my_escape($str) {
        return strtr($str, array('"' => '\"', " " => "&nbsp;"));
    }
}

/**
 * 比较2个行业是否是父子行业
 * $father 父行业
 * $sun 子行业
 * 如果$sun在$father的子行业里面或者等于$father,返回true,否则，返回false
 */

if (!function_exists("you_belong_to_me")) {
    function you_belong_to_me($father, $sun) {
//        echo $father.'->'.$sun."<br/>";

        if (empty($father) || empty($sun))
            return false;

        if ($father == $sun)
            return true;

        if ($father < $sun) {
            return $father == substr($sun, 0, strlen($father));
        } else {
            return false;
        }
    }
}


if (!function_exists("setDisplayByParam")) {
    function setDisplayByParam($info, $infoItem) {
        if (!is_null($info) && !is_null($infoItem)) {
            echo "";
        } else {
            echo "display: none";
        }
    }
}

if (!function_exists("getDisplayByCountParam")) {
    function getDisplayByCountParam($info) {
        if (!is_null($info) && count($info) > 0) {
            echo count($info);
        } else {
            echo 0;
        }
    }
}


if (!function_exists("getDateFromBackend")) {
    function getDateFromBackend($info, $infoItem) {
        if (!is_null($info && !is_null($infoItem))) {
            echo $infoItem;
        } else {
            echo "";
        }
    }
}

if (!function_exists("showRefRating")) {
    function showRefRating($imageSrc, $num) {
        if($num <= 0){
            return;
        }
        echo "<span>";
        $label = Lang::get('people_reference.all_list.stats');

        echo $label;

        for($i = 0; $i < $num; $i++){
            echo '<img src="'.$imageSrc.'" align="absmiddle"/>';
        }

        echo "</span>";
    }
}

if( ! function_exists("parseMemberType")){
    function parseMemberType($memberType){
        switch($memberType){
            case MEMBER_TYPE_BUYER_ID:
                $type = "buyer";
                break;
            case MEMBER_TYPE_SELLER_ID:
                $type = "seller";
                break;
            case MEMBER_TYPE_OTHER_ID:
                $type = "other";
                break;
            default:
                $type = "all";
                break;
        }

        return $type;
    }
}

if( ! function_exists("isChecked")){
    function isChecked($currVal, $targetVal){
        if($currVal==$targetVal){
            echo "checked";
        }
    }
}

if( ! function_exists("canEditCompanyRef")){
    /**
     * 能否修改评论过的公司评价
     *
     * @param $networkId
     * @param $companyId
     * @param $userId
     * @return bool
     */
    function canEditCompanyRef($networkId, $companyId, $userId){
        $canEdit = false;

        //connected to the company
        if(CacheApi::isConnectedCompany($networkId, $userId, $companyId)){
            $canEdit = true;
        }

        if(!$canEdit){
            //和同事是loop
            $cuApi = new CompanyUserApi();
            $colleagueIds = $cuApi->getColleagues($companyId, array(), -1);
            $isLoop = false;
            $user = getLoginUser();
            foreach($colleagueIds as $id){
                $member_type = CacheApi::getUserProperty($id, "member_type");
                //member type can not be other and can not be same as login user
                if( ($member_type == MEMBER_TYPE_OTHER_ID)
                    || ($member_type == $user->member_type)){
                    continue;
                }
                if(CacheApi::isInLoop($networkId, $userId, $id)){
                    $isLoop = true;
                    break;
                }
            }

            $canEdit = $isLoop;

        }

        return $canEdit;
    }
}

if( ! function_exists("canEditProductRef")){
    /**
     * 能否修改评论过的产品评价
     *
     * @param $networkId
     * @param $productId
     * @param $userId
     * @param $productOwnerId
     * @return bool
     */
    function canEditProductRef($networkId, $productId, $userId, $productOwnerId){
        $canEdit = false;

        //connected to the company
        if(CacheApi::isConnectedProduct($networkId, $userId, $productId)){
            $canEdit = true;
        }

        if(!$canEdit){
            //和产品的owner是loop
            $isLoop = CacheApi::isInLoop($networkId,$userId, $productOwnerId);

            $canEdit = $isLoop;

        }

        return $canEdit;
    }
}

if(! function_exists('getContinents')){
    /**
     * 获取七大洲数组
     */
    function getContinents(){
        return Lang::get('continents')[0];
    }
}

if(! function_exists('getCountries')){
    /**
     * 获取国家数组
     * @param $continent
     * @return mixed
     */
    function getCountries($continent = null){

        if($continent)
            return Lang::get('continents')[$continent];
        else
            return Lang::get('country');
    }
}

if(! function_exists('is_country_in_continent')){

    function is_country_in_continent($country, $continent){

        if($country && $continent){
            $all_in_continent = getCountries($continent);
            if(!in_array($country, $all_in_continent) && !in_array($country, array_flip($all_in_continent)))
               return false;
        }

        return true;
    }
}


if( ! function_exists("getIndustryById")) {

    function getIndustryById($id)
    {

        $industryName = Lang::get('category.' . $id);

        if (strpos($industryName, 'category') == 0) {
            return '';
        }

        return $industryName;
    }

}


if( !function_exists("getBeginTimeOfToday")) {

    function getBeginTimeOfToday(){
        $dateOfToday = date('Y-m-d') . ' 00:00:00';

        return strtotime($dateOfToday);
    }

}

if( !function_exists("getBeginTimeOfYesterday")) {

    function getBeginTimeOfYesterday(){
        $beginTimeOfToday = getBeginTimeOfToday();

        return $beginTimeOfToday - 24 * 60 * 60;
    }

}

if( !function_exists("cut_str_by_words_no_pad")) {
    function cut_str_by_words_no_pad($source, $limit)
    {
        $source = trim($source);

        $words = explode(' ', $source);

        if (count($words) > $limit) {
            return implode(" ", array_slice($words, 0, $limit));
        }

        return $source;
    }
}

if( !function_exists("cut_str_by_letters_no_pad")) {
    function cut_str_by_letters_no_pad($source, $limit)
    {
        $source = trim($source);

        if (strlen($source) > $limit) {
            return substr($source, 0, $limit);
        }

        return $source;
    }
}








if( !function_exists("getBeginTimeFromDate")) {

    function getBeginTimeFromDate($date){
        return strtotime($date . ' 00:00:00');
    }

}

if( !function_exists("getEndTimeFromDate")) {

    function getEndTimeFromDate($date){
        return strtotime($date . ' 23:59:59');
    }

}


if( !function_exists("post_url")){

    function post_url($url, $data){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // post数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // post的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}

if( !function_exists("update_chat_password")){

    function update_chat_password($uid, $newPassword){
        $post_data = array(
            "uid" => $uid,
            "password" => $newPassword
        );

        if (USE_SYNC) {
            //异步处理注册chat用户
            $data = array(
                'url' => CHAT_RESET_PASSWORD_SERVICE_ADDRESS,
                'params' => $post_data
            );
            try{
                Queue::push('QueueController@handlePostURL', $data);
            }catch (Exception $e) {
                Log::info("===========queue push error===============");
                Log::error($e);
                post_url(CHAT_RESET_PASSWORD_SERVICE_ADDRESS, $post_data);
            }

        }else{
            post_url(CHAT_RESET_PASSWORD_SERVICE_ADDRESS, $post_data);
        }
    }
}











