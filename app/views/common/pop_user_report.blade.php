<!--pop invite-->
<div id="user_report_container" class="pop_width400 hide">
    <div class="pop_width400_title">User Results Survey for TraDove B2B Social Network</div>
    <div class="pop_width400_content">
        <ul>
            @if(!$hasUserReport)
                <li class="li380">Please tell us about your experience using TraDove B2B social network!</li>
            @else
                <li class="li380">Your latest report is {{getDateByTimestamp($userReportLastTime)}} ( {{floor((time()-$userReportLastTime)/(3600*24))}}  days ago)</li>
                <li class="li380">&nbsp;</li>
                <li class="li380">Your current report records are as follows:</li>
                @if($curr_user->member_type == MEMBER_TYPE_SELLER_ID)
                <li class="li380">Potential buyers found:&nbsp;<span>{{$userReportBuyerTotal}}</span></li>
                @elseif($curr_user->member_type == MEMBER_TYPE_BUYER_ID)
                <li class="li380">Potential sellers found:&nbsp;<span>{{$userReportSellerTotal}}</span></li>
                @endif
                <li class="li380">Potential business partners found:&nbsp;<span>{{$userReportPartnerTotal}}</span></li>
                @if($curr_user->member_type == MEMBER_TYPE_BUYER_ID || $curr_user->member_type == MEMBER_TYPE_SELLER_ID)
                <li class="li380">Agreements or transactions reached:&nbsp;<span>{{$userReportAgreementTotal}}</span></li>
                @endif
                <li class="li380">&nbsp;</li>
            @endif
            <li class="li380"><span>Please report your new number of:</span></li>
            <li class="li380">&nbsp;</li>
            @if($curr_user->member_type == MEMBER_TYPE_SELLER_ID)
                <li class="li380">Potential buyers found:(for seller)</li>
                <li class="li380"><input name="buyers_num" class="input300" type="text" /></li>
                <li class="li380">&nbsp;</li>
            @elseif($curr_user->member_type == MEMBER_TYPE_BUYER_ID)
                <li class="li380">Potential sellers found:(for buyer)</li>
                <li class="li380"><input name="sellers_num" class="input300" type="text" /></li>
                <li class="li380">&nbsp;</li>
            @endif
            <li class="li380">Potential business partners found:(for buyer, seller, or other)</li>
            <li class="li380"><input name="partners_num" class="input300" type="text" /></li>
            <li class="li380">&nbsp;</li>
            @if($curr_user->member_type == MEMBER_TYPE_BUYER_ID || $curr_user->member_type == MEMBER_TYPE_SELLER_ID)
                <li class="li380">Agreements or transactions reached:(buyer or seller)</li>
                <li class="li380"><input name="agreements_num" class="input300" type="text" /></li>
                <li class="li380">&nbsp;</li>
            @endif
            <li class="li380btn">
                <input id="submit_report_user" type="button" class="btn_blue_22" value="Submit" />
                <input id="close_user_report" type="button" class="btn_gray_22" value="Cancel" />
            </li>
            <li class="li380">&nbsp;</li>
        </ul>
    </div>
</div>
<!--pop invite over-->
<script>
    $(document).ready(function () {
        var popUserReportLayer = null;

        function cleanData() {
            $("input[name=buyers_num]").val('');
            $("input[name=sellers_num]").val('');
            $("input[name=partners_num]").val('');
            $("input[name=agreements_num]").val('');
        }

        function checkData(){
            var buyers_num = $("input[name=buyers_num]").val();
            var sellers_num = $("input[name=sellers_num]").val();
            var partners_num = $("input[name=partners_num]").val();
            var agreements_num = $("input[name=agreements_num]").val();

            var hasData = false;

            if(buyers_num || sellers_num || partners_num || agreements_num){
                hasData = true;
            }

            if(!hasData){
                return false;
            }

            if(buyers_num && !$.isNumeric(buyers_num)){
                return false;
            }

            if(sellers_num && !$.isNumeric(sellers_num)){
                return false;
            }

            if(partners_num && !$.isNumeric(partners_num)){
                return false;
            }

            if(agreements_num && !$.isNumeric(agreements_num)){
                return false;
            }

            return true;
        }

        $("#close_user_report").click(function(){
            //close layer
            if(popUserReportLayer){
                cleanData();
                layer.close(popUserReportLayer);
            }
        });

        $("#submit_report_user").click(function(){
            //commit report user information
            var buyers_num = $("input[name=buyers_num]").val();
            var sellers_num = $("input[name=sellers_num]").val();
            var partners_num = $("input[name=partners_num]").val();
            var agreements_num = $("input[name=agreements_num]").val();


            if(!checkData()){
                alert("invalid data found, check your input")
                return;
            }

            var btn = $(this);

            $.ajax({
                url: '/user_report/save',
                type: 'POST',
                async: false,
                data: {
                    "buyers_num": buyers_num,
                    "sellers_num": sellers_num,
                    "partners_num": partners_num,
                    "agreements_num": agreements_num
                },
                beforeSend: function () {
                    btn.showLoading();
                },
                success: function (data) {
                    if (data == 'success') {
                        cleanData();
                        btn.hideLoading();

                        if(popUserReportLayer){
                            layer.close(popUserReportLayer);
                        }
                    }
                    location.href = "/home";
                },
                error: function (data) {
                    btn.hideLoading();
                    console.error(data);
                }
            });

        });
        $("#show_user_report_link").click(function () {
            //show layer
            popUserReportLayer = $.layer({
                type: 1,
                closeBtn: 0,
                title: false,
                fix: false,
                border: [0],
                page: {dom: '#user_report_container'},
                offset: ['' , '30%'],
                zIndex: 1000,
                success: function () {
                }
            });
        });
    });
</script>