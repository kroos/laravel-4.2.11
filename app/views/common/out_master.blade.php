<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="TraDove | The premier business social network | b2b social media | Top B2B websites" />
    <title>{{$pageTitle}}</title>
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/layer/layer.js')}}
    {{HTML::script('js/tradove.js')}}
    {{HTML::script('js/common.js')}}
    {{HTML::script('js/jquery.showLoading.min.js')}}
    @yield('js')
    @yield('css')
</head>

<body class="body_login">

<div class="top_shadow"></div>

<div class="top">
    @include('common.out_header')
    @include('common.out_nav')
</div>
<!-- feedback start -->
@include('common.feedback')
<!-- feedback end -->
<div class="middle">
    @yield('content')
</div>

@include('common.footer')

@yield('footer_js')
</body>

</html>