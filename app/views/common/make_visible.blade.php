<div class="left_650_c2_c_detail">
    <ul>
        <li>
            {{Lang::get('product.common_lab_make_visible_to')}}
            <input name="{{$inputName}}" type="radio" value="{{MAKE_VISIBLE_LOOP}}" @if($visibleValue == MAKE_VISIBLE_LOOP) checked="checked" @endif/>{{Lang::get('product.common_lab_visible_loop')}}
            <input name="{{$inputName}}" type="radio" value="{{MAKE_VISIBLE_PUBLIC}}" @if($visibleValue == MAKE_VISIBLE_PUBLIC) checked="checked" @endif/>{{Lang::get('product.common_lab_visible_public')}}
            <input name="{{$inputName}}" type="radio" value="{{MAKE_VISIBLE_SYSTEM}}" @if($visibleValue == MAKE_VISIBLE_SYSTEM || empty($visibleValue)) checked="checked" @endif/>{{Lang::get('product.common_lab_visible_system')}}
        </li>
        <li>
            You can choose to disclose this information to your Business Loop (Loop), everyone (Public) or nobody (except matches made by our system).
        </li>
    </ul>
</div>