<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="TraDove | The premier business social network | b2b social media | Top B2B websites" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <title>
        @if(empty($pageTitle))
        TraDove
        @else
        {{$pageTitle}}
        @endif
    </title>
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::style('css/showLoading.css')}}
    {{HTML::style('css/industry.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/layer/layer.js')}}
    {{HTML::script('js/jquery.showLoading.min.js')}}
    {{HTML::script('js/common.js')}}
    {{HTML::script('js/tradove.js')}}
    {{HTML::script('js/underscore-min.js')}}
    {{ HTML::script("js/new-outside-invite.js") }}
    @yield('js')
    @yield('css')
</head>

<body>
    <div class="top_shadow"></div>
    <div class="top">
    </div>
    <!-- feedback start -->
    <!-- feedback end -->
    <div class="middle">

        <!--top ads-->
        <div class="space20"></div>
        <div class="div980">
            @yield('ads_top')
        </div>
        <div class="space20"></div>
        <!--top ads over-->

        <div class="div980">
            <!--Left-->
            <div class="left_650">
                @yield('content_left')
            </div>
            <!--Left over-->

            <!--right-->
            <div class="right_320">
                @yield('content_right')
            </div>
            <!--right over-->

        </div>

        <!--bottom ads-->
        <div class="space30"></div>
        <div class="div980">
            @yield('ads_bottom')
        </div>
        <div class="space30"></div>
        <!--bottom ads over-->

    </div>

    <script type="text/javascript" src="/js/industry-v2.0.js"></script>
    <!--<script type="text/javascript" src="/js/industry.js"></script>-->
</body>

</html>