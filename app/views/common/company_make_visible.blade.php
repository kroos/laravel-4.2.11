<div class="left_650_c2_c_detail">
    <ul>
        <li>
            {{Lang::get('company_profile.common_lab_make_visible_to')}}
            @if($visibleValue == 1)
            <input name="{{$inputName}}" type="radio" value="1" checked="checked"/>{{Lang::get('company_profile.common_lab_visible_connected')}}
            @else
            <input name="{{$inputName}}" type="radio" value="1" />{{Lang::get('company_profile.common_lab_visible_connected')}}
            @endif

            @if($visibleValue == 2 || empty($visibleValue))
            <input name="{{$inputName}}" type="radio" value="2" checked="checked"/>{{Lang::get('company_profile.common_lab_visible_public')}}
            @else
            <input name="{{$inputName}}" type="radio" value="2" />{{Lang::get('company_profile.common_lab_visible_public')}}
            @endif

            @if($visibleValue == 4)
            <input name="{{$inputName}}" type="radio" value="4" checked="checked"/>{{Lang::get('company_profile.common_lab_visible_system')}}
            @else
            <input name="{{$inputName}}" type="radio" value="4" />{{Lang::get('company_profile.common_lab_visible_system')}}
            @endif
        </li>
    </ul>
</div>