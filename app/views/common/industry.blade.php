<?php

    //整个div的ID
    if(empty($industryDivId)){$industryDivId = 'industryTree';}
    //选择行业后设置的行业ID的隐藏input的ID
    if(empty($industryId)){$industryId='industry';}
    if(empty($industrySubmitName)){$industrySubmitName='industry';}
    //行业搜索框的ID
    if(empty($searchIndustryId)){$searchIndustryId='searchIndustry';}

    //第一次进来之后,控件显示的值
    //showType = 1 All | showType = 2 current industry
    if(empty($showType)){$showType=1;}

    //选择行业后, 如果要跳转, 跳转后默认显示到行业框的数据
    //如果是搜索使用的是industry
    $m = $module; $i = $_GET['i']; $id = $_GET['industry'];
    $selectedIndustryId = (strcmp($m, 'search') == 0 ? $id : (empty($i) ? $id : $i));

    //如果不显示All, 显示为空的字符串
    if(strcmp($isShowAll, 'N') == 0){
        $selectedIndustryText = ((is_null($i) && is_null($id)) ? '' : Category::getNameById($selectedIndustryId));
    }else{
        $selectedIndustryText = (empty($selectedIndustryId)) ? 'All' : Category::getNameById($selectedIndustryId);
    }

    //设置触发行业选择的input框属性
    if(empty($triggerId)){$triggerId='showIndustry';};
    if(empty($triggerClass)){$triggerClass='input400';};
    if(empty($triggerName)){$triggerName='industry';};

    //当前公司的行业
    $userApi = new UsersApi();
    $companyIndustry = $userApi->getUserIndustry(Session::get('network_id'), Session::get('uid'));

    //初始化行业数据,有的页面是update页面
    if(!empty($initVal)){ $selectedIndustryId = $initVal; $selectedIndustryText = Category::getNameById($selectedIndustryId);}

?>
@if(strcmp($m, 'search') == 0)
    <input type="text" name="" id="{{$triggerId}}" class="{{$triggerClass}}" value="{{$selectedIndustryText}}"/>
@else
    <input type="text" id="{{$triggerId}}" class="{{$triggerClass}}" value="{{$selectedIndustryText}}"/>
@endif
<div id="{{$industryDivId}}" class="industry_main_div" style="display: none">
    <input type="hidden" id="{{$industryId}}" name="{{$industrySubmitName}}" value="{{$selectedIndustryId}}">
    <div class="industry_current">Current： <span>{{$selectedIndustryText}}</span></div>
    <div class="industry_searchinput">
        <input type="text" id="{{$searchIndustryId}}" class="input_industry">
    </div>
    <div class="space10"></div>
    <div class="industry_result">

        <?php $levelTwo = Redis::keys('industry:2:*');?>
        @foreach($levelTwo as $two)
            <?php
            $sub= Redis::hGetAll($two);
            $keys = explode(':', $two);
            $ids = 's_'.$keys[2];
            ?>
            <div id="{{$ids}}" class="industry_result_2nd" style="display:none;">
                <div class="industry_result_2nd_title">{{Category::getNameById($keys[2])}}</div>
                <div class="industry_result_overflow_2nd">
                    @foreach($sub as $key=>$val)
                        <?php $sub2= Redis::hGetAll('industry:3:'.$key);?>
                        @if(count($sub2) == 0)
                            <div id="{{$key}}" class="industry_result_2nd_2" vl="{{$val}}">{{$val}}</div>
                        @else
                            <div id="{{$key}}" class="industry_result_2nd_1" vl="{{$val}}">{{$val}}

                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        @endforeach

        <?php $levelThree = Redis::keys('industry:3:*');?>
        @foreach($levelThree as $three)
            <?php
            $sub= Redis::hGetAll($three);
            $keys = explode(':', $three);
            $ids = 't_'.$keys[2];
            $kk = 'industry:3:'.$keys[2];
            $sub2 = Redis::hGetAll($kk);
            ?>
            <div id="{{$ids}}" class="industry_result_3nd" style="display:none;">
                <div class="industry_result_2nd_title">{{Category::getNameById($keys[2])}}</div>
                <div class="industry_result_overflow_2nd">
                    @foreach($sub2 as $k=>$v)
                        <div id="{{$k}}"  class="industry_result_2nd_2" vl="{{$v}}">{{$v}}</div>
                    @endforeach
                </div>
            </div>
        @endforeach

        <div class="industry_result_overflow">
            @if(strcmp($isShowAll, 'N') !== 0)
                <div id="0" class="industry_result_2" vl="All">All</div>
            @endif
            @if($companyIndustry != -1 && !empty($companyIndustry))
                <div id="{{$companyIndustry}}" class="industry_result_2" vl="{{Category::getNameById($companyIndustry)}}"><b>Your Company Industry:</b>&nbsp;{{Category::getNameById($companyIndustry)}}</div>
            @endif
            <?php $levelOne = Redis::lRange('industry:1:list',0, -1);?>
            @foreach($levelOne as $one)
                <?php
                $cts=Redis::hGet('industry:1', $one);
                $ltwo= Redis::hGetAll('industry:2:'.$one);
                $cls = (count($ltwo)> 0 ? 'industry_result_1' : 'industry_result_2');
                ?>
                <div id="{{$one}}" class="{{$cls}}" vl="{{$cts}}">{{$cts}}</div>
            @endforeach
        </div>
        <div class="industry_search_overflow" style="display: none"></div>
    </div>
</div>
