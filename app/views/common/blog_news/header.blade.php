<div class="div980">
	<div class="navigation_logo">

        @if(Session::get('network_id') == 0)
            {{ HTML::image( "imgs/logo.gif" ) }}
        @else
            <div class="network_logo">
                <img src="{{FileManager::getFile('network_logo',CacheApi::getNetworkInfo(Session::get('network_id'), 'logo'))}}"/>
            </div>
            <div class="network_name">{{CacheApi::getNetworkInfo(Session::get('network_id'))}}</div>
            <div class="company_name">By TraDove</div>
        @endif

    </div>
	<div class="navigation_logo_words">{{trans('common.blog_header_title')}}</div>
    <div class="navigation_login" id="loginSection">
        <div class="div_left">
            <span>{{trans('login.login_lab_email')}}: <input id="blog_news_email" class="input100_login" name="" type="text">&nbsp;&nbsp;</span>
            <span>{{trans('login.login_lab_password')}}: <input id="blog_news_password" class="input100_login" name="" type="password"></span>
        </div>
        <div class="div_right">
            <input  id="blog_news_login_btn" type="button" class="btn_gray_22_login_2" value="{{trans('common.header_btn_login')}}">
        </div>
        <div class="div_right2">
            <!--<input name="" type="checkbox" value="">&nbsp;Remember me |--> <a href="javascript:void(0)" onclick="showForgetPwdDom();">Forgot Password?</a>
        </div>
    </div>
    <div class="navigation_login" id="logOutSection" style="display: none">
        <div class="navigation_login_option">
            <a href="/logout">Logout</a>
        </div>
    </div>
</div>