<?php
$uid = Session::get('uid');
$wpId = isset($_SESSION['wpid']) ? $_SESSION['wpid'] : '';
//$wpId = Session::get('wpid');
$blogger_home_url = get_author_posts_url($wpId);

$cuApi = new CompanyUserApi();

$company = $cuApi->getCompanyByOwnerId($uid);
$isCompanyOwner = !is_null($company);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <title>{{$pageTitle}}</title>
    @include('common.tip')
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/showLoading.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::style('css/industry.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/layer/layer.min.js')}}
    {{HTML::script('js/jquery.showLoading.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/chosen.jquery.min.js')}}
    {{HTML::script('js/common.js')}}
    {{HTML::script('js/tradove.js')}}
    {{HTML::script('js/underscore-min.js')}}
	<script type="text/javascript">
	$(function(){

	    var uid = '<?php echo $uid?>',
	        wpId = '<?php echo $wpId?>',
	        blogUrl = '<?php echo $blogger_home_url?>',
	        isCompanyOwner = '<?php echo $isCompanyOwner?>';

	    if(uid){

            if(wpId){
                $('#loginSection').hide();
                $('#logOutSection').show();
                $('#myblog_li').show();
            }

	        if(!isCompanyOwner){
	            $('#mynews_li').hide();
	        }

	    }else{
            $('#mynews_li').hide();
            //$('#myblog_li').hide();
	    }

		$("#blog_news_login_btn").click(function(){

			var email = $('#blog_news_email').val();
			var remember = $('input[name="remember_me"]').val();
			var pwd = $('#blog_news_password').val();

			if(email.length == 0 || pwd.length == 0){
	            layer.msg('email and password must be input', 3, 3);
				return false;
			}

			var url = '/blog_news/login/'+email;

			$.get(url,{'password':pwd, 'remember':remember},function(data){
				if(data.result == true){
                    $('#loginSection').hide();
                    $('#logOutSection').show();
                    $('#myblog_li').show();
				}
			},'json');

		});


        //忘记密码
        var forgetPwdLayerIndex = 0;
        function showForgetPwdDom(){
            $("#result_info").html("&nbsp;");
            forgetPwdLayerIndex = $.layer({
                type: 1,
                title: false,
                fix: false,
                border: [0],
                area: ['550px', 'auto'],
                page: {dom: '#forget_pwd'},
                offset: ['200px' , '50%'],
                zIndex:1000,
                success: function () {
                }
            });
        }

        function forgetPwd(){
            var cc= $("#confirm_btn").parent();

            $("#result_info").html("&nbsp;");
            var companyEmail = $("#email").val();
            if (companyEmail == '' || !isEmail(companyEmail)) {
                $("#result_info").html("{{Lang::get('login.login_msg_validation_email')}}");
                return;
            }

            if(companyEmail == 'admin@tradove.net'){
                $("#result_info").html("{{Lang::get('login.forget_pwd_admin')}}");
                return;
            }

            var exist = false;
            $.ajax({
                url: '/user/check_email_exist',
                type: 'POST',
                async:false,
                data: {
                    "email": companyEmail
                },
                beforeSend: function () {
                    cc.showLoading();
                },
                success: function (data) {
                    var result = $.parseJSON(data);
                    if (! result.exist) {
                        $("#result_info").html(result.msg);
                        cc.hideLoading();
                    } else {
                        exist = true;
                        $("#result_info").html("&nbsp;");
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });

            if(exist){
                //generate a password for user and email it
                $.ajax({
                    url: '/user/forget_pwd',
                    type: 'POST',
                    data: {
                        "email": companyEmail
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        var json = $.parseJSON(data);
                        if (json.result) {
                            layer.close(forgetPwdLayerIndex);
                            tradove.msg(json.msg, 1, 10);
                            //$("#result_info").html(json.msg);
                        }
                        cc.hideLoading();
                    },
                    error: function (data) {
                    }
                });
            }
        }
	});
	</script>
    @yield('js')
    @yield('css')
</head>

<body>

<div class="top_shadow"></div>

<div class="top">
    @if($uid)
        @include('common.header')
        @include('common.nav')
    @else
        @include('common.out_header')
        @include('common.out_nav')
    @endif
</div>

<!-- feedback start -->
@include('common.feedback')
<!-- feedback end -->

<div class="middle">
	<!--top ads-->
	<div class="space20"></div>
    {{-- @include('home.recommend.ads_top') --}}
	@yield('ads_top')
	<div class="space20"></div>
	<!--top ads over-->

	<div class="div980">
	<!--Left-->
	@yield('content_left')
	<!--Left over-->

	<!--right-->
	@yield('content_right')	
	<!--right over-->
	</div>
	 
	<!--bottom ads-->
	<div class="space30"></div>
    {{-- @include('home.recommend.ads_bottom') --}}
	@yield('ads_bottom')
	<div class="space30"></div>
	<!--bottom ads over-->
</div>

	@include('common.footer')

    <div id="forget_pwd" style="display: none" class="pop_width550">
        <div class="pop_width550_title">Forgot your password?</div>
        <div class="pop_width550_content">
            <ul>
                <li>
                    <div class="div380b">
                            <span style="margin-left: 20px;">
                                • Please enter your company email address and we will send you a new password.
                            </span>
                    </div>
                </li>
                <li>
                    <div class="div380b">
                        <span>&nbsp;</span>
                    </div>
                </li>
                <li class="li530">
                    <div class="div140">Your Account Email:</div>
                    <div class="div380">
                        <input id="email" type="text" value="" class="input300"/>
                    </div>
                </li>
                <li class="li530">
                    <div class="div380b">
                        <span id="result_info">&nbsp;</span>
                    </div>
                </li>
                <li class="li530">
                    <div class="div380b">
                        <input id="confirm_btn" type="button" class="btn_gray_22" value="Confirm" onclick="forgetPwd();"/>
                    </div>
                </li>
            </ul>
        </div>

    <script type="text/javascript" src="/js/industry-v2.0.js"></script>
        <!--<script type="text/javascript" src="/js/industry.js"></script>-->
    </div>
    </body>
    </html>
