<div class="div980">
    <div class="navigation_left">
        <ul>
            <li>
                <div class="navigation_left_diva" onclick="location.href='{{asset('home')}}';">Home</div>
            </li>
            <li><div class="navigation_left_diva" onclick="location.href='{{asset('about_us')}}';">{{trans('common.blog_navi_about')}}</div></li>
            <li><div class="navigation_left_diva" onclick="location.href='{{asset('privacy_policy')}}';">{{trans('common.blog_navi_article')}}</div></li>
            <li><div class="navigation_left_diva" onclick="location.href='{{asset('leads_list')}}'">TraDove Leads</div></li>
            <li><div class="navigation_left_diva" onclick="location.href='{{action('BlogController@home')}}'">TraDove Blogs/News</div></li>
            <!--<li><div class="navigation_left_diva" onclick="location.href='{{asset('tradove_news/0')}}';">TraDove News</div></li>-->
        </ul>
    </div>
    <div class="navigation_right"></div>
    <!--
    <div class="navigation_right">
        @if( ! Session::get("uid") )
        <a href="javascript:void(0)" onclick="showForgetPwdDom();">Forgot Password?</a>
        @endif
    </div>
    -->
</div>
<!--pop forget password-->
<div id="forget_pwd" style="display: none" class="pop_width550">
    <div class="pop_width550_title">Forgot your password?</div>
        <div class="pop_width550_content">
            <ul>
                <li>
                    <div class="div380b">
                        <span style="margin-left: 20px;">
                            • Please enter your company email address and we will send you a new password.
                        </span>
                    </div>
                </li>
                <li>
                    <div class="div380b">
                        <span>&nbsp;</span>
                    </div>
                </li>
                <li class="li530">
                    <div class="div140">Your Account Email:</div>
                    <div class="div380">
                        <input id="email" type="text" value="" class="input300"/>
                    </div>
                </li>
                <li class="li530">
                    <div class="div380b">
                        <span id="result_info">&nbsp;</span>
                    </div>
                </li>
                <li class="li530">
                    <div class="div380b">
                        <input id="confirm_btn" type="button" class="btn_gray_22" value="Confirm" onclick="forgetPwd();"/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--pop registration over-->
<script>
    var forgetPwdLayerIndex = 0;
    function showForgetPwdDom(){
        $("#result_info").html("&nbsp;");
        forgetPwdLayerIndex = $.layer({
            type: 1,
            title: false,
            fix: false,
            border: [0],
            area: ['550px', 'auto'],
            page: {dom: '#forget_pwd'},
            offset: ['200px' , '50%'],
            zIndex:1000,
            success: function () {
            }
        });
    }

    function forgetPwd(){
        var cc= $("#confirm_btn").parent();

        $("#result_info").html("&nbsp;");
        var companyEmail = $("#email").val();
        if (companyEmail == '' || !isEmail(companyEmail)) {
            $("#result_info").html("{{Lang::get('login.login_msg_validation_email')}}");
            return;
        }

        if(companyEmail == 'admin@tradove.net'){
            $("#result_info").html("{{Lang::get('login.forget_pwd_admin')}}");
            return;
        }

        var exist = false;
        $.ajax({
            url: '/user/check_email_exist',
            type: 'POST',
            async:false,
            data: {
                "email": companyEmail
            },
            beforeSend: function () {
                cc.showLoading();
            },
            success: function (data) {
                var result = $.parseJSON(data);
                if (! result.exist) {
                    $("#result_info").html(result.msg);
                    cc.hideLoading();
                } else {
                    exist = true;
                    $("#result_info").html("&nbsp;");
                }
            },
            error: function (data) {
                alert(data);
            }
        });

        if(exist){
            //generate a password for user and email it
            $.ajax({
                url: '/user/forget_pwd',
                type: 'POST',
                data: {
                    "email": companyEmail
                },
                beforeSend: function () {
                },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.result) {
                        layer.close(forgetPwdLayerIndex);
                        tradove.msg(json.msg, 1, 10);
                        //$("#result_info").html(json.msg);
                    }
                    cc.hideLoading();
                },
                error: function (data) {
                }
            });
        }
    }
</script>