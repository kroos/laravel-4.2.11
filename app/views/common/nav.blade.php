<div class="div980">
    <div class="navigation_left">
        <ul>
            <li>
                <div class="navigation_left_diva" onclick="location.href='{{asset('home')}}';">
                    {{Lang::get('common.nav_menu_home')}}
                </div>
            </li>

            <li id="menu_people">
                <div class="navigation_left_diva">{{Lang::get('common.nav_menu_people')}}</div>

                <div id="submenu_people" class="navigation_left_diva_down hide">
                    <ul>
                        <li class="navigation_l_d_d_a" url="{{asset('people/show')}}">
                            {{Lang::get('common.nav_menu_people_profile')}}
                        </li>
                        <!--
                        <li class="navigation_l_d_d_a" url="{{asset('people/refs_received')}}">
                            {{Lang::get('common.nav_menu_people_reference')}}
                        </li>
                        -->
                        <li class="navigation_l_d_d_a" url="{{asset('loop')}}">
                            {{Lang::get('common.nav_menu_people_loop')}}
                        </li>
                        <li class="navigation_l_d_d_a" url="{{asset('people/provide_ref/1')}}">
                            {{Lang::get('common.nav_menu_people_provide_reference')}}
                        </li>
                        <li class="navigation_l_d_d_a" url="{{asset('group/joined')}}">
                            {{Lang::get('common.nav_menu_people_group')}}
                        </li>
                    </ul>
                </div>
            </li>

            <li id="menu_product">
                <div class="navigation_left_diva">{{Lang::get('common.nav_menu_product')}}</div>
                <div id="submenu_product" class="navigation_left_diva_down hide">
                    <ul>
                        @if($curr_user->member_type == MEMBER_TYPE_BUYER_ID)
                            <!--
                            <li class="navigation_l_d_d_a" url="{{asset('product/refs_sent')}}">
                                {{Lang::get('common.nav_menu_product_reference')}}
                            </li>
                            -->
                        @elseif($curr_user->member_type == MEMBER_TYPE_SELLER_ID)
                            <li class="navigation_l_d_d_a" url="{{asset('product/list')}}">
                                {{Lang::get('common.nav_menu_product_profile')}}
                            </li>
                            <!--
                            <li class="navigation_l_d_d_a" url="{{asset('product/reference/my_products')}}">
                                {{Lang::get('common.nav_menu_product_reference')}}
                            </li>
                            -->
                        @endif
                        <li class="navigation_l_d_d_a" url="{{asset('product_connect_list')}}">
                            {{Lang::get('common.nav_menu_product_connect')}}
                        </li>
                    </ul>
                </div>
            </li>

            <li id="menu_company">
                <div class="navigation_left_diva">{{Lang::get('common.nav_menu_company')}}</div>
                <div id="submenu_company" class="navigation_left_diva_down hide">
                    <ul>
                        <li class="navigation_l_d_d_a" url="{{asset('company/show')}}">
                            {{Lang::get('common.nav_menu_company_profile')}}
                        </li>
                        <!--
                        <li class="navigation_l_d_d_a" url="{{asset('company/refs_received')}}">
                            {{Lang::get('common.nav_menu_company_reference')}}
                        </li>
                        -->
                        <li class="navigation_l_d_d_a" url="{{asset('company_connect_list')}}">
                            {{Lang::get('common.nav_menu_company_connect')}}
                        </li>
                    </ul>
                </div>
            </li>
            @if(getLoginUser()->member_type != MEMBER_TYPE_OTHER_ID)
            <li>
                <div class="navigation_left_diva" onclick="location.href='{{asset('leads')}}';">
                    {{Lang::get('leads.leads')}}
                </div>
            </li>
            @endif
            <!--
            <li>
                <div class="navigation_left_diva" onclick="location.href='{{asset('tradove_news/0')}}';">
                    {{Lang::get('common.nav_menu_news')}}
                </div>
            </li>
            -->
            <li>
                <div class="navigation_left_diva" onclick="location.href='{{action('BlogController@home')}}';">
                    {{Lang::get('common.nav_menu_news_blogs')}}
                </div>
            </li>

            <li>
                <div class="navigation_left_diva" onclick="location.href='{{asset('industry/people')}}';">
                    {{Lang::get('common.nav_menu_industry')}}
                </div>
            </li>
            <?php

            //network invite菜单的逻辑
            //1.如果类型是buyside/sellside, 只有本公司的人可以使用
            //2.如果类型是其它, 所有人可见
            $userId = Session::get('uid');
            $currentNetworkId = Session::get('network_id');

            $networkApi = new NetworkApi();
            $isNetworkOwner = $networkApi->isNetworkOwner($currentNetworkId,$userId);
            $network = $networkApi->getNetworkById($currentNetworkId);

            $companyUserApi = new CompanyUserApi();
            $isInTheSameCompany = $companyUserApi->isColleagues($userId, $network->admin_id);

            if($isNetworkOwner){
            ?>
                <li>
                    <div class="navigation_left_diva" onclick="location.href='{{asset('network/admin')}}';">
                        {{Lang::get('common.nav_menu_network_manage')}}
                    </div>
                </li>
            <?php
            }elseif( !$isNetworkOwner && !empty($currentNetworkId)){
            ?>
                @if($network->type == NETWORK_TYPE_SELL_SIDE || $network->type == NETWORK_TYPE_BUY_SIDE)
                    @if($isInTheSameCompany)
                    <li>
                        <div class="navigation_left_diva" onclick="location.href='{{asset('network/invite/0')}}';">
                            {{Lang::get('common.nav_menu_network_common')}}
                        </div>
                    </li>
                    @endif
                @else
                    <li>
                        <div class="navigation_left_diva" onclick="location.href='{{asset('network/invite/0')}}';">
                            {{Lang::get('common.nav_menu_network_common')}}
                        </div>
                    </li>
                @endif
            <?php
            }
            ?>

        </ul>
    </div>


    <div class="navigation_right">
        <!--@include('common.ad_invite_linkedin')-->
        <!--
        <a href="#">{{Lang::get('common.nav_menu_business_tools')}}</a>
        <a href="/people/upgrade">{{Lang::get('common.nav_menu_upgrade')}}</a>
        -->
        <a href="javascript:showFeedback('right_feedback');">{{Lang::get('common.nav_menu_feedback')}}</a>
    </div>


</div>



<script type="text/javascript">

    // Move this into common.js
    /**
    $('.navigation_l_d_d_a').each(function(){
        $(this).click(function(){
            window.location.href = $(this).attr('url');
        });
    }); */

    $(document).ready(function() {

        $('[id^="menu"]').mouseenter(function() {
            // $(this)..css('position', 'absolute').css('zIndex', 1000);
            menu = $(this).attr("id");
            submenu = menu.replace('menu', 'submenu');

            $('#' + submenu).attr("class", "navigation_left_diva_down");
            // $('#' + submenu).css('zIndex', 1000);
            // $('#' + submenu).parent().append($('#' + submenu));

        });

        // 鼠标从图标移走
        $('[id^="menu"]').mouseleave(function() {
            menu = $(this).attr("id");
            submenu = menu.replace('menu', 'submenu');

            $('#' + submenu).attr("class", "navigation_left_diva_down hide");
            // $('#' + submenu).css('zIndex', -1);
        });

    });

</script>