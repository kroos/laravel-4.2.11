<div id="pop_invitation_form" class="pop_width450" style="display: none">
    <div class="pop_width450_title">
        {{Lang::get('connect.send_invitation')}}
    </div>
    <div class="pop_width450_content">
        <ul>
            <li class="li430">
                <div class="div100">{{Lang::get('leads.form_name')}}</div>
                <div class="div380">{{$receiver_name}}</div>
                <input name="invitation_receiver_id" type="hidden" value="{{$receiver_id}}" />
                <input name="invitation_type" type="hidden" value="{{$type}}" />
                <input name="invitation_target" type="hidden" value="{{$target_id}}" />
            </li>
    @if($type == 1)
            <?php
            $loop_user_api = new LoopUserApi;
            $is_colleague = $loop_user_api->isColleague($receiver_id, Session::get("uid"));
            ?>
            @if($is_colleague & 1)
            <li class="li430 loopTypeRow" id="loop_colleague">
                <div class="div100">&nbsp;</div>
                <div class="div380">
                    <input name="loop_type" type="radio" value="{{LOOP_R_COLLEAGUE}}" />
                    &nbsp;Colleague
                </div>
            </li>
            @endif
            @if($is_colleague & 2)
            <li class="li430 loopTypeRow" id="loop_buying_selling">
                <div class="div100">&nbsp;</div>
                <div class="div380">
                    <input name="loop_type" type="radio" value="{{LOOP_R_BUYING_SELLING}}"/>
                    &nbsp;Buying/Selling
                </div>
            </li>
            @endif
            @if($is_colleague & 4)
            <li class="li430 loopTypeRow" id="loop_partner">
                <div class="div100">&nbsp;</div>
                <div class="div380">
                    <input name="loop_type" type="radio" value="{{LOOP_R_PARTNERS}}"/>
                    &nbsp;Partner（Investment，collaboration，etc）
                </div>
            </li>
            @endif
            <li class="li430 loopTypeRow">
                <div class="div100"></div>
                <div class="div380">
                    <input name="loop_type" type="radio" value="{{LOOP_R_OTHER}}" checked="checked" />
                    &nbsp;Other Relationships
                </div>
            </li>
    @endif
            <li class="li430">
                <div class="div100">{{Lang::get('leads.form_subject')}}</div>
                <div class="div380">
                    <input name="invitation_subject" class="input300" type="text" value="{{$subject}}" />
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <textarea name="invitation_content" class="textarea300" cols="" rows="">{{$content}}</textarea>
                </div>
            </li>
            <li class="li430">&nbsp;</li>
            <li class="li430">
                <div class="div380b">
                    <input type="button" class="btn_gray_22" value="{{Lang::get('leads.send')}}" id="invitation_send" />
                </div>
            </li>
            <li class="li430">&nbsp;</li>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("li.loopTypeRow:first div.div100").text("Loop Type:");
    });
</script>