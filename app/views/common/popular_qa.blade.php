@if(!is_null($popularQA))
@foreach($popularQA as $qa)
<li class="pop_w800_r_word_link">&bull;&nbsp;
    <a href="/qa/{{$qa->id}}">{{transfer($qa->question,80)}}</a>
</li>
@endforeach
@endif
<li>&nbsp;</li>
<li class="pop_w800_r_word_link">
    <a href="/qa_list">{{Lang::get('feedback.q_a.lnk.see_all', array('num'=> $total))}}&nbsp;&nbsp;
        <img src="/imgs/icon/blue_arrow.png" border="0" align="absmiddle" />
    </a>
</li>
<li>&nbsp;</li>