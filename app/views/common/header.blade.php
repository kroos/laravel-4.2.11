<div class="div980">
    <div class="navigation_logo">

        @if(Session::get('network_id') == 0)
        {{ HTML::image( "imgs/logo.gif" ) }}
        @else
        <div class="network_logo">
            <img src="{{FileManager::getFile('network_logo',CacheApi::getNetworkInfo(Session::get('network_id'), 'logo'))}}"/>
        </div>
        <div class="network_name">{{CacheApi::getNetworkInfo(Session::get('network_id'))}}</div>
        <!-- div class="company_name">TraD<span>o</span>ve Network</div -->
        <div class="company_name">By TraDove</div>
        @endif

    </div>

    @include('common.head.search')
    @include('common.head.shortcuts')

</div>

