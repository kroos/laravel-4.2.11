<div id="pop_message_form" class="pop_width450" style="display: none">
    <div class="pop_width450_title">
        {{Lang::get('connect.send_message')}}
    </div>
    <div class="pop_width450_content">
        <ul>
            <input name="link_url" type="hidden" value="" />
            <input name="link_text" type="hidden" value="" />
			<input name="msg_type" type="hidden" value="" />
			<input name="target_name" type="hidden" value="" />

            <li class="li430">
                <div class="div100">To:</div>
                <div class="div380" id="message_receiver_name"></div>
                <input name="message_receiver_id" type="hidden" />
            </li>
            <li class="li430">
                <div class="div100">{{Lang::get('leads.form_subject')}}</div>
                <div class="div380">
                    <input name="message_subject" class="input300" type="text" length="100" />
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <textarea name="message_content" class="textarea300" length="500"></textarea>
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <span id="left_letters">500</span>&nbsp;characters left
                </div>
            </li>
            <li class="li430">&nbsp;</li>
            <li class="li430">
                <div class="div380b">
                    <input type="button" class="btn_gray_22" value="{{Lang::get('leads.send')}}" onclick="tradove.sendMessage()" />
                </div>
            </li>
            <li class="li430">&nbsp;</li>
        </ul>
    </div>
</div>