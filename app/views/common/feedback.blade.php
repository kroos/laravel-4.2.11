@if(Session::get("uid") || Session::get("admin"))
<?php require_once(public_path()."/locale.php"); ?>
<script>
    function showFeedback(id){
        //$("#feedback_title").hide();
        $("#feedback_content").show();
        $("#feedback_content div:gt(0)").hide();
        $("#"+id).show();
        center($("#feedback_content"));
        $(".pop_width800_left ul li:gt(1)").removeClass().addClass("pop_w800_l_notselect");
        $("#"+id+"_menu").removeClass().addClass("pop_w800_l_select");
        clearSuggestion();
        //$("#suggestion_subject").val("{{Lang::get('feedback.suggestion.txt.subject')}}");
        //$("#suggestion_content").val("{{Lang::get('feedback.suggestion.txt.content')}}");
    }

    function center(obj){
        obj.css("position","fixed");
        obj.css("z-index","10000");
        obj.css("top", Math.max(0, (($(window).height() - obj.outerHeight()) / 2)) + "px");
        obj.css("left", Math.max(0, (($(window).width() - obj.outerWidth()) / 2)) + "px");
    }

    function hideFeedback(){
        //$("#feedback_title").show();
        $("#feedback_content").hide();
    }

    function sendSuggestion(){
        var subjectObj = $("#suggestion_subject");
        var contentObj = $("#suggestion_content");

        if( ! subjectObj.val() ){
            layer.tips(lang.suggestion.subject_empty, subjectObj, 3, subjectObj.width(), 0);
            return;
        }

        if( ! contentObj.val() ){
            layer.tips(lang.suggestion.content_empty, contentObj, 3, contentObj.width(), 0);
            return;
        }

        var cc = $("#feedback_content");
        $.ajax({
            type: "POST",
            url: "/post_suggestion",
            data :{
                "subject": subjectObj.val(),
                "content": contentObj.val()
            },
            beforeSend : function(){
                cc.showLoading();
            },
            success: function(response){
                cc.hideLoading();
                tradove.msg(lang.suggestion.post_success, 10);
                hideFeedback();
            },
            error:function(data){
                cc.hideLoading();
                console.error(data);
                tradove.msg(lang.suggestion.post_failed, 10);
            }
        });
    }

    function clearSuggestion(){
        $("#suggestion_subject").val('');
        $("#suggestion_content").val('');
    }
    function searchQA(){
        var keyword = $('input[name="qa_keyword"]').val();

        if(keyword){
            location.href = "/qa_list?keyword="+keyword;
        }else{
            location.href = "/qa_list";
        }
    }
</script>
<!--
<div style="cursor:pointer;position: fixed; top: 150px; left: 0px; padding: 0; margin: 0;z-index: 10000;">
    <div id="feedback_title" onclick="showFeedback('right_feedback');">
        {{HTML::image("imgs/body_feedback.gif") }}
    </div>
</div>
-->
<div id="feedback_content" class="pop_width800" style="display: none">
    <div class="pop_width800_left">
        <ul>
            <li class="pop_w800_l_title">@lang("feedback.menu.lab.title")</li>
            <li>&nbsp;</li>
            <li id="right_feedback_menu" class="pop_w800_l_select" onclick="showFeedback('right_feedback')">&bull;&nbsp;@lang("feedback.menu.lab.suggestion")</li>
            <li id="right_q_a_menu" class="pop_w800_l_notselect" onclick="showFeedback('right_q_a')">&bull;&nbsp;@lang("feedback.menu.lab.q_a")</li>
            <li id="right_contact_menu" class="pop_w800_l_notselect" onclick="showFeedback('right_contact')">&bull;&nbsp;@lang("feedback.menu.lab.contact_us")</li>
            <li class="pop_w800_l_notselect" onclick="hideFeedback()">&bull;&nbsp;@lang("feedback.menu.lab.close_window")</li>
        </ul>
    </div>
    <div id="right_feedback" class="pop_width800_right">
        <ul>
            <li class="pop_w800_r_word_bule">@lang("feedback.suggestion.lab.tips")</li>
            <li>&nbsp;</li>
            <li>
                <span>@lang("feedback.suggestion.lab.subject")</span>
                <input id="suggestion_subject" type="text" class="input400" value="" />
            </li>
            <li>&nbsp;</li>
            <li>
                <span>@lang("feedback.suggestion.lab.content")</span>
                <textarea id="suggestion_content" class="textarea400" cols="" rows=""></textarea>
            </li>
            <li>&nbsp;</li>
            <li>
                <span>&nbsp;</span>
                <input type="button" class="btn_blue_22" onclick="sendSuggestion();" value="{{Lang::get('feedback.suggestion.btn.send')}}" />&nbsp;&nbsp;
                <input type="button" class="btn_gray_22" onclick="hideFeedback();" value="{{Lang::get('feedback.suggestion.btn.cancel')}}" /></li>
            <li>&nbsp;</li>
            <!--
            <li class="pop_w800_r_word_bule">@lang("feedback.suggestion.lab.thanks")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.suggestion.lab.better")</li>
            -->
        </ul>
    </div>
    <div id="right_contact" class="pop_width800_right">
        <ul>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_name")</li>
            <li><hr class="hr100"></li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_address1")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_address2")</li>
            <li><hr class="hr100"></li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_tel")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_fax")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_email")</li>
        </ul>
    </div>
    <div id="right_help" class="pop_width800_right">
        <ul>
            <li class="pop_w800_r_word_bule">@lang("feedback.help.lab.title")</li>
            <li><hr class="hr100"></li>
            <li>&bull;&nbsp;<a href="/blog/?p=680">@lang("feedback.help.lnk.blog")</a></li>
            <li><hr class="hr100"></li>
            <li>&bull;&nbsp;<a href="/blog/?p=964">@lang("feedback.help.lnk.upload_product")</a></li>
            <li><hr class="hr100"></li>
            <li>&nbsp;</li>
        </ul>
    </div>
    <div id="right_q_a" class="pop_width800_right">
        <ul>
            <li class="pop_w800_r_word_bule_big">@lang('feedback.q_a.lab.title')</li>
            <li>
                <input type="text" name="qa_keyword" class="input400" value="" />&nbsp;
                <input type="button" class="btn_blue_22" value="{{Lang::get('feedback.q_a.btn.search')}}"  onclick="searchQA();"/>
            </li>
            <li>&nbsp;</li>
            <li><hr class="hr100"></li>
            <li class="pop_w800_r_word_bule_big">@lang('feedback.q_a.lab.popular_answer')</li>
            @include('common.popular_qa')
        </ul>
    </div>
    -->
</div>
@else
<script type="text/javascript">
    function center(obj){
        obj.css("position","fixed");
        obj.css("z-index","10000");
        obj.css("top", Math.max(0, (($(window).height() - obj.outerHeight()) / 2)) + "px");
        obj.css("left", Math.max(0, (($(window).width() - obj.outerWidth()) / 2)) + "px");
    }

    function showContact(){
        $("#feedback_content").show();
        center($("#feedback_content"));
    }

    function hideContact(){
        $("#feedback_content").hide();
    }
</script>
<div id="feedback_content" class="pop_width800" style="display: none">
    <div class="pop_width800_left">
        <ul>
            <li class="pop_w800_l_title">@lang("feedback.menu.lab.contact_us")</li>
            <li>&nbsp;</li>
            <li id="right_contact_menu" class="pop_w800_l_select" onclick="showContact();">&bull;@lang("feedback.menu.lab.contact_us")</li>
            <li class="pop_w800_l_notselect" onclick="hideContact()">&bull;@lang("feedback.menu.lab.close_window")</li>
        </ul>
    </div>
    <div id="right_contact" class="pop_width800_right">
        <ul>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_name")</li>
            <li><hr class="hr100"></li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_address1")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_address2")</li>
            <li><hr class="hr100"></li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_tel")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_fax")</li>
            <li class="pop_w800_r_word_bule">@lang("feedback.contact.lab.company_email")</li>
        </ul>
    </div>
</div>
@endif