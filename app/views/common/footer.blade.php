<div class="bottom">
    <div class="div980">
        <div class="bottom_option">
            <a href="/about_us">{{Lang::get('common.footer_lnk_about')}}</a>
            @if(Session::get('uid') ||Session::get('admin'))
            <!--
            <span>|</span>
            <a href="javascript:void(0)" onclick="if(typeof showFeedback === 'function'){showFeedback('right_feedback')}">{{Lang::get('common.footer_lnk_problem_report')}}</a>
            -->
            <span>|</span>
            <a href="javascript:void(0)" onclick="if(typeof showFeedback === 'function'){showFeedback('right_q_a')}">{{Lang::get('common.footer_lnk_help_center')}}</a>
            <span>|</span>
            <a href="#">{{Lang::get('common.footer_lnk_advertising')}}</a>


            <span>|</span>
            <a href="/people/upgrade">{{Lang::get('common.footer_lnk_upgrade_your_account')}}</a>


            @endif

            <!--
            <span>|</span>
            <a href="/out/{{MEMBER_TYPE_BUYER_ID}}/0">{{Lang::get('common.footer_lnk_browse_buyer')}}</a>
            <span>|</span>
            <a href="/out/{{MEMBER_TYPE_SELLER_ID}}/0">{{Lang::get('common.footer_lnk_browse_seller')}}</a>
            <span>|</span>
            <a href="/out/{{MEMBER_TYPE_OTHER_ID}}/0">{{Lang::get('common.footer_lnk_browse_other')}}</a>
            -->

            <span>|</span>
            <a href="/out/{{OUTSIDE_LISTING_COMPANY}}/0">{{Lang::get('common.footer_lnk_browse_company')}}</a>
            <span>|</span>
            <a href="/out/{{OUTSIDE_LISTING_PRODUCT}}/0">{{Lang::get('common.footer_lnk_browse_product_service')}}</a>
            <span>|</span>
            <a href="javascript:void(0)" onclick="if(typeof showFeedback === 'function'){showFeedback('right_contact')}else{showContact();}">{{Lang::get('common.footer_lnk_contact_us')}}</a></div>
        <div class="bottom_copyright">{{Lang::get('common.footer_lab_copyright')}}</div>
        <div class="space30"></div>
        <div class="space30"></div>
    </div>
</div>