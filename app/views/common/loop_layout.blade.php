<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{$pageTitle}}</title>
    
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    @yield('js')

    {{HTML::style('css/main.css')}}
    @yield('css')
</head>

<body>

<div class="top_shadow"></div>
<div class="top">
	@include('common.header')
    @include('common.nav')
</div>

<div class="middle">
	<!--top ads-->
	<div class="space20"></div>
	<div class="div980">@yield('ads_top')</div>
	<div class="space20"></div>
	<!--top ads over-->

	<div class="div980">
		<!--Left-->
		<div class="left_780">
			@yield('content_left')
		</div>
		<!--Left over-->

		<!--right-->
		<div class="right_190">
			@yield('content_right')			
		</div>
		<!--right over-->
	</div>
	
	<!--bottom ads-->
	<div class="space30"></div>
	<div class="div980">
		@yield('ads_bottom')
	</div>
	<div class="space30"></div>
	<!--bottom ads over-->

</div>
	
@include('common.footer')
</body>
</html>