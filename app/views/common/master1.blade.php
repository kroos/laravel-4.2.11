<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="TraDove | The premier business social network | b2b social media | Top B2B websites" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <title>{{$pageTitle}}</title>
    @include('common.tip')
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::style('css/showLoading.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/chosen.jquery.min.js')}}
    {{HTML::script('js/layer/layer.js')}}
    {{HTML::script('js/jquery.showLoading.min.js')}}
    {{HTML::script('js/common.js')}}
    {{HTML::script('js/tradove.js')}}
    {{HTML::script('js/underscore-min.js')}}

    @yield('js')
    @yield('css')
</head>

<body>
    <div class="top_shadow"></div>
    <div class="top">
        @include('common.header')
        @include('common.nav')
    </div>
    <!-- feedback start -->
    @include('common.feedback')
    <!-- feedback end -->
    <div class="middle">

        <!--top ads-->
        <div class="space20"></div>
        <div class="div980">
            @include('home.recommend.ads_top')
            @yield('ads_top')
        </div>
        <div class="space20"></div>
        <!--top ads over-->

        <div class="div980">
            <!--Center-->
            @yield('content_center')
            <!--Center over-->
        </div>

        <!--bottom ads-->
        <div class="space30"></div>
        <div class="div980">
            @include('home.recommend.ads_bottom')
            @yield('ads_bottom')
        </div>
        <div class="space30"></div>
        <!--bottom ads over-->

    </div>

    @include('common.footer')
    <script type="text/javascript" src="/js/industry-v2.0.js"></script>
</body>

@yield('last_js')
</html>