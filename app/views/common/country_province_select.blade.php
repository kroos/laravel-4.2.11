
<script type="text/javascript">
    function changeProvinceInputToSel() {
        var origal = $("#province_input");
        var tpl = $("#provinceTemplate").clone();
        $(tpl).removeAttr("id");
        $(tpl).removeAttr("style");
        $(tpl).show();

        var provinceSel = $(tpl).find("select");
        provinceSel.attr("id", "province_input");
        provinceSel.attr("name", $(origal).attr('name'));
        $(provinceSel).val($(origal).val());
        provinceSel.insertBefore(origal);
        provinceSel.val($("#us_state_val").val());

        origal.remove();
    }

    function rebackProvinceToInput(provinceOrigInput) {
        var provinceSel = $("#province_input");
        var tagName = $(provinceSel)[0].tagName;
        if(tagName!='INPUT'){
            provinceOrigInput.attr("id", "province_input");
            $(provinceOrigInput).insertBefore(provinceSel);
            provinceSel.remove();
        }

    }

    $(document).ready(function () {
        var provinceOrigInput = $("#province_input").clone();
        provinceOrigInput.removeAttr("id");
        var countrySelectVal = $("#country_sel").val();
        if(countrySelectVal=='us'){
            changeProvinceInputToSel();
        }

        $("#country_sel").change(function () {
            var selectedVal = $(this).val();
            if (selectedVal == 'us') {
                changeProvinceInputToSel();
            } else {
                rebackProvinceToInput(provinceOrigInput);
            }
        });
    });

</script>
<div id="provinceTemplate" style="display: none">
    {{Form::select('province',Lang::get('province'),null,array())}}
</div>



