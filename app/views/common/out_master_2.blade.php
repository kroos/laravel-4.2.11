<?php
$uid = Session::get('uid');
$wpId = $_SESSION['wpid'];
//$wpId = Session::get('wpid');
$blogger_home_url = get_author_posts_url($wpId);

$cuApi = new CompanyUserApi();

$company = $cuApi->getCompanyByOwnerId($uid);
$isCompanyOwner = !is_null($company);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="TraDove | The premier business social network | b2b social media | Top B2B websites" />
    <title>{{$pageTitle}}</title>
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/layer/layer.js')}}
    {{HTML::script('js/tradove.js')}}
    {{HTML::script('js/common.js')}}
    <script type="text/javascript">
        $(function(){

            var uid = '<?php echo $uid?>',
                wpId = '<?php echo $wpId?>',
                blogUrl = '<?php echo $blogger_home_url?>',
                isCompanyOwner = '<?php echo $isCompanyOwner?>';

            if(uid){

                $('.navigation_login ul li:lt(3)').hide();

                if(!wpId){
                    $('#myblog_li').hide();
                }else{
                    $('#myblog').attr('href', blogUrl);
                }

                if(!isCompanyOwner){
                    $('#mynews_li').hide();
                }

            }else{
                $('.navigation_login ul li:gt(2)').hide();
                //$('.navigation_login ul li:lt(2)').show();
            }

            $("#blog_news_login_btn").click(function(){

                var email = $('#blog_news_email').val();
                var remember = $('input[name="remember_me"]').val();
                var pwd = $('#blog_news_password').val();

                if(email.length == 0 || pwd.length == 0){
                    layer.msg('email and password must be input', 3, 3);
                    return false;
                }

                var url = '/blog_news/login/'+email;

                $.get(url,{'password':pwd, 'remember':remember}, function(data){
                    if(data.result == true){
//                        $('.navigation_login ul li:lt(2)').hide();
//                        $('#blog_news_login_btn').hide();
//                        $('.navigation_login ul li:gt(2)').show();
//                        $("#myblog").attr("href",data.blogger_home_url);

                        $('#loginSection').hide();
                        $('#logOutSection').show();
                        $('#myblog_li').show();
                    }
                },'json');

            });
        });
    </script>
    @yield('js')
    @yield('css')
</head>

<body>

<div class="top_shadow"></div>
<div class="top">
    @include('common.blog_news.header')
    @include('common.out_nav')
</div>
<!-- feedback start -->
@include('common.feedback')
<!-- feedback end -->
<div class="middle">
    @yield('content')
</div>

@include('common.footer')

@yield('footer_js')
</body>

</html>