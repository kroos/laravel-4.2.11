<script type="text/javascript">
    base_url = '{{asset('/')}}';
    login_user = {
        id:"{{Session::get('uid')}}",
        name:"{{getLoginUser()->first_name. ' '.getLoginUser()->last_name}}"
    };
    json = {
        invite:"{{Lang::get('tip.invite')}}",
        connect:"{{Lang::get('tip.connect')}}",
        connected:"{{Lang::get('tip.connected')}}",

        send : "{{Lang::get('tip.send')}}",
        sent : "{{Lang::get('tip.sent')}}",
        del : "{{Lang::get('tip.delete')}}",

        loading : "{{Lang::get('tip.loading')}}",
        timeout : "{{Lang::get('tip.timeout')}}",
        confirm : "{{Lang::get('tip.confirm')}}",

        msg : "{{Lang::get('tip.msg')}}",
        //caution : "{{Lang::get('tip.caution')}}",
        caution : "",

        success : "{{Lang::get('tip.success')}}",
        fail : "{{Lang::get('tip.fail')}}",

        subject_check : "{{Lang::get('tip.subject_check')}}",
        content_check : "{{Lang::get('tip.content_check')}}",
        industry_check : "{{Lang::get('tip.industry_check')}}",
        start_date_check : "{{Lang::get('tip.start_date_check')}}",
        end_date_check : "{{Lang::get('tip.end_date_check')}}",
        receiver_check : "{{Lang::get('tip.receiver_check')}}"
    }
</script>