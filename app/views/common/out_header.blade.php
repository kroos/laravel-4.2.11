<div class="div980">
    <div class="navigation_logo"><img src="/imgs/logo.gif"></div>
    <div class="navigation_logo_words">The Largest and Most Trusted Global Business Network</div>
    <div class="navigation_login">
        <form id="login_form" method="post" action="{{ action('UserController@login') }}">
        <div class="div_left">
            <span>{{Lang::get('login.login_lab_email')}}: <input id="login_email" type="text" name="email" value="" class="input100_login"/>&nbsp;&nbsp;</span>
            <span>{{Lang::get('login.login_lab_password')}}: <input id="login_password" type="password" name="password" value="" class="input100_login"/></span>
            <input type="hidden" name="to" value="{{$redirect_url}}" />
            <input type="hidden" name="uid" value="{{$uid}}" />
            <input type="hidden" name="ntid" value="{{$ntid}}" />
        </div>
            <!-- div class="div_right"><input type="submit" value="{{Lang::get('login.login_lab_sign_in')}}" class="btn_gray_22_login_2"></div -->

        <div class="div_right">
            <div class="input59_login" onclick="loginSubmit()">{{Lang::get('login.login_lab_sign_in')}}</div>
        </div>
        <!-- input type="button" value="{{Lang::get('login.login_lab_sign_in')}}" class="input59_login" onclick="loginSubmit()" / -->

        <div class="div_right2">
            {{Form::checkbox('remember_me', true, false)}}&nbsp;Remember me
            @if( ! Session::get("uid") )
            |<a href="javascript:void(0)" onclick="showForgetPwdDom();">&nbsp;Forgot Password?</a>
            @endif
            &nbsp;&nbsp;
            <div class="input59_login_orange"  onclick="gotoRegister()">{{Lang::get('login.login_lab_join_in')}}</div>
        </form>

        <!--
        <ul>
            <form id="login_form" method="post" action="{{ action('UserController@login') }}">
            <li id="navigation_login">
                {{Lang::get('login.login_lab_email')}}:
                <input id="login_email" type="text" name="email" value="" class="input100_login"/>
                <input type="hidden" name="to" value="{{$redirect_url}}" />
                <input type="hidden" name="uid" value="{{$uid}}" />
                <input type="hidden" name="ntid" value="{{$ntid}}" />
            </li>
            <li>{{Lang::get('login.login_lab_password')}}: <input id="login_password" type="password" name="password" value="" class="input100_login"/></li>
            <li>{{Form::checkbox('remember', 'remember me', false, array('id'=>'remember'))}}</li>
            <li>
                <input type="submit" value="{{Lang::get('login.login_lab_sign_in')}}" class="btn_gray_22_login">
            </li>
            </form>
        </ul>
        -->
    </div>
</div>

<!--pop forget password-->
<div id="forget_pwd" style="display: none" class="pop_width550">
    <div class="pop_width550_title">Forgot your password?</div>
    <div class="pop_width550_content">
        <ul>
            <li>
                <div class="div380b">
                        <span style="margin-left: 20px;">
                            Enter your registered email and you will be sent a link to reset password.
                        </span>
                </div>
            </li>
            <li>
                <div class="div380b">
                    <span>&nbsp;</span>
                </div>
            </li>
            <li class="li530">
                <div class="div140">Your Account Email:</div>
                <div class="div380">
                    <input id="email" type="text" value="" class="input300"/>
                </div>
            </li>
            <li class="li530">
                <div class="div380b">
                    <span id="result_info">&nbsp;</span>
                </div>
            </li>
            <li class="li530">
                <div class="div380b">
                    <input id="confirm_btn" type="button" class="btn_gray_22" value="Confirm" onclick="forgetPwd();"/>
                </div>
            </li>
        </ul>
    </div>

</div>
<!--pop registration over-->

<script>


    $(document).on("keydown", "#login_password", function (e) {
        var curKey = e.which;
        if(curKey == 13){
            loginSubmit();
            return false;
        }
    });

    var forgetPwdLayerIndex = 0;

    function loginSubmit() {

        var loginEmail = $("#login_email").val();
        var loginPassword = $("#login_password").val();
        if (loginEmail == '' || loginPassword == '') {
            tradove.msg('{{Lang::get('login.login_msg_required')}}', 5, 3);
            // layer.tips("{{Lang::get('login.login_msg_required')}}", "#navigation_login", 0, "400px", 0);
            return false;
        }

        $("#login_form").submit();

    }

    function gotoRegister() {
        window.location.href = "/t_reg";
    }

    function showForgetPwdDom(){
        $("#result_info").html("&nbsp;");
        forgetPwdLayerIndex = $.layer({
            type: 1,
            title: false,
            fix: false,
            border: [0],
            area: ['550px', 'auto'],
            page: {dom: '#forget_pwd'},
            offset: ['200px' , '50%'],
            zIndex:1000,
            success: function () {
            }
        });
    }

    function forgetPwd(){
        var cc= $("#confirm_btn").parent();

        $("#result_info").html("&nbsp;");
        var companyEmail = $("#email").val();
        if (companyEmail == '' || !isEmail(companyEmail)) {
            $("#result_info").html("{{Lang::get('login.login_msg_validation_email')}}");
            return;
        }

        if(companyEmail == 'admin@tradove.net'){
            $("#result_info").html("{{Lang::get('login.forget_pwd_admin')}}");
            return;
        }

        var exist = false;
        $.ajax({
            url: '/user/check_email_exist',
            type: 'POST',
            async:false,
            data: {
                "email": companyEmail
            },
            beforeSend: function () {
                cc.showLoading();
            },
            success: function (data) {
                var result = $.parseJSON(data);
                if (! result.exist) {
                    $("#result_info").html(result.msg);
                    cc.hideLoading();
                } else {
                    exist = true;
                    $("#result_info").html("&nbsp;");
                }
            },
            error: function (data) {
                alert(data);
            }
        });

        if(exist){
            //generate a password for user and email it
            $.ajax({
                url: '/user/forget_pwd',
                type: 'POST',
                data: {
                    "email": companyEmail
                },
                beforeSend: function () {
                },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.result) {
                        layer.close(forgetPwdLayerIndex);
                        tradove.msg(json.msg, 1, 10);
                        //$("#result_info").html(json.msg);
                    }
                    cc.hideLoading();
                },
                error: function (data) {
                }
            });
        }
    }
</script>