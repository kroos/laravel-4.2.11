@extends('common.master')

@section('js')
    <script type="text/javascript">
        function go_home(){
            window.location.href = "/home";
        }
    </script>
@stop

@section('content_left')
    <div class="left_650">
        <div class="left_650_column2">
            <div class="left_650_c_title_black">System Notification</div>
            <div class="left_650_c2_content">
                <div class="space30"></div>
                <div class="left_650_c2_c_detail">
                    <div class="div90"><img src="/imgs/icon/icon_404_alert.png" /></div>
                    <div class="div500"><br/>Sorry, the URL you entered is invalid!<br/>
                        <div class="title600_black">This content has been deleted or the URL has expired.</div>
                    </div>
                </div>
                <div class="space10"></div>
            </div>
            <div class="left_650_c2_content">
                <div class="left_650_c2_c_detail">
                    <ul>
                        <li><input type="button" class="btn_blue_22" value="Back to Homepage" onclick="go_home()"/></li>
                    </ul>
                </div>
            </div>
            <div class="left_top">&nbsp;</div>
        </div>
    </div>
@stop

@section('content_right')
    @include('home.recommend.people')
    @include('home.recommend.product')
    @include('home.recommend.company')
@stop

@section('ads_bottom')
@stop