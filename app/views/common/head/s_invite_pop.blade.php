{{--HTML::script('active_account/js/ajaxfileupload.js')--}}
{{--HTML::script('active_account/js/outlook_file_invite.js')--}}
{{--HTML::script('active_account/js/yahoo_contacts.js')--}}
{{--HTML::script('active_account/js/email_invite_common.js')--}}
{{--HTML::script('active_account/js/email_friend_contacts.js')--}}

{{HTML::script('js/tradove.js')}}
{{HTML::script('js/pop_win.js')}}
{{HTML::script('js/invitation.js')}}
{{HTML::script('/js/async.js')}}


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    var popWinInvite;
    var my_layer;

    $(document).ready(function () {
        var inviteUrl = '/invite/email';
        //outlookFileInvite("/contact/upload", inviteUrl);
        //yahooContactsInvite(inviteUrl);
        //emailFriendInvite(inviteUrl, "{{Lang::get('login.login_msg_validation_email')}}");


        function cleanData() {
            $("li[data-val=user-group]").each(function (index) {
                $(this).find("input[name=partner_email]").val('');
                $(this).find("input[name=partner_firstname]").val('');
            });
        }

        function verfiyGroup(fnameInput, emailInput, callback) {
            var firstName = $(fnameInput).val();
            var email = $(emailInput).val();

            //console.log(firstName + "," + email);


            $(fnameInput).parent().parent().find('li[name=error_msg_node]').remove();


            if (firstName != '' && email != '') {

                //console.log(isEmail(email));

                if (!isEmail(email)) {
                    //console.log('email error.');
                    var msg = $('#error_msg').clone();
                    $(msg).removeAttr("id");
                    $(msg).attr("name", "error_msg_node");
                    $(emailInput).parent().parent().append(msg);
                    //$(msg).insertAfter((emailInput).parent().parent());
                    $(msg).find("div.red_word").text("{{Lang::get('login.login_msg_validation_email')}}");
                    $(msg).show();
                    callback(false);
                } else {

                    //console.log('send to backend.');
                    //callback(firstName + ':' + email);

                    //server verify email

                    $.ajax({
                        url: '/user/check_email_for_home_page',
                        type: 'POST',
                        async: false,
                        data: {
                            "email": email
                        },
                        beforeSend: function () {
                        },
                        success: function (data) {
                            var result = $.parseJSON(data);
                            if (result.result) {
                                var msg = $('#error_msg').clone();
                                $(msg).removeAttr("id");
                                $(msg).attr("name", "error_msg_node");
                                $(emailInput).parent().parent().append(msg);
                                //$(msg).insertAfter((emailInput).parent().parent());
                                $(msg).find("div.red_word").text(result.msg);
                                $(msg).show();

                                layer.close(my_layer);
                                callback(false);

                            } else {
                                //console.log('sucess.');
                                callback(firstName + ':' + email);
                            }
                        },
                        error: function (data) {
                            return false;
                        }
                    });


                }

            } else {


                if (firstName == '' && email != '') {
                    var msg = $('#error_msg').clone();
                    $(msg).removeAttr("id");
                    $(msg).attr("name", "error_msg_node");
                    $(fnameInput).parent().parent().append(msg);
                    //$(msg).insertAfter((fnameInput).parent().parent());
                    $(msg).find("div.red_word").text("{{Lang::get('login.login_msg_validation_partner_first_name')}}");
                    $(msg).show();

                    callback(false);

                } else if (firstName != '' && email == '') {
                    var msg = $('#error_msg').clone();
                    $(msg).removeAttr("id");
                    $(msg).attr("name", "error_msg_node");
                    $(emailInput).parent().parent().append(msg);
                    //$(msg).insertAfter((emailInput).parent().parent());
                    $(msg).find("div.red_word").text("{{Lang::get('login.login_msg_validation_email')}}");
                    $(msg).show();

                    callback(false);

                } else {
                    callback("empty");
                }
            }
        }

        var invitePartnerPop = null;
        $(document).on("click", "#email-friend-invite-container-close", function () {
            cleanData();
            layer.close(invitePartnerPop);
        });

        $(document).on("click", "#invite_partner_cancel", function () {
            cleanData();
            layer.close(invitePartnerPop);
        });
        $(".show-email-friends-invite-href").click(function () {
            invitePartnerPop = $.layer({
                type: 1,
                closeBtn: 0,
                title: false,
                fix: false,
                border: [0],
                page: {dom: '#email-friend-invite-container'},
                offset: ['' , '30%'],
                zIndex: 1000,
                success: function () {
                }
            });
        });

        $("#invite_partner_submit").click(function () {


            my_layer = layer.load(0);

            var emailGroup = [];
            $("li[data-val=user-group]").each(function (index) {
                var email = $(this).find("input[name=partner_email]");
                var firstName = $(this).find("input[name=partner_firstname]");
                emailGroup.push({
                    email: email,
                    name: firstName
                });
            });


            async.parallel([
                        function (callback) {
                            verfiyGroup(emailGroup[0].name, emailGroup[0].email, function (result) {
                                callback(null, result);
                            });
                        },
                        function (callback) {
                            verfiyGroup(emailGroup[1].name, emailGroup[1].email, function (result) {
                                callback(null, result);
                            });
                        },
                        function (callback) {
                            verfiyGroup(emailGroup[2].name, emailGroup[2].email, function (result) {
                                callback(null, result);
                            });
                        }, function (callback) {
                            verfiyGroup(emailGroup[3].name, emailGroup[3].email, function (result) {
                                callback(null, result);
                            });
                        }, function (callback) {
                            verfiyGroup(emailGroup[4].name, emailGroup[4].email, function (result) {
                                callback(null, result);
                            });
                        }
                    ],
                    function (err, results) {

                        var isBreak = false;
                        var emails = [];
                        for (var i = 0; i < results.length; i++) {
                            if (results[i] != false && results[i] != 'empty') {
                                emails.push(results[i]);
                            } else if (results[i] == false) {
                                isBreak = true;
                                break;
                            }
                        }

                        if (!isBreak && emails.length > 0) {
                            $.ajax({
                                url: '/home/login_invite_partner',
                                type: 'POST',
                                async: false,
                                data: {
                                    "emails": emails.join(',')
                                },
                                beforeSend: function () {
                                },
                                success: function (data) {
                                    layer.close(my_layer);
                                    tradove.msg("{{Lang::get('login.login_msg_validation_partner_invitation_success')}}", 1, 5);
                                },
                                error: function (data) {
                                    return false;
                                }
                            });
                        }

                        layer.close(my_layer);
                    });
        });


    });



</script>

<input id="current_full_name" value="{{$curr_user->first_name}} {{$curr_user->last_name}}" type="hidden"/>
<div id="yahoo-contacts-invite-list" class="assign_admin" style="display: none">
    <div class="pop_title">
        <div class="title">&nbsp;&nbsp;Yahoo Contacts Invite</div>
        <div class="close">
            <input id="yahoo-contacts-close" class="btn_close16" name="" type="button"/>
        </div>
    </div>
    <div class="pop_reply">
        <div class="pop_message">
            <ul>
                <li class="words">We've found <label id="yahoo-contacts-list-count"></label> people in your contact
                    list.
                    Select the contact you'd like to invite.
                </li>
                <li class="words">
                    @if(Session::get("network_id")!=0)
                        {{Lang::get('login.login_msg_rejected_email_private_network')}}
                    @else
                        {{Lang::get('login.login_msg_general_email_invite_tips')}}
                    @endif
                </li>

                <li class="words">&nbsp;</li>
                <li class="words">
                    <div class="words_option">
                        <a id="yahoo-contacts-list-select-all" href="javascript:void(0);">Select all</a>&nbsp;|&nbsp;
                        <a id="yahoo-contacts-list-select-none" href="javascript:void(0);">Select none</a></div>
                    <div class="words_select_tab">
                        <ul id="yahoo-contacts-list-list">
                        </ul>
                    </div>
                </li>
                <li class="words">&nbsp;</li>
                <li class="words"><input id="yahoo-contacts-list-invite-btn" class="btn_ashy160" type="button"
                                         value="Send 0 invitations"/>&nbsp;&nbsp;
                    <a id="yahoo-contacts-list-invite-cancel" href="javascript:void(0);">Cancel</a></li>
                <li class="btn"></li>
            </ul>
        </div>
    </div>
</div>


<div id="email-friend-invite-container" class="pop_width550" style="display: none;">
    <div class="pop_width550_title">Invite your colleagues and friends to join by email<span><a
                    id="email-friend-invite-container-close" href="javascript:void(0);"><img
                        src="/imgs/icon/icon_delete_12px.png" border="0"/></a></span></div>

    <div class="pop_width550_content">
        <ul>
            <li data-val="user-group" class="li530">
                <div class="div100">Company Email:</div>
                <div class="divinput"><input name="partner_email" class="input120" type="text"/>&nbsp;</div>
                <div class="div100">First Name:</div>
                <div class="divinput"><input name="partner_firstname" class="input120" type="text"/>&nbsp;</div>
            </li>
            <li class="li530">
                <hr class="hr100">
            </li>
            <li data-val="user-group" class="li530">
                <div class="div100">Company Email:</div>
                <div class="divinput"><input name="partner_email" class="input120" type="text"/>&nbsp;</div>
                <div class="div100">First Name:</div>
                <div class="divinput"><input name="partner_firstname" class="input120" type="text"/>&nbsp;</div>
            </li>
            <li class="li530">
                <hr class="hr100">
            </li>
            <li data-val="user-group" class="li530">
                <div class="div100">Company Email:</div>
                <div class="divinput"><input name="partner_email" class="input120" type="text"/>&nbsp;</div>
                <div class="div100">First Name:</div>
                <div class="divinput"><input name="partner_firstname" class="input120" type="text"/>&nbsp;</div>
            </li>
            <li class="li530">
                <hr class="hr100">
            </li>
            <li data-val="user-group" class="li530">
                <div class="div100">Company Email:</div>
                <div class="divinput"><input name="partner_email" class="input120" type="text"/>&nbsp;</div>
                <div class="div100">First Name:</div>
                <div class="divinput"><input name="partner_firstname" class="input120" type="text"/>&nbsp;</div>
            </li>
            <li class="li530">
                <hr class="hr100">
            </li>
            <li data-val="user-group" class="li530">
                <div class="div100">Company Email:</div>
                <div class="divinput"><input name="partner_email" class="input120" type="text"/>&nbsp;</div>
                <div class="div100">First Name:</div>
                <div class="divinput"><input name="partner_firstname" class="input120" type="text"/>&nbsp;</div>
            </li>
            <li class="li530">
                <hr class="hr100">
            </li>
            <li class="li530">&nbsp;</li>
            <li class="li530"><input id="invite_partner_submit" type="button" class="btn_blue_22" value="Submit"/>&nbsp;&nbsp;
                <input id="invite_partner_cancel" type="button" class="btn_gray_22" value="Cancel"/></li>
            </li>
            <li class="li530">&nbsp;</li>
        </ul>
    </div>
</div>

<!--pop invite-->


<div id="invite_registed_pop_top" class="pop_width300 hide">
    <div class="pop_width300_content">
        <div class="div280"><span>This email has been registered</span>, you can send a invitation message to the
            account.
        </div>
        <div class="div280">&nbsp;</div>
        <div class="div280center">
            <input id="invite_registed_pop_confirm_top" type="button" class="btn_gray_22" value="Confirm"/>&nbsp;&nbsp;
            <input name="invite_registed_pop_cancel_top" type="button" class="btn_gray_22" value="Cancel"/></div>
        <div class="div280">&nbsp;</div>
    </div>
</div>
<!--pop invite over-->

<div id="invite_not_registed_pop_top" class="pop_width300  hide">
    <div class="pop_width300_content">
        <div class="div280"><span>This email hasn't been registered</span>, you can send a invitation email to this
            email.
        </div>
        <div class="div280">&nbsp;</div>
        <div class="div280center">
            <input id="invite_not_registed_pop_confirm_top" type="button" value="Confirm" class="btn_gray_22">&nbsp;&nbsp;
            <input name="invite_registed_pop_cancel_top" type="button" value="Cancel" class="btn_gray_22"></div>
        <div class="div280">&nbsp;</div>
    </div>
</div>


<div id="loop-invite-container" class="pop_width450" style="display: none;">
    <div class="pop_width450_title">Send Invitation<span><a href="javascript:void(0)" onclick="closePopWin()"><img
                        src="/imgs/icon/icon_delete_12px.png" border="0"/></a></span></div>
    <div class="pop_width450_content">
        <ul>
            <li class="li430">
                <div class="div100">To:</div>
                <div class="div380"><input id="s_loop_user" name="" class="input300" type="text"/>&nbsp;</div>
            </li>
            <div id="loop_type_d" style="display: none">
                <li class="li430">
                    <div class="div100">Loop Type:</div>
                    <div class="div380">
                        <input name="loop_type_s" type="radio" value="{{LOOP_R_OTHER}}" checked="checked"/>
                        &nbsp;Other Relationships
                    </div>
                </li>
                <li class="li430" id="loop_partner_s">
                    <div class="div100">&nbsp;</div>
                    <div class="div380">
                        <input name="loop_type_s" type="radio" value="{{LOOP_R_PARTNERS}}"/>
                        &nbsp;Partner（Investment，collaboration，etc）
                    </div>
                </li>
                <li class="li430" id="loop_buying_selling_s">
                    <div class="div100">&nbsp;</div>
                    <div class="div380">
                        <input name="loop_type_s" type="radio" value="{{LOOP_R_BUYING_SELLING}}"/>
                        &nbsp;Buying/Selling
                    </div>
                </li>
                <li class="li430" id="loop_colleague_s">
                    <div class="div100">&nbsp;</div>
                    <div class="div380">
                        <input name="loop_type_s" type="radio" value="{{LOOP_R_COLLEAGUE}}"/>
                        &nbsp;Colleague
                    </div>
                </li>
            </div>
            <li class="li430">
                <div class="div100">Subject:</div>
                <div class="div380">
                    <input name="s_loop_subject" class="input300" type="text"/>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <textarea name="s_loop_content" class="textarea300" length="500" cols="" rows=""></textarea>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b"><span id="left_letters_s_loop_content">500</span> characters left</div>
            </li>
            <li class="li430">&nbsp;</li>
            <li class="li430">
                <div class="div380b">
                    <input id="s_btn_loop" type="button" class="btn_blue_22" value="Send"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <!-- input type="button" class="btn_gray_22" value="cancel" / -->
                </div>
            </li>
            <li class="li430">&nbsp;</li>
        </ul>
    </div>
</div>


<div id="company-connect-container" class="pop_width450" style="display: none;">
    <div class="pop_width450_title">Send Invitation<span><a href="javascript:void(0)" onclick="closePopWin()"><img
                        src="/imgs/icon/icon_delete_12px.png" border="0"/></a></span></div>
    <div class="pop_width450_content">
        <ul>
            <li class="li430">
                <div class="div100">To:</div>
                <div class="div380"><input id="s_company_name" name="" class="input300" type="text"/>&nbsp;</div>
            </li>
            <li class="li430">
                <div class="div100">Owner:</div>
                <div id="s_company_owner" class="div380"></div>
            </li>
            <li class="li430">
                <div class="div100">Subject:</div>
                <div class="div380">
                    <input name="s_company_subject" class="input300" type="text"/>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <textarea name="s_company_content" class="textarea300" length="500" cols="" rows=""></textarea>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b"><span id="left_letters_s_company_content">500</span> characters left</div>
            </li>
            <li class="li430">&nbsp;</li>
            <li class="li430">
                <div class="div380b">
                    <input id="s_btn_company" type="button" class="btn_blue_22" value="Send"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <!-- input type="button" class="btn_gray_22" value="cancel" / -->
                </div>
            </li>
            <li class="li430">&nbsp;</li>
        </ul>
    </div>
</div>

<div id="product-connect-container" class="pop_width450" style="display: none;">
    <div class="pop_width450_title">Send Invitation<span><a href="javascript:void(0)" onclick="closePopWin()"><img
                        src="/imgs/icon/icon_delete_12px.png" border="0"/></a></span></div>
    <div class="pop_width450_content">
        <ul>
            <li class="li430">
                <div class="div100">To:</div>
                <div class="div380"><input id="s_product_name" name="" class="input300" type="text"/>&nbsp;</div>
            </li>
            <li class="li430">
                <div class="div100">Owner:</div>
                <div id="s_product_owner" class="div380"></div>
            </li>
            <li class="li430">
                <div class="div100">Subject:</div>
                <div class="div380">
                    <input name="s_product_subject" class="input300" type="text"/>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b">
                    <textarea name="s_product_content" class="textarea300" length="500" cols="" rows=""></textarea>&nbsp;
                </div>
            </li>
            <li class="li430">
                <div class="div380b"><span id="left_letters_s_product_content">500</span> characters left</div>
            </li>
            <li class="li430">&nbsp;</li>
            <li class="li430">
                <div class="div380b">
                    <input id="s_btn_product" type="button" class="btn_blue_22" value="Send"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <!-- input type="button" class="btn_gray_22" value="cancel" / -->
                </div>
            </li>
            <li class="li430">&nbsp;</li>
        </ul>
    </div>
</div>


<div id="cloudspange_invite_success_container" class="pop_width300" style="display: none;">
    <div class="pop_width300_content">
        <div class="div280"><span>Invitation sent successfully.</span></div>
        <div class="div280">&nbsp;</div>
        <div class="div280center">
            <input id="cloudspange_invite_confirm_btn" type="button" class="btn_gray_22" value="Confirm" />
        </div>
        <div class="div280">&nbsp;</div>
    </div>
</div>


<!--pop invite over-->
@include('invitation.linkedin.linedin_invite_pop')



<input id="s_receiver_id" name="receiver" type="hidden"/>
<input id="s_receiver_name" type="hidden"/>
<input id="s_target_id" type="hidden"/>

<li id="error_msg" style="display: none">
    <div class="div90">&nbsp;</div>
    <div class="div500">
        <div class="red_word"></div>
    </div>
</li>
