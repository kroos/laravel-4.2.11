<div class="navi_s_pop_300">
    <div class="navi_s_p_300_title1">
        <span>@lang('common.s_invite_lab_contacts')</span>
        </br><div class="div_word_ashy">@lang('common.s_invite_lab_connections')</div>
    </div>
    <div class="navi_s_p_300_content">
        <!--
        <div class="navi_s_p_3_c_280p">
            <div class="photo"><a href="/invite/linkedin/connections"><img src="/imgs/invite_icon/linkedin.gif" border="0" /></a></div>
            <div class="detail">
                <ul>
                    <li class="words">Invite your LinkedIn contacts to join your TraDove Business Loop</li>
                </ul>
            </div>
        </div>
        -->

        <div class="navi_s_p_3_c_280p">
            <div class="photo"><a class="linkedin_upload_file_node" href="javascript:void(0);"><img src="/imgs/invite_icon/linkedintotradove.gif"  border="0"/></a></div>
            <div class="detail">
                <ul>
                    <li class="words"><a class="linkedin_upload_file_node" href="javascript:void(0);">Upload your LinkedIn Connections File</a></li>
                </ul>
            </div>
        </div>
        <div class="navi_s_p_3_c_280p">
            <div class="photo"><a class="show-email-friends-invite-href" href="javascript:void(0);"><img src="/imgs/invite_icon/shortcut_emailinvite.gif"  border="0" /></a></div>
            <div class="detail">
                <ul>
                    <li class="words"><a class="show-email-friends-invite-href" href="javascript:void(0);">Send Loop invitations by email</a></li>
                </ul>
            </div>
        </div>
        <div class="navi_s_p_3_c_280p">
            <div class="photo"><a class="cs_import" href="javascript:void(0);"><img src="/imgs/invite_icon/invitecontacts.png"  border="0" /></a></div>
            <div class="detail">
                <ul>
                    <li class="words"><a class="cs_import" href="javascript:void(0);">Import contacts from an Address Book</a></li>
                </ul>
            </div>
        </div>

        <div class="space10"></div>
    </div>

</div>
