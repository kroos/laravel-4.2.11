<div class="navi_s_pop_300">
    <!--Column-->
    <div class="navi_s_p_300_title" url="{{asset('message/inbox/all')}}">
        {{Lang::get('common.s_message_lab_message', array('msgNum' => $msgCount))}}
        </br><div class="div_word_ashy">Send one-to-one messages to your Loop contacts</div>
        <span>
            {{HTML::image("imgs/navi_s_p_300_title_arrow.gif")}}
        </span>
    </div>

    <div class="navi_s_p_300_content">
        @if(!empty($unreadMsgs))
        @foreach($unreadMsgs as $msg)
        <div class="navi_s_p_3_c_280p">
            <div class="photo">
                <a href="{{asset('people/show/'.$msg->senderid)}}">
                    {{HTML::image(FileManager::getFile('user_logo', $msg->photo))}}
                </a>
            </div>
            <div class="detail">
                <ul>
                    <li class="title"><a href="/message/detail/inbox/{{$msg->id}}">{{$msg->title}}</a></li>
                    <!-- li class="words">{{$msg->content}}</li -->
                    <li class="words">
                        {{Lang::get('common.s_message_lab_by')}}
                        <a href="/people/show/{{$msg->senderid}}">{{$msg->first_name . ' ' . $msg->last_name}}</a>
                    </li>
                    <li class="words">
                        {{getDateByTimestamp($msg->publish_time)}}
                    </li>
                </ul>
            </div>
        </div>
        @endforeach
        @endif
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="{{asset('invitation/inbox')}}">
        {{Lang::get('common.s_message_lab_invitation', array('invitationNum' => $invitationCount))}}
        </br><div class="div_word_ashy">Send your colleagues and friends a Loop Invitation through TraDove or email</div>
        <span>
            {{HTML::image("imgs/navi_s_p_300_title_arrow.gif")}}
        </span>
    </div>

    <div class="navi_s_p_300_content">

        <!-- for -->
        @if(!empty($unreadInvitations))
        @foreach($unreadInvitations as $invitation)
        <div class="navi_s_p_3_c_280p">
            <div class="photo">
                <a href="{{asset('people/show/'.$invitation->sender_id)}}">
                {{HTML::image(FileManager::getFile('user_logo', $invitation->photo))}}
                </a>
            </div>
            <div class="detail">
                <ul>
                    <li class="title">
                        <a href="/invitation/detail/inbox/{{$invitation->id}}">{{$invitation->title}}</a>
                    </li>

                    <li class="words">
                        {{Lang::get('common.s_message_lab_by')}}
                        <a href="/people/show/{{$invitation->sender_id}}">{{$invitation->first_name . ' ' . $invitation->last_name}}</a>
                        <!-- Position - Company - Location... -->
                    </li>
                    <li class="words">{{getDateByTimestamp($invitation->publish_time)}}</li>
                </ul>
            </div>
        </div>
        @endforeach
        @endif
        <!-- endfor -->

    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="{{asset('proposal/inbox')}}">
        @lang('common.s_message_lab_proposal', array('proposalNum' => $propositionCount))
        </br><div class="div_word_ashy">Send and receive buying/selling proposals with your Loop contacts</div>
        <span>
            {{HTML::image("imgs/navi_s_p_300_title_arrow.gif")}}
        </span>
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="{{asset('discussion/inbox')}}">
        @lang('common.s_message_lab_private_discussion', array('discussionNum' => $pDiscussionCount))
        </br><div class="div_word_ashy">Send and receive messages with multiple Loop contacts</div>
        <span>
            {{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}
        </span>
    </div>

    <!--
    <div class="navi_s_p_300_content">

        <div class="navi_s_p_3_c_280p">
            <div class="photo">{{ HTML::image("imgs/photo_people.gif") }}</div>
            <div class="detail">
                <ul>
                    <li class="title"><a href="#">Discussion Title</a></li>
                    <li class="words">content content content content content content...</li>
                    <li class="words">By <a href="#">Loop Name</a>2013-09-23</li>
                </ul>
            </div>
        </div>

    </div>
-->
    <!--Column over-->

</div>