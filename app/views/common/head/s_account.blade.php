<div class="navi_s_pop_300">
    <!--Column-->
    <div class="navi_s_p_300_title2">
        {{$curr_user['first_name'] . ' ' . $curr_user['last_name']}}
        <a href="{{asset('logout')}}">{{Lang::get('common.s_account_lnk_sign_out')}}</a>
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title2">
        {{Lang::get('common.s_account_lab_account_type')}}
        <span>{{AccountType::getAccountTypeNameByID($curr_user['member_type'])}}
        </span>
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title2">
        {{Lang::get('common.s_account_lab_vip_level')}}
        <span>{{VIPLevel::getVIPNameByID($curr_user['vip_level'])}}</span>

        <!--
        @if($curr_user['vip_level'] < VIPLevel::ID_UNLIMITED)
        <a href="/people/upgrade">{{Lang::get('common.s_account_lnk_upgrade')}}</a>
        @endif
        -->
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title2">
        {{Lang::get('common.s_account_lab_email')}}
        <span>{{$curr_user['email']}}</span>

        <a href="/changeEmail">{{Lang::get('common.s_account_lnk_edit')}}</a>
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title2">
        {{Lang::get('common.s_account_lab_tel')}}
        <span>{{UsersApi::formatTelephone($curr_user['telephone'])}}</span>

        <a href="/changeMobile">{{Lang::get('common.s_account_lnk_edit')}}</a>
    </div>
    <!--Column over-->


    <!--Column-->
    {{--
    <div class="navi_s_p_300_title" url="{{asset('changeEmail')}}">
        {{Lang::get('common.s_account_lnk_change_account_email')}}
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    --}}
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="{{asset('changePassword')}}">
        {{Lang::get('common.s_account_lnk_change_password')}}
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="/people/email/setting">
        {{Lang::get('common.s_account_lnk_setting')}}
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    <!--Column-->
    @if(Session::get('member_type') == MEMBER_TYPE_SELLER_ID)
    <div class="navi_s_p_300_title" url="/payment/setting">
        {{Lang::get('common.s_account_lnk_payment_setting')}}
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    @endif
    <!--Column over-->
</div>
