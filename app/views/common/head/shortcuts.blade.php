<script type="text/javascript">

    $(document).ready(function() {
        // 鼠标移动到图标
        $('.navi_short_unit').mouseenter(function() {
            icon_id = $(this).attr("id");
            div_id = 'pop_' + icon_id;

            $('#' + div_id).attr("class", "navi_short_pop");

            icon_img = $(this).find('img').attr('src');
            icon_img = icon_img.replace('_a.gif', '_b.gif');
            $(this).find('img').attr('src', icon_img);

        });

        // 鼠标从图标移走
        $('.navi_short_unit').mouseleave(function() {

            icon_id = $(this).attr("id");
            div_id = 'pop_' + icon_id;

            $('#' + div_id).attr("class", "hide");

            icon_img = $(this).find('img').attr('src');
            icon_img = icon_img.replace('_b.gif', '_a.gif');
            $(this).find('img').attr('src', icon_img);

        });

        $('[id^="pop_"]').mouseenter(function() {
            $(this).attr("class", "navi_short_pop");
        });

        $('[id^="pop_"]').mouseleave(function() {
            $(this).attr("class", "hide");
        });
    });

</script>

<div class="navigation_shortcuts">
    <div id="message" class="navi_short_unit">{{ HTML::image( "imgs/shortcuts_message_a.gif" ) }}</div>
    <div id="invite" class="navi_short_unit">{{ HTML::image( "imgs/shortcuts_invite_a.gif" ) }}</div>
    <div id="network" class="navi_short_unit">{{ HTML::image( "imgs/shortcuts_network_a.gif" ) }}</div>
    <div id="account" class="navi_short_unit">{{ HTML::image( "imgs/shortcuts_account_a.gif" ) }}</div>

    @if(!empty($totalUnreadCount))
    <div class="navigation_message_notice">
        <div class="navigation_message_n_c">{{$totalUnreadCount}}</div>
    </div>
    @endif
</div>

<!--message-->
<!-- div id="pop_message" class="navi_short_pop" -->
<div id="pop_message" class="hide">
    @include('common.head.s_message')
</div>

<!--invite-->
<!-- div id="pop_invite" class="navi_short_pop" -->
<div id="pop_invite" class="hide">
    @include('common.head.s_invite')
</div>

<!--network-->
<!-- div id="pop_network" class="navi_short_pop" -->
<div id="pop_network" class="hide">
    @include('common.head.s_network')
</div>

<!--account-->
<!-- div id="pop_account" class="navi_short_pop" -->
<div id="pop_account" class="hide">
    @include('common.head.s_account')
</div>


@include('common.head.s_invite_pop')

