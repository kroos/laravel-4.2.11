<div class="navigation_search">
    <div id="down_arrow" class="navi_search_select">
        @if(!empty($searchType))
            @if($searchType == 'buyer')
            {{ HTML::image( "imgs/navigation_search_icon1.png", null, array("id" => "sel_icon","to"=> action('SearchController@buyer') )) }}
            @endif
            @if($searchType == 'seller')
            {{ HTML::image( "imgs/navigation_search_icon2.png", null, array("id" => "sel_icon","to"=> action('SearchController@seller') )) }}
            @endif
            @if($searchType == 'other')
            {{ HTML::image( "imgs/navigation_search_icon7.png", null, array("id" => "sel_icon","to"=> action('SearchController@other') )) }}
            @endif
            @if($searchType == 'product')
            {{ HTML::image( "imgs/navigation_search_icon3.png", null, array("id" => "sel_icon","to"=> action('SearchController@product') )) }}
            @endif
            @if($searchType == 'company')
            {{ HTML::image( "imgs/navigation_search_icon4.png", null, array("id" => "sel_icon","to"=> action('SearchController@company') )) }}
            @endif
            @if($searchType == 'group')
            {{ HTML::image( "imgs/navigation_search_icon5.png", null, array("id" => "sel_icon","to"=> action('SearchController@group') )) }}
            @endif
            @if($searchType == 'web')
            {{ HTML::image( "imgs/navigation_search_icon6.png", null, array("id" => "sel_icon","to"=> action('SearchController@web') )) }}
            @endif
        @else
            {{ HTML::image( "imgs/navigation_search_icon1.png", null, array("id" => "sel_icon","to"=> action('SearchController@buyer') )) }}
        @endif
    </div>

    <div id="search_type" class="hide">
        <div class="navi_s_s_down_bg_top">{{ HTML::image("imgs/navi_s_down_bg_top.png") }}</div>

        <div class="navi_s_s_down_border" style="z-index: 999">
            <div class="navi_s_s_down_unit" to="{{asset('search_buyer')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon1.png" ) }}</div><div><span>Buyer</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_seller')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon2.png" ) }}</div><div><span>Seller</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_other')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon7.png" ) }}</div><div><span>Other</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_product')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon3.png" ) }}</div><div><span>Products/Services</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_company')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon4.png" ) }}</div><div><span>Company</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_group')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon5.png" ) }}</div><div><span>Group</span></div></div>
            <div class="navi_s_s_down_unit" to="{{asset('search_web')}}"><div class="divicon">{{ HTML::image( "imgs/navigation_search_icon6.png" ) }}</div><div><span>Company Web</span></div></div>
        </div>

        <div class="navi_s_s_down_bg_bottom">{{ HTML::image( "imgs/navi_s_down_bg_bottom.png" ) }}</div>

    </div>

    <div class="navi_search_input">
        <input class="navi_search_input_textarea" type="text" value="{{$main_search}}" />
    </div>
    <div class="navi_search_btn" id="search">{{ HTML::image( "imgs/navi_search_btn.gif",null,array("border"=>"0") ) }}</div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        // 弹出下拉，再点击一次收回
//        $('#down_arrow').click(function() {
//            if($('#search_type').attr("class") == "hide") {
//                $('#search_type').attr("class", "navi_search_s_down");
//            } else {
//                $('#search_type').attr("class", "hide");
//            }
//        });
//
//        $('#search_type').mouseout(function() {
//            // $('#search_type').attr("class", "hide");
//        });

        $('#down_arrow').hover(
            function(){
                $('#search_type').attr("class", "navi_search_s_down");
            },
            function(){
                $('#search_type').attr("class", "hide");
            }
        );

        $('#search_type').hover(
            function(){
                $('#search_type').attr("class", "navi_search_s_down");
            },
            function(){
                $('#search_type').attr("class", "hide");
            }
        );
        // 选择搜索类别后收回下拉
        $(".navi_s_s_down_unit").click(function(){
            $('#sel_icon').attr('src', $(this).find('img').attr('src'));
            $('#sel_icon').attr('to', $(this).attr('to'));
            $('#search_type').attr("class", "hide");
        });

        $("#search").click(function(){
            s = $(".navi_search_input_textarea").val();
            window.location.href = $("#sel_icon").attr('to') + "?search=" + encodeURIComponent(s);
        });

        $(".navi_search_input_textarea").keydown(function(e){
            if(e.keyCode == 13){
                s = $(".navi_search_input_textarea").val();
                window.location.href = $("#sel_icon").attr('to') + "?search=" + encodeURIComponent(s);
            }
        });
    });

</script>