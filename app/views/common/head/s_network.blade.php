<div class="navi_s_pop_300">
    <!--Column-->
    <div class="navi_s_p_300_title1">{{Lang::get('common.s_network_lab_current_network')}}: <span>{{$currentNetwork}}</span></div>
    <!--Column over-->

    <!--Column-->
    <?php
        $nu = new NetworkUserApi();
        $is = $nu->isJoinedOtherNetworkExceptGeneral(Session::get('uid'));
        if(empty($is)){
    ?>
        <div class="navi_s_p_300_title" url="/network/goto/0">
            {{Lang::get('common.s_message_lnk_enter_general_network')}}
            </br><div class="div_word_ashy">Switch to our General B2B Social Network</div>
            <span>
                {{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}
            </span>
        </div>
    <?php } ?>

    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="/network/joined">
        {{Lang::get('common.s_message_lab_select_your_network')}}
        </br><div class="div_word_ashy">Switch to a private network (related to an industry, company buying, company selling, etc.) you have joined</div>
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    <div class="navi_s_p_300_content">

        <!-- for -->
        {{--
        <div class="navi_s_p_3_c_280p">
            <div class="photo">{{ HTML::image("imgs/photo_network.gif") }}</div>
            <div class="detail">
                <ul>
                    <li class="title"><a href="#">Network Name</a></li>
                    <li class="words">Introduction...</li>
                </ul>
            </div>
        </div>
        --}}
        <!-- endfor -->

        @if(!empty($joinedNetworks))
        @foreach($joinedNetworks as $network)
        <div class="navi_s_p_3_c_280p">
            <div class="photo">{{HTML::image(FileManager::getFile('network_logo', $network->photo))}}</div>
            <div class="detail">
                <ul>
                    <li class="title"><a href="/network/goto/{{$network->id}}">{{$network->name}}</a></li>
                    <li class="words">{{mb_strimwidth(nl2br($network->network_desc),0,90,"...")}}</li>
                </ul>
            </div>
        </div>
        @endforeach
        @endif

    </div>
    <!--Column over-->

    <!--Column-->
    <div class="navi_s_p_300_title" url="/network/suggest">
        {{Lang::get('common.s_message_lnk_join_network')}}
        </br><div class="div_word_ashy">Join a private network (related to an industry, company buying, company selling, etc.) you are interested in</div>
        <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
    </div>
    <!--Column over-->

    <!--Column-->
    <?php
        if(Session::get('network_id') == 0){
            $cuApi = new CompanyUserApi();
            $user_company = $cuApi -> getCompany(Session::get('uid'));
            if(!empty($user_company)){
                $user = getLoginUser();
                $network_type = 0;
                if($user->member_type == MEMBER_TYPE_BUYER_ID){
                    $network_type = NETWORK_TYPE_BUY_SIDE;
                }
                if($user->member_type == MEMBER_TYPE_SELLER_ID){
                    $network_type = NETWORK_TYPE_SELL_SIDE;
                }
                if($network_type){
                    $networkApi = new NetworkApi;
                    $network = $networkApi->getCompanyNetworkOfType($user_company->id, $network_type);
                    if(empty($network)){
    ?>
        <div class="navi_s_p_300_title" url="/network/show_create">
            {{Lang::get('common.s_message_lnk_create_network')}}
            </br><div class="div_word_ashy">Create a private network (industry, company buyside-supply chain, company sellside, associations, tradeshows)</div>
            <span>{{ HTML::image("imgs/navi_s_p_300_title_arrow.gif") }}</span>
        </div>
    <?php
                    }
                }
            }
        }
    ?>
</div>