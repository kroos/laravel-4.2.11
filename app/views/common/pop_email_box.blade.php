<div id="emailBox" class="pop_width400" style="display: none; position: fixed; opacity: 1; z-index: 11000; left: 50%; margin-left: -202px; top: 200px;">

    <input name="url_to" type="hidden" value="" />
    <input name="objType" type="hidden" value="{{$objType}}" />
    <input name="objId" type="hidden" value="{{$objId}}" />
    <input id="assign_id" name="assign_id" type="hidden" value="" />

    <div class="pop_width400_title">{{Lang::get('common.pop_email_box_lab_title', array('typeName' => $typeName))}}
        <span>
            <a id="closeBox" href="#">
                <img src="/imgs/icon/icon_delete_12px.png" border="0" />
            </a>
        </span>
        </div>
        <div class="pop_width400_content">
        <ul>

            <!--
            <li class="li380"><span>Original product owner:</span></li>
            <li class="li380">User Name</li>
            <li class="li380">&nbsp;</li>
            -->

            <li class="li380"><span>{{Lang::get('common.pop_email_box_lab_person')}}</span></li>
            <li class="li380"><input id="assign_name" name="assign_name" class="input300" type="text" /></li>
            <!-- li class="li380">{{Lang::get('common.pop_email_box_lab_note')}}</li -->
            <li class="li380">&nbsp;</li>
            <li class="li380btn">
                <input id="submitEmail" type="button" class="btn_blue_22" value="{{Lang::get('common.btn_submit')}}" />&nbsp;&nbsp;
                <input id="cancelEmail" type="button" class="btn_gray_22" value="{{Lang::get('common.btn_cancel')}}" /></li>
            <li class="li380">&nbsp;</li>
        </ul>
    </div>

	{{HTML::script('js/tradove.js')}}
    <script type="text/javascript">
        $('#submitEmail').click(function(){

            var redirectUrl = $("input[name='url_to']").attr('value');

            var objType = $("input[name='objType']").attr('value');
            var objId = $("input[name='objId']").val();
            var newOwnerEmail = $("[name='newOwnerEmail']").val();
            var assignId = $("input[name='assign_id']").val();

            var ids;

            if(objId) {
                ids = [objId];
            } else {
                ids = getSelectedIds();
            }

            if(ids == null  || ids.length == 0) {
                tradove.msg("Please choose an object first");
                return false;
            }

            if( !assignId || !objType || !ids ) {
                tradove.msg("Please choose an object first");
                return false;
            }

            $.ajax({

                url:"/assign",
                dataType: 'json',
                type: 'POST',

                data: {
                    "objType": objType,
                    "objIds[]": ids,
                    "assignId": assignId,
                    "newOwnerEmail": newOwnerEmail
                },

                success: function (data) {
                    if(data.result == 'success') {
                        tradove.msg("Assign ownership message sent successfully.");
                        closeEmailBox();
                    } else {
                        tradove.msg("Assign ownership message sent failed!" + data.error_msg);
                        closeEmailBox();
                    }

                    if(redirectUrl) {
                        location.href = redirectUrl;
                    }

                },

                error: function (data) {
                    tradove.msg("Assign ownership message sent failed!" + data.error);
                    closeEmailBox();
                }

            });

        });

        $('#cancelEmail').click(function(){
            closeEmailBox();
        });

        $('#closeBox').click(function(){
            closeEmailBox();
        });

        function closeEmailBox() {
            $("#lean_overlay").fadeOut(200);
            $('#emailBox').css('display', 'none');
        }

    </script>
</div>
