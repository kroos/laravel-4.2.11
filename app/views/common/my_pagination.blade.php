<?php
    //add query
    $arr = getUrlQueryString();
    unset($arr["page"]);
    foreach($arr as $key => $val){
        $paginator->addQuery($key, urldecode($val));
    }

    $countPerPage = $paginator->getPerPage();
    $currentPage = $paginator->getCurrentPage() - 1;
    $lastPage = $paginator->getLastPage();
    $pages = 10;
?>
@if($paginator->getTotal() > $countPerPage)
<div class="left_650_c2_a_r_page">
    <div>
        @if($currentPage > 0)
            <input type="button" class="btn_gray_22" value="{{Lang::get('common.pagination.btn_previous')}}"  onclick="location.href='{{$paginator->getUrl($currentPage)}}'"/>
        @endif
    </div>
    <div>
        @if($lastPage - ($currentPage - $currentPage % $pages) < $pages )
            @for($page = $currentPage - $currentPage % $pages + 1; $page <= $lastPage; $page++)
                @if($page == $currentPage+1)
                    <a href="{{$paginator->getUrl($page)}}" class="current">{{$page}}</a>
                @else
                    <a href="{{$paginator->getUrl($page)}}">{{$page}}</a>
                @endif
            @endfor
        @else
            @for($page = $currentPage - $currentPage % $pages + 1; $page <= $currentPage - $currentPage % $pages + $pages; $page++)
                @if($page == $currentPage+1)
                    <a href="{{$paginator->getUrl($page)}}" class="current">{{$page}}</a>
                @else
                    <a href="{{$paginator->getUrl($page)}}">{{$page}}</a>
                @endif
            @endfor
        @endif
    </div>
    <div>
        @if($currentPage+1 < $lastPage)
            <input type="button" class="btn_gray_22" value="{{Lang::get('common.pagination.btn_next')}}"   onclick="location.href='{{$paginator->getUrl($currentPage+2)}}'"/>
        @endif

    </div>
</div>
@endif