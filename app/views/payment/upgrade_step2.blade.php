@extends('common.master')

@section('js')
<script type="text/javascript">

    $(document).ready(function() {

        $("input[name='time_span_sel']").change(function() {

            var span = $("input[name='time_span_sel']:checked").val();

            sd = Date.today();
            ed = Date.today();

            if( span == 'quarter' ) {
                amount = 300;
                ed.addMonths( 3 );
                ed.addDays(-1);
            } else {
                amount = 1000;
                ed.addMonths( 12 );
                ed.addDays(-1);
            }

            var time_span = 'From ' + sd.toString('yyyy-MM-dd') + ' To ' + ed.toString('yyyy-MM-dd');

            // var time_span = 'From 2013 - 2014';
            $('#hidden_amount').val( amount );
            $('#time_span').text( time_span );
            $('#time_frame').text( 'Time Frame: 1 ' + span );

            $('#time_span').text( time_span );
            $('#total_purchase').text( '$' + amount );
            $('#total').text( '$' + amount );

        });

        $('#accept').change(function() {

            if ($('#accept').is(':checked') ) {
                $("#submit").attr("class","btn_green110");
                $('#submit').removeAttr('disabled');
            } else {
                $("#submit").attr("class","btn_ashy110");
                $('#submit').attr('disabled', 'disabled' );
            }

        });

    });

</script>
@stop

@section('content_left')
<div class="left_650_column2">
    <div class="left_650_c_title_black">Payment</div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_title">Current</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  Gold(Annual)</div>
                    <div class="detail100">$333.25/Annual</div>
                </li>
                <li>
                    <div class="title500"><span>From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">New</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  Platinum(Annual)</div>
                    <div class="detail100">$333.25/Annual</div>
                </li>
                <li>
                    <div class="title500"><span>From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">Credit from previous subscription purchases:</div>
                    <div class="detail100">($103.22)</div>
                </li>
                <li>&nbsp;</li>
                <li>
                    <div class="title500">Total Purchases:</div>
                    <div class="detail100">$333.22</div>
                </li>
                <li>
                    <div class="title500">Discount/Credits:</div>
                    <div class="detail100">($103.22)</div>
                </li>
                <li>
                    <div class="title500">Estimated Sales Tax:</div>
                    <div class="detail100">$0.00</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">Total Due</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">$1000.00</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>&nbsp;</li>
                <li class="notice"><input name="" type="checkbox" value="" /><span class="notice_span">By checking this box, you agree to the <a href="#">TraDove Payment Policy</a>. All purchases are final and non-refundable.</span></li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input type="button" class="btn_blue_22" value="Make Payment" />&nbsp;&nbsp;<input type="button" class="btn_gray_22" value="Cancel" /></li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>
</div>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop