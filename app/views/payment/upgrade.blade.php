<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{$pageTitle}}</title>
    {{HTML::style('css/tradove_common.css')}}
    {{HTML::style('css/jquery-ui.css')}}
    {{HTML::style('css/showLoading.css')}}
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/jquery-ui.js')}}
    {{HTML::script('js/layer/layer.min.js')}}
    {{HTML::script('js/jquery.showLoading.min.js')}}
    {{HTML::script('js/common.js')}}
    {{HTML::script('js/tradove.js')}}

    <script type="text/javascript">

        $(document).ready(function() {

            /*$("input[name='package']").change(function() {

                var package = $("input[name='package']:checked").val();

                $('#u_form').attr('method', 'post').attr('action', '/payment/step1/');
                var vipLevel = parseInt( package / 2 ) * 2;

                //$('input:submit').attr( "disabled", "disabled" ).removeClass().addClass("btn_ashy80");
                //$("#btn_" + vipLevel).removeAttr("disabled").removeClass().addClass("btn_green80");

            });*/

            var turnToOrderPage = function(btnObj){
                var curSelVipLevel = btnObj.attr("vip-level");
                $("input[name='purchaseType']").val(btnObj.val());
                var selRadPackages = $("input[vip-level='" + curSelVipLevel + "']:checked");
                console.log("select package length: " + selRadPackages.length);
                if(selRadPackages.length == 0){
                    return false;
                }else{
                    $("#u_form").submit();
                }
            }

            var optEvents = function(){
                var memberType = $("input[name='memberType']").val();
                var otherMemberType = $("input[name='otherMemberType']").val();
                var buyerMemberType = $("input[name='buyerMemberType']").val();

                return {
                    init: function(){
                        var vipLevel = parseInt($("input[name='vipLevel']").val());
                        var packages = $("input[name='package']");
                        /*$.each(packages, function(){
                            if(parseInt($(this).attr("vip-level")) < vipLevel){
                                $(this).attr("disabled", "disabled");
                            }else{
                                $(this).removeAttr("disabled");
                            }
                        });*/

                        console.log("test");
                        var ashyDivs = $("div.level_column").filter(function(){
                            if($(this).attr("div-level") <= vipLevel){
                                return true;
                            }else{
                                return false;
                            }
                        });

                        ashyDivs.removeClass("payment_level_normal").addClass("payment_level_ashy")
                                .find("ul>li:first").removeClass("payment_level_selected_top").addClass("payment_level_ashy_top");
                                //.find("input[name='package']").attr("disabled", "disabled");

                        // member type is other, hidden radio and button
                        if(memberType === otherMemberType || memberType == buyerMemberType){
                            $("input[name='package']").hide();
                            $("input[name='btnUpgrade']").hide();
                            $("input[name='btnRenew']").hide();
                        }else{
                            $("input[name='package']").show();
                            $("input[name='btnUpgrade']").show();
                            $("input[name='btnRenew']").show();
                        }
                    },
                    changePackage: function(){
                        $("input[name='package']").click(function(){
                            $("input[name='discount']").val($(this).attr('discount'));
                            $("input[name='vipLevel']").val($(this).attr('vip-level'));
                        });
                    },
                    submitUpgrade: function(){
                        $("input[name='btnUpgrade']").click(function(){
                            turnToOrderPage($(this));
                        });
                    },
                    submitRenew: function(){
                        $("input[name='btnRenew']").click(function(){
                            turnToOrderPage($(this));
                        });
                    }
                };
            }();

            optEvents.init();
            optEvents.changePackage();
            optEvents.submitUpgrade();
            optEvents.submitRenew();

        });

    </script>

    @yield('js')
    @yield('css')
</head>

<body>

<div class="top_shadow"></div>
<div class="top">
</div>
<!-- feedback start -->
<div style="display:none;position: fixed; top: 265px; left: 0px; padding: 0; margin: 0;}">
    <div style="border: 1px solid #808080;">feedback</div>
</div>
<!-- feedback end -->
<div class="middle">

    <!--top ads-->
    <div class="space20"></div>
    <div class="div980">
        @yield('ads_top')
    </div>
    <div class="space20"></div>
    <!--top ads over-->

    @include('payment.vip_level')

    <!--bottom ads-->
    <div class="space30"></div>
    <div class="div980">
        @yield('ads_bottom')
    </div>
    <div class="space30"></div>
    <!--bottom ads over-->

</div>

</body>

</html>