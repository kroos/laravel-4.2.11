@extends('common.master')

@section('js')
<script type="text/javascript">
    $(document).ready(function() {

        $('#accept').change(function() {

            if ($('#accept').is(':checked') ) {
                //$("#submit").attr("class","btn_green110");
                $('#btnConfirm').removeAttr('disabled');
            } else {
                //$("#submit").attr("class","btn_ashy110");
                $('#btnConfirm').attr('disabled', 'disabled' );
            }
        });

        $("#btnConfirm").click(function(){
            var orderId = $("#orderId").val();
            // create order
            var orderInfo = {
                orderId: orderId,
                package: $("#package").val(),
                vipLevel: $("#vipLevel").val(),
                discount: $("#discount").val(),
                singlePrice: $("#singlePrice").val(),
                monthCount: $("#monthCount").val(),
                amount: $("#paid_amount").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                isAutoRenew: $("#isAutoRenew").val(),
                purchaseType: $("#purchaseType").val(),
                payType: $("#payType").val()
            };
            var detailForm = $("#detailForm");
            $.ajax({
                type: "post",
                url: "/payment/order/update",
                async: false,
                dataType: "json",
                data: orderInfo,
                success: function(){
                    console.log("save order successful");
                    var payments = $("#payments").val();
                    console.log("payments: " + payments);
                    if(payments == 0){
                        location.href = "/payment/order/upgrade/unpass/paypal?item_number=" + orderId;
                        return false;
                    }else{
                        console.log("turn to paypal page");
                        $("#detailForm").submit();
                    }
                }
            });
        });

        $("#btnCancel").click(function(){
            $("#detailForm").attr("action", "/payment/step1");
            $("#detailForm").submit();
        });
    });
</script>
@stop

@section('content_left')
<div class="left_650_column2">

    <form id="detailForm" method="post" action="{{PAYMENT_POST_URL}}">
        <input type="hidden" name="business" value="{{PAYMENT_BUSINESS_NAME}}" />
        <input type="hidden" name="cbt" value="Confirm and Return to TraDove" />
        <input type="hidden" name="item_name" value="{{$level}}" />
        <input type="hidden" name="item_number" value="{{$order->orderid}}" />
        <input type="hidden" name="currency_code" value="USD" />
        <input type="hidden" name="lc" value="US" />
        <input type="hidden" name="rm" value="2" />
        <input type="hidden" name="return" value="{{$success_url}}" />
        <input type="hidden" name="cancel_return" value="{{$cancel_url}}" />
        <input type="hidden" name="notify_url" value="{{$ipn_url}}" />

        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="bn" value="PP-BuyNowBF" />
        <!--input type="hidden" id="hidden_amount" name="amount" value="{{$order->payments}}" /-->
        <input type="hidden" id="hidden_amount" name="amount" value="1" />
        @if($order->payments == 0)
        <input type="hidden" id="payments" value="{{$order->payments}}" />
        @else
        <input type="hidden" id="payments" value="1" />
        @endif
        <!--input type="hidden" id="payments" value="{{$order->payments}}" />-->

    <div class="left_650_c_title_black">{{Lang::get('payment.step2_lab_payment')}}</div>
    <div class="left_650_c2_content">
        <div class="space20"></div>
        <input type="hidden" id="package" name="package" value="{{$package}}" />
        <input type="hidden" id="vipLevel" name="vipLevel" value="{{$vipLevel}}" />
        <input type="hidden" id="discount" name="discount" value="{{$discount}}" />
        <input type="hidden" id="singlePrice" name="singlePrice" value="{{$singlePrice}}" />
        <input type="hidden" id="monthCount" name="monthCount" value="{{$monthCount}}" />
        <input type="hidden" id="paid_amount" name="paid_amount" value="{{$amount}}" />
        <input type="hidden" id="startDate" name="startDate" value="{{$startDate}}" />
        <input type="hidden" id="endDate" name="endDate" value="{{$endDate}}" />
        <input type="hidden" id="orderId" name="orderId" value="{{$orderId}}" />
        <input type="hidden" id="isAutoRenew" name="isAutoRenew" value="{{$isAutoRenew}}" />
        <input type="hidden" id="purchaseType" name="purchaseType" value="{{$purchaseType}}" />
        <input type="hidden" id="payType" name="payType" value="{{$payType}}" />
        <div class="left_650_c2_c_title">{{Lang::get('payment.step2_lab_payment_details')}}</div>

        <!--
        <div class="left_650_c2_c_title">Current</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  {{$order->description}}</div>
                    <div class="detail100">${{$order->price}} / {{$order->period}}</div>
                </li>
                <li>
                    <div class="title500"><span>From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">New</div>
        -->

        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500"><span>{{Lang::get('payment.step2_lab_purchased_item')}}</span> {{$level}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500"><span>{{Lang::get('payment.step2_lab_unit_price')}}</span> ${{$order->price * $order->month_count - $order->discount}}/{{$order->period}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">
                        <span>{{Lang::get('payment.step2_lab_order_quantity')}}</span> {{$order->quantity}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        @if($order->is_auto_renew == 1)
                        <span class="red_word">{{Lang::get('payment.step2_lab_auto_renew')}}</span>
                        @endif
                    </div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500"><span>From {{$order->fromtime}} To {{$order->endtime}}</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_sub_total')}}</div>
                    <div class="detail100">${{number_format(($order->price * $order->month_count - $order->discount) * $order->quantity, 2)}}</div>
                </li>
                <li>
                    <div class="space20"></div>
                    <div class="title500">{{Lang::get('payment.step2_lab_credit_previous')}}</div>
                    <div class="detail100">${{number_format($order->overage, 2, '.', ',')}}</div>
                </li>
                <li>
                    <div class="space20"></div>
                    <div class="title500">{{Lang::get('payment.step2_lab_total_payment')}}</div>
                    <div class="detail100">${{number_format($order->payments, 2, '.', ',')}}</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li class="notice">
                    <input id="accept" name="" type="checkbox" value="" />
                        <span class="notice_span">By checking this box, you agree to the
                            <a href="/payment_policy">TraDove Payment Policy</a>.
                            All purchases are final and non-refundable.
                        </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input id="btnConfirm" type="button" class="btn_blue_22" value="{{Lang::get('payment.step2_btn_make_payment')}}" disabled="disabled" />&nbsp;&nbsp;
                    <input type="button" id="btnCancel" class="btn_gray_22" value="{{Lang::get('payment.step2_btn_cancel')}}" />
                </li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>
    </form>
</div>
@stop

@section('content_right')
@stop

@section('ads_bottom')
@stop