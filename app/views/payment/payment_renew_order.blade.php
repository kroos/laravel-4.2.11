@extends('common.master')

@section('js')
{{HTML::script('js/date.js')}}

<script type="text/javascript">

    $(document).ready(function() {

        var optEvents = function(){
            var monthCount = $("input[name='monthCount']").val();
            var singlePrice = $("input[name='singlePrice']").val();
            var discount = $("input[name='discount']").val();
            var startDate = $("#effectiveStartDate").text();
            var onetimePayType = $("input[name='oneTimePayType']").val();

            var getFormattedDatePart = function(datePart){
                  console.log("getFormattedDatePart datePart: " + datePart);
                  if(datePart < 10){
                      return '0' + datePart;
                  }else{
                      return datePart;
                  }
            };

            var computeEndDate = function(startDate, monthCount){
                var startDate = startDate.replace(/-/gm, '/');
                var beginDate = new Date(startDate);
                console.log("beginDate.getYear: " + beginDate.getYear());
                beginDate.setMonth(beginDate.getMonth() + monthCount);
                console.log("beginDate.getYear 2: " + beginDate.getYear());
                var currentDate = parseInt(beginDate.getFullYear()) + '-' + getFormattedDatePart(parseInt(beginDate.getMonth()) + 1) + '-' + getFormattedDatePart(parseInt(beginDate.getDate()));
                var endDate = new Date(currentDate);
                var yesterday_milliseconds = endDate.getTime() - 1000 * 60 * 60 * 24;
                endDate.setTime(yesterday_milliseconds);
                console.log("endDate: " + endDate);
                return parseInt(endDate.getFullYear()) + '-' + getFormattedDatePart(parseInt(endDate.getMonth()) + 1) + '-' + getFormattedDatePart(parseInt(endDate.getDate()));
            };

            var exchangeOnetimeType = function(itemAmount){
                if(itemAmount != '' && !isNaN(itemAmount)){
                    var totalAmount = monthCount * itemAmount;
                    var totalPrice = singlePrice * totalAmount * (1 - discount);
                    $("#totalPrice").text(formatNumber(totalPrice, 2, ','));

                    // compute date
                    var endDate = computeEndDate(startDate, totalAmount);
                    $("#effectiveEndDate").text(endDate);
                    $("input[name='startDate']").val(startDate);
                    $("input[name='endDate']").val(endDate);
                    $("input[name='quantity']").val(itemAmount);
                }else{
                    $("#totalPrice").text(0.00);
                    $("#effectiveEndDate").text(startDate);
                    $("input[name='startDate']").val(startDate);
                    $("input[name='endDate']").val(startDate);
                    $("input[name='quantity']").val(0);
                }
            };

            var formatDate = function(dateObj, format){
                var o = {
                    "M+" : dateObj.getMonth()+1, //month
                    "d+" : dateObj.getDate(),    //day
                    "h+" : dateObj.getHours(),   //hour
                    "m+" : dateObj.getMinutes(), //minute
                    "s+" : dateObj.getSeconds(), //second
                    "q+" : Math.floor((dateObj.getMonth()+3)/3),  //quarter
                    "S" : dateObj.getMilliseconds() //millisecond
                }
                if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
                        (dateObj.getFullYear()+"").substr(4 - RegExp.$1.length));
                for(var k in o)if(new RegExp("("+ k +")").test(format))
                    format = format.replace(RegExp.$1,
                            RegExp.$1.length==1 ? o[k] :
                                    ("00"+ o[k]).substr((""+ o[k]).length));
                return format;
            }

            var formatNumber = function(num, precision, separator){
                var parts;
                // 判断是否为数字
                if (!isNaN(parseFloat(num)) && isFinite(num)) {
                    // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
                    // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(num))
                    // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
                    // 的值变成了 12312312.123456713
                    num = Number(num);
                    // 处理小数点位数
                    num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
                    // 分离数字的小数部分和整数部分
                    parts = num.split('.');
                    // 整数部分加[separator]分隔, 借用一个著名的正则表达式
                    parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

                    return parts.join('.');
                }
                return NaN;
            }

            return {
                init: function(){
//                    var totalAmount = monthCount;
//                    var totalPrice = singlePrice * totalAmount * (1 - discount);
                    $("#totalPrice").text(0.00);
                    $("input[name='amount']").text(1);
                    $("input[name='quantity']").val(0);
                    // transiently comment auto billing function
//                    var isAutoRenew = $("input[name='isAutoRenew']");
//                    isAutoRenew.attr("checked", "checked");
//                    isAutoRenew.val(1);

                    //$("input[name='amount']").val(1);
                },
                changeQuantity: function(){
                    $("input[name='sel_quantity']").change(function() {
                        var quantity = $("input[name='sel_quantity']:checked").val();
                        var price = $("#pid").val();
                        var amount = quantity * 3 * price;

                        amount = parseFloat(amount).toFixed(2);

                        sd = Date.today();
                        ed = Date.today();
                        ed.addMonths( quantity * 3 );
                        ed.addDays(-1);

                        var time_span = 'From ' + sd.toString('yyyy-MM-dd')
                                + ' To ' + ed.toString('yyyy-MM-dd');

                        $('#time_span').text( time_span );
                        $('#total_amount').text( '$' + amount );

                    });
                },
                avoidCharPaste: function(){
                    $("input[name='amount']").on("afterpaste", function(){
                        $(this).val($(this).val().replace(/\D/g,''));
                    });
                },
                computeTotalPrice: function(){
                    $("input[name='amount']").keyup(function(){
                        var payType = $("input[name='radPayType']:checked").val();
                        if(payType == onetimePayType){
                            $(this).val($(this).val().replace(/\D/g,''));
                            var itemAmount = $(this).val();
                            exchangeOnetimeType(itemAmount);
                        }
                    });
                },
                exchangePayType: function(){
                    $("input[name='radPayType']").click(function(){
                        if($(this).val() == onetimePayType){
                            // enable amount input text
                            $("input[name='amount']").removeAttr("disabled");
                            var itemAmount = $("input[name='amount']").val();
                            exchangeOnetimeType(itemAmount);
                        }else{
                            $("input[name='amount']").attr("disabled", "disabled");
                            var monthlyPrice = $("input[name='singlePrice']").val();
                            $("#totalPrice").text(parseFloat(monthlyPrice).toFixed(2));
                            $("input[name='quantity']").val(1);

                            // set endtime
                            var startDate = new Date();
                            var startDateStr = formatDate(startDate, "yyyy-MM-dd");
                            var endDate = computeEndDate(startDateStr, 1);
                            $("input[name='startDate']").val(startDateStr);
                            $("input[name='endDate']").val(endDate);
                        }

                        $("input[name='payType']").val($(this).val());
                    });
                },
                confirmOrder: function(){
                    $("#btnConfirmOrder").click(function(){
                        var payType = $("input[name='payType']").val();
                        var amount = 1;
                        if(payType == onetimePayType){
                            amount = $("input[name='amount']").val();
                        }
                        var totalPrice = $("#totalPrice").text();
                        console.log("confirmOrder: " + amount + "-" + totalPrice);
                        if(amount == '' || amount == 0 || totalPrice == '' || totalPrice == 0){
                            return false;
                        }
                    });
                },
                cancelOrder: function(){
                    $("#btnCancel").click(function(){
                        location.href = "/people/upgrade";
                    });
                }
            };
        }();

        optEvents.init();
        optEvents.changeQuantity();
        optEvents.avoidCharPaste();
        optEvents.computeTotalPrice();
        optEvents.exchangePayType();
        optEvents.confirmOrder();
        optEvents.cancelOrder();
    });

</script>
@stop

@section('content_left')

<form id="orderForm" method="post" action="/payment/step2">

    <input type="hidden" name="package" value="{{$package['id']}}" />
    <input type="hidden" name="vipLevel" value="{{$vipLevel}}" />
    <input type="hidden" name="nid" value="{{$nid}}" />
    <input type="hidden" name="discount" value="{{$discount}}" />
    <input type="hidden" name="startDate" value="{{$startDate}}" />
    <input type="hidden" name="endDate" value="{{$endDate}}" />
    <input type="hidden" name="purchaseType" value="{{$purchaseType}}" />
    <input type="hidden" name="oneTimePayType" value="{{PAYMENT_PAY_TYPE_ONETIME}}" />
    <input type="hidden" name="autoBillingPayType" value="{{PAYMENT_PAY_TYPE_AUTO_BILLING}}" />
    <input type="hidden" name="singlePrice" value="{{$package['price']}}" />
    <input type="hidden" name="monthCount" value="{{$package['month_count']}}" />
    <input type="hidden" name="payType" value="{{$payType}}" />
    <input type="hidden" name="quantity" />

    <div class="left_650_column2">
        <div class="left_650_c_title_black">{{Lang::get('payment.step1_lab_payment')}}</div>
        <div class="left_650_c2_content">
            <div class="space20"></div>
            <div class="left_650_c2_c_title">{{Lang::get('payment.step1_lab_fill_orders')}}</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title600_weight">
                            <div style="width:590px;float:left;"><input name="radPayType" type="radio" value="{{PAYMENT_PAY_TYPE_ONETIME}}" checked="true" />&nbsp;&nbsp;{{Lang::get('payment.step1_lab_onetime_purchase')}}</div>
                        </div>
                    </li>
                    <li>
                        <div class="title500"><span>Purchased item:</span>  {{$package['level']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Unit price:</span> ${{$package['price'] * $package['month_count'] * (1 - $package['discount'])}} / {{$package['period']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Order quantity:</span> <input class="input30" name="amount" type="text" /></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span id="effectivePeriod">From <span id="effectiveStartDate">{{$startDate}}</span> To <span id="effectiveEndDate">{{$endDate}}</span></span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title600_weight">
                            <div style="width:590px;float:left;">
                                @if($recurringOrderExists)
                                    <input name="radPayType" type="radio" value="{{PAYMENT_PAY_TYPE_AUTO_BILLING}}" disabled="disabled" />
                                @else
                                    <input name="radPayType" type="radio" value="{{PAYMENT_PAY_TYPE_AUTO_BILLING}}" />
                                @endif
                                &nbsp;&nbsp;Auto-renew:
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="title500"><span>Purchased item:</span>  {{$package['level']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Unit price:</span> {{$package["price"]}}/month</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title600_weight">
                            <div style="width:590px;float:left;font-weight:normal;color:#8b451b;">Following this purchase, your account will be renewed monthly using your provided payment information. You may edit this preference in the Payment Settings menu.</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="space20"></div>
                        <div class="title500">{{Lang::get('payment.step1_lab_total_due')}}</div>
                        <div class="detail100">$<span id="totalPrice"></span></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_detail">
                <ul>
                    <li><input type="submit" id="btnConfirmOrder" class="btn_blue_22" value="{{Lang::get('payment.step1_btn_create_order')}}" />&nbsp;&nbsp;<input type="button" id="btnCancel" class="btn_gray_22" value="{{Lang::get('payment.step1_btn_cancel')}}" /></li>
                </ul>
            </div>
        </div>
        <div class="space20"></div>
        <div class="left_top"> </div>
    </div>
</form>
@stop

@section('content_right')
@stop

@section('ads_bottom')
@stop