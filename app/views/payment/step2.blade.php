@extends('common.master')

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#accept').change(function() {

            if ($('#accept').is(':checked') ) {
                $("#submit").attr("class","btn_green110");
                $('#submit').removeAttr('disabled');
            } else {
                $("#submit").attr("class","btn_ashy110");
                $('#submit').attr('disabled', 'disabled' );
            }
        });
    });
</script>
@stop

@section('content_left')
<div class="left_650_column2">

    <form method="post" action="{{PAYMENT_POST_URL}}">
        <input type="hidden" name="business" value="kent.yan@tradove.net" />
        <input type="hidden" name="item_name" value="{{$order->description}}" />
        <input type="hidden" name="item_number" value="{{$order->orderid}}" />
        <input type="hidden" name="currency_code" value="USD" />
        <input type="hidden" name="lc" value="US" />

        <input type="hidden" name="return" value="{{$success_url}}" />
        <input type="hidden" name="cancel_return" value="{{$cancel_url}}" />
        <input type="hidden" name="notify_url" value="{{$ipn_url}}" />

        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="bn" value="PP-BuyNowBF" />
        <input type="hidden" id="hidden_amount" name="amount" value="{{$order->payments}}" />


    <div class="left_650_c_title_black">{{Lang::get('payment.step2_lab_payment')}}</div>
    <div class="left_650_c2_content">

        <!--
        <div class="left_650_c2_c_title">Current</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  {{$order->description}}</div>
                    <div class="detail100">${{$order->price}} / {{$order->period}}</div>
                </li>
                <li>
                    <div class="title500"><span>From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">New</div>
        -->

        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_order_id')}}&nbsp;{{$order->orderid}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>

                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_subscription')}}&nbsp;{{$order->description}}&nbsp;(${{$order->price}} / {{$order->period}})</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_time_span')}}&nbsp;{{$timeSpan}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>

                <li>&nbsp;</li>

                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_total_purchases')}}</div>
                    <div class="detail100">${{$order->price}}</div>
                </li>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_discount')}}</div>
                    <div class="detail100">${{$order->discount}}</div>
                </li>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_estimated_sales_tax')}}</div>
                    <div class="detail100">${{$order->tax}}</div>
                </li>
                <li>
                    <div class="title500">{{Lang::get('payment.step2_lab_credit_previous')}}</div>
                    <div class="detail100">${{$order->overage}}</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">{{Lang::get('payment.step2_lab_total_due')}}</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">${{$order->payments}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>&nbsp;</li>
                <li class="notice"><input id="accept" name="" type="checkbox" value="" />
                    <span class="notice_span">By checking this box, you agree to the
                        <a href="/payment_policy">TraDove Payment Policy</a>.
                        All purchases are final and non-refundable.
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input id="submit" type="submit" class="btn_blue_22" value="{{Lang::get('payment.step2_btn_make_payment')}}" disabled="disabled" />&nbsp;&nbsp;
                    <input type="button" class="btn_gray_22" onclick="javascript:history.back(-1);" value="{{Lang::get('payment.step2_btn_cancel')}}" />
                </li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>
    </form>
</div>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop