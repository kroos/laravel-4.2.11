@extends('common.master')

@section('js')
{{HTML::script('js/date.js')}}

<script type="text/javascript">

    $(document).ready(function() {

        $("input[name='sel_quantity']").change(function() {
            var quantity = $("input[name='sel_quantity']:checked").val();
            var price = $("#pid").val();
            var amount = quantity * 3 * price;

            amount = parseFloat(amount).toFixed(2);

            sd = Date.today();
            ed = Date.today();
            ed.addMonths( quantity * 3 );
            ed.addDays(-1);

            var time_span = 'From ' + sd.toString('yyyy-MM-dd')
                + ' To ' + ed.toString('yyyy-MM-dd');

            $('#time_span').text( time_span );
            $('#total_amount').text( '$' + amount );

        });

    });

</script>
@stop


@section('content_left')
<form method="get" action="/people/upgrade_step2">
<div class="left_650_column2">
    <div class="left_650_c_title_black">Payment</div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_title">Fill orders</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  Gold(Annual)</div>
                    <div class="detail100">$333.25/Annual</div>
                </li>

                <li>
                    @if($annualOrQuarter == 'quarter')
                    <div class="title500">Time Frame:
                        <input name="sel_quantity" type="radio" value="1" checked="checked" />&nbsp;1 Quarter
                        <input name="sel_quantity" type="radio" value="2" />&nbsp;2 Quarters
                        <input name="sel_quantity" type="radio" value="3" />&nbsp;3 Quarters
                    </div>
                    <div class="detail100">&nbsp;</div>
                    @else
                    <div>Time Frame:&nbsp;&nbsp;1&nbsp;&nbsp;year</div>
                    @endif
                </li>

                <li>
                    <div class="title500"><span id="time_span">From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">Total Due</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div id="total_amount" class="title500">$333.25</div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input type="submit" class="btn_blue_22" value="Next" />&nbsp;&nbsp;
                    <input type="button" class="btn_gray_22" value="Cancel" />
                </li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>
</div>
</form>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop