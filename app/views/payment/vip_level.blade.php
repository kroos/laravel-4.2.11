<form id="u_form" method="post" action="/payment/step1">
<div class="div980">
        <!--Left-->
        <div class="left_980">
            <div class="left_980_column">
                <div class="left_980_c_title_black">{{Lang::get('payment.vip_level_lab_upgrade_your_account')}}</div>
                @if($memberType == MEMBER_TYPE_OTHER_ID)
                <div class="left_980_c_content">
                    <span class="red_word">{{Lang::get('payment.vip_level_lab_other_type_user_notice')}}</span>
                </div>
                @endif
                <div class="left_980_c_content">
                    <input type="hidden" name="vipLevel" value="{{$vipLevel}}" />
                    <input type="hidden" name="discount" value="" />
                    <input type="hidden" name="memberType" value="{{$memberType}}" />
                    <input type="hidden" name="otherMemberType" value="{{MEMBER_TYPE_OTHER_ID}}" />
                    <input type="hidden" name="buyerMemberType" value="{{MEMBER_TYPE_BUYER_ID}}" />
                    <input type="hidden" name="purchaseType" />
                    <div id="msg" style="color: red;"></div>
                    <div class="payment_part">
                        <ul>
                            <li class="payment_part_top">{{Lang::get('payment.vip_level_lab_features')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item1')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item2')}}<br><span>{{Lang::get('payment.vip_level_lab_item2_desc')}}</span></li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item3')}}<br><span>{{Lang::get('payment.vip_level_lab_item3_desc')}}</span></li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item4')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item5')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item6')}}<br><span>{{Lang::get('payment.vip_level_lab_item6_desc')}}</span></li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item7')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item8')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item9')}}</li>
                            <li class="payment_partname">{{Lang::get('payment.vip_level_lab_item10')}}</li>
                        </ul>
                    </div>
                    <div div-level="1" class="level_column payment_level_ashy">
                        <ul>
                            <li class="payment_level_ashy_top">
                                <ul>
                                    <li class="level_note">
                                        @if($vipLevel == 1)
                                        {{Lang::get('payment.vip_level_lab_your_current_account')}}
                                        @else
                                        &nbsp;
                                        @endif
                                    </li>
                                    <li class="level_title">{{Lang::get('payment.level_name_1')}}</li>
                                    <li class="level_price">{{Lang::get('payment.vip_level_lab_basic_free')}}</li>
                                    <li class="level_btn"></li>
                                </ul>
                            </li>
                            <li class="payment_level_ashy_content">&nbsp;</li>
                            <li class="payment_level_ashy_content"><br /><br />1</li>
                            <li class="payment_level_ashy_content"><br />100</li>
                            <li class="payment_level_ashy_content">3</li>
                            <li class="payment_level_ashy_content">1</li>
                            <li class="payment_level_ashy_content"><br /><br />0</li>
                            <li class="payment_level_ashy_content">1</li>
                            <li class="payment_level_ashy_content">1</li>
                            <li class="payment_level_ashy_content">1</li>
                            <li class="payment_level_ashy_content">1</li>
                        </ul>
                    </div>
                    <div div-level="4" class="level_column payment_level_normal">
                        <ul>
                            <li class="payment_level_selected_top">
                                <ul>
                                    <li class="level_note">
                                        @if($vipLevel == 4)
                                        {{Lang::get('payment.vip_level_lab_your_current_account')}}
                                        @else
                                        &nbsp;
                                        @endif
                                    </li>

                                    <li class="level_title">{{Lang::get('payment.level_name_4')}}</li>
                                    <li class="level_price"><input name="package" vip-level="4" discount="0" type="radio" value="4" />&nbsp;<span>${{$packages[4]['price']}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_monthly')}}</li>
                                    <li class="level_price"><input name="package" vip-level="4" discount="0.2" type="radio" value="5" />&nbsp;<span>${{$packages[5]['price'] * $packages[5]['month_count'] * (1 - $packages[5]['discount'])}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_year')}} ({{Lang::get('payment.vip_level_lab_year_discount')}})</li>
                                    <li class="level_btn">
                                        @if($vipLevel < 4)
                                        <input id="btn_4" class="btn_blue_22" vip-level="4" value="{{Lang::get('payment.vip_level_btn_upgrade')}}" name="btnUpgrade" type="button" />
                                        @elseif($vipLevel == 4)
                                        <input class="btn_blue_22" vip-level="4" value="{{Lang::get('payment.vip_level_btn_renew')}}" name="btnRenew" type="button" />
                                        @endif
                                    </li>
                                </ul>
                            </li>
                            <li class="payment_level_ashy_content">Yes</li>
                            <li class="payment_level_ashy_content"><br /><br />3</li>
                            <li class="payment_level_ashy_content"><br />300</li>
                            <li class="payment_level_ashy_content">10</li>
                            <li class="payment_level_ashy_content">3</li>
                            <li class="payment_level_ashy_content"><br /><br />+6</li>
                            <li class="payment_level_ashy_content">3</li>
                            <li class="payment_level_ashy_content">3</li>
                            <li class="payment_level_ashy_content">3</li>
                            <li class="payment_level_ashy_content">3</li>
                        </ul>
                    </div>
                    <div div-level="8" class="level_column payment_level_normal">
                        <ul>
                            <li class="payment_level_selected_top">
                                <ul>
                                    <li class="level_note">
                                        @if($vipLevel == 8)
                                        {{Lang::get('payment.vip_level_lab_your_current_account')}}
                                        @else
                                        &nbsp;
                                        @endif
                                    </li>
                                    <li class="level_title">{{Lang::get('payment.level_name_8')}}</li>
                                    <li class="level_price"><input name="package" vip-level="8" discount="0" type="radio" value="8" />&nbsp;<span>${{$packages[8]['price']}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_monthly')}}</li>
                                    <li class="level_price"><input name="package" vip-level="8" discount="0.2" type="radio" value="9" />&nbsp;<span>${{$packages[9]['price'] * $packages[9]['month_count'] * (1 - $packages[9]['discount'])}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_year')}} ({{Lang::get('payment.vip_level_lab_year_discount')}})</li>
                                    <li class="level_btn">
                                        @if($vipLevel < 8)
                                        <input id="btn_8" class="btn_blue_22" vip-level="8" value="{{Lang::get('payment.vip_level_btn_upgrade')}}" name="btnUpgrade" type="button" />
                                        @elseif($vipLevel == 8)
                                        <input class="btn_blue_22" vip-level="8" value="{{Lang::get('payment.vip_level_btn_renew')}}" name="btnRenew" type="button" />
                                        @endif
                                    </li>
                                </ul>
                            </li>
                            <li class="payment_level_selected_content">Yes</li>
                            <li class="payment_level_selected_content"><br /><br />7</li>
                            <li class="payment_level_selected_content"><br />700</li>
                            <li class="payment_level_selected_content">30</li>
                            <li class="payment_level_selected_content">7</li>
                            <li class="payment_level_selected_content"><br /><br />+8</li>
                            <li class="payment_level_selected_content">7</li>
                            <li class="payment_level_selected_content">7</li>
                            <li class="payment_level_selected_content">7</li>
                            <li class="payment_level_selected_content">7</li>
                        </ul>
                    </div>
                    <div div-level="16" class="level_column payment_level_normal">
                        <ul>
                            <li class="payment_level_normal_top">
                                <ul>
                                    <li class="level_note">
                                        @if($vipLevel == 16)
                                        {{Lang::get('payment.vip_level_lab_your_current_account')}}
                                        @else
                                        &nbsp;
                                        @endif
                                    </li>

                                    <li class="level_title">{{Lang::get('payment.level_name_16')}}</li>
                                    <li class="level_price"><input name="package" vip-level="16" discount="0" type="radio" value="16" />&nbsp;<span>${{$packages[16]['price']}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_monthly')}}</li>
                                    <li class="level_price"><input name="package" vip-level="16" discount="0.2" type="radio" value="17" />&nbsp;<span>${{$packages[17]['price'] * $packages[17]['month_count'] * (1 - $packages[17]['discount'])}}</span>&nbsp;{{Lang::get('payment.vip_level_lab_year')}} ({{Lang::get('payment.vip_level_lab_year_discount')}})</li>
                                    <li class="level_btn">
                                        @if($vipLevel < 16)
                                        <input id="btn_16" class="btn_blue_22" vip-level="16" value="{{Lang::get('payment.vip_level_btn_upgrade')}}" name="btnUpgrade" type="button" />
                                        @elseif($vipLevel == 16)
                                        <input class="btn_blue_22" vip-level="16" value="{{Lang::get('payment.vip_level_btn_renew')}}" name="btnRenew" type="button" />
                                        @endif
                                    </li>
                                </ul>
                            </li>
                            <li class="payment_level_normal_content">Yes</li>
                            <li class="payment_level_normal_content"><br /><br />No Limit</li>
                            <li class="payment_level_normal_content"><br />No Limit</li>
                            <li class="payment_level_normal_content">No Limit</li>
                            <li class="payment_level_normal_content">No Limit</li>
                            <li class="payment_level_normal_content"><br /><br />+10</li>
                            <li class="payment_level_normal_content">No Limit</li>
                            <li class="payment_level_normal_content">No Limit</li>
                            <li class="payment_level_normal_content">No Limit</li>
                            <li class="payment_level_normal_content">No Limit</li>
                        </ul>
                    </div>

                </div>
                <div class="space30"></div>
                <div class="space30"></div>
            </div>
        </div>
        <!--Left over-->
    </div>
</form>