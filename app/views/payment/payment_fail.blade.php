@extends('common.master')

@section('js')
<script>
    $(function(){
        $("#btnReUpgrade").click(function(){
            location.href = "/people/upgrade";
        });

        $("#btnTurnToHome").click(function(){
            location.href = "/home";
        });
    });
</script>
@stop

@section('content_left')
<div class="left_650">
    <div class="left_650_column2">
        <div class="left_650_c_title_black">Payment</div>
        <div class="left_650_c2_content">
            <div class="space20"></div>
            <div class="left_650_c2_c_title">Your payment was unsuccessful. Please try again.</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title500"><span>VIP-{{$vip_level}}</span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>From {{$from_time}} To {{$end_time}}</span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Total payment: ${{payment}}</span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_detail">
                <ul>
                    <li><input type="button" id="btnReUpgrade" class="btn_blue_22" value="Retry Payment" />&nbsp;&nbsp;<input type="button" id="btnTurnToHome" class="btn_gray_22" value="Cancel" /></li>
                </ul>
            </div>
        </div>
        <div class="space20"></div>
        <div class="left_top"> </div>
    </div>
</div>
@stop

@section('content_right')
@stop

@section('ads_bottom')
@stop