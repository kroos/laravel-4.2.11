@extends('common.master')

@section('js')
{{HTML::script('js/date.js')}}

<script type="text/javascript">

    $(document).ready(function() {

        $("input[name='sel_quantity']").change(function() {
            var quantity = $("input[name='sel_quantity']:checked").val();
            var price = $("#pid").val();
            var amount = quantity * 3 * price;

            amount = parseFloat(amount).toFixed(2);

            sd = Date.today();
            ed = Date.today();
            ed.addMonths( quantity * 3 );
            ed.addDays(-1);

            var time_span = 'From ' + sd.toString('yyyy-MM-dd')
                + ' To ' + ed.toString('yyyy-MM-dd');

            $('#time_span').text( time_span );
            $('#total_amount').text( '$' + amount );

        });

    });

</script>
@stop

@section('content_left')

<form method="post" action="/payment/step2">

    <input type="hidden" name="package" value="{{$package['id']}}" />
    <input type="hidden" name="nid" value="{{$nid}}" />

    <div class="left_650_column2">
        <div class="left_650_c_title_black">{{Lang::get('payment.step1_lab_payment')}}</div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_title">{{Lang::get('payment.step1_lab_fill_orders')}}</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title500">{{Lang::get('payment.step1_lab_subscription')}}&nbsp;{{$package['name']}}</div>
                        <div class="detail100">${{$package['price']}} / {{$package['period']}}</div>
                    </li>

                    <li>
                        <div>{{Lang::get('payment.step1_lab_time_frame')}}&nbsp;&nbsp;1&nbsp;&nbsp;{{$package['period']}}</div>
                    </li>

                    <li>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
            <div class="left_650_c2_c_title">{{Lang::get('payment.step1_lab_total_due')}}</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div id="total_amount" class="title500">${{$package['price']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_detail">
                <ul>
                    <li><input type="submit" class="btn_blue_22" value="{{Lang::get('payment.step1_btn_create_order')}}" />&nbsp;&nbsp;
                        <input type="button" id="btnCancel" class="btn_gray_22" value="{{Lang::get('payment.step1_btn_cancel')}}" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="space20"></div>
        <div class="left_top"> </div>
    </div>
</form>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop