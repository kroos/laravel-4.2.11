@extends('common.master')

@section('js')
{{HTML::script('js/date.js')}}
<script type="text/javascript">

    $(document).ready(function() {

        $("input[name='time_span_sel']").change(function() {

            var span = $("input[name='time_span_sel']:checked").val();

            sd = Date.today();
            ed = Date.today();

            if( span == 'quarter' ) {
                amount = 300;
                ed.addMonths( 3 );
                ed.addDays(-1);
            } else {
                amount = 600;
                ed.addMonths( 12 );
                ed.addDays(-1);
            }

            var time_span = 'From ' + sd.toString('yyyy-MM-dd') + ' To ' + ed.toString('yyyy-MM-dd');

            $('#hidden_amount').val( amount );
            $('#time_span').text( time_span );
            $('#time_frame').text( 'Time Frame: 1 ' + span );

            $('#time_span').text( time_span );
            $('#total_purchase').text( '$' + amount );
            $('#total').text( '$' + amount );

        });


        $('#accept').change(function() {
            if ($('#accept').is(':checked') ) {
                // $("#submit").attr("class","btn_green110");
                $('#submit').removeAttr('disabled');
            } else {
                // $("#submit").attr("class","btn_ashy80bb");
                $('#submit').attr('disabled', 'disabled' );
            }
        });

    });
</script>
@stop

@section('content_left')
<div class="left_650_column2">

    <form method="post" action="{{$actionUrl}}">

    <input type="hidden" name="business" value="kent.yan@tradove.net" />
    <input type="hidden" name="item_name" value="{{$itemName}}" />
    <input type="hidden" name="item_number" value="{{$orderId}}" />
    <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="lc" value="US" />

    <input type="hidden" name="return" value="{{BASE_URL}}payment/payment_success.php" />
    <input type="hidden" name="cancel_return" value="{{BASE_URL}}network/suggest" />
    <input type="hidden" name="notify_url" value="{{BASE_URL}}payment/payment_ipn.php" />

    <input type="hidden" name="cmd" value="_xclick" />
    <input type="hidden" name="bn" value="PP-BuyNowBF" />
    <input type="hidden" id="hidden_amount" name="amount" value="{{$amount}}" />

    <div class="left_650_c_title_black">Payment</div>
    <div class="left_650_c2_content">

        <div class="left_650_c2_c_title">Please select the package</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">
                        <input name="time_span_sel" type="radio" value="year" checked="checked" />&nbsp;Annual      $600/year
                    </div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <!-- li>
                    <div class="title500">
                        <input name="time_span_sel" type="radio" value="quarter" />&nbsp;Quarterly   $300/quarter
                    </div>
                    <div class="detail100">&nbsp;</div>
                </li -->
            </ul>
        </div>

        <!--
        <div class="order_title">Please select the package</div>
        <div class="space10"></div>
        <div class="order_content">
            <div class="order_content_l"><input name="time_span_sel" type="radio" value="year" checked="checked" style="vertical-align:text-bottom;" />&nbsp;Annual&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$1000/year</div>
        </div>
        <div class="order_content">
            <div class="order_content_l"><input name="time_span_sel" type="radio" value="quarter" style="vertical-align:text-bottom;" />&nbsp;Quarterly&nbsp;&nbsp;&nbsp;$300/quarter</div>
        </div>
        <div class="space20"></div>
        -->

        <div class="left_650_c2_c_title">Order Summary</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Subscription:  Apply to join network {{$networkName}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500" id="time_frame">Time Frame: 1 year</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500"><span id="time_span">{{$span}}</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">Total Purchase:</div>
                    <div class="detail100" id="total_purchase">${{$amount}}</div>
                </li>
                <li>
                    <div class="title500">Estimated Sales Tax:</div>
                    <div class="detail100">${{$tax}}</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">Total Due</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500" id="total">${{$total}}</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>&nbsp;</li>
                <li class="notice"><input id="accept" name="" type="checkbox" value="" /><span class="notice_span">By checking this box, you agree to the <a href="/payment_policy" traget="_blank">TraDove Payment Policy</a>. All purchases are final and non-refundable.</span></li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input id="submit" type="submit" class="btn_blue_22" value="Make Payment" disabled="disabled" />&nbsp;&nbsp;
                    <!-- input type="button" class="btn_gray_22" value="Cancel" /-->
                </li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>

    </form>
</div>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop