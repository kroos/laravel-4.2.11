@extends('common.master')

@section('js')
@stop

@section('content_left')
<div class="left_650_column2">
    <div class="left_650_c_title_black">Payment</div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_title">Fill orders</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">Service Type:  Network</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500">Subscription:  Apply to join a network</div>
                    <div class="detail100">$1000</div>
                </li>
                <li>
                    <div class="title500">Time Frame:   1 year</div>
                    <div class="detail100">&nbsp;</div>
                </li>
                <li>
                    <div class="title500"><span>From 2014-01-21 To 2015-01-21</span></div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
        <div class="left_650_c2_c_title">Total Due</div>
        <div class="left_650_c2_c_detail">
            <ul>
                <li>
                    <div class="title500">$1000.00</div>
                    <div class="detail100">&nbsp;</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_detail">
            <ul>
                <li><input type="button" class="btn_blue_22" value="Next" />&nbsp;&nbsp;<input type="button" class="btn_gray_22" value="Cancel" /></li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top"> </div>
</div>
@stop

@section('content_right')
@include('home.recommend.people')
@include('home.recommend.product')
@include('home.recommend.company')
@stop

@section('ads_bottom')
@stop