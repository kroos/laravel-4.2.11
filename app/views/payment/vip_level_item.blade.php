
@if($vipLevel <= $thisLevel)
<div class="payment_level_ashy">
    <ul>
        <li class="payment_level_ashy_top">
@else
<div class="payment_level_normal">
    <ul>
        <li class="payment_level_normal_top">
@endif

            <ul>
                <li class="level_note">
                    @if($vipLevel == $thisLevel)
                    {{Lang::get('payment.vip_level_lab_your_current_account')}}
                    @else
                    &nbsp;
                    @endif
                </li>

                <li class="level_title">{{Lang::get('payment.level_name_' . $thisLevel)}}</li>
                @if($thisLevel == 1)
                <li class="level_price">{{Lang::get('payment.vip_level_lab_basic_free')}}</li>
                <li class="level_price">&nbsp;</li>
                <li class="level_btn"></li>
                @else
                <li class="level_price"><input name="package" type="radio" value="{{$thisLevel}}" />&nbsp;{{Lang::get('payment.vip_level_lab_annual')}}&nbsp;<span>$9999</span>/y</li>
                <li class="level_price"><input name="package" type="radio" value="{{$thisLevel+1}}" />&nbsp;{{Lang::get('payment.vip_level_lab_monthly')}}&nbsp;<span>$999</span>/m</li>
                <li class="level_btn">
                    @if($vipLevel < $thisLevel)
                    <input id="btn_16" class="btn_blue_22" value="{{Lang::get('payment.vip_level_btn_upgrade')}}" name="" type="submit" />
                    @else
                    <input id="btn_16" class="btn_blue_22" value="{{Lang::get('payment.vip_level_btn_upgrade')}}" name="" type="submit" />
                    @endif
                </li>
                @endif
            </ul>
        </li>

        <li class="payment_level_ashy_content">&nbsp;</li>
        <li class="payment_level_ashy_content"><br><br>1</li>
        <li class="payment_level_ashy_content"><br>10</li>
        <li class="payment_level_ashy_content">1</li>
        <li class="payment_level_ashy_content"><br>1</li>
        <li class="payment_level_ashy_content"><br>1</li>
        <li class="payment_level_ashy_content"><br>No</li>
        <li class="payment_level_ashy_content">1</li>

    </ul>
</div>