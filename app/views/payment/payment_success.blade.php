@extends('common.master')

@section('js')
<script>
    $(document).ready(function(){
        var optEvents = function(){
            return {
                init: function(){
                    var newVipName = $("#upgradedVIPName").val();
                    $("#pop_account div.navi_s_p_300_title2").eq(2).children("span").text(newVipName);
                },
                turnToHomePage: function(){
                    $("#btnTurnToHome").click(function(){
                        location.href = "/home";
                    });
                }
            };
        }();
        optEvents.init();
        optEvents.turnToHomePage();
    });
</script>
@stop

@section('content_left')
<div class="left_650">
    <div class="left_650_column2">
        <div class="left_650_c_title_black">Payment</div>
        <div class="left_650_c2_content">
            <div class="space20"></div>
            <input type="hidden" id="upgradedVIPName" value="{{$vip_level}}" />
            <div class="left_650_c2_c_title">Your payment of ${{$payment}} was successful.</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title500"><span>VIP-{{$vip_level}}</span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>From {{$from_time}} To {{$end_time}}</span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_detail">
                <ul>
                    <li><input type="button" id="btnTurnToHome" class="btn_blue_22" value="Back Home" /></li>
                </ul>
            </div>
        </div>
        <div class="space20"></div>
        <div class="left_top"> </div>
    </div>
</div>
@stop

@section('content_right')
@stop

@section('ads_bottom')
@stop