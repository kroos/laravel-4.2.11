@extends('common.master')

@section('js')
<script>
    $(document).ready(function(){

    });
</script>
@stop

@section('content_left')
<div class="left_650_column2">
    <div class="left_650_c_title_black">Payment Setting</div>
    <div class="left_650_c2_content">
        <div class="left_650_c2_c_title">Membership Level：{{$vip_level}}
            &nbsp;&nbsp;[<a href="/people/upgrade" style="padding:0;">Upgrade</a>]</div>
        <div class="left_650_c2_c_detail">
            <ul>
                @if(!empty($recurringOrder) && $recurringOrder->status == Order::ORDER_STATUS_RECURRING_AUTO_PAID)
                <li><div class="title">Subscription ends</div><div class="detail">Your account is set to auto-renew.</div></li>
                @elseif(!empty($recurringOrder) && $recurringOrder->status == Order::ORDER_STATUS_CANCELED && empty($order))
                <li><div class="title">Subscription ends</div><div class="detail">{{explode(' ', $recurringOrder->endtime)[0]}}</div></li>
                @elseif(!empty($order))
                <li><div class="title">Subscription ends</div><div class="detail">{{explode(' ', $order->endtime)[0]}}&nbsp;&nbsp;[<a href="/people/upgrade" style="padding:0;">Renew</a>]</div></li>
                @else
                <li><div class="title">Subscription ends</div><div class="detail">---</div></li>
                @endif
                @if(empty($recurringOrder) || $recurringOrder->status == Order::ORDER_STATUS_CANCELED)
                <li>
                    <div class="title">Auto-renew</div>
                    <div class="detail">Disabled</div>
                </li>
                @else
                <li>
                    <div class="title">Auto-renew</div>
                    <div class="detail">Enabled&nbsp;&nbsp;[<a href="/payment/auto_billing/cancel" style="padding:0;">Disable</a>]</div>
                </li>
                @endif
                <li>
                    <div class="title">Payment Details</div>
                    <div class="detail">{{$paymentDetailInfo}}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="space20"></div>
    <div class="left_top">
        <div class="left_top_icon"><img src="/imgs/icon/icon_top.png" align="absmiddle" />&nbsp;<a href="#">Top</a></div>
    </div>
</div>
@stop

@section('content_right')
@stop

@section('ads_bottom')
@stop