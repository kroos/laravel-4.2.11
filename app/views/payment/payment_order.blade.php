@extends('common.master')

@section('js')
{{HTML::script('js/date.js')}}

<script type="text/javascript">

    $(document).ready(function() {

        var optEvents = function(){
            var monthCount = $("input[name='monthCount']").val();
            var singlePrice = $("input[name='singlePrice']").val();
            var discount = $("input[name='discount']").val();
            var startDate = $("#effectiveStartDate").text();

            var getFormattedDatePart = function(datePart){
                  console.log("getFormattedDatePart datePart: " + datePart);
                  if(datePart < 10){
                      return '0' + datePart;
                  }else{
                      return datePart;
                  }
            };

            var computeEndDate = function(startDate, monthCount){
                var startDate = startDate.replace(/-/gm, '/');
                var beginDate = new Date(startDate);
                console.log("beginDate.getYear: " + beginDate.getYear());
                beginDate.setMonth(beginDate.getMonth() + monthCount);
                console.log("beginDate.getYear 2: " + beginDate.getYear());
                var currentDate = parseInt(beginDate.getFullYear()) + '-' + getFormattedDatePart(parseInt(beginDate.getMonth()) + 1) + '-' + getFormattedDatePart(parseInt(beginDate.getDate()));
                var endDate = new Date(currentDate);
                var yesterday_milliseconds = endDate.getTime() - 1000 * 60 * 60 * 24;
                endDate.setTime(yesterday_milliseconds);
                console.log("endDate: " + endDate);
                return parseInt(endDate.getFullYear()) + '-' + getFormattedDatePart(parseInt(endDate.getMonth()) + 1) + '-' + getFormattedDatePart(parseInt(endDate.getDate()));
            };

            var formatNumber = function(num, precision, separator){
                var parts;
                // 判断是否为数字
                if (!isNaN(parseFloat(num)) && isFinite(num)) {
                    // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
                    // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(num))
                    // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
                    // 的值变成了 12312312.123456713
                    num = Number(num);
                    // 处理小数点位数
                    num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
                    // 分离数字的小数部分和整数部分
                    parts = num.split('.');
                    // 整数部分加[separator]分隔, 借用一个著名的正则表达式
                    parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

                    return parts.join('.');
                }
                return NaN;
            }

            return {
                init: function(){
//                    var totalAmount = monthCount;
//                    var totalPrice = singlePrice * totalAmount * (1 - discount);
                    $("#totalPrice").text(0.00);
                    $("input[name='amount']").text(1);
                    // transiently comment auto billing function
//                    var isAutoRenew = $("input[name='isAutoRenew']");
//                    isAutoRenew.attr("checked", "checked");
//                    isAutoRenew.val(1);

                    //$("input[name='amount']").val(1);
                },
                changeQuantity: function(){
                    $("input[name='sel_quantity']").change(function() {
                        var quantity = $("input[name='sel_quantity']:checked").val();
                        var price = $("#pid").val();
                        var amount = quantity * 3 * price;

                        amount = parseFloat(amount).toFixed(2);

                        sd = Date.today();
                        ed = Date.today();
                        ed.addMonths( quantity * 3 );
                        ed.addDays(-1);

                        var time_span = 'From ' + sd.toString('yyyy-MM-dd')
                                + ' To ' + ed.toString('yyyy-MM-dd');

                        $('#time_span').text( time_span );
                        $('#total_amount').text( '$' + amount );

                    });
                },
                avoidCharPaste: function(){
                    $("input[name='amount']").on("afterpaste", function(){
                        $(this).val($(this).val().replace(/\D/g,''));
                    });
                },
                checkRenew: function(){
                    $("input[name='isAutoRenew']").click(function(){
                        var autoRenew = $("input[name='isAutoRenew']");
                        console.log("autoRenew checked: " + autoRenew.is(":checked"));
                        if(autoRenew.is(":checked")){
                            autoRenew.val(1);
                        }else{
                            autoRenew.val(0);
                        }
                    });
                },
                computeTotalPrice: function(){
                    $("input[name='amount']").keyup(function(){
                        $(this).val($(this).val().replace(/\D/g,''));
                        var itemAmount = $(this).val();
                        if(itemAmount != '' && !isNaN(itemAmount)){
                            var totalAmount = monthCount * itemAmount;
                            var totalPrice = singlePrice * totalAmount * (1 - discount);
                            $("#totalPrice").text(formatNumber(totalPrice, 2, ','));

                            // compute date
                            var endDate = computeEndDate(startDate, totalAmount);
                            $("#effectiveEndDate").text(endDate);
                            $("input[name='endDate']").val(endDate);
                            $("input[name='quantity']").val(itemAmount);
                        }else{
                            $("#totalPrice").text(0.00);
                            $("#effectiveEndDate").text(startDate);
                            $("input[name='endDate']").val(startDate);
                            $("input[name='quantity']").val(0);
                        }
                    });
                },
                confirmOrder: function(){
                    $("#btnConfirmOrder").click(function(){
                        var amount = $("input[name='amount']").val();
                        var leastUpgradeAmount = $("input[name='leastUpgradeAmount']").val();
                        var totalPrice = $("#totalPrice").text();
                        console.log("confirmOrder: " + amount + "-" + totalPrice);
                        if(amount == '' || amount == 0 || totalPrice == '' || totalPrice == 0){
                            return false;
                        }
                        console.log("compare: " + parseFloat(amount) + "-" + parseFloat(leastUpgradeAmount));
                        if(parseFloat(amount) < parseFloat(leastUpgradeAmount)){
                            //$("#noticeMsg").text("Please input a quantity over than least purchase quantity.");
                            $("input[name='amount']").focus();
                            return false;
                        }
                    });
                },
                cancelOrder: function(){
                    $("#btnCancel").click(function(){
                        location.href = "/people/upgrade";
                    });
                }
            };
        }();

        optEvents.init();
        optEvents.changeQuantity();
        optEvents.avoidCharPaste();
        optEvents.checkRenew();
        optEvents.computeTotalPrice();
        optEvents.confirmOrder();
        optEvents.cancelOrder();
    });

</script>
@stop

@section('content_left')

<form id="orderForm" method="post" action="/payment/step2">

    <input type="hidden" name="package" value="{{$package['id']}}" />
    <input type="hidden" name="vipLevel" value="{{$vipLevel}}" />
    <input type="hidden" name="nid" value="{{$nid}}" />
    <input type="hidden" name="discount" value="{{$discount}}" />
    <input type="hidden" name="startDate" value="{{$startDate}}" />
    <input type="hidden" name="endDate" value="{{$endDate}}" />
    <input type="hidden" name="purchaseType" value="{{$purchaseType}}" />
    <input type="hidden" name="payType" value="{{PAYMENT_PAY_TYPE_ONETIME}}" />
    <input type="hidden" name="quantity" />

    <div class="left_650_column2">
        <div class="left_650_c_title_black">{{Lang::get('payment.step1_lab_payment')}}</div>
        <div class="left_650_c2_content">
            <div class="space20"></div>
            <div class="left_650_c2_c_title">{{Lang::get('payment.step1_lab_fill_orders')}}</div>
            <div class="left_650_c2_c_detail">
                <input type="hidden" name="monthCount" value="{{$package['month_count']}}" />
                <input type="hidden" name="singlePrice" value="{{$package['price']}}" />
                <input type="hidden" name="leastUpgradeAmount" value="{{$leastUpgradeAmount}}" />
                <ul>
                    <li>
                        <div class="title500"><span>Purchased item:</span>  {{$package['level']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Unit price:</span> ${{$package['price'] * $package['month_count'] * (1 - $package['discount'])}} / {{$package['period']}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    @if($upgraded)
                    <li>
                        <div class="title500"><span>Current Membership Plan remaining:</span> {{$restPeriod}} {{$period}}(s)</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500"><span>Minimum order required to upgrade Membership Plan:</span> {{$leastUpgradeAmount}}</div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    @endif
                    <li>
                        <div class="title500"><span>Order quantity:</span> <input class="input30" name="amount" type="text" /></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li>
                        <div class="title500">
                            <span id="effectivePeriod">From <span id="effectiveStartDate">{{$startDate}}</span> To <span id="effectiveEndDate">{{$endDate}}</span></span>
                        </div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                    <li style="display: none;">
                        <div class="title600_weight"><span>Auto-renew:</span>&nbsp;<input name="isAutoRenew" type="checkbox" value="0" />&nbsp;<span class="red_word">I accept <a href="#">Payment Policy</a>. Future payments will be charged automatically to your credit card or PayPal.</span></div>
                    </li>
                </ul>
            </div>
            <div class="left_650_c2_c_title">{{Lang::get('payment.step1_lab_total_due')}}</div>
            <div class="left_650_c2_c_detail">
                <ul>
                    <li>
                        <div class="title500">$<span id="totalPrice"></span></div>
                        <div class="detail100">&nbsp;</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="left_650_c2_content">
            <div class="left_650_c2_c_detail">
                <ul>
                    <li><input type="submit" id="btnConfirmOrder" class="btn_blue_22" value="{{Lang::get('payment.step1_btn_create_order')}}" />&nbsp;&nbsp;
                        <input type="button" id="btnCancel" class="btn_gray_22" value="{{Lang::get('payment.step1_btn_cancel')}}" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="space20"></div>
        <div class="left_top"> </div>
    </div>
</form>
@stop

@section('content_right')

@stop

@section('ads_bottom')
@stop