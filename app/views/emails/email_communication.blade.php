@extends('emails.common.master')

@section('content')
<div class="email_main">
    <div class="email_name_title">
        {{Lang::get('communication.email_title', array('name'=> $receiverName))}}:
    </div>
    <div class="email_content">
        <div class="word_big_blue">
            {{Lang::get('communication.email_welcome')}}
            <span class="tradove"><a href="{{BASE_URL}}">{{Lang::get('communication.email_tradove')}}</a></span>!
        </div>
        <div class="word_mid_gray">
            {{$content}}
        </div>
        <div class="space20">&nbsp;</div>
        <div class="btn_center">
            <a href="{{$visitUrl}}">
                <input type="button" class="btn_blue190x60" value="{{$btn_value}}"/>
            </a>
        </div>
        <div class="space30">&nbsp;</div>
        <div class="word_s_gray">
        </div>
        <div class="space20">&nbsp;</div>
        <hr class="hr100">
        @include('emails.common.footer_address')
    </div>
    <div class="space40">&nbsp;</div>
</div>

@stop