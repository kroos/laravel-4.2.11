<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TraDove</title>
    <style type="text/css">
        body {font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;}
        a:link {color:#4e84a9; text-decoration:none;}
        a:visited {color:#4e84a9;text-decoration:none;}
        a:hover {color: #4e84a9;text-decoration:underline;}
        a:active {color: #4e84a9;text-decoration: none;}
    </style>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="200px" align="left"><img src="{{BASE_URL}}imgs/email/email_logo.gif" /></td>
                    <td width="400px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="594" border="0" cellpadding="0" cellspacing="3" bgcolor="#95aab8">
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table width="594" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="10px" bgcolor="#f2f2f2"></td>
                                        </tr>
                                        <tr>
                                            <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$receiverName}}:</td>
                                        </tr>
                                        <tr>
                                            <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif" /></td>
                                        </tr>
                                    </table>
                                    <table width="594" border="0" cellpadding="0" cellspacing="20">
                                        <!-- tr>
                                            <td align="left" style="color:#666;font-size:16px;line-height:24px;"><a href="#">TraDove</a> member User Name is looking at your company, your products or services!</td>
                                        </tr -->
                                        <tr>
                                            <td align="left" style="color:#666;font-size:12px;line-height:18px;">
                                                {{$updates}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="middle" height="80px">
                                                <a href="{{$visitUrl}}">
                                                    <!-- img src="{{BASE_URL}}imgs/email/btn_click_to_join_tradove.gif" / -->
                                                    <img src="{{$imageUrl}}" />
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color:#999;font-size:12px;line-height:14px;">{{$senderInfo}}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#666;font-size:12px;line-height:14px;">Yours sincerely,</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#666;font-size:12px;line-height:14px;">TraDove Customer Service Team</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="15px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#666;font-size:12px;line-height:14px;"><a href="mailto:customer.service@tradove.com">customer.service@tradove.com</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                                                            <a href="http://www.tradove.com">www.tradove.com</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="40px"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="10px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="color:#666;font-size:12px;line-height:18px;text-align:left;">&nbsp;&nbsp;{{Lang::get("common.footer_lab_copyright")}}</td>
                </tr>
                <tr>
                    <td colspan="3" height="30px">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>