@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$name}}:</td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif" /></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">Thank you for your interest in joining <a href="{{BASE_URL}}">TraDove!</a> </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">Your registration information was submitted for review. We'll notify you of the result via email within 24-48 hours.</td>
    </tr>
    <tr>
        <td align="left" style="color:#999;font-size:12px;line-height:14px;">PS: Please add <a href="mailto:customer.service@tradove.com">{{MANAGER_EMAIL_ADDRESS}}</a> to your address book to ensure timely communication from TraDove.</td>
    </tr>
    <tr>
        <td>
            @include('emails.common.footer_address')
        </td>
    </tr>
</table>
@stop