@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2"
            style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear
            {{$name}},
        </td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img
                src="{{BASE_URL}}imgs/email/email_username_bottom.gif"/></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Congratulations! Your application to join TraDove has been approved. Please click the URL to activate your account.
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td align="center" valign="middle" height="80px">
            <a href="{{$active_url}}">
                <img src="{{BASE_URL}}imgs/email/btn_activate_account.gif" border="0"/>
            </a>
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#999;font-size:12px;line-height:14px;">We're so glad you've chosen to join TraDove, the premier B2B social network, which connects corporate buyers and sellers of all sizes for global and domestic trade.</td>
    </tr>
    <tr>
        <td>
            @include('emails.common.footer_address')
        </td>
    </tr>
</table>
@stop