@extends('emails.common.master')

@section('content')

<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear
            {{$name}}:
        </td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif"/></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Welcome to <a href="{{BASE_URL}}">TraDove!</a>
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We're sorry, but we could not register you as a member. We cannot verify that you work for a valid company.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#999;font-size:12px;line-height:14px;">
            We apologize for the inconvenience. Thank you for using TraDove.
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">Yours sincerely,</td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">TraDove Customer Service Team</td>
                </tr>
                <tr>
                    <td height="15px"></td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;"><a href="mailto:customer.service@tradove.com">customer.service@tradove.com</a></td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                        <a href="http://www.tradove.com">www.tradove.com</a>
                    </td>
                </tr>
                <tr>
                    <td height="40px"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

@stop