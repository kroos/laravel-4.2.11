@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Thank you for your interest in joining TraDove Business Network! Your registration information has been submitted for review. We'll notify you of the result via email within 24-48 hours.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            PS: Please add {{MANAGER_EMAIL_ADDRESS}} to your address book to ensure timely communication from TraDove.
        </td>
    </tr>

@stop