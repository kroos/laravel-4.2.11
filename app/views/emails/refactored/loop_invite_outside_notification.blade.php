@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has invited you to join their Loop on TraDove Business Network. Please follow the link to view on TraDove:
        </td>
    </tr>
@stop

@section('content_down')
@include('emails.common.email_tradove_desc2')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We look forward to seeing you.
        </td>
    </tr>
@stop