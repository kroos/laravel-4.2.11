@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Congratulations! Your application to join TraDove has been approved. Please click the link below to activate your account.
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We're so glad you've chosen to join TraDove, the premier Business Network, which connects corporate buyers, sellers, others, their products/services and companies.
        </td>
    </tr>


@stop
