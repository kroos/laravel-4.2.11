@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has invited you to join their business group ({{$groupName}}) on TraDove B2B social network. Click the button below to view.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Group Name: {{$groupName}}<br />
            Group Description: {{$groupDesc}}
        </td>
    </tr>

    @include('emails.common.email_tradove_desc')
    @include('emails.common.email_group_desc')

@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We look forward to seeing you.
        </td>
    </tr>
@stop