@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            There is a new reply to your group posting. Click the button below to view.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Group Posting Subject: {{$postingTitle}}
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
    @include('emails.common.email_group_desc')
@stop