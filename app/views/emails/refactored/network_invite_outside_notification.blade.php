@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has invited you to join their private social network on TraDove B2B social network. Click the button below to join.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Private Network Name: {{$privateNetworkName}}<br />
            Private Network Description: {{$privateNetworkDesc}}
        </td>
    </tr>

    @include('emails.common.email_tradove_desc')
    @include('emails.common.email_network_desc')
@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We look forward to seeing you.
        </td>
    </tr>
@stop