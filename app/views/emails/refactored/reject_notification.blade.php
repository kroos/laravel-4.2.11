@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Thank you for your interest in joining TraDove Business Network!
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We’re sorry, but we are unable to approve you as a member. We could not verify that you work for a valid company.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We apologize for the inconvenience and thank you for your interest in TraDove.
        </td>
    </tr>

@stop