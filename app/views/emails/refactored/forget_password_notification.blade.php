@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:14px;">Greetings from TraDove!</td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:14px;">In order to reset your password, please click the link below:</td>
    </tr>

@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:14px;">The link will expire after 24 hours.</td>
    </tr>
@stop
