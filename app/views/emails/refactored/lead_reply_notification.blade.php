@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            There is a new reply to the lead you posted. Click the button below to view.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Lead Subject: {{$leadTitle}}
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
@stop