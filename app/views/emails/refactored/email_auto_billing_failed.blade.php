@extends('emails.common.email_master')

@section('content_up')
<tr>
    <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="color:#666;font-size:13px;line-height:18px;">Attempts to auto-renew your TraDove subscription have failed. To prevent a lapse in your premium Membership Plan, please sign into www.tradove.com and update your account details.</td>
            </tr>
            <tr>
                <td style="height:12px;"></td>
            </tr>
        </table>
    </td>
</tr>
@stop

@section('content_down')
@include('emails.common.email_tradove_desc2')
@stop