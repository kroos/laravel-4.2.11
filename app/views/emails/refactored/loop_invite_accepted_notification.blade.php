@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}&nbsp{{$senderInfo}} has accepted your Loop invitation on TraDove B2B social network. Click the button below to view.
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
@stop