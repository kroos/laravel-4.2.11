@extends('emails.common.email_master')

@section('content_up')
<tr>
    <td align="left" width="554px">
        <table width="100%" border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="color:#333;font-size:14px;line-height:18px;">Your payment details and order receipt:</td>
                        </tr>
                        <tr>
                            <td style="height:12px;"></td>
                        </tr>
                        <tr>
                            <td style="color:#333;font-size:12px;line-height:14px;">
                                <table width="100%" style="background-color:#f4f4f4;" border="0" cellpadding="10" cellspacing="0">
                                    <tr>
                                        <td align="left" valign="top" style="color:#333;font-size:14px;line-height:18px;"><span style="color:#999;font-size:12px;">TraDove ID</span></br>{{$tradove_id}}</td>
                                        <td align="left" valign="top" style="color:#333;font-size:14px;line-height:18px;" rowspan="3"><span style="color:#999;font-size:12px;">Payment details</span></br>{{$payment_detail}}</td>
                                        <td align="right" valign="top" style="color:#333;font-size:14px;line-height:18px;" rowspan="3"><span style="color:#999;font-size:12px;">Total price</span></br>${{$total_price}}</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="color:#333;font-size:14px;line-height:18px;"><span style="color:#999;font-size:12px;">Date</span></br>{{$create_time}}</td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="color:#333;font-size:14px;line-height:18px;"><span style="color:#999;font-size:12px;">Order no.</span></br>{{$order_number}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px;"></td>
                        </tr>
                        <tr>
                            <td style="color:#333;font-size:12px;line-height:14px;">
                                <table width="100%" border="0" cellpadding="10" cellspacing="0">
                                    <tr>
                                        <td align="left" style="background-color:#999999;color:#fff;font-size:14px;line-height:20px;">Purchased item</td>
                                        <td align="left" style="background-color:#999999;color:#fff;font-size:14px;line-height:20px;">Unit price</td>
                                        <td align="left" style="background-color:#999999;color:#fff;font-size:14px;line-height:20px;">Order quantity</td>
                                        <td align="right" style="background-color:#999999;color:#fff;font-size:14px;line-height:20px;">Total price</td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height:5px;"> </td>
                                    </tr>
                                    @foreach($payment_item_array as $paymentItem)
                                    <tr>
                                        <td align="left" style="color:#333;font-size:12px;line-height:20px;">{{$paymentItem->item_name}}</td>
                                        <td align="left" style="color:#333;font-size:12px;line-height:20px;">${{$paymentItem->unit_price}} per {{$paymentItem->period}}</td>
                                        <td align="left" style="color:#333;font-size:12px;line-height:20px;">{{$paymentItem->order_quantity}}</td>
                                        <td align="right" style="color:#333;font-size:12px;line-height:20px;">${{$paymentItem->single_total_price}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right" style="color:#333;font-size:12px;line-height:20px;border:1px solid #efefef;border-left:0px;">Deduction from account credit remaining</td>
                                        <td align="right" style="color:#333;font-size:12px;line-height:20px;border:1px solid #efefef;border-left:0px;border-right:0px">- ${{$paymentItem->deduction_remain}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="3" align="right" style="color:#000;background-color:#efefef;font-size:12px;line-height:20px;border:1px solid #efefef;border-left:0px;">Total</td>
                                        <td align="right" style="color:#000;background-color:#efefef;font-size:12px;line-height:20px;border:1px solid #efefef;border-left:0px;border-right:0px"> ${{$total_price}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:12px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
@stop

@section('content_down')
@include('emails.common.email_tradove_desc2')
@stop