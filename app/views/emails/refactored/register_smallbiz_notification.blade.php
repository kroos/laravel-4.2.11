@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Congratulations! Your application to join TraDove has been approved. Please click the link below to activate your account.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            You have been allocated to TraDove's Small Biz Private Network because either:<br />
            <ul>
                <li>Your registration email is not a valid company email.</li>
                <li>The company email address provided does not meet the requirements for TraDove's General B2B Network.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            In the Small Biz Network, you can experience all the functions provided by TraDove and communicate with our worldwide community of small business buyers and sellers.
        </td>
    </tr>

@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We're so glad you've chosen to join TraDove, the premier Business Network which connects corporate buyers, sellers, their products/services and companies.
        </td>
    </tr>
@stop