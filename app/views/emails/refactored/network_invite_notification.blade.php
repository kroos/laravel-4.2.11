@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has invited you to join the Private Network ({{$networkName}}). Please follow the link to view on TraDove:
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Network Name: {{$networkName}}<br />
            Network Description: {{$networkDesc}}
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc2')
    @include('emails.common.email_network_desc')
@stop