@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Greetings from TraDove!
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Please confirm the change to your registered email address by clicking the link below. By keeping your information up to date, we can provide you with the best possible user experience.
        </td>
    </tr>

@stop

@section('content_down')
    <!--  对应app和word的新模板，以前的desc模板仍然保留方便app没有涉及到的模板使用，如果以后确定这个新模板了，需要全部用这个新的desc -->
    @include('emails.common.email_tradove_desc2')
@stop
