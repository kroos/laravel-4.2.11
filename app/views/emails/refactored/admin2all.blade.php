@extends('emails.common.email_master')

@section('content_up')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$content}}
        </td>
    </tr>
@stop