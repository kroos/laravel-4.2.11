@extends('emails.common.email_master')

@section('content_up')

    @foreach($data as $one)
        <tr>
            <td align="left" style="color:#666;font-size:12px;line-height:18px;">
                You have {{$one['newMsgCount']}} new message(s) and {{$one['unreadMsgCount']}} unread message(s) in your {{$one['networkName']}} inbox.
                <br />
                <a href="{{$one['visitUrl']}}">&gt;&nbsp;Click this link to continue to TraDove.</a>
            </td>
        </tr>
    @endforeach

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
@stop