@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} would like to invite you to join their group on TraDove B2B social network. Click the button below to view the request.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Group Name: {{$groupName}}<br />
            Group Description: {{$groupDesc}}
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
    @include('emails.common.email_group_desc')
@stop