@extends('emails.common.email_master')

@section('content_up')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Your new email has been approved to access the TraDove General B2B Network. To complete the process, please click the link below.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            After you sign in, you will be directed to the TraDove General B2B Network. Here, you can meet and connect with a global community of business professionals and access a wide range of business information. You may switch between the General Network and your private network at any time.
        </td>
    </tr>
@stop

@section('content_down')
    <!--  对应app和word的新模板，以前的desc模板仍然保留方便app没有涉及到的模板使用，如果以后确定这个新模板了，需要全部用这个新的desc -->
    @include('emails.common.email_tradove_desc2')
@stop
