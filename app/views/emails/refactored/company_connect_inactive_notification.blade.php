@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has requested to connect with your company on TraDove B2B social network. Click the button below to view.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Company Name: {{$companyName}}<br />
            Company Description: {{$companyDesc}}
        </td>
    </tr>

    @include('emails.common.email_tradove_desc')

@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We look forward to seeing you.
        </td>
    </tr>
@stop