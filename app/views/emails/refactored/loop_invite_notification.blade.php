@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has invited you to join their Loop on TraDove Business Network. Please follow the link to view on TraDove:
        </td>
    </tr>

@stop

@section('content_down')
    <!--  对应app和word的新模板，以前的desc模板仍然保留方便app没有涉及到的模板使用，如果以后确定这个新模板了，需要全部用这个新的desc -->
    @include('emails.common.email_tradove_desc2')
@stop