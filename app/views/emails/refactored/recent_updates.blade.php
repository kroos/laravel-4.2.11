@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            The following are recent updates from your connected people, products/services and companies on TraDove B2B social network.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            @foreach($updatedItems as $item)
                {{$item}}<br />
            @endforeach
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Click the button below to view.
        </td>
    </tr>

@stop
