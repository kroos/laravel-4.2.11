@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} has accepted your group invitation on TraDove B2B social network. Click the below button to view.
        </td>
    </tr>

@stop

@section('content_down')
    @include('emails.common.email_tradove_desc')
    @include('emails.common.email_group_desc')
@stop