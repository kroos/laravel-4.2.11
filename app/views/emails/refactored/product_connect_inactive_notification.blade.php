@extends('emails.common.email_master')

@section('content_up')

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            {{$senderName}}{{$senderInfo}} is interested in your products/services on TraDove B2B social network. Click the button below to accept their connection.
        </td>
    </tr>

    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Products/Services Name: {{$productName}}<br />
            Products/Services Description: {{$productDesc}}
        </td>
    </tr>

    @include('emails.common.email_tradove_desc')

@stop

@section('content_down')
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            We look forward to seeing you.
        </td>
    </tr>
@stop