@extends('emails.common.master')

@section('content')
<div class="email_main">
    <div class="email_name_title">Dear {{$customerName}}:</div>
    <div class="email_content">
        <div class="word_big_blue">Welcome to <span class="tradove"><a href="BASE_URL">TraDove</a></span>!</div>
        <div class="word_mid_gray">Please click the button below to reset your password.</div>
        <div class="space20">&nbsp;</div>
        <div class="btn_center">
            <a href="{{ URL::to('password/reset', array($token)) }}">
                <input type="button" class="btn_blue190x60" value="Reset  Password"/>
            </a>
        </div>
        <div class="space20">&nbsp;</div>
        <hr class="hr100">
        @include('emails.common.footer_address')
    </div>
    <div class="space40">&nbsp;</div>
</div>
@stop