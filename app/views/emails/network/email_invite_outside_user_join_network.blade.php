@extends('emails.common.master')

@section('content')

<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <!--
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">
        </td>
        -->
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
        Dear {{$receiverFirstName}}:
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            I would like to invite you to join a private network on TraDove.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">This B2B commercial service is provided by TraDove to large companies, to help buyers and sellers find information faster and easier.
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Network name:{{$networkName}}
        </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Description:{{$networkDesc}}
        </td>
    </tr>
    @if(!empty($companyName))
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            Owner:{{$companyName}}
        </td>
    </tr>
    @endif

    <tr>
        <td align="center" valign="middle" height="80px"><a href="{{BASE_URL}}"><img
                    src="{{BASE_URL}}imgs/email/btn_click_to_join_tradove.gif" border="0"/></a></td>
    </tr>
    <tr>
        <td align="left" style="color:#999;font-size:12px;line-height:14px;">We look forward to seeing you. There is no cost and you can quickly, easily register as a buyer or seller.
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;">
                    </td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">{{$from_name}}</td>
                </tr>
                <tr>
                    <td height="40px"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@stop