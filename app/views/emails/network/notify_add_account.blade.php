@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$userName}}:</td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif" /></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">Your partner {{$adminUserName}} has added you into their TraDove private network.</td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">This private network is a B2B commercial service provided by TraDove for large companies, to help buyers and sellers find information faster and easier.</td>
    </tr>
    <tr>
        <td align="center" valign="middle" height="80px"><a href="{{BASE_URL}}"><img src="{{BASE_URL}}imgs/email/btn_click_go_to_tradove.gif" border="0" /></a></td>
    </tr>
    <tr>
        <td align="left" style="color:#999;font-size:12px;line-height:14px;">
            Network name:{{$networkName}}<br />
            Network Type:{{$networkType}}<br />
            Description:{{$networkDesc}}<br />
            Owner:{{$companyName}}
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">{{$adminUserName}} {{$companyName}}</td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">TraDove Customer Service Team</td>
                </tr>
                <tr>
                    <td height="15px"></td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">{{MANAGER_EMAIL_ADDRESS}}</td>
                </tr>
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                        <a href="http://www.tradove.com">www.tradove.com</a>
                    </td>
                </tr>
                <tr>
                    <td height="40px"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@stop