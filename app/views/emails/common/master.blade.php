<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>TraDove</title>
    <style type="text/css">
        a:link {
            color: #436c87;
            text-decoration: none;
        }

        a:visited {
            color: #436c87;
            text-decoration: none;
        }

        a:hover {
            color: #436c87;
            text-decoration: underline;
        }

        a:active {
            color: #436c87;
            text-decoration: none;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);
            background-repeat: repeat;
        }

    </style>
</head>


<body style="font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;">
    <tr>
        <td align="center">
            <table width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="200px" align="left"><img src="{{BASE_URL}}imgs/email/email_logo.gif"/></td>
                    <td width="400px">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table width="594" border="0" cellpadding="0" cellspacing="3" bgcolor="#95aab8">
                            <tr>
                                <td bgcolor="#ffffff">
                                    @yield('content')
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" height="10px"></td>
                </tr>

                <tr>
                    <td colspan="3" style="color:#666;font-size:12px;line-height:18px;text-align:left;">
                        &nbsp;&nbsp;{{Lang::get("common.footer_lab_copyright")}}
                    </td>
                </tr>

                <tr>
                    <td colspan="3" height="30px">&nbsp;</td>
                </tr>

            </table>
        </td>
    </tr>
</table>

</body>

</html>