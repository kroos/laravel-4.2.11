{{-- For refactoring all the email templates --}}

<tr>
    <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
            </tr>
            <tr>
                <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                    @lang("email.temp_master.lab.sincerely")
                </td>
            </tr>
            <tr>
                <td height="15px"></td>
            </tr>
            <tr>
                <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                    @lang("email.temp_master.lab.service_team")
                </td>
            </tr>
            <tr>
                <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                    <a href="mailto:customer.service@tradove.com">@lang("email.temp_master.lab.customer_email")</a>
                </td>
            </tr>
            <tr>
                <td align="left" style="color:#666;font-size:12px;line-height:14px;">
                    <a href="http://www.tradove.com">www.tradove.com</a>
                </td>
            </tr>
            <tr>
                <td height="40px"></td>
            </tr>
        </table>
    </td>
</tr>