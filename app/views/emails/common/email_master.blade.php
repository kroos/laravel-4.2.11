<!DOCTYPE html>
<html lang="en-US">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TraDove</title>

    <style type="text/css">
        body{
            font-family: Arial, Helvetica, sans-serif;
            background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);
            background-repeat: repeat;
        }
        a:link {color:#4e84a9; text-decoration:none;}
        a:visited {color:#4e84a9;text-decoration:none;}
        a:hover {color: #4e84a9;text-decoration:underline;}
        a:active {color: #4e84a9;text-decoration: none;}
    </style>

</head>

<body style="font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif;background-image: url({{BASE_URL}}imgs/email/email_body_bg.gif);background-repeat: repeat;">
    <tr>
        <td align="center">
            <table width="600" border="0" cellpadding="0" cellspacing="0">

                <tr>
                    <td width="200px" align="left">
                        <img src="{{BASE_URL}}imgs/email/email_logo.gif" />
                    </td>
                    <td width="400px">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table width="594" border="0" cellpadding="0" cellspacing="3" bgcolor="#95aab8">
                            <tr>
                                <td bgcolor="#ffffff">

                                    {{-- Email Header --}}
                                    <table width="594" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="10px" bgcolor="#f2f2f2"></td>
                                        </tr>

                                        <tr>
                                            <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;{{Lang::get('email.temp_master.lab.title_dear', array('name'=> $receiverName))}},
                                            </td>
                                        </tr>

                                        <tr>
                                            <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif" /></td>
                                        </tr>

                                    </table>


                                    {{-- Email Body --}}
                                    <table width="594" border="0" cellpadding="0" cellspacing="20">

                                        {{-- Content of Up Part --}}
                                        @yield('content_up')

                                        @if(!empty($visitUrl))
                                        {{-- Image Button Link --}}
                                        <tr>
                                            <td align="left" style="font-size:14px;line-height:18px;">
                                                <a style="font-size:12px;" href="{{$visitUrl}}">
                                                    @if(!empty($imageUrl))
                                                    <img src="{{$imageUrl}}" border="0">
                                                    @endif
                                                    @if(!empty($linkLabel))
                                                    {{$linkLabel}} >>
                                                    @endif
                                                </a>
                                            </td>
                                        </tr>
                                        @endif

                                        {{-- Content of Down Part --}}
                                        @yield('content_down')

                                        @include("emails.common.email_footer")

                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" height="10px"></td>
                </tr>

                @include("emails.common.email_copyright")

                <tr>
                    <td colspan="3" height="30px">&nbsp;</td>
                </tr>

            </table>
        </td>

    </tr>

</table>

</body>
</html>
