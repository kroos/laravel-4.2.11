<tr>
    <td align="left" style="color:#666;font-size:12px;line-height:18px;">
        TraDove connects a worldwide business community of high-quality corporate buyers, sellers, other professionals, their products/services and companies through effective B2B social networking tools. Shorten buying and selling cycles and facilitate business transactions with expediency and transparency on TraDove.
    </td>
</tr>