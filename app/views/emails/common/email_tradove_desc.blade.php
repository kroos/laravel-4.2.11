<tr>
    <td align="left" style="color:#666;font-size:12px;line-height:18px;">
        TraDove connects corporate buyers, sellers, other business professionals, their products/services and companies through effective B2B social networking tools. TraDove shortens buying and selling cycles and increases information transparency.
    </td>
</tr>