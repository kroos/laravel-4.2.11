@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$userName}}:</td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif" /></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">Welcome to <a href="{{BASE_URL}}">TraDove!</a> </td>
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">Please click the button to confirm the change to your account's registered email address.</td>
    </tr>
    <tr>
        <td align="center" valign="middle" height="80px"><a href="{{BASE_URL}}confirmAccount?chgid={{$activeId}}"><img src="{{BASE_URL}}imgs/email/btn_change_your_email.gif" border="0" /></a></td>
    </tr>
    <tr>
        <td>
            @include('emails.common.footer_address')
        </td>
    </tr>
</table>
@stop