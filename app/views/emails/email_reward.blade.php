<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TraDove</title>
    <style type="text/css">
        <!--
        body {font-family: Arial, Helvetica, sans-serif;background-image: url(https://www.tradove.com/imgs/email/email_body_bg.gif);background-repeat: repeat;}
        a:link {color:#4e84a9; text-decoration:underline;}
        a:visited {color:#4e84a9;text-decoration:underline;}
        a:hover {color: #4e84a9;text-decoration:none;}
        a:active {color: #4e84a9;text-decoration: underline;}
        -->
    </style>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;background-image: url(https://www.tradove.com/imgs/email/email_body_bg.gif);background-repeat: repeat;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="200px" align="left"><img src="https://www.tradove.com/imgs/email/email_logo.gif" /></td>
                    <td width="400px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="594" border="0" cellpadding="0" cellspacing="3" bgcolor="#95aab8">
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table width="594" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="10px" bgcolor="#f2f2f2"></td>
                                        </tr>
                                        <tr>
                                            <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$fName}}:</td>
                                        </tr>
                                        <tr>
                                            <td height="auto" bgcolor="#f2f2f2"><img src="https://www.tradove.com/imgs/email/email_username_bottom.gif" border="0" /></td>
                                        </tr>
                                    </table>
                                    <table width="594" border="0" cellpadding="0" cellspacing="20">
                                        <tr>
                                            <td align="left" style="color:#555;font-size:14px;line-height:24px;">As the best global B2B social network linking  corporate buyers, sellers, their products/services, and companies, we aim to provide the best B2B social-networking services to users like you so you can shorten the cycle to find the right trusted buyers and sellers, network with them, and do even more. To benefit more buyers/sellers  like you, we have started a user invitation reward program. You can now use our system to invite your business contacts by email or LinkedIn. Every week, we will select one buyer and one seller with the most invitations and award him/her $50 in cash.</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color:#555;font-size:14px;line-height:24px;">To invite your contacts, you can login to TraDove with your company email and click the email and LinkedIn invite links on the right hand side of the home page. Please note you can only invite contacts with their business emails. Also, due to LinkedIn limitation, you can only invite 10 contacts a day. For more reward program detail, just  go to   <a href="https://www.tradove.com/user_reward">https://www.tradove.com/user_reward</a>.  Once selected, you will automatically become member of our user group so you can get the priority to try our new features and we can learn more about your needs.</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color:#555;font-size:14px;line-height:24px;">Thank you so much,</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#555;font-size:14px;line-height:14px;">Solar Qi</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#555;font-size:14px;line-height:14px;">User Reward Manager</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#555;font-size:14px;line-height:14px;">TraDove, Inc.</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="color:#555;font-size:14px;line-height:14px;">solar.qi@tradove.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20px"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="10px"></td>
                </tr>
                <tr>
                    <td colspan="3" style="color:#666;font-size:12px;line-height:18px;text-align:left;">&nbsp;Copyright &copy; 2010, 2016 TraDove, Inc. All rights reserved.</td>
                </tr>
                <tr>
                    <td colspan="3" height="30px">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>