@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="10px" bgcolor="#f2f2f2"></td>
    </tr>
    <tr>
        <td height="40px" align="left" bgcolor="#f2f2f2" style="color:#5d839e;font-size:18px;line-height:40px;">&nbsp;&nbsp;&nbsp;&nbsp;Dear {{$firstName}}:
        </td>
    </tr>
    <tr>
        <td height="10px" bgcolor="#f2f2f2"><img src="{{BASE_URL}}imgs/email/email_username_bottom.gif"/></td>
    </tr>
</table>
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <!--
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">
        </td>
        -->
    </tr>
    <tr>
        <td align="left" style="color:#666;font-size:12px;line-height:18px;">
            I would like to invite you to join my business loop at <a href="{{BASE_URL}}">TraDove</a> B2B Social Network. I like TraDove, because it is a business oriented social network with better credibility. It connects corporate buyers, sellers, and others together with their products/services and companies so they can do business with better convenience and efficiency. You can simply click below button to join.
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle" height="80px"><a href="{{BASE_URL}}"><img
                    src="{{BASE_URL}}imgs/email/btn_click_to_join_tradove.gif" border="0"/></a></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">TraDove Customer Service Team</td>
                </tr>

                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:14px;">{{MANAGER_EMAIL_ADDRESS_FROM}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@stop