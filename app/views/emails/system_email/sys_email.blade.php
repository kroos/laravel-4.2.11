@extends('emails.common.master')

@section('content')
<table width="594" border="0" cellpadding="0" cellspacing="20">
    <tr>
        <td align="left" style="color:#666;font-size:16px;line-height:24px;">
            Welcome to <a href="{{BASE_URL}}">TraDove!</a>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" style="color:#666;font-size:12px;line-height:30px;">
                        {{$content}}
                    </td>
                </tr>
                <tr>
                    <td height="10px"></td>
                </tr>
                <tr>
                    <td><hr style="width:100%;height:0; border:0;border-top:1px dashed #cccccc;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@stop