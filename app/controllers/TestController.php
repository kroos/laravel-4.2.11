<?php

use PayPal\EBLBaseComponents\ManageRecurringPaymentsProfileStatusRequestDetailsType;
use PayPal\PayPalAPI\ManageRecurringPaymentsProfileStatusReq;
use PayPal\PayPalAPI\ManageRecurringPaymentsProfileStatusRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

class TestController extends BaseController {

   protected  $emailApi;

    function __construct() {
        $this->emailApi = new EmailApi();
    }

   public function testIpn(){
       $ipnDetails = Input::all();
       Log::info("testIpn ipnDetails: ". json_encode($ipnDetails));

       $myPaypal = new Paypal();

       // Log the IPN results
       $myPaypal->logIpn = TRUE;

       // Enable test mode if needed
       $myPaypal->enableTestMode();

       if ($myPaypal->validateIpn()) {
           if(isset($myPaypal->ipnData['recurring'])){
               Log::info("recurring ipn logic");
           }else{
               Log::info("instant payment ipn logic");
           }
       }

       return Response::json(json_encode($ipnDetails));
   }

   /*public function changeSubscriptionStatus($profile_id, $action){
       $api_request = 'USER=' . PAYMENT_API_USERNAME
           .  '&PWD=' . PAYMENT_API_PASSWORD
           .  '&SIGNATURE=' . PAYMENT_API_SIGNATURE
           .  '&VERSION=76.0'
           .  '&METHOD=ManageRecurringPaymentsProfileStatus'
           .  '&PROFILEID=' . urlencode( $profile_id )
           .  '&ACTION=' . urlencode( $action )
           .  '&NOTE=' . urlencode( 'Profile cancelled at store' );

       $ch = curl_init();
       curl_setopt( $ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp' ); // For live transactions, change to 'https://api-3t.paypal.com/nvp'
       curl_setopt( $ch, CURLOPT_VERBOSE, 1 );

       // Uncomment these to turn off server and peer verification
       // curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
       // curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
       curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
       curl_setopt( $ch, CURLOPT_POST, 1 );

       // Set the API parameters for this transaction
       curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );

       // Request response from PayPal
       $response = curl_exec( $ch );

       // If no response was received from PayPal there is no point parsing the response
       if( ! $response )
           die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );

       curl_close( $ch );

       // An associative array is more usable than a parameter string
       parse_str( $response, $parsed_response );

       return $parsed_response;
   }*/

    public function initSubscriptionStatusChange(){
        return View::make("test.manage_recurring_payment");
    }

    public function changeSubscriptionStatus(){
        $manageRPPStatusReqestDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType();
        $manageRPPStatusReqestDetails->Action = Input::get("action");
        $manageRPPStatusReqestDetails->ProfileID = Input::get("profileID");

        $manageRPPStatusReqest = new ManageRecurringPaymentsProfileStatusRequestType();
        $manageRPPStatusReqest->ManageRecurringPaymentsProfileStatusRequestDetails = $manageRPPStatusReqestDetails;

        $manageRPPStatusReq = new ManageRecurringPaymentsProfileStatusReq();
        $manageRPPStatusReq->ManageRecurringPaymentsProfileStatusRequest = $manageRPPStatusReqest;

        $credentials = array(
            "mode" => "sandbox",
            "acct1.UserName" => PAYMENT_API_USERNAME,
            "acct1.Password" => PAYMENT_API_PASSWORD,
            "acct1.Signature" => PAYMENT_API_SIGNATURE
        );

        $paypalService = new PayPalAPIInterfaceServiceService($credentials);

        /* wrap API method calls on the service object with a try catch */
        $manageRPPStatusResponse = $paypalService->ManageRecurringPaymentsProfileStatus($manageRPPStatusReq);
        return json_encode($manageRPPStatusResponse);
    }

   public function setTestSession(){
       Session::put('uid', '270632');
   }

    public function testBillingMail(){
        $paymentItems = array();
        $paymentItem = new stdClass();
        $paymentItem->item_name = 'VIP - Gold';
        $paymentItem->unit_price = 50;
        $paymentItem->period = 'month';
        $paymentItem->order_quantity = 3;
        $paymentItem->single_total_price = 150;
        $paymentItem->deduction_remain = 0;
        $paymentItem->payments = 150;
        $totalPrice = 150;

        array_push($paymentItems, $paymentItem);

        $mailData = array(
            'receiverName' => 'siwei',
            'tradove_id' => 'kroos.long@tradove.net',
            'payment_detail' => 'PayPal<br />kent.yan-buyer@tradove.net',
            'total_price' => 150,
            'create_time' => '2016-08-17',
            'order_number' => 'UV-270632-e3ad2a9a3v92',
            'payment_item_array' => $paymentItems,
            'visitUrl' => BASE_URL,
            'linkLabel' => Lang::get('email.link_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING)
        );
        $user = new stdClass();
        $user->email = 'kroos.long@tradove.net';

        // 发电子账单的邮件
        //$this->emailApi->sendMailByTemplateId(EmailTemplateManager::EMAIL_PAYMENT_E_BILLING, 'lsiwei17@126.com', null, $mailData);

        $bodyData = array(
            'receiverName' => 'siwei',
        );
//
//        $this->emailApi->sendMailByTemplateId(EmailTemplateManager::EMAIL_CHANGE_REJECT_NOTIFICATION, '284832355@qq.com', null, $bodyData);
        Mail::send('emails.refactored.email_e_bills', $mailData, function ($message) {
            $message->to('kroos.long@tradove.net')->subject(Lang::get('email.subject_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING));
        });
    }

    public function sendAutoBillingFailedMail(){
        $mailData = array(
            'receiverName' => 'siwei',
            'visitUrl' => BASE_URL,
            'linkLabel' => Lang::get('email.link_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED)
        );
        Mail::send('emails.refactored.email_auto_billing_failed', $mailData, function ($message) {
            $message->to('kroos.long@tradove.net')->subject(Lang::get('email.subject_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED));
        });
    }

    public function testLogBlocks(){
        Log::info("true 0");
        if(true){
            Log::info("true 1");

            if(true){
                Log::info("true 2");

                if(true){
                    Log::info("true 3");

                    if(true){
                        Log::info("true 4");
                    }
                }
            }
        }
    }
}
