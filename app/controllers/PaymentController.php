<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 14-01-27
 * To change this template use File | Settings | File Templates.
 */

use PayPal\EBLBaseComponents\ManageRecurringPaymentsProfileStatusRequestDetailsType;
use PayPal\PayPalAPI\ManageRecurringPaymentsProfileStatusReq;
use PayPal\PayPalAPI\ManageRecurringPaymentsProfileStatusRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

class PaymentController extends BaseController {

    protected $usersApi;
    protected $orderApi;
    protected $paymentDetailApi;
    protected $emailApi;

    function __construct() {
        $this->usersApi = new UsersApi();
        $this->orderApi = new OrderApi();
        $this->paymentDetailApi = new PaypalPaymentDetailApi();
        $this->emailApi = new EmailApi();
    }

    public function upgrade() {

        Log::info("upgrade started");
        $data = array();

        $user = $this->usersApi->getCurrentUser();
        Log::info("upgrade user: ".json_encode($user));
        $packages = Config::get('order.package');

        // $settingApi = new SettingApi;
        // $setting = $settingApi->getPaymentSetting();

        $data['pageTitle'] = Lang::get('payment.upgrade_lab_page_title');
        $data['vipLevel'] = $user->vip_level;
        $data['memberType'] = $user->member_type;
        // $data['setting'] = $setting;

        $data['packages'] = $packages;

        return View::make('payment.upgrade', $data);

    }


    public function upgradeStep1() {

        /**
        $vip_level = intval( $packageId / 2 ) * 2;
        $annual_or_month = $packageId % 2;

        $prices = Order::getSettingPrices();

        $prices_annual = $prices["annual"];
        $prices_month = $prices["month"];

        if( $annual_or_month == PAY_PERIOD_ANNUAL ) {
            $price = $prices_annual[$vip_level];
        } else {
            $price = $prices_month[$vip_level];
        }

        $price = number_format($price, 2, '.', '');

        $level_note = Order::$_LEVEL_TITLE[$vip_level];

        if( $annual_or_month == PAY_PERIOD_ANNUAL ) {
            $level_note .= '(Annual)';
            $price_note = '$' . $price . '/Annual';
            $subfix = 'Year';
            $amount = $price;
        } else {
            $level_note .= '(Month)';
            $price_note = '$' . $price . '/Month';
            $subfix = 'Quarter';
            $amount = $price * 3;
        }

        $amount = number_format($amount, 2, '.', '');


        $today = date(DEFAULT_DATE_FORMAT); //current date
        if( $annual_or_month == PAY_PERIOD_ANNUAL ) {
            $end_time = strtotime(date( DEFAULT_DATE_FORMAT, strtotime($today)) . "+1 year");
        } else {
            $end_time = strtotime(date(DEFAULT_DATE_FORMAT, strtotime($today)) . "+3 month");
        }

        $end_date = getDateByTimestamp( $end_time );
        $span = 'From ' . $today . ' To ' . $end_date;
        */

/*        $annualOrQuarter = 'quarter';

        if($packageId % 2 == 0) {
            $annualOrQuarter = 'annual';
        }

        $data = array();

        $user = $this->usersApi->getCurrentUser();

        $data['pageTitle'] = Lang::get('people.membership_lab_page_title');
        $data['vipLevel'] = intval( $packageId / 2 ) * 2;
        $data['annualOrQuarter'] = $annualOrQuarter;*/


        $data = array();

        $packageId = Input::get('package');

        $packageInfo = Config::get('order.package')[$packageId];

        $data['pageTitle'] = Lang::get('people.membership_lab_page_title');
        $data['package'] = $packageInfo;

        return View::make('payment.upgrade_step1', $data);

    }


    public function upgradeStep2() {

        $data = array();

        $user = $this->usersApi->getCurrentUser();

        $data['pageTitle'] = Lang::get('people.membership_lab_page_title');
        $data['vipLevel'] = $user->vip_level;

        return View::make('payment.upgrade_step2', $data);

    }

    public function step1() {

        // Join network
        $userId = Session::get("uid");
        $packageId = Input::get("package");
        $discount = Input::get("discount");
        $package = Config::get('order.package')[$packageId];
        $vipLevel = Input::get("vipLevel");
        $purchaseType = Input::get("purchaseType");
        $monthCount = $package['month_count'];
        $period = $package['period'];

        Log::info("step 1 params: " . $packageId . "|" . $discount . "|" . $vipLevel. "|" . $purchaseType . "|" . $period);

        /*$nid = Input::get("nid");
        $networkApi = new NetworkApi();

        if($packageId == Order::ORDER_PACKAGE_JOIN_NETWORK) {
            if(!$networkApi->isMemberOfCompanyWhichOwnThisNetwork(Session::get('uid'), $nid)) {
                $packageId = Order::ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER;
                $package = Config::get('order.package')[$packageId];
            }
        }

        if($nid > 0) {
            $network = $networkApi->getNetworkById($nid);
            $package['name'] = sprintf($package['name'], $network->name);
        }*/
        // 有效期
        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade')){
            $startDate = date('Y-m-d');
        }else{
            $order = $this->orderApi->getByUserIdAndTypeAndRenewTypeAndStatus($userId, PAYMENT_ORDER_VIP_TYPE, false, Order::ORDER_STATUS_PAID);
            // 查询是否一开始
            if(empty($order)){
                $order = $this->orderApi->getRecurringOrder($userId);
            }

            if(empty($order)){
                $startDate = date('Y-m-d');
            }else{
                $startDate = date('Y-m-d', strtotime('+1 day', strtotime($order->endtime)));
            }
        }
        $endDate = $startDate;

        /* $annualOrQuarter = 'quarter';
        if($packageId % 2 == 0) {
            $annualOrQuarter = 'annual';
        }*/

        $data = array();

        $data['pageTitle'] = Lang::get('payment.step1_lab_page_title');
        $data['package'] = $package;
        $data['nid'] = 0;
        $data['discount'] = $discount;
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $data['vipLevel'] = $vipLevel;
        $data['period'] = $period;
        $data['purchaseType'] = $purchaseType;

        // page parameters settings to do
        // 查询即时支付是否还存在
        $order = $this->orderApi->getByUserIdAndTypeAndRenewTypeAndStatus($userId, PAYMENT_ORDER_VIP_TYPE, false, Order::ORDER_STATUS_PAID);
        $queries = DB::getQueryLog();
        foreach ($queries as $index => $query) {
            Log::debug("step1 query " . $index . ": " . json_encode($query));
        }
        Log::info("step 1 order: ". json_encode($order));
        if(empty($order) || $order->package == PAYMENT_ORDER_VIP_FREE_PACKAGE){
            $data['upgraded'] = false;
            $data["leastUpgradeAmount"] = 1;
        }else{
            $data['upgraded'] = true;
            $now = time();
            if(!empty($order->fromtime) && !empty($order->endtime)){
                $endTimeStr = strtotime($order->endtime);
                Log::info("now $endTimeStr: " . $now . "|" . $endTimeStr);
                if($period == PAYMENT_ORDER_VIP_PERIOD_MONTH){
                    $restPeriod = number_format(($endTimeStr - $now) / (3600 * 24 * 30), 0);
                }else{
                    $restPeriod = number_format(($endTimeStr - $now) / (3600 * 24 * 365), 0);
                }
            }else{
                $restPeriod = 0;
            }

            $data["restPeriod"] = $restPeriod;
            // 计算升级剩余费用用于高等级的费用数量匹配，比如等级2剩余150，等级3单价100，这样默认应该只能填大于2的数量
            $overage = $this->orderApi->calculateOverage($userId, $packageId, false, Order::ORDER_STATUS_PAID);
            $singlePrice = $package["price"] * $package["month_count"] * (1 - $package["discount"]);
            Log::info("step 1 package id: " . $package["id"]);
            Log::info("step 1 package singlePrice: " . $singlePrice);
            Log::info("step 1 package overage: " . $overage);
            $upgradeData = $this->orderApi->judgeIfOverageOverNextLevelExpense($overage, $singlePrice, $purchaseType);

            $data["leastUpgradeAmount"] = $upgradeData->quantity;
        }

        // $data['vipLevel'] = intval( $packageId / 2 ) * 2;
        // $data['annualOrQuarter'] = $annualOrQuarter;
        $data['payType'] = PAYMENT_PAY_TYPE_ONETIME;
        $lastOrder = $this->orderApi->getLastUpgradeOrderOrRecurringOrder($userId);
        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade') && !empty($lastOrder)){
            return View::make('payment.payment_order', $data);
        }else{
            $recurringOrder = $this->orderApi->getByUserIdAndTypeAndRenewTypeAndStatus($userId, PAYMENT_ORDER_VIP_TYPE, true, Order::ORDER_STATUS_RECURRING_AUTO_PAID);
            $data["recurringOrderExists"] = empty($recurringOrder) ? false : true;
            return View::make('payment.payment_renew_order', $data);
        }

    }

    // turn to payment detail pages
    public function step2() {

        // Generate Order
        $uid = Session::get('uid');
        $packageId = Input::get("package");
        $vipLevel = Input::get("vipLevel");
        $package = Config::get("order.package")[$packageId];
        $discountRate = Input::get("discount");
        $singlePrice = Input::get("singlePrice");
        $monthCount = Input::get("monthCount");
        $quantity = Input::get("quantity");
        $startDate = Input::get("startDate");
        $endDate = Input::get("endDate");
        $discount = $singlePrice * $monthCount * $discountRate;
        $isAutoRenew = Input::get("isAutoRenew");
        // 表示升级还是renew
        $purchaseType = Input::get("purchaseType");
        // 表示onetime instant payment or auto billing payment
        $payType = Input::get("payType");
        $isAutoRenew = $payType == PAYMENT_PAY_TYPE_ONETIME ? false : true;

        $tax = 0;

        if($isAutoRenew){
            $monthCount = 1;
            $discount = 0;
            $discountRate = 0;
            $successUrl = BASE_URL. "payment/auto_billing/success";
        }else{
            $successUrl = BASE_URL . "payment/success";
        }
        $cancelUrl = BASE_URL . "people/upgrade";
        $ipnUrl = BASE_URL . "payment/ipn";


        $nid = 0;
        if($packageId == Order::ORDER_PACKAGE_JOIN_NETWORK
            || $packageId == Order::ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER
            || $packageId == Order::ORDER_PACKAGE_OF_CREATE_NETWORK) {

            $nid = Input::get("nid");
            $networkApi = new NetworkApi();

            $network = $networkApi->getNetworkById($nid);
            $package['name'] = sprintf($package['name'], $network->name);

            $cancelUrl = BASE_URL . "network/suggest";
        }

        $orderStatus = $isAutoRenew ? Order::ORDER_STATUS_RECURRING_AUTO_PAID : Order::ORDER_STATUS_PAID;

        $order = $this->orderApi->getByUserIdAndTypeAndRenewTypeAndStatus($uid, PAYMENT_ORDER_VIP_TYPE, $isAutoRenew, $orderStatus);
        if(empty($order)){
            $order = new Order();
        }


        $orderId = $this->orderApi->generateID($uid, $packageId);

        $order->orderid = $orderId;
        $order->uid = $uid;
        $order->package = $vipLevel;
        $order->nid = $nid;
        $order->price = $singlePrice;
        if($payType == PAYMENT_PAY_TYPE_ONETIME){
            $order->period = $package['period'];
            $order->is_auto_renew = 0;
        }else{
            $order->period = PAYMENT_DEFAULT_PAY_PERIOD;
            $order->is_auto_renew = 1;
        }
        $order->month_count = $monthCount;
        $order->quantity = $quantity;
        $order->description = $package['name'];
        $order->creattime = time();
        $order->fromtime = $startDate;
        $order->endtime = $endDate;
        $order->status = Order::ORDER_STATUS_NEW;
        $order->paidtime = 0;
        $order->discount = $discount;
        $order->overage = 0;
        $order->tax = $tax;
        $order->payments = 0;
        $order->txnid = "";
        $order->is_auto_renew = $isAutoRenew;
        $order->type = PAYMENT_ORDER_VIP_TYPE;
        $order->tax = 0;

        // 判断是升级还是续费
        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade')){
            $overage = $this->orderApi->calculateOverage($uid, $packageId, $isAutoRenew, $orderStatus);
            $order->overage = number_format($overage, 2, ".", "");
        }else{
            $order->overage = 0;
        }
        Log::info("step2 elems: ".$singlePrice."|".$monthCount."|".$discount."|".$quantity."|".$order->overage."|".$tax);
        $payments = ($singlePrice * $monthCount - $discount) * $quantity - $order->overage + $tax;
        if($payments < 0) {
            $payments = 0;
        }
        $order->payments = $payments;
        Log::info("step2 payments: ".$order->payments);

        $data = array();

        $data['pageTitle'] = Lang::get('payment.step2_lab_page_title');

        // $data['vipLevel'] = $user->vip_level;
        $data['order'] = $order;

        $today = date('Y-m-d');
        $timeSpan = 'From ' . $today;
        $next_year = mktime(0,0,0,date("m"),date("d")-1,date("Y")+1);
        $timeSpan .= ' To ' . date('Y-m-d', $next_year);

        $data['timeSpan'] = $timeSpan;

        $data['success_url'] = $successUrl;
        $data['cancel_url'] = $cancelUrl;
        $data['ipn_url'] = $ipnUrl;

        // 保存order信息到下个页面
        $data['package'] = $packageId;
        $data['vipLevel'] = $vipLevel;
        $data['discount'] = $discountRate;
        $data['singlePrice'] = $singlePrice;
        $data['monthCount'] = $monthCount;
        $data['amount'] = $quantity;
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $data['orderId'] = $orderId;
        $data['isAutoRenew'] = $isAutoRenew;
        $data['purchaseType'] = $purchaseType;
        $data['payType'] = $payType;
        $data['level'] = $package['level'];

        Log::info("step2 payType: ". $payType);

        if($payType == PAYMENT_PAY_TYPE_ONETIME){
            return View::make('payment.payment_payment', $data);
        }else{
            return View::make('payment.payment_payment_autorenew', $data);
        }
    }

    private function computeUsedDates($orderHistory){
        $instantHistories = $this->orderApi->getInstantOrderHistories($orderHistory->function_related_id);
        $totalFromDate = $instantHistories[0]->fromtime;
        $totalEndDate = $instantHistories[0]->endtime;
        $currentDateStr = date('Y-m-d');

        if($currentDateStr < $totalEndDate){
            $usedDates = date_diff(date_create($totalFromDate), date_create($currentDateStr))->format('%a');
            return $usedDates;
        }else{
            return 0;
        }
    }

    private function setOrderHistoryTimes($orderHistory){
//        if(empty($orderHistory->expiry_date)){
//            $orderHistory->expiry_date = $orderHistory->endtime;
//        }else{
//            $orderHistory->expiry_date = date('Y-m-d', strtotime('+' + $orderHistory->quantity + ' month', strtotime($orderHistory->expiry_date)));
//        }

        $instantOrder = $this->orderApi->getInstantOrder($orderHistory->uid);
        $recurringOrder = $this->orderApi->getRecurringOrder($orderHistory->uid);

        if(empty($instantOrder) && empty($recurringOrder)){
            $orderHistory->actual_start_date = $orderHistory->fromtime;
            $orderHistory->expiry_date = $orderHistory->endtime;
            $orderHistory->save();
        }
        if(empty($instantOrder) && !empty($recurringOrder)){
            // 已经存在自动购买，不可能再自动购买了，只能即时购买，即时升级也只能即时购买，实际时间保持一致
            $currentDate = date("Y-m-d");
            if($orderHistory->is_auto_renew == 1 && $recurringOrder->status == Order::ORDER_STATUS_CANCELED && $currentDate < $recurringOrder->endtime){
                $orderHistory->actual_start_date = date('Y-m-d', strtotime('+1 day', strtotime($recurringOrder->endtime)));
                $orderHistory->expiry_date = date('Y-m-d', strtotime('+1 month', strtotime($orderHistory->actual_start_date)));
            }else{
                $orderHistory->actual_start_date = $orderHistory->fromtime;
                $orderHistory->expiry_date = $orderHistory->endtime;
            }
            $orderHistory->save();
        }
        if(!empty($instantOrder) && empty($recurringOrder)){
            if($orderHistory->is_auto_renew == 1){
                $orderHistory->actual_start_date = $orderHistory->fromtime;
                $orderHistory->expiry_date = $orderHistory->endtime;

                // 重新计算即时付费order时间
                $originalFromDate = date_create($instantOrder->fromtime);
                $currentDate = date_create(date('Y-m-d'));
                $usedDates = date_diff($originalFromDate, $currentDate)->format('%a');

                $instantOrder->fromtime = date('Y-m-d', strtotime('+'.$orderHistory->quantity.' day', strtotime($orderHistory->expiry_date)));
                $instantOrder->endtime = date('Y-m-d', strtotime('+1 month -'.$usedDates.' day', strtotime($instantOrder->endtime)));

                $orderHistory->save();
                $instantOrder->save();
            }else{
                $orderHistory->actual_start_date = $orderHistory->fromtime;
                $orderHistory->expiry_date = $orderHistory->endtime;
                $orderHistory->save();
            }
        }
        if(!empty($instantOrder) && !empty($recurringOrder)){
            // 可能存在recurring order canceled的情况还可以自动购买
            $currentDate = date("Y-m-d");
            if($orderHistory->is_auto_renew == 1 && $recurringOrder->status == Order::ORDER_STATUS_CANCELED && $currentDate < $recurringOrder->endtime){
                $orderHistory->actual_start_date = date('Y-m-d', strtotime('+1 day', strtotime($recurringOrder->endtime)));
                $orderHistory->expiry_date = date('Y-m-d', strtotime('+1 month -1 day', strtotime($orderHistory->actual_start_date)));

                $usedDates = $this->computeUsedDates($orderHistory);

                Log::info("setOrderHistoryTimes orderHistory: ".json_encode($orderHistory));
                Log::info("setOrderHistoryTimes orderHistory: ".json_encode($instantOrder));
                $instantOrder->fromtime = date('Y-m-d', strtotime('+1 day', strtotime($orderHistory->expiry_date)));
                $instantOrder->endtime = date('Y-m-d', strtotime('+1 month -'.$usedDates.' day', strtotime($instantOrder->endtime)));
                Log::info("setOrderHistoryTimes instantOrder times: ".$instantOrder->fromtime."|".$instantOrder->endtime);

                $orderHistory->save();
                $instantOrder->save();
            }else{
                $orderHistory->actual_start_date = $orderHistory->fromtime;
                $orderHistory->expiry_date = $orderHistory->endtime;
                $orderHistory->save();
            }
        }
    }

    public function saveOrder(){
        $uid = Session::get('uid');
        $orderId = Input::get("orderId");
        $packageId = Input::get("package");
        $vipLevel = Input::get("vipLevel");
        $package = Config::get("order.package")[$packageId];
        $discountRate = Input::get("discount");
        $singlePrice = Input::get("singlePrice");
        $monthCount = Input::get("monthCount");
        $quantity = Input::get("amount");
        $startDate = Input::get("startDate");
        $endDate = Input::get("endDate");
        $discount = $singlePrice * $monthCount * $discountRate;
        // upgrade or renew
        $purchaseType = Input::get("purchaseType");
        // onetime or auto billing payment type
        $payType = Input::get("payType");
        $isAutoRenew = $payType == PAYMENT_PAY_TYPE_ONETIME ? false : true;

        Log::info("saveOrder orders: ". $startDate. "|". $endDate);

        $tax = 0;

        $nid = 0;
        if($packageId == Order::ORDER_PACKAGE_JOIN_NETWORK
            || $packageId == Order::ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER
            || $packageId == Order::ORDER_PACKAGE_OF_CREATE_NETWORK) {

            $nid = Input::get("nid");
            $networkApi = new NetworkApi();

            $network = $networkApi->getNetworkById($nid);
            $package['name'] = sprintf($package['name'], $network->name);

            $cancelUrl = BASE_URL . "network/suggest";
        }

        $user = $this->usersApi->getCurrentUser();

        $orderStatus = $isAutoRenew ? Order::ORDER_STATUS_RECURRING_AUTO_PAID : Order::ORDER_STATUS_PAID;
        $order = $this->orderApi->getByUserIdAndTypeAndRenewType($uid, PAYMENT_ORDER_VIP_TYPE, $isAutoRenew);
        Log::info("saveOrder order:". json_encode($order));
        if(empty($order)){
            $order = new Order();
            $order->status = Order::ORDER_STATUS_NEW;
            $order->orderid = $orderId;
            $order->package = $user->vip_level;
        }

        $order->uid = $uid;
        //$order->package = $vipLevel;
        $order->next_package = $vipLevel;
        // 需要在支付成功后再更新升级后的当前订单信息， 这个当前订单只是当前支付成功的订单，如果支付失败不会更新
        $order->type = PAYMENT_ORDER_VIP_TYPE;

        // 支付成功后更新价格信息
        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade')){
            $overage = $this->orderApi->calculateOverage($uid, $packageId, $isAutoRenew, $orderStatus);
        }else{
            $overage = 0;
        }

        $order->is_auto_renew = $payType == PAYMENT_PAY_TYPE_ONETIME ? 0 : 1;

        $payments = ($singlePrice * $monthCount - $discount) * $quantity - $overage + $tax;
        if($payments < 0) {
            $payments = 0;
        }

        $order->save();


        $orderHistory = new OrderHistory();
        $orderHistory->orderid = $orderId;
        $orderHistory->uid = $uid;
        $orderHistory->package = $vipLevel;
        $orderHistory->nid = $nid;
        $orderHistory->price = $singlePrice;
        $orderHistory->period = $package['period'];
        $orderHistory->month_count = $monthCount;
        $orderHistory->quantity = $quantity;
        $orderHistory->description = $package['name'];
        $orderHistory->creattime = time();
        $orderHistory->fromtime = $startDate;
        $orderHistory->endtime = $endDate;
        $orderHistory->status = Order::ORDER_STATUS_NEW;
        $orderHistory->paidtime = 0;
        $orderHistory->discount = $discount;
        $orderHistory->overage = $overage;
        $orderHistory->tax = $tax;
        $orderHistory->txnid = "";
        $orderHistory->is_auto_renew = $order->is_auto_renew;
        $orderHistory->payments = $payments;
        $orderHistory->type = PAYMENT_ORDER_VIP_TYPE;

        if($purchaseType == Lang::get('payment.vip_level_btn_upgrade')){
            $orderHistory->function_type = PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE;
            $orderHistory->save();
            // 保存一次function_relate_id，方便后面统计总的某个等级的时间和总费用
            //if($payType == PAYMENT_PAY_TYPE_ONETIME){
            $orderHistory->function_related_id = $orderHistory->id;
            $orderHistory->operation_type = PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE;
            $orderHistory->save();
            //}
        }else{
            $lastUpgradeOrderHistory = $this->orderApi->getLastUpgradeOrderOrRecurringOrderHistory($uid);
            Log::info("saveOrder lastUpgradeOrderHistory: ".json_encode($lastUpgradeOrderHistory));
            $queries = DB::getQueryLog();
            foreach ($queries as $index => $query) {
                Log::debug("saveOrder sql: " . $index . ": " . json_encode($query));
            }
            $orderHistory->function_type = PAYMENT_ORDER_FUNCTION_TYPE_RENEW;
            Log::info("saveOrder payType: ".$payType);
            if($payType == PAYMENT_PAY_TYPE_ONETIME){
                // 第一次是自动续费的情况，第二次是renew，但是instant payment的情况
                if(empty($lastUpgradeOrderHistory)){
                    $orderHistory->save();
                    $orderHistory->function_related_id = $orderHistory->id;
                }else{
                    $orderHistory->function_related_id = $lastUpgradeOrderHistory->function_related_id;
                }
            }else{
                $orderHistory->function_related_id = $lastUpgradeOrderHistory->function_related_id;
            }
            $orderHistory->operation_type = PAYMENT_ORDER_FUNCTION_TYPE_RENEW;
        }

        // 计算时间
        $this->setOrderHistoryTimes($orderHistory);

        $data = array(
            'result' => 1,
            'message' => ''
        );

        return Response::json($data);
    }




    public function orderList() {

        $uid = Session::get('uid');
        $paidOrders = $this->orderApi->getValidOrders($uid, Order::ORDER_STATUS_PAID, 0);
        $unpaidOrders = $this->orderApi->getValidOrders($uid, Order::ORDER_STATUS_NEW, 0);

        $data = array();

        $data['pageTitle'] = '';
        $data['paidOrders'] = $paidOrders;
        $data['unpaidOrders'] = $unpaidOrders;
        $data['isSelf'] = true;
        //$data['profile'] = getLoginUser();

        return View::make('people.profile.order_list', $data);

    }


    public function orderDetail($id) {

        $successUrl = BASE_URL . "payment/success?orderId=".$id;
        $cancelUrl = BASE_URL . "people/upgrade";
        $ipnUrl = BASE_URL . "payment/ipn";

        $data = array();

        $uid = Session::get('uid');
        $order = $this->orderApi->getOrder($uid, $id);

        if(empty($order)) {
            return View::make('errors.error', $data);
        }

        if($order->package == 0) {
            $cancelUrl = BASE_URL . "network/suggest";
        }

        // $overage = $this->orderApi->calculateOverage($uid, $order->package);

        // 增加了自动续费后，增加了方法参数
        $overage = $this->orderApi->calculateOverage($uid, $order->package, false, Order::ORDER_STATUS_PAID);
        $order->overage = $overage;
        $order->tax = 0;

        $payments = $order['price'] - $overage;
        if($payments < 0) {
            $payments = 0;
        }

        $order->payments = $payments;
        $data = array();

        $data['pageTitle'] = Lang::get('people.membership_lab_page_title');

        // $data['vipLevel'] = $user->vip_level;
        $data['order'] = $order;

        $today = date('Y-m-d');
        $timeSpan = 'From ' . $today;
        $next_year = mktime(0,0,0,date("m"),date("d")-1,date("Y")+1);
        $timeSpan .= ' To ' . date('Y-m-d', $next_year);

        $data['timeSpan'] = $timeSpan;

        $data['success_url'] = $successUrl;
        $data['cancel_url'] = $cancelUrl;
        $data['ipn_url'] = $ipnUrl;

        return View::make('payment.step2', $data);

    }

    public function deleteOrder() {
        $id = Input::get('id');

        $affected = 0;
        if($this->orderApi->deleteOrder($id)) {
            $affected++;
        }

        echo json_encode( array('affected' => $affected));
    }

    private function savePaidOrderInfos($isAutoRenew, $callbackType){
        Log::info("savePaidOrderInfos started");
        $orderId = Input::get("item_number");
        Log::info("savePaidOrderInfos orderId: ".$orderId);
        $isPaypalCallback = empty($orderId) ? false : true;
        $orderId = empty($orderId) ? Input::get("orderId") : $orderId;
        Log::info("payment success: ".$orderId);
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $order = $this->orderApi->getByUserIdAndTypeAndRenewType($orderHistory->uid, PAYMENT_ORDER_VIP_TYPE, $isAutoRenew);
        Log::info("order: ".json_encode(empty($order) ? 'sss': $order));
        $user = $this->usersApi->getUserById($orderHistory->uid);
        Log::info("payment order: ".$orderHistory->uid."-".$orderHistory->package);

        $lastOrder = $this->orderApi->getLastUpgradeOrderOrRecurringOrder($orderHistory->uid);
        if(empty($orderHistory)){
            return Redirect::to("payment/failed")->with("item_number", $orderId);
        }else{
            //$orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
            // 更新订单信息(无法判断paypal先发signup还是先发auto paid)
            if($order->status != Order::ORDER_STATUS_RECURRING_AUTO_PAID || $order->status != Order::ORDER_STATUS_PAID){
                $this->orderApi->updatePaidOrder($order, $orderHistory, $callbackType);
            }

            // 更新用户等级
            $this->usersApi->upgradeVipLevel($orderHistory->uid, $orderHistory->package);
        }

        // 如果升级且存在自动续费的情况，cancel自动续费（时间已经合并到升级后的即时续费中了）
        $package = Config::get('order.package')[$orderHistory->package];
        if($orderHistory->operation_type == PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE && !empty($lastOrder) && $callbackType == PAYMENT_INSTANT_TXN_TYPE){
            // modify to disabled orders to do
            //$this->orderApi->cancelRecurringOrder($orderHistory->uid);
            //$this->orderApi->cancelOrders($orderHistory->uid);
            $this->orderApi->disableRecurringOrder($orderHistory->uid);
        }

        // 保存paypal payment 创建成功的返回paypal 支付详情
        if($isPaypalCallback){
            $totalPrice = Input::get("payment_gross");

            Log::info("origin create time: " . $orderHistory->creattime);
            date_default_timezone_set("Etc/GMT+8");
            $createTime = date('Y-m-d H:i:s', $orderHistory->creattime);
            Log::info("transfered create time: " . $createTime);
            $unitPrice = $orderHistory->price * $orderHistory->month_count - $orderHistory->discount;

            $paymentItems = array();
            $paymentItem = new stdClass();
            $paymentItem->item_name = 'VIP - '.$package['level'];
            $paymentItem->unit_price = $unitPrice;
            $paymentItem->period = $orderHistory->period;
            $paymentItem->order_quantity = $orderHistory->quantity;
            $paymentItem->single_total_price = number_format($unitPrice * $orderHistory->quantity, 2);
            $paymentItem->deduction_remain = $orderHistory->overage;
            $paymentItem->payments = $orderHistory->payments;
            $totalPrice = $orderHistory->payments;

            array_push($paymentItems, $paymentItem);

            $billPlanPayment = $this->savePaypalBillingPlanInfos($order->uid);

            $paymentDetail = $this->getPaymentInfo((object)$billPlanPayment);

            $mailData = array(
                'receiverName' => $user->first_name,
                'tradove_id' => 'kroos.long@tradove.net',
                'payment_detail' => $paymentDetail,
                'total_price' => $totalPrice,
                'create_time' => $createTime,
                'order_number' => $orderId,
                'payment_item_array' => $paymentItems,
                'visitUrl' => BASE_URL,
                'linkLabel' => Lang::get('email.link_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING)
            );

            // 发电子账单的邮件
            $this->emailApi->sendMailByTemplateId(EmailTemplateManager::EMAIL_PAYMENT_E_BILLING, 'kroos.long@tradove.net', null, $mailData);
            //Mail::send('emails.refactored.email_e_bills', $mailData, function ($message) {
            //    $message->to('kroos.long@tradove.net')->subject(Lang::get('email.subject_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING));
            //});

            $data = array(
                "uid" => $user->id,
                "payment" => $orderHistory->payments,
                "vip_level" => $package['level'],
                "from_time" => explode(' ', $orderHistory->fromtime)[0],
                "end_time" => explode(' ', $orderHistory->endtime)[0]
            );

            return $data;
        }else{
            return array();
        }

    }

    /*public function saveRecurringOrderInfos(){
        $orderId = Input::get("item_number");
        $isPaypalCallback = empty($orderId) ? false : true;
        $orderId = empty($orderId) ? Input::get("orderId") : $orderId;
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $order = $this->orderApi->getByUserIdAndTypeAndRenewType($orderHistory->uid, PAYMENT_ORDER_VIP_TYPE, true);
        $user = $this->usersApi->getUserById($orderHistory->uid);
        Log::info("payment order: ".$orderHistory->uid."-".$orderHistory->package);

        if(empty($orderHistory)){
            return Redirect::to("payment/failed")->with("item_number", $orderId);
        }else{
            //$orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);

            // 获取当前等级所有的renew的订单历史记录方便计算总价格和时间范围
            $samePackageOrderHistories = $this->orderApi->getAllOrderHistoriesOfSamePackageLevel((int)$orderHistory->function_related_id);
            Log::info("one time payment success order histories: ". $samePackageOrderHistories[0]->payments);
            // 更新订单信息
            $this->orderApi->updatePaidOrder($order, $orderHistory, $samePackageOrderHistories);
            // 更新用户等级
            $this->usersApi->upgradeVipLevel($orderHistory->uid, $orderHistory->package);
        }

        $package = Config::get('order.package')[$orderHistory->package];

        // 保存paypal payment 创建成功的返回paypal 支付详情
        if($isPaypalCallback){
            $totalPrice = Input::get("payment_gross");

            Log::info("origin create time: " . $orderHistory->creattime);
            date_default_timezone_set("Etc/GMT+8");
            $createTime = date('Y-m-d H:i:s', $orderHistory->creattime);
            Log::info("transfered create time: " . $createTime);
            $unitPrice = $orderHistory->price * $orderHistory->month_count - $orderHistory->discount;

            $paymentItems = array();
            $paymentItem = new stdClass();
            $paymentItem->item_name = 'VIP - '.$package['name'];
            $paymentItem->unit_price = $unitPrice;
            $paymentItem->period = $orderHistory->period;
            $paymentItem->order_quantity = $orderHistory->quantity;
            $paymentItem->single_total_price = number_format($unitPrice * $orderHistory->quantity, 2);
            $paymentItem->deduction_remain = $orderHistory->overage;
            $paymentItem->payments = $orderHistory->payments;
            $totalPrice = $orderHistory->payments;

            array_push($paymentItems, $paymentItem);

            $billPlanPayment = $this->savePaypalBillingPlanInfos($order->uid);

            $paymentDetail = $this->getPaymentInfo((object)$billPlanPayment);

            $mailData = array(
                'receiverName' => $user->first_name,
                'tradove_id' => $user->email,
                'payment_detail' => $paymentDetail,
                'total_price' => $totalPrice,
                'create_time' => $createTime,
                'order_number' => $orderId,
                'payment_item_array' => $paymentItems,
                'visitUrl' => BASE_URL,
                'linkLabel' => Lang::get('email.link_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING)
            );

            // 发电子账单的邮件
            //EmailApi::sendMailByTemplateId(EmailTemplateManager::EMAIL_PAYMENT_E_BILLING, $user->email, null, $mailData);

            $data = array(
                "payment" => $orderHistory->payments,
                "vip_level" => $package['level'],
                "from_time" => explode(' ', $orderHistory->fromtime)[0],
                "end_time" => explode(' ', $orderHistory->endtime)[0]
            );

            return $data;
        }else{
            return array();
        }
    }*/

    public function ipn() {
        Log::info("payment ipn started");

        $ipnParams = Input::all();
        Log::error("ipn ipnParams: ". json_encode($ipnParams));

        // Create an instance of the paypal library
        $myPaypal = new Paypal();

        // Log the IPN results
        $myPaypal->logIpn = TRUE;

        // Enable test mode if needed
        $myPaypal->enableTestMode();

        // validate ipn
        if ($myPaypal->validateIpn()) {

            $orderId = Input::get("item_number");

            Log::info("ipn txn type: ". $myPaypal->ipnData['txn_type']);

            // recurring charge
            if($myPaypal->ipnData['txn_type'] == PAYMENT_INSTANT_TXN_TYPE){
                Log::info("instant payment ipn logic");
                // instant payment
                if ($myPaypal->ipnData['payment_status'] == 'Completed' && $myPaypal->ipnData['test_ipn'] == 1) {
                    Log::info("payment ipn status completed. ");

                    $data = $this->savePaidOrderInfos(false, PAYMENT_INSTANT_TXN_TYPE);


                    if(!empty($data)) {
                        return View::make('payment.payment_success', $data);
                    } else {
                        return View::make('payment.payment_fail', $data);
                    }
                }else{
                    Log::info("payment ipn status: ". $myPaypal->ipnData['payment_status']);
                }
            }else if($myPaypal->ipnData['txn_type'] == PAYMENT_SUBSCRIPTION_SIGN_UP_TXN_TYPE){
                Log::info("recurring ipn logic sign up");
                if($myPaypal->ipnData['payer_status'] == 'verified' && $myPaypal->ipnData['test_ipn'] == 1){
                    $data = $this->savePaidOrderInfos(true, PAYMENT_SUBSCRIPTION_SIGN_UP_TXN_TYPE);

                    if(!empty($data)) {
                        return View::make('payment.payment_success', $data);
                    } else {
                        return View::make('payment.payment_fail', $data);
                    }
                }else{
                    // recurring pay failed
                    Log::info("payment ipn status: ". $myPaypal->ipnData['payment_status']);
                }
            }else if($myPaypal->ipnData['txn_type'] == PAYMENT_SUBSCRIPTION_PAYMENT_TXN_TYPE){
                Log::info("recurring ipn logic paid success");
                $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
                $order = $this->orderApi->getByUserIdAndTypeAndRenewType($orderHistory->uid, PAYMENT_ORDER_VIP_TYPE, true);

                $this->orderApi->updatePaidOrder($order, $orderHistory, PAYMENT_SUBSCRIPTION_PAYMENT_TXN_TYPE);

                $this->savePaypalBillingPlanInfos($orderHistory->uid);

                //$this->orderApi->postponeInstantOrderOneMonth($orderHistory->uid);
            }else if($myPaypal->ipnData['txn_type'] == PAYMENT_SUBSCRIPTION_FAILED_TXN_TYPE){
                $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
                $user = $this->usersApi->getUserById($orderHistory->uid);

                $mailData = array(
                    'receiverName' => $user->first_name,
                    'visitUrl' => BASE_URL,
                    'linkLabel' => Lang::get('email.link_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED)
                );

                $this->emailApi->sendMailByTemplateId(EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED, 'kroos.long@tradove.net', null, $mailData);
                //Mail::send('emails.refactored.email_auto_billing_failed', $mailData, function ($message) {
                //    $message->to('kroos.long@tradove.net')->subject(Lang::get('email.subject_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED));
                //});
                // 更新状态
                $order = $this->orderApi->getOrderByOrderId($orderHistory->uid, $orderId);
                $orderHistory->status = Order::ORDER_STATUS_PAID_FAILED;
                $order->status = Order::ORDER_STATUS_PAID_FAILED;

                $orderHistory->save();
                $order->save();
            }else if($myPaypal->ipnData['txn_type'] == PAYMENT_SUBSCRIPTION_EXPIRED_TXN_TYPE){
                $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);

                $order = $this->orderApi->getOrderByOrderId($orderHistory->uid, $orderId);
                $orderHistory->status = Order::ORDER_STATUS_EXPIRED;
                $order->status = Order::ORDER_STATUS_EXPIRED;

                $orderHistory->save();
                $order->save();
            }else if($myPaypal->ipnData['txn_type'] == PAYMENT_SUBSCRIPTION_CANCELED_TXN_TYPE){
                $subscrId= Input::get('subscr_id');
                $paymentDetails = $this->paymentDetailApi->getPaymentDetailsBySubscrId($subscrId);
                if(count($paymentDetails) > 0){
                    $orderId = $paymentDetails[0]->item_number;
                    $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
                    $this->orderApi->cancelRecurringOrder($orderHistory->uid);
                }
            }
        }
    }

    private function savePaypalBillingPlanInfos($uid){
        $billPlanPayment = Input::all();
        $billPlanPayment['uid'] = $uid;
        Log::info("savePaypalBillingPlanInfos billPlanPayment: " .json_encode($billPlanPayment));
        if(!empty($billPlanPayment)){
            $paymentDetail = $this->paymentDetailApi->getPaymentDetailWithTypeAndOrderId($billPlanPayment['txn_type'], $billPlanPayment['item_number']);
            if(empty($paymentDetail)){
                $this->paymentDetailApi->savePaymentDetail($billPlanPayment);
            }
        }
        return $billPlanPayment;
    }

    public function success() {
        $orderId = Input::get("item_number");
        Log::info("payment success: ".$orderId);
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $package = Config::get('order.package')[$orderHistory->package];

        $data = array(
            "payment" => $orderHistory->payments,
            "vip_level" => $package['level'],
            "from_time" => explode(' ', $orderHistory->fromtime)[0],
            "end_time" => explode(' ', $orderHistory->endtime)[0]
        );

        return View::make('payment.payment_success', $data);
    }

    public function autoBillingSuccess(){
        $orderId = Input::get("item_number");
        Log::info("autoBillingSuccess orderId: ".$orderId);
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $package = Config::get('order.package')[$orderHistory->package];

        $data = array(
            "payment" => $orderHistory->payments,
            "vip_level" => $package['level'],
            "from_time" => explode(' ', $orderHistory->fromtime)[0],
            "end_time" => explode(' ', $orderHistory->endtime)[0]
        );
        return View::make('payment.payment_success', $data);
    }

    public function failed() {
        $orderId = Input::get("item_number");
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $orderHistory->status = Order::ORDER_STATUS_PAID_FAILED;
        $orderHistory->save();

        $package = Config::get('order.package')[$orderHistory->package];

        $data = array(
            "vip_level" => $package["level"],
            "from_time" => explode(' ', $orderHistory->from_time),
            "end_time" => explode(' ', $orderHistory->end_time),
            "payment" => $orderHistory->payments
        );

        return View::make('payment.payment_fail', $data);
    }

    private function getPaymentInfo($paymentDetail){
        $paymentDetailInfo = '';
        if(empty($paymentDetail->receipt_id)){
            $paymentDetailInfo .= Lang::get('payment.setting_lab_detail_from_account'). '<br />';
            $paymentDetailInfo .= $paymentDetail->payer_email;
        }else{
            $paymentDetailInfo .= Lang::get('payment.setting_lab_detail_from_credit_card'). '<br />';
            $paymentDetailInfo .= $paymentDetail->last_name . ' '. $paymentDetail->first_name. '<br />';
            $paymentDetailInfo .= $paymentDetail->address_city . ' ' . $paymentDetail->address_street. '<br />';
            $paymentDetailInfo .= $paymentDetail->address_country;
        }
        return $paymentDetailInfo;
    }

    public function settingPayment(){
        $userId = Session::get("uid");
        $users = $this->usersApi->getUserById($userId);
        $recurringOrder = $this->orderApi->getRecurringOrder($userId);
        $order = $this->orderApi->getByUserIdAndTypeAndRenewTypeAndStatus($userId, PAYMENT_ORDER_VIP_TYPE, false, Order::ORDER_STATUS_PAID);

        Log::info("settingPayment recurringOrder: ".json_encode($recurringOrder));
        Log::info("settingPayment order: ".json_encode($order));
        if(!empty($recurringOrder)){
            $paymentDetail = $this->paymentDetailApi->getPaymentDetail($recurringOrder->orderid);
        }else if(!empty($order)){
            $paymentDetail = $this->paymentDetailApi->getPaymentDetail($order->orderid);
        }

        //Log::info("settingPayment paymentDetail: ". json_encode($paymentDetail));

        if(!empty($paymentDetail)){
            $paymentDetailInfo = $this->getPaymentInfo($paymentDetail);
        }else{
            // 如果某次支付不需要走paypal，这样payment info就需要取最近一次paypal的支付信息
            if(!empty($order) && $order->payments == 0){
               $paymentDetail = $this->paymentDetailApi->getLatestPaymentDetail($userId);
                Log::info("inner settingPayment paymentDetail: ". json_encode($paymentDetail));
                if(!empty($paymentDetail)){
                    $paymentDetailInfo = $this->getPaymentInfo($paymentDetail);
                }
            }else{
                $paymentDetailInfo = '';
            }
        }

        // free等级的seller用户不显示支付信息
        if($users->vip_level == VIP_LEVEL_OF_FREE){
            $paymentDetailInfo = '';
        }

        $package = Config::get('order.package')[$users->vip_level];

        $data = array(
            "vip_level" => $package['level'],
            "order" => $order,
            "recurringOrder" => $recurringOrder,
            "paymentDetailInfo" => $paymentDetailInfo
        );

        return View::make('payment.payment_setting', $data);
    }

    private function changeSubscriptionStatus($profileId, $action){
        $manageRPPStatusReqestDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType();
        $manageRPPStatusReqestDetails->Action = $action;
        $manageRPPStatusReqestDetails->ProfileID = $profileId;

        $manageRPPStatusReqest = new ManageRecurringPaymentsProfileStatusRequestType();
        $manageRPPStatusReqest->ManageRecurringPaymentsProfileStatusRequestDetails = $manageRPPStatusReqestDetails;

        $manageRPPStatusReq = new ManageRecurringPaymentsProfileStatusReq();
        $manageRPPStatusReq->ManageRecurringPaymentsProfileStatusRequest = $manageRPPStatusReqest;

        $credentials = array(
            "mode" => "sandbox",
            "acct1.UserName" => PAYMENT_API_USERNAME,
            "acct1.Password" => PAYMENT_API_PASSWORD,
            "acct1.Signature" => PAYMENT_API_SIGNATURE
        );

        $paypalService = new PayPalAPIInterfaceServiceService($credentials);

        /* wrap API method calls on the service object with a try catch */
        $manageRPPStatusResponse = $paypalService->ManageRecurringPaymentsProfileStatus($manageRPPStatusReq);
        return $manageRPPStatusResponse;
    }

    public function cancelAutoBilling(){

        $uid = Session::get('uid');
        $recurringOrder = $this->orderApi->getLastUpgradeRecurringOrder($uid);
        $paymentDetail = $this->paymentDetailApi->getPaymentDetail($recurringOrder->orderid);
        if(!empty($paymentDetail)){
            Log::info("cancel auto billing subscr id: ".$paymentDetail->subscr_id);
            try{
                $manageRPPStatusResponse = $this->changeSubscriptionStatus($paymentDetail->subscr_id, PAYMENT_SUBSCRIPTION_CANCEL_STATUS);
                /*if($manageRPPStatusResponse->Ack == 'Success'){
                    $this->orderApi->cancelRecurringOrder($uid);
                }*/
            }catch(ErrorException $e){
                Log::error("cancel paypal auto billing with errors: ". json_encode($e));
            }

        }

        return Redirect::to("payment/setting");
    }

    public function updateUpgradeInfosWithZeroFee(){
        $orderId = Input::get("item_number");
        $orderHistory = $this->orderApi->getOrderHistoryByOrderId($orderId);
        $order = $this->orderApi->getByUserIdAndTypeAndRenewType($orderHistory->uid, PAYMENT_ORDER_VIP_TYPE, false);

        // 只有高等级升级方法才会被调用，高等级升级第一次只能是即时购买
        $this->orderApi->updatePaidOrder($order, $orderHistory, PAYMENT_INSTANT_TXN_TYPE);
        // 更新user状态
        $this->usersApi->upgradeVipLevel($orderHistory->uid, $orderHistory->package);

        return Redirect::to("payment/success?item_number=".$orderId);
    }
}