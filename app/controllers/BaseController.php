<?php

class BaseController extends Controller {

    public function __construct()
    {
        // Perform CSRF check on all post/put/patch/delete requests
        // $this->beforeFilter('csrf', array('on' => array('post', 'get', 'put', 'patch', 'delete')));
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    public function redirect(){
        $redirect_url = Input::get('to');
        $uid = Input::get('uid');
        $ntid = Input::get('ntid');

        if(!empty($redirect_url)){
            $is_logined = Session::get('uid');
            if(empty($is_logined)){
                return Redirect::to("/login?to=$redirect_url&uid=$uid&ntid=$ntid");
            }else{
                if($is_logined == $uid){
                    //if url contains '___', change '___' into '?'
                    $redirect_url = str_replace('___', '?', $redirect_url);
                    return Redirect::to(urldecode($redirect_url));
                }else{
                    return Redirect::to('/home');
                }
            }
        }
    }
}