<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-9-17
 * Time: 上午10:57
 * To change this template use File | Settings | File Templates.
 */


define('TRADOVE_VERSION', 'V2');

define('BASE_URL', 'http://103.219.193.16/');
define('API_URL', 'http://119.84.60.71:9999/');

define('ONE_YEAR', 31536000);
define('ONE_MONTH', 2592000);
define('ONE_WEEK', 604800);
define('ONE_DAY', 86400);
define('ONE_HOUR', 3600);
define('ONE_MINUTE', 60);

define('GLOBAL_BOOLEAN_YES', 1);
define('GLOBAL_BOOLEAN_NO', 0);

define('GLOBAL_STAT_NORMAL', 0);
define('GLOBAL_STAT_DELETE', 1);

//home
define("HOME_PAGE_NUMBER", 20);

//connect
define("CONNECT_S_PAGE_NUMBER", 10);
define("CONNECT_SUGGEST_PAGE_NUMBER", 10);

//leads
define("LEADS_PAGE_NUMBER", 10);

define("SEARCH_PAGE_NUMBER", 20);

define("MEMBER_PAGE_NUMBER", 10);

//outsidelist
define('LISTING_PAGE_NUMBER', 50);

//Gmail invitation
define('GMAIL_CLIENT_ID', '424408588161-dsf9q52c5m3m74nnmg7d1p8vb3ua12oc.apps.googleusercontent.com');
define('GMAIL_CLIEND_SECRET', 'PColB9w5UvxOXciPVEhuLy-U');
define('GMAIL_REDIRECT_URI', BASE_URL . 'invite/gmail/contacts');
define('GMAIL_OUR_URL', 'https://www.tradove.com');

//Linkedin invitation for demo2.tradove
//define('LINKEDIN_API_KEY', '754dkw9bcuv4n6');
//define('LINKEDIN_API_SECRET', 'arNz6qSJWjIiUflB');
//define('LINKEDIN_SCOPE', 'r_contactinfo r_fullprofile r_basicprofile r_network ');

//for invite company profile demo2.tradove
//define('LINKEDIN_API_KEY_COMPANY', '754rw37p55q0g4');
//define('LINKEDIN_API_SECRET_COMPANY', 'D2ygWthOYcWDqLv3');
//define('LINKEDIN_SCOPE_COMPANY', 'r_contactinfo r_fullprofile r_basicprofile r_network ');


//Likedin invitation for tradove
define('LINKEDIN_API_KEY', '75d0kdhrpz7ql9');
define('LINKEDIN_API_SECRET', 'XtCekz6zgCPAQ3r4');
define('LINKEDIN_SCOPE', 'r_contactinfo r_fullprofile r_basicprofile r_network w_messages');
//for invite company profile
define('LINKEDIN_API_KEY_COMPANY', '75gztwkdonlt8m');
define('LINKEDIN_API_SECRET_COMPANY', 'rDHGUsYqvAnBb8sk');
define('LINKEDIN_SCOPE_COMPANY', 'r_contactinfo r_fullprofile r_basicprofile r_network w_messages');


//Yahoo invitaion
define("YAHOO_OAUTH_CONSUMER_KEY", "dj0yJmk9a2lxMUtsMlZ1VlJuJmQ9WVdrOWJrcHhlRFpQTkdrbWNHbzlNVFV3T0RFMU1qSTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD0xMg--");
define("YAHOO_OAUTH_CONSUMER_SECRET", "b74fc12b489ac2c35ea64aec44ae214bbe26ad57");
define('YAHOO_OAUTH_APP_ID', 'nJqx6O4i');
define('YAHOO_OAUTH_DOMAIN', BASE_URL . 'active5');
define('YAHOO_CALLBACK_URL', BASE_URL . 'invite/yahoo/contacts');
define('YAHOO_OUR_URL', 'https://www.tradove.com');

//system message type
define('SYSTEM_MESSAGE_TYPE_EMAIL', 1);
define('SYSTEM_MESSAGE_TYPE_MESSAGE', 2);

//default number per page
define('DEFAULT_NUM_PER_PAGE', 10);

//member type ID
define('MEMBER_TYPE_BUYER_ID', 1);
define('MEMBER_TYPE_SELLER_ID', 2);
define('MEMBER_TYPE_OTHER_ID', 3);
define('MEMBER_TYPE_SERVICE_ID', 8);
define('MEMBER_TYPE_ADMIN_ID', 9);
define('MEMBER_TYPE_ALL_ID', 10);

//invite type
define('INVITE_TYPE_LOOP', 1);
define('INVITE_TYPE_GROUP', 2);
define('INVITE_TYPE_PRODUCT', 3);
define('INVITE_TYPE_COMPANY', 4);
define('INVITE_TYPE_NETWORK', 5);

//reference type
define('REFERENCE_TYPE_USER', 1);
define('REFERENCE_TYPE_PRODUCT', 2);
define('REFERENCE_TYPE_COMPANY', 3);

//reference default show length
define('REF_DEFAULT_SHOW_LEN', 100);
//discussion default show length
define('DISCUSSION_DEFAULT_SHOW_LEN', 200);

//news abstract default show length
define('NEWS_ABSTRACT_DEFAULT_SHOW_LEN', 300);
//group description default show length
define('GROUP_DESCRIPTION_DEFAULT_SHOW_LENGTH', 200);

//default show length for items' description in industry menu
define("INDUSTRY_DESCRIPTION_DEFAULT_SHOW_LENGTH", 200);

//default show length for right side recommend items
define("RECOMMEND_ITEMS_DEFAULT_SHOW_LENGTH", 100);

//product type
define('PRODUCT_TYPE_PRODUCT', 1);
define('PRODUCT_TYPE_SERVICE', 2);

//diversity status
define('DIVERSITY_OF_WOMAN', 1);
define('DIVERSITY_OF_MINORITY', 2);
define('DIVERSITY_OF_VETERAN', 4);

//network type
define('NETWORK_TYPE_WOMAN_MINORITY', 1);
define('NETWORK_TYPE_BUY_SIDE', 2);
define('NETWORK_TYPE_INDUSTRY', 3);
define('NETWORK_TYPE_SELL_SIDE', 4);
define('NETWORK_TYPE_TRADESHOW', 5);
define('NETWORK_TYPE_ASSOCIATION', 6);
define('NETWORK_TYPE_LOYALTY', 7);
define('NETWORK_TYPE_OTHER', 8);

//the max length of field
define('MAX_LEN_PRODUCT_DESC', 2000);
define('MAX_LEN_PRODUCT_SPEC', 2000);
define('MAX_WORDS_PRODUCT_CUSTOMER', 5);
define('MAX_WORDS_PRODUCT_FEATURE', 5);

//excel extension type
define('EXCEL_EXTENSION_2003', "xls");
define('EXCEL_EXTENSION_2007', "xlsx");

//file type for file upload
define('LOGO_USER', '/tmp/logo/user/');
define('LOGO_PRODUCT', '/tmp/logo/product/');
define('LOGO_COMPANY', '/tmp/logo/company/');
define('LOGO_GROUP', '/tmp/logo/group/');
define('LOGO_NETWORK', '/tmp/logo/network/');

/**
 * Group constants start
 */

//group member status
define('GROUP_MEMBER_STAT_REQUEST', 0);
define('GROUP_MEMBER_STAT_REQUEST_BLOCKED', 10);
define('GROUP_MEMBER_STAT_BLOCKED', 20);
define('GROUP_MEMBER_STAT_APPROVE', 30);
define('GROUP_MEMBER_STAT_DELETE', 40);

//user state
define('USER_STATE_OF_REGISTERED', 0);
define('USER_STATE_OF_APPROVE', 1);
define('USER_STATE_OF_REJECT', 2);
define('USER_STATE_OF_SENT_ACTIVE', 9);
define('USER_STATE_OF_NETWORK_IMPORTED', 5);


define('POSTING_TYPE_OF_DISCUSSION', 1);
define('POSTING_TYPE_OF_BUYER', 2);
define('POSTING_TYPE_OF_SELLER', 3);

define('GROUP_MEMBER_TYPE_MEMBER', 0);
define('GROUP_MEMBER_TYPE_ADMIN', 1);

define('GROUP_INVITATION_STAT_SENT', 0);
define('GROUP_INVITATION_STAT_ACCEPT', 10);
define('GROUP_INVITATION_STAT_WITHDRAW', 20);

define('GROUP_ACCESS_APPROVAL', 1);
define('GROUP_ACCESS_NO_LIMIT', 2);
define('GROUP_ACCESS_MEMBER_INVITE', 3);

define('GROUP_CHGOWNER_STAT_SENT', 0);
define('GROUP_CHGOWNER_STAT_ACCEPT', 10);
define('GROUP_CHGOWNER_STAT_REJECT', 20);
define('GROUP_CHGOWNER_STAT_INVALID', 30);

define('GROUP_ACTION_TYPE_DISCUSSION', 0);
define('GROUP_ACTION_TYPE_COMMENT', 10);

define('GROUP_JOIN_REQUEST_ACTION_JOIN', 1);
define('GROUP_JOIN_REQUEST_ACTION_DECLINE', 2);
define('GROUP_JOIN_REQUEST_ACTION_DECLINE_AND_BLOCK', 3);
define('GROUP_JOIN_REQUEST_ACTION_UNBLOCK', 4);

define('GROUP_MANAGE_JOIN_REQ_NEED_HANDLE', 0);
define('GROUP_MANAGE_JOIN_REQ_ACCEPT', 1);
define('GROUP_MANAGE_JOIN_REQ_REJECT', 2);

define('GROUP_ANNOUNCE', 1);
define('GROUP_RULE', 2);

//used for group discussion/posting/comments manage
define('GROUP_ATTITUDE_TYPE_POSTING_DISCUSSION', 0);
define('GROUP_ATTITUDE_TYPE_COMMENT', 10);

define('GROUP_ATTITUDE_INAPPROPRIATE_YES', 1);
define('GROUP_ATTITUDE_INAPPROPRIATE_NO', 0);

//group discussion/posting/comments block/unblock
define('GROUP_ATTITUDE_BLOCKED_YES', 1);
define('GROUP_ATTITUDE_BLOCKED_NO', 0);

//for group posting/discussion/comments action
define('GROUP_MANAGE_ACTION_MOVE', 1);
define('GROUP_MANAGE_ACTION_BLOCK', 2);
define('GROUP_MANAGE_ACTION_RECOVER', 3);

//for group manage navi
define('GROUP_MANAGE_NAVI_DISCUSSION', 1);
define('GROUP_MANAGE_NAVI_POSTING', 2);
define('GROUP_MANAGE_NAVI_JOINREQUEST', 3);
define('GROUP_MANAGE_NAVI_INVITE', 4);

define('GROUP_MANAG_CHANGE_LINK', '/xxx/xxx');

/**
 * Group constants end
 */

define('LIMIT_TYPE_COMMON', 1); //common
define('LIMIT_TYPE_SEARCH', 2); //search

define('VIP_LEVEL_OF_FREE', 1);
define('VIP_LEVEL_OF_ENTRY', 2);
define('VIP_LEVEL_OF_GOLD', 4);
define('VIP_LEVEL_OF_PLATINUM', 8);
define('VIP_LEVEL_OF_UNLIMITED', 16);

define('MANAGER_EMAIL_ADDRESS', 'customer.service@tradove.com');
//define('MANAGER_EMAIL_ADDRESS_FROM', 'TraDove Customer Service<customer.service@tradove.com>');
define('MANAGER_EMAIL_ADDRESS_FROM', 'zahuifangexia0720@163.com');

define('FEEDBACK_EMAIL_ADDRESS1', 'users@tradove.net');
define('FEEDBACK_EMAIL_ADDRESS2', 'kent.yan@tradove.net');

//for blog
define('USER_HAS_BLOG', 1);
define('USER_NO_BLOG', 0);
define('USER_CLOSE_BLOG', 2);
define('QUERY_POSTS_NUM', 10);
define('QUERY_POSTS_HOME_NUM', 30);
define('HOT_BLOG_NUM', 5);
define('HOT_TOPIC_NUM', 5);
define('STICKY_POST_NUM', 2);
define('OLD_DOMAIN', 'https://www.tradove.com/blog');
define('NEW_DOMAIN', 'https://www.tradove.com/blog');
define('BLOG_POST_DIRECTLY', 'blog/wp-admin/post-new.php');
define('USER_META_TRADOVE_ID', 'tradove_user_id');
define('POST_NUM_META_VALUE_NO', 0);
define('BLOG_CONTENT_CUT_LENGTH', 600);

define('MAKE_VISIBLE_TO_LOOP', 1);
define('MAKE_VISIBLE_TO_CONTACT', 2);
define('MAKE_VISIBLE_TO_GROUP', 3);
define('MAKE_VISIBLE_TO_PUBLIC', 4);
define('MAKE_VISIBLE_TO_SYSTEM_USE', 5);

define('PRODUCT_DISVISIBLE', 0);
define('PRODUCT_VISIBLE', 1);

define('OUR_URL', 'https://www.tradove.com');

define('INVITATION_STATUS_NONE', 0);
define('INVITATION_STATUS_ACCP', 1);
define('INVITATION_STATUS_REFUSED', 2);

define('FILE_TYPE_GIF', 'gif');
define('FILE_TYPE_PNG', 'png');
define('FILE_TYPE_JPG', 'jpg');
define('FILE_TYPE_PDF', 'pdf');
define('FILE_TYPE_DOCX', 'docx');
define('FILE_TYPE_DOC', 'doc');
define('FILE_TYPE_DOT', 'dot');
define('FILE_TYPE_PPTX', 'pptx');
define('FILE_TYPE_PPT', 'ppt');
define('FILE_TYPE_TXT', 'txt');
define('FILE_TYPE_XLS', 'xls');
define('FILE_TYPE_XLS_OFFICE', 'xls_office');
define('FILE_TYPE_OTHER', 'xls_other');
define('FILE_TYPE_XLSX', 'xlsx');

//for user profile title
define('USER_PROFILE_TITLE_MR', 1);
define('USER_PROFILE_TITLE_MS', 2);
define('USER_PROFILE_TITLE_MISS', 3);
define('USER_PROFILE_TITLE_MRS', 4);

//leads posting type
define('LEAD_POSTING_TYPE_BUYING', 1);
define('LEAD_POSTING_TYPE_SELLING', 2);

//search type
define('SEARCH_FROM_DB_REDIS', 3);
define('SEARCH_FROM_SOLR', 4);

//page number for tracking list
define("TRACKING_PAGE_NUMBER", 10);

//tracking type
define('TRACKING_USER', 0);
define('TRACKING_PRODUCT', 1);
define('TRACKING_COMPANY', 2);

define('ADD_NETWORK_ACCOUNT_EXIST_SUCCESS', 1);
define('ADD_NETWORK_ACCOUNT_EXIST_FAILE_CAUSE_SELLER', 2);
define('ADD_NETWORK_ACCOUNT_EXIST_FAILE_CAUSE_NOT_COLLEAGUES', 3);
define('ADD_NETWORK_ACCOUNT_CREATE_NEW_SUCCESS', 4);

//message type
define('MESSAGE_TYPE_COMMON', 0);
define('MESSAGE_TYPE_CHANGE_PRODUCT', 1);
define('MESSAGE_TYPE_CHANGE_COMPANY', 2);
define('MESSAGE_TYPE_CHANGE_GROUP', 3);
define('MESSAGE_TYPE_CHANGE_NETWORK', 4);

//outside listing
define('OUTSIDE_LISTING_COMPANY', 4);
define('OUTSIDE_LISTING_PRODUCT', 5);

//select plugin default number of retrieving persons
define('NUMBER_OF_RETRIEVED_PERSONS', 10);
define('NUMBER_OF_RETRIEVED_TOTAL_PERSONS', 1000);

define('OWNER_CHANGE_COMPANY', 1);
define('OWNER_CHANGE_PRODUCT', 2);
define('OWNER_CHANGE_GROUP', 3);
define('OWNER_CHANGE_NETWORK', 4);

//for sending mail
define('GLOBAL_SEND_MAIL_YES', 1);
define('GLOBAL_SEND_MAIL_NO', 0);

//for news
define('NEWS_ATTITUDE_TYPE_NEWS', 1);
define('NEWS_ATTITUDE_TYPE_COMMENT', 2);
define('NEWS_ATTITUDE_STATUS_UNBLOCK', 0);
define('NEWS_ATTITUDE_STATUS_BLOCK', 1);
define('NEWS_TOP_N', 10);

//for redis
define('REDIS_USER_IMPORT_NUMBER', 100);
define('REDIS_KEY_EXPIRE', 1800);
define('REDIS_RECOMMEND_NUMBER', 2000);
define('REDIS_RECOMMEND_NUMBER_2', 5000);
define('REDIS_ARRAY_CHUNK_NUMBER', 1000);

//for log
define('LOG_ADMIN_REDIS_INIT_FILE', '/var/log/laravel/log.txt');
define('LOG_SYS_ERROR_FILE', '/var/log/laravel/sys_error.txt');

// for payment
//define('PAYMENT_POST_URL', 'https://www.paypal.com/cgi-bin/webscr');
define('PAYMENT_BUSINESS_NAME', 'kent.yan-facilitator@tradove.net');
//define('PAYMENT_BUSINESS_NAME', 'kent.yan@tradove.net');
define('PAYMENT_POST_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
define('PAYMENT_ORDER_VIP_TYPE', 1);
define('PAYMENT_ORDER_VIP_FREE_PACKAGE', 1);
define('PAYMENT_ORDER_VIP_GOLD_PACKAGE', 4);
define('PAYMENT_ORDER_VIP_PLATINUM_PACKAGE', 8);
define('PAYMENT_ORDER_VIP_UNLIMITED_PACKAGE', 16);
define('PAYMENT_ORDER_VIP_PERIOD_MONTH', 'month');
define('PAYMENT_ORDER_VIP_PERIOD_YEAR', 'year');
define('PAYMENT_ORDER_FUNCTION_TYPE_UPGRADE', 1);
define('PAYMENT_ORDER_FUNCTION_TYPE_RENEW', 2);
define('PAYMENT_PAY_TYPE_ONETIME', 1);
define('PAYMENT_PAY_TYPE_AUTO_BILLING', 2);
define('PAYMENT_DEFAULT_PAY_PERIOD', 'month');
define('PAYMENT_FIRST_UPGRADE_LEVEL', 'Gold');

define('PAYMENT_INSTANT_TXN_TYPE', 'web_accept');
define('PAYMENT_SUBSCRIPTION_SIGN_UP_TXN_TYPE', 'subscr_signup');
define('PAYMENT_SUBSCRIPTION_PAYMENT_TXN_TYPE', 'subscr_payment');
define('PAYMENT_SUBSCRIPTION_FAILED_TXN_TYPE', 'subscr_failed');
define('PAYMENT_SUBSCRIPTION_EXPIRED_TXN_TYPE', 'subscr_eot');
define('PAYMENT_SUBSCRIPTION_CANCELED_TXN_TYPE', 'subscr_cancel');

define('PAYMENT_SUBSCRIPTION_CANCEL_STATUS', 'Cancel');
define('PAYMENT_SUBSCRIPTION_SUSPEND_STATUS', 'Suspend');
define('PAYMENT_SUBSCRIPTION_REACTIVE_STATUS', 'Reactivate');

define('PAYMENT_API_USERNAME', 'kent.yan-facilitator_api1.tradove.net');
define('PAYMENT_API_PASSWORD', '5TU4U8HGTVKUH89L');
define('PAYMENT_API_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31ARXaUPeOt.1v7RZp.WFV3lvo0qb3');

define('POPULAR_QA_COUNT', 10);

//for remember me
define('REMEMEBER_ME', 'tradove_remember_me');
define('EXPIRE_TIME', 7776000);

//for advertisement
define('AD_LOCATION_TOP', 't');
define('AD_LOCATION_BOTTOM', 'b');
define('AD_LOCATION_RIGHT', 'r');

//for loop relationship
define('LOOP_R_OTHER', 0);
define('LOOP_R_BUYING_SELLING', 1);
define('LOOP_R_PARTNERS', 2);
define('LOOP_R_COLLEAGUE', 3);

define('UNSUBSCRIBE_SALT', 'TraDove-19-04-CQ');

//for search record
define("SEARCH_RECORD_USER", 1);
define("SEARCH_RECORD_PRODUCT", 2);
define("SEARCH_RECORD_COMPANY", 3);

//for assign type
define("ASSIGN_TYPE_OF_COMPANY", 1);
define("ASSIGN_TYPE_OF_PRODUCT", 2);
define("ASSIGN_TYPE_OF_GROUP", 3);
define("ASSIGN_TYPE_OF_NETWORK", 4);

//for completion
define("COMPLETION_NUM_USER", 22);
define("COMPLETION_NUM_COMPANY", 21);

//for message center number show
define("R_MESSAGE", 1);
define("R_PROPOSAL", 2);
define("R_DISCUSSION", 3);
define("R_INVITATION", 4);

// for cache count of unread messages|invitations|proposals|private discussions
define("UNREAD_COUNT_TYPE_OF_ALL", 0);
define("UNREAD_COUNT_TYPE_OF_MESSAGE", 1);
define("UNREAD_COUNT_TYPE_OF_INVITATION", 2);
define("UNREAD_COUNT_TYPE_OF_PROPOSAL", 3);
define("UNREAD_COUNT_TYPE_OF_PRIVATE_DISCUSSION", 4);

define("DISPLAY_MAX_UNREAD_COUNT", 99);

define("MAKE_VISIBLE_LOOP", 1);
define("MAKE_VISIBLE_PUBLIC", 2);
define("MAKE_VISIBLE_SYSTEM", 4);

//product visible type
define('PRODUCT_VISIBLE_TYPE_LOOP', 1);
define('PRODUCT_VISIBLE_TYPE_PUBLIC', 2);
define('PRODUCT_VISIBLE_TYPE_SYSTEM', 4);

//reference score
define('REF_SCORE_ONE', 1);
define('REF_SCORE_TOW', 2);
define('REF_SCORE_THREE', 3);
define('REF_SCORE_FOUR', 4);
define('REF_SCORE_FIVE', 5);

define('INVITE_COLLEAGUES_AT_HOME',1);
define('INVITE_COLLEAGUES_TEMPLATE',2);
define('INVITE_COLLEAGE_LOGIN',3);

define('PRIVATE_NETWORK_SMALL_BIZ','Small Biz');

define('PREFIX_OF_EMAIL_TEMPLATE_NAME', 'emails.refactored.');

define('SEND_MAIL_FLAG', Config::get("database.isSendMail"));

//statistics type
define('STATISTICS_REGISTERED_MEMBER', 1);
define('STATISTICS_MESSAGE_CENTER', 2);
define('STATISTICS_GROUP', 3);
define('STATISTICS_LEADS', 4);
define('STATISTICS_BLOGS', 5);
define('STATISTICS_REFERENCE', 6);
define('STATISTICS_CONNECTION', 7);
define('STATISTICS_NETWORK', 8);
define('STATISTICS_SEARCH', 9);
define('STATISTICS_PAYING', 10);
define('STATISTICS_PROFILE', 11);
define('USER_REPORTING', 12);
