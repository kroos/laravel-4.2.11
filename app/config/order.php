<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 14-4-3
 * Time: 上午10:59
 * To change this template use File | Settings | File Templates.
 */

return array(

    'package' => array(

        // If I'm member of the company which created this network
        0 => array("id" => 0, "name" => "Join Network of %s", "price" => 599.00, "period" => "year", "month_count" => 12),
        // If I'm not member of the company which created this network
        64 => array("id" => 64, "name" => "Join Network of %s", "price" => 999.00, "period" => "year", "month_count" => 12),

        1 => array("id" => 1, "level" => "Free", "name" => "Free", "price" => 0, "discount" => 0, "period" => "month", "month_count" => 1),
        2 => array("id" => 2, "level" => "Entry", "name" => "Upgrade to Level of Entry", "price" => 0, "discount" => 0, "period" => "month", "month_count" => 1),
        4 => array("id" => 4, "level" => "Gold", "name" => "Upgrade to Level of Gold", "price" => 50.00, "discount" => 0, "period" => "month", "month_count" => 1),
        5 => array("id" => 5, "level" => "Gold", "name" => "Upgrade to Level of Gold", "price" => 50.00, "discount" => 0.2, "period" => "year", "month_count" => 12),
        8 => array("id" => 8, "level" => "Platinum", "name" => "Upgrade to Level of Platinum", "price" => 100.00, "discount" => 0, "period" => "month", "month_count" => 1),
        9 => array("id" => 9, "level" => "Platinum", "name" => "Upgrade to Level of Platinum", "price" => 100.00, "discount" => 0.2, "period" => "year", "month_count" => 12),
        16 => array("id" => 16, "level" => "Unlimited", "name" => "Upgrade to Level of Unlimited", "price" => 200.00, "discount" => 0, "period" => "month", "month_count" => 1),
        17 => array("id" => 17, "level" => "Unlimited", "name" => "Upgrade to Level of Unlimited", "price" => 200.00, "discount" => 0.2, "period" => "year", "month_count" => 12),

        32 => array("id" => 32, "name" => "Create Network of %s", "price" => 599.00, "period" => "year", "month_count" => 12),

    ),

);