<?php

return array(

    EmailTemplateManager::EMAIL_RECENT_UPDATES  => 'recent_updates',
    EmailTemplateManager::EMAIL_ADMIN2ALL => 'admin2all',
    EmailTemplateManager::EMAIL_MESSAGE_NOTIFICATION => 'message_notification',
    EmailTemplateManager::EMAIL_PROPOSAL_NOTIFICATION => 'proposal_notification',
    EmailTemplateManager::EMAIL_DISCUSSION_NOTIFICATION => 'discussion_notification',

    EmailTemplateManager::EMAIL_GRP_POSTING_NOTIFICATION => 'grp_posting_notification',
    EmailTemplateManager::EMAIL_GRP_DISCUSSION_NOTIFICATION => 'grp_discussion_notification',
    EmailTemplateManager::EMAIL_LEAD_INDUSTRY_NOTIFICATION => 'lead_industry_notification',

    EmailTemplateManager::EMAIL_PROPOSAL_REPLY_NOTIFICATION => 'proposal_reply_notification',
    EmailTemplateManager::EMAIL_DISCUSSION_REPLY_NOTIFICATION => 'discussion_reply_notification',

    EmailTemplateManager::EMAIL_GRP_POSTING_REPLY_NOTIFICATION => 'grp_posting_reply_notification',
    EmailTemplateManager::EMAIL_GRP_DISCUSSION_REPLY_NOTIFICATION => 'grp_discussion_reply_notification',

    EmailTemplateManager::EMAIL_LEAD_REPLY_NOTIFICATION => 'lead_reply_notification',
    EmailTemplateManager::EMAIL_BLOG_REPLY_NOTIFICATION => 'blog_reply_notification',
    EmailTemplateManager::EMAIL_LOOP_INVITE_NOTIFICATION => 'loop_invite_notification',

    EmailTemplateManager::EMAIL_PRODUCT_CONNECT_NOTIFICATION => 'product_connect_notification',
    EmailTemplateManager::EMAIL_COMPANY_CONNECT_NOTIFICATION => 'company_connect_notification',

    EmailTemplateManager::EMAIL_GRP_INVITE_NOTIFICATION => 'grp_invite_notification',
    EmailTemplateManager::EMAIL_NETWORK_INVITE_NOTIFICATION => 'network_invite_notification',

    EmailTemplateManager::EMAIL_GRP_JOIN_REQUEST => 'grp_join_request',
    EmailTemplateManager::EMAIL_LOOP_INVITE_ACCEPTED_NOTIFICATION => 'loop_invite_accepted_notification',

    EmailTemplateManager::EMAIL_PRODUCT_CONNECT_ACCEPTED_NOTIFICATION => 'product_connect_accepted_notification',
    EmailTemplateManager::EMAIL_COMPANY_CONNECT_ACCEPTED_NOTIFICATION => 'company_connect_accepted_notification',

    EmailTemplateManager::EMAIL_GRP_INVITE_ACCEPTED_NOTIFICATION => 'grp_invite_accepted_notification',
    EmailTemplateManager::EMAIL_NETWORK_INVITE_ACCEPTED_NOTIFICATION => 'network_invite_accepted_notification',
    EmailTemplateManager::EMAIL_REGISTER_NOTIFICATION => 'register_notification',
    EmailTemplateManager::EMAIL_ACTIVE_NOTIFICATION => 'active_notification',
    EmailTemplateManager::EMAIL_REJECT_NOTIFICATION => 'reject_notification',

    EmailTemplateManager::EMAIL_LOOP_INVITE_OUTSIDE_NOTIFICATION => 'loop_invite_outside_notification',
    EmailTemplateManager::EMAIL_NETWORK_INVITE_OUTSIDE_NOTIFICATION => 'network_invite_outside_notification',

    EmailTemplateManager::EMAIL_LOOP_INVITE_INACTIVE_NOTIFICATION => 'loop_invite_inactive_notification',

    EmailTemplateManager::EMAIL_PRODUCT_CONNECT_INACTIVE_NOTIFICATION => 'product_connect_inactive_notification',
    EmailTemplateManager::EMAIL_COMPANY_CONNECT_INACTIVE_NOTIFICATION => 'company_connect_inactive_notification',
    EmailTemplateManager::EMAIL_GRP_INVITE_INACTIVE_NOTIFICATION => 'grp_invite_inactive_notification',
    EmailTemplateManager::EMAIL_NETWORK_INVITE_INACTIVE_NOTIFICATION => 'network_invite_inactive_notification',

    EmailTemplateManager::EMAIL_CHANGE_EMAIL_NOTIFICATION => 'change_email_notification',
    EmailTemplateManager::EMAIL_FORGET_PASSWORD_NOTIFICATION => 'forget_password_notification',
    EmailTemplateManager::EMAIL_REGISTER_ACTIVE_SMALLBIZ_NOTIFICATION => 'register_smallbiz_notification',

    EmailTemplateManager::EMAIL_EVERYDAY_MESSAGE_NOTIFICATION => 'everyday_msg_notification',

    EmailTemplateManager::EMAIL_CHANGE_APPROVED_NOTIFICATION => 'email_change_approved_notification',
    EmailTemplateManager::EMAIL_CHANGE_REJECT_NOTIFICATION => 'email_change_reject_notification',

    EmailTemplateManager::EMAIL_PAYMENT_E_BILLING => 'email_e_bills',
    EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED => 'email_auto_billing_failed'

);
