<?php

/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-11-15
 */

class AccountType {

    const ID_BUYER = 1;
    const ID_SELLER = 2;
    const ID_OTHER = 3;
    const ID_CUSTOMER_SERVICE = 8;
    const ID_ADMIN = 9;

    const LANG_FILE = 'common';
    const LANG_KEY_PREFIX = 'account_type_';

    public static function getAccountTypeNameByID($id) {
        $lang_key = self::LANG_FILE . '.' . self::LANG_KEY_PREFIX . $id;
        return Lang::get($lang_key);
    }

}