<?php

/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-11-15
 */

class VIPLevel {

    const ID_FREE = 1;
    const ID_ENTRY = 2;
    const ID_GOLD = 4;
    const ID_PLATINUM = 8;
    const ID_UNLIMITED = 16;

    const LANG_FILE = 'common';
    const LANG_KEY_PREFIX = 'vip_level_';

    public static function getVIPNameByID($id) {
        $lang_key = self::LANG_FILE . '.' . self::LANG_KEY_PREFIX . $id;
        return Lang::get($lang_key);
    }

    // return array(ID => vip level name)
    public static function getList() {

        return array(
            ID_FREE => self::getVIPNameByID(ID_FREE),
            ID_ENTRY => self::getVIPNameByID(ID_ENTRY),
            ID_GOLD => self::getVIPNameByID(ID_GOLD),
            ID_PLATINUM => self::getVIPNameByID(ID_PLATINUM),
            ID_UNLIMITED => self::getVIPNameByID(ID_UNLIMITED)
        );
    }

}