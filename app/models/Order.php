<?php

/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 */


class Order extends BaseModel {

    const ORDER_STATUS_NEW = 0;
    const ORDER_STATUS_PAID = 1;
    const ORDER_STATUS_RECURRING_SIGNED_UP = 2;
    const ORDER_STATUS_RECURRING_AUTO_PAID = 3;
    const ORDER_STATUS_EXPIRED = 4;
    const ORDER_STATUS_PAID_FAILED = 5;
    const ORDER_STATUS_CANCELED = 6;
    const ORDER_STATUS_DISABLED = 7;

    const ORDER_PACKAGE_JOIN_NETWORK = 0;
    const ORDER_PACKAGE_OF_FREE = 1;
    const ORDER_PACKAGE_OF_ENTRY = 2;
    const ORDER_PACKAGE_OF_GOLD = 4;
    const ORDER_PACKAGE_OF_PLATINUM = 8;
    const ORDER_PACKAGE_OF_UNLIMITED = 16;
    const ORDER_PACKAGE_OF_CREATE_NETWORK = 32;

    const ORDER_PACKAGE_JOIN_NETWORK_NOT_MEMBER = 64;

    protected $table = "orders";

    protected $fillable = array(

        "id",
        "orderid",
        "uid",
        "package",
        "nid",
        "price",
        "period",  // every day / every month / every year
        "quantity",
        "description",
        "creattime",
        "fromtime",
        "endtime",
        "status",
        "paidtime",
        "discount",
        "overage",
        "tax",
        "payments",
        "txnid", // Result from paypal
        "month_count",
        "is_auto_renew",
        "type",
        "next_package"

        // price * month_count * quantity - discount - overage + tax = payments

    );

}