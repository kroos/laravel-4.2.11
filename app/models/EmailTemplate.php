<?php

/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 */

class EmailTemplate extends BaseModel {

    protected $table = "email_templates";

    protected $fillable = array(
        "name",
        "subject",
        "content",
        "content_type",
    );

    public function scopeByName($query, $name) {
        if (!empty($name)) {
            $query->where("name", '=', $name);
        }
    }

}