<?php
/**
 * Created by JetBrains PhpStorm.
 * User: asus
 * Date: 13-9-30
 * Time: 下午2:15
 * To change this template use File | Settings | File Templates.
 */

class Users extends BaseModel {

    protected $table = "users";
    //hide the password,when convert to json
    protected $hidden = array('password');
    public $bid = false;
    public $user_login = false;
    public $user_pass = false;

    protected $appends = ['name'];

    protected $fillable = array(
        'email', 'weblink', 'password', 'first_name', 'last_name', 'second_email',
        'member_type', 'state', 'activeid', 'activetime', 'registration_time', 'vip_level',
        'resend_count', 'refuse_mail', 'deleted', 'has_blog', 'photo',
        'title', 'current_job_title', 'previous_job_title', 'previous_company', 'previous_industry',
        'previous_sec_industry', 'recent_highlights','people_biz', 'people_visible', 'company_biz', 'company_visible',
        'favourite_products',  'product_visible', 'favourite_jobs',
        'favourite_skills', 'involved_associations', 'visited_tradeshows', 'educations', 'city',
        'province', 'country', 'zip_code', 'personal_email', 'telephone',
        'partner_person', 'partner_company', "video_url", "covered_territories","is_import", "in_chat","is_from_mobile"
    );

    public function getNameAttribute(){
        return $this->first_name . ' ' .$this->last_name;
    }

    public function scopeById($query, $user_id) {
        if (!is_null($user_id) && isset($user_id)) {
            $query->where("id", '=', $user_id);
        }
    }

    public function scopeByEmail($query, $email) {
        if (!empty($email)) {
            $query->where("email", "=", $email);
        }
    }

    public function scopeByEmailLike($query, $email) {
        if (!empty($email)) {
            $query->where("email", "like", '%'.$email.'%');
        }
    }


    public function scopeByLetter($query, $letter) {
        if (!empty($letter)) {
            $query->where("email", "like", $letter.'%');
        }
    }

    public function scopeByWeblink($query, $weblink) {
        if (!empty($weblink)) {
            $query->where("weblink", '=', $weblink);
        }
    }

    public function scopeByFirstName($query, $first_name) {
        if (!empty($first_name)) {
            $query->where("first_name", "=", $first_name);
        }
    }

    public function scopeByLastName($query, $last_name) {
        if (!empty($last_name)) {
            $query->where("last_name", "=", $last_name);
        }
    }

    public function scopeByWholeName($query, $whole_name) {
        if (!empty($whole_name)) {
            $query->where(DB::raw('CONCAT(first_name," ", last_name)'), "=", $whole_name);
        }
    }

    public function scopeByMemberType($query, $member_type, $operator = '=') {
        if (!empty($member_type)) {
            $query->where("member_type", $operator, $member_type);
        }
    }

    public function scopeByNotAdmin($query, $isAdmin) {
        if ($isAdmin) {
            $query->where("member_type", "!=", MEMBER_TYPE_ADMIN_ID);
        }
    }

    public function scopeByState($query, $state) {
        if (isset($state)) {
            $query->where("state", "=", $state);
        }
    }

    public function scopeByStates($query, $states) {
        if (isset($states)) {
            $query->whereIn("state", $states);
        }
    }

    public function scopeByRefuseMail($query, $refuse) {
        if (isset($refuse)) {
            $query->where("refuse_mail", "=", $refuse);
        }
    }

    public function scopeByActiveId($query, $active_id) {
        if (!empty($active_id)) {
            $query->where("activeid", "=", $active_id);
        }
    }

    public function scopeGetBuyer($query){
        $query->where('member_type',"=",MEMBER_TYPE_BUYER_ID);
    }

    public function scopeGetSeller($query){
        $query->where('member_type',"=",MEMBER_TYPE_SELLER_ID);
    }

    public function scopeByFavJob($query, $favJob){
        $jb = trim($favJob);
        if(!empty($jb)){
            $query->where('favourite_jobs',"like","%".$jb."%");    
        }
    }

    public function scopeByFavProudct($query, $favProudct){
        $pd = trim($favProudct);
        if(!empty($pd)){
            $query->where('favourite_products',"like","%".$pd."%");    
        }
    }

    public function scopeByFavSkill($query, $favSkill){
        $sk = trim($favSkill);
        if(!empty($sk)){
            $query->where('favourite_skills',"like","%".$sk."%");    
        }
    }

    public function scopeByInvolvedAssociation($query, $involvedAsso){
        $iv = trim($involvedAsso);
        if(!empty($iv)){
            $query->where('involved_associations',"like","%".$iv."%");    
        }
    }

    public function scopeByVisitedTradeshow($query, $visitedTradeshow){
        $vt = trim($visitedTradeshow);
        if(!empty($vt)){
            $query->where('visited_tradeshows',"like","%".$vt."%");    
        }
    }

    public function scopeByEducations($query, $educations){
        $edu = trim($educations);
        if(!empty($edu)){
            $query->where('educations',"like","%".$edu."%");    
        }
    }

    /**
     * filter by registration time
     *
     * @param $query
     * @param int $startTime
     * @param int $endTime
     * @return mixed
     */
    public function scopeByRegistrationTime( $query, $startTime, $endTime ){
        if( ! empty( $startTime ) && ! empty( $endTime ) ){
            $query->whereBetween( "registration_time", array( $startTime, $endTime ) );
        }else if( ! empty( $startTime ) ){
            $query->where( "registration_time", ">=", $startTime );
        }else if( ! empty( $endTime ) ){
            $query->where( "registration_time", "<=", $endTime );
        }
    }

    /**
     * filter by activation time
     *
     * @param $query
     * @param int $startTime
     * @param int $endTime
     * @return mixed
     */
    public function scopeByActivationTime( $query, $startTime, $endTime ){
        if( ! empty( $startTime ) && ! empty( $endTime ) ){
            $query->whereBetween( "activetime", array( $startTime, $endTime ) );
        }else if( ! empty( $startTime ) ){
            $query->where( "activetime", ">=", $startTime );
        }else if( ! empty( $endTime ) ){
            $query->where( "activetime", "<=", $endTime );
        }
    }

    public function scopeByImport($query, $is_import) {
        if ($is_import !== false) {
            $query->where("is_import", "=", $is_import);
        }
    }

    //many to many
    public function company(){
        return $this->belongsToMany("Company", "company_user", "user_id", "company_id");
    }

    public function getUserFullName(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getInfoAttribute(){

        $info = "";

        if (!empty($this->current_job_title)) {
            $info .= $this->current_job_title . ' - ';
        }

        if (!empty($this->company)) {
            $info .= $this->company . ' - ';
        } else {
            $companyApi = new CompanyApi();
            $userCompany = $companyApi->getCompanyByUser($this->id);
            if (!empty($userCompany)) {
                $comapnyName = $userCompany->name;
                if (!empty($comapnyName)) {
                    $info .= $comapnyName . ' - ';
                }
            }
        }

        if($info){
            $info = rtrim($info, ' - ');
            $info .= '<br/>';
        }

        if (!empty($this->city)) {
            $info .= $this->city . ' - ';
        }

        if (!empty($this->province)) {
            $province = Lang::get('province')[$this->province];
            if(empty($province)){
                $info .=  $this->province. ' - ';
            }else{
                $info .=  $province. ' - ';
            }

        }

        $country = CacheApi::getUserProperty($this->id, 'country');
        if (!empty($country)) {
            $country = trans('country.' . strtolower($country));
            $info .= $country;
        }

        if ($info) {
            $info = rtrim($info, ' - ');
        }

        return $info;
    }

    public static function updateIndex($uid){
        DB::table('users')->where("id", $uid)->increment('no_meaning');
    }
}