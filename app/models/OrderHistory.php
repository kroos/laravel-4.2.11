<?php
/**
 * Created by JetBrains PhpStorm.
 * User: long
 * Date: 16-6-17
 * Time: 下午5:08
 * To change this template use File | Settings | File Templates.
 */
class OrderHistory extends BaseModel
{
    public $table = "orders_history";

    protected $fillable = array(

        "id",
        "orderid",
        "uid",
        "package",
        "nid",
        "price",
        "period",  // every day / every month / every year
        "quantity",
        "description",
        "creattime",
        "fromtime",
        "endtime",
        "status",
        "paidtime",
        "discount",
        "overage",
        "tax",
        "payments",
        "txnid", // Result from paypal
        "month_count",
        "is_auto_renew",
        "type",
        "function_type",
        "function_related_id"
        // price * month_count * quantity - discount - overage + tax = payments

    );
}
