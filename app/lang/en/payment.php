<?php

return array(

    // 'upgrade_lab_page_title' => 'Upgrade VIP Level',
    'upgrade_lab_page_title' => 'TraDove',

    'level_name_1' => 'Free',
    'level_name_2' => 'Entry',
    'level_name_4' => 'Gold',
    'level_name_8' => 'Platinum',
    'level_name_16' => 'Unlimited',

    'vip_level_lab_annual' => 'Annual',
    'vip_level_lab_monthly' => 'per month',
    'vip_level_lab_year' => 'per year',
    'vip_level_lab_year_discount' => '20% off',

    'vip_level_lab_upgrade_your_account' => 'Upgrade your account',
    'vip_level_lab_other_type_user_notice' => 'If your account-type is Other, you may only choose a Free Membership Plan.',
    'vip_level_lab_features' => 'Features',
    'vip_level_lab_item1' => 'Priority Customer Service',
    'vip_level_lab_item2' => 'Most Recent Profile Views',
    'vip_level_lab_item2_desc' => '(See who viewed your Personal, Products/Services, & Company Profiles)',
    'vip_level_lab_item3' => 'Search Results Limit',
    'vip_level_lab_item3_desc' => '(Loop, Products/Services, & Company)',
    'vip_level_lab_item4' => 'Maximum Products/Services Displayed',
    'vip_level_lab_item5' => 'Proposal and Private Group Discussion Limit/week',
    'vip_level_lab_item6' => 'Profile Visibility Boost',
    'vip_level_lab_item6_desc' => '(Appear more often in Suggested Loops, Products/Services, & Company)',
    'vip_level_lab_item7' => 'Owned Groups Limit',
    'vip_level_lab_item8' => 'Seller Group Postings/week',
    'vip_level_lab_item9' => 'Seller Leads/week',
    'vip_level_lab_item10' => 'Joined Private Networks Limit',

    'vip_level_lab_basic_free' => 'Basic Free',
    'vip_level_lab_your_current_account' => 'Your Current Account',
    'vip_level_btn_upgrade' => 'Upgrade',
    'vip_level_btn_renew' => 'Renew',

    // 'step1_lab_page_title' => 'Create Order',
    'step1_lab_page_title' => 'TraDove',
    'step1_lab_payment' => 'Payment',
    'step1_lab_fill_orders' => 'Order Summary',
    'step1_lab_subscription' => 'Subscription:',
    'step1_lab_time_frame' => 'Time Frame:',
    'step1_lab_total_due' => 'Total Due',
    'step1_lab_onetime_purchase' => 'One-time Purchase:',
    'step1_btn_create_order' => 'Next',
    'step1_btn_cancel' => 'Cancel',


    // 'step2_lab_page_title' => 'Order Detail',
    'step2_lab_page_title' => 'TraDove',
    'step2_lab_payment' => 'Payment',
    'step2_lab_payment_details' => 'Payment Details',

    'step2_lab_order_id' => 'Order ID:',

    'step2_lab_purchased_item' => 'Purchased item:',
    'step2_lab_unit_price' => 'Unit price:',
    'step2_lab_order_quantity' => 'Order quantity:',
    'step2_lab_auto_renew' => 'Auto-renew',
    'step2_lab_sub_total' => 'Subtotal:',
    'step2_lab_credit_previous' => 'Deduction from account credit remaining:',
    'step2_lab_total_payment' => 'Total payment:',


    'step2_lab_subscription' => 'Subscription:',
    'step2_lab_time_span' => 'Time span:',
    'step2_lab_total_purchases' => 'Total Purchases:',
    'step2_lab_discount' => 'Discount:',
    'step2_lab_estimated_sales_tax' => 'Estimated Sales Tax:',
    'step2_lab_total_due' => 'Total Due',
    'step2_btn_make_payment' => 'Make Payment',
    'step2_btn_cancel' => 'Cancel',

    'setting_lab_detail_from_credit_card' => 'Credit card',
    'setting_lab_detail_from_account' => 'PayPal'

);
