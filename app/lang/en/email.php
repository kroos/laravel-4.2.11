<?php

return array(

    'temp_master.lab.title_dear'  =>  'Dear :name',
    'temp_master.lab.welcome'  =>  'Welcome to ',
    'temp_master.lnk.tradove'  =>  'TraDove',
    'temp_master.lab.description'  =>  " We're so glad you've chosen to join TraDove, the premier B2B social network that allows corporate buyers and sellers, both large and small, the ability to find, connect, and conduct global trade with each other.",
    'temp_master.lab.sincerely'  =>  'Sincerely,',
    'temp_master.lab.service_team'  =>  'TraDove Customer Service Team',
    'temp_master.lab.customer_email'  =>  'customer.service@tradove.com',
    'temp_master.lab.copy_right'  =>  'Copyright © 2010, 2016 TraDove, Inc. All rights reserved.',

    //for group discussion and posting
    'grp_posting.lab.content'  =>  ':sender has launched a  :type',
    'grp_posting.lab.type_discussion'  =>  'discussion',
    'grp_posting.lab.type_posting'  =>  'posting',

    'login_forget_pwd.lab.content'  => 'Your new password is :password',
    'login_forget_pwd.lab.subject'  =>  'Your TraDove password',

    'email2inactive.subject' => ":name has sent a message to you",
    'email2inactive.content' => "Please click the button to view the detail",

    // All email template subjects
    'subject_' . EmailTemplateManager::EMAIL_RECENT_UPDATES  => "Recent updates from your connections on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_ADMIN2ALL => ":sys_email_title",
    'subject_' . EmailTemplateManager::EMAIL_MESSAGE_NOTIFICATION => ":sender_name sent you a message on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_PROPOSAL_NOTIFICATION => ":sender_name has sent you a business proposal on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_DISCUSSION_NOTIFICATION => ":sender_name has posted a private discussion on TraDove ",

    'subject_' . EmailTemplateManager::EMAIL_GRP_POSTING_NOTIFICATION => "New buyer posting in the group on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_GRP_DISCUSSION_NOTIFICATION => "New discussion in the group on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_LEAD_INDUSTRY_NOTIFICATION => "New buying lead for your industry on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_PROPOSAL_REPLY_NOTIFICATION => "There's a new reply to your proposal",
    'subject_' . EmailTemplateManager::EMAIL_DISCUSSION_REPLY_NOTIFICATION => "There's a new reply to your private discussion",

    'subject_' . EmailTemplateManager::EMAIL_GRP_POSTING_REPLY_NOTIFICATION => "There's a new reply to your group posting",
    'subject_' . EmailTemplateManager::EMAIL_GRP_DISCUSSION_REPLY_NOTIFICATION => "There's a new reply to the group discussion you are in",
    'subject_' . EmailTemplateManager::EMAIL_LEAD_REPLY_NOTIFICATION => "There's a new reply to the lead you posted",

    'subject_' . EmailTemplateManager::EMAIL_BLOG_REPLY_NOTIFICATION => "There's a new comment on your TraDove blog",

    'subject_' . EmailTemplateManager::EMAIL_LOOP_INVITE_NOTIFICATION => ":sender_name has invited you to join their Business Loop on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_PRODUCT_CONNECT_NOTIFICATION => ":sender_name would like to connect with your Products/Services on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_COMPANY_CONNECT_NOTIFICATION => ":sender_name would like to connect with your company on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_GRP_INVITE_NOTIFICATION => ":sender_name has invited you to join their group on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_NETWORK_INVITE_NOTIFICATION => ":sender_name has invited you to join their Private Network",

    'subject_' . EmailTemplateManager::EMAIL_GRP_JOIN_REQUEST => ":sender_name would like to join your group on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_LOOP_INVITE_ACCEPTED_NOTIFICATION => ":sender_name has accepted your loop invitation on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_PRODUCT_CONNECT_ACCEPTED_NOTIFICATION => ":sender_name has accepted your Products/Services connection invitation on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_COMPANY_CONNECT_ACCEPTED_NOTIFICATION => ":sender_name has accepted your company connection invitation on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_GRP_INVITE_ACCEPTED_NOTIFICATION => ":sender_name has accepted your group invitation on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_NETWORK_INVITE_ACCEPTED_NOTIFICATION => ":sender_name has accepted your network invitation on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_REGISTER_NOTIFICATION => "Your TraDove registration information has been submitted for review",
    'subject_' . EmailTemplateManager::EMAIL_ACTIVE_NOTIFICATION => "Registration Confirmation for TraDove Global Business Network",
    'subject_' . EmailTemplateManager::EMAIL_REJECT_NOTIFICATION => "Your Registration with TraDove",

    'subject_' . EmailTemplateManager::EMAIL_LOOP_INVITE_OUTSIDE_NOTIFICATION => ":sender_name has invited you to join him/her on TraDove Business Network",
    'subject_' . EmailTemplateManager::EMAIL_NETWORK_INVITE_OUTSIDE_NOTIFICATION => ":sender_name has invited you to join their private B2B social network on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_LOOP_INVITE_INACTIVE_NOTIFICATION => ":sender_name has invited you to join their Business Loop on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_PRODUCT_CONNECT_INACTIVE_NOTIFICATION => ":sender_name is interested in your products/services on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_COMPANY_CONNECT_INACTIVE_NOTIFICATION => ":sender_name wants to connect with your company on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_GRP_INVITE_INACTIVE_NOTIFICATION => ":sender_name has invited you to join their group on TraDove",
    'subject_' . EmailTemplateManager::EMAIL_NETWORK_INVITE_INACTIVE_NOTIFICATION => ":sender_name has invited you to be in their private social network on TraDove",

    'subject_' . EmailTemplateManager::EMAIL_CHANGE_EMAIL_NOTIFICATION => "Confirm TraDove registration email change",
    'subject_' . EmailTemplateManager::EMAIL_FORGET_PASSWORD_NOTIFICATION => "Reset Your TraDove Password",

    'subject_' . EmailTemplateManager::EMAIL_REGISTER_ACTIVE_SMALLBIZ_NOTIFICATION => 'Registration Confirmation for TraDove Business Network',

    'subject_' . EmailTemplateManager::EMAIL_EVERYDAY_MESSAGE_NOTIFICATION => 'New Message(s) on TraDove',

    'subject_' . EmailTemplateManager::EMAIL_CHANGE_APPROVED_NOTIFICATION => 'Approval of your TraDove email change',
    'subject_' . EmailTemplateManager::EMAIL_CHANGE_REJECT_NOTIFICATION => 'Email Change Unsuccessful',
    // payment
    'subject_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING => 'Your TraDove order receipt',
    'subject_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED => 'Your TraDove membership requires your attention',

    // link added by long start
    'link_' . EmailTemplateManager::EMAIL_ACTIVE_NOTIFICATION => 'Activate TraDove',
    'link_' . EmailTemplateManager::EMAIL_LOOP_INVITE_INACTIVE_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_LOOP_INVITE_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_LOOP_INVITE_OUTSIDE_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_REGISTER_ACTIVE_SMALLBIZ_NOTIFICATION => 'Activate TraDove',
    'link_' . EmailTemplateManager::EMAIL_CHANGE_EMAIL_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_FORGET_PASSWORD_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_CHANGE_APPROVED_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_CHANGE_REJECT_NOTIFICATION => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_PAYMENT_E_BILLING => 'Go to TraDove',
    'link_' . EmailTemplateManager::EMAIL_AUTO_BILLING_FAILED => 'Go to TraDove'

    // link added by long end
);