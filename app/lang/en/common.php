<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-11-15
 */

// 公用模块
return array(

    
    'blog_header_title' => 'The Largest and Most Trusted Global Business Network',
    'blog_navi_title' => 'TraDove Blogs',
    'blog_navi_about' => 'About TraDove',
    'blog_navi_article' => 'TraDove Privacy Policy',

    'header_btn_login' => 'Login',

    'footer_lnk_about' => 'About',
    'footer_lnk_problem_report' => 'Problem Report',
    'footer_lnk_help_center' => 'Help Center',
    'footer_lnk_advertising' => 'Advertising',
    'footer_lnk_upgrade_your_account' => 'Upgrade Your Account',
    'footer_lnk_contact_us' => 'Contact Us',

    //outside listing
    'footer_lnk_browse_seller' => 'Browse Seller',
    'footer_lnk_browse_buyer' => 'Browse Buyer',
    'footer_lnk_browse_other' => 'Browse Other',
    'footer_lnk_browse_company' => 'Browse Companies',
    'footer_lnk_browse_product_service' => 'Browse Products/Services',
    //'outside_list_title' => 'Outside Listings',
    'outside_list_title' => 'TraDove',
    'outside_list_subtitle' => 'Browse :type',
    'outside_list_subtitle_buyer' => 'Buyer',
    'outside_list_subtitle_seller' => 'Seller',
    'outside_list_subtitle_other' => 'Other',
    'outside_list_subtitle_company' => 'Companies',
    'outside_list_subtitle_ps' => 'Products/Services',
    'outside_detail_return' => 'Return to', 
    'outside_detail_position' => 'Position',
    'outside_detail_company' => 'Company',
    'outside_detail_country' => 'Country',
    'outside_detail_industry' => 'Industry',
    'outside_listing_login'=>'Log in for more information',
    'outside_detail_website' => 'Website',
    'outside_detail_primary_industry' => 'Primary Industry',
    'outside_detail_desc' => 'General Description',

    'footer_lab_copyright' => 'Copyright &copy; 2010, 2016 TraDove, Inc. All rights reserved.',

    // View Nav
    // Menu items
    'nav_menu_home' => 'Home',
    'nav_menu_home_profile' => 'Profile',
    'nav_menu_home_reference' => 'Reference',
    'nav_menu_home_loop' => 'Loop',
    'nav_menu_home_group' => 'Group',

    'nav_menu_people' => 'People',
    'nav_menu_people_profile' => 'Profile',
    'nav_menu_people_reference' => 'Reference',
    'nav_menu_people_loop' => 'Loop',
    'nav_menu_people_provide_reference' => 'Provide Reference',
    'nav_menu_people_group' => 'Groups',

    'nav_menu_product' => 'Products/Services',
    'nav_menu_product_profile' => 'Profile',
    'nav_menu_product_reference' => 'Reference',
    'nav_menu_product_connect' => 'Connect',
    'nav_menu_product_browse' => 'Browse',

    'nav_menu_company' => 'Company',
    'nav_menu_company_profile' => 'Profile',
    'nav_menu_company_reference' => 'Reference',
    'nav_menu_company_connect' => 'Connect',
    'nav_menu_company_browse' => 'Browse',

    'nav_menu_industry' => 'Industry',
    'nav_menu_industry_profile' => 'Profile',
    'nav_menu_industry_reference' => 'Reference',
    'nav_menu_industry_loop' => 'Loop',
    'nav_menu_industry_group' => 'Group',

    'nav_menu_leads' => 'Buying/Selling leads',
    'nav_menu_leads_profile' => 'Profile',
    'nav_menu_leads_reference' => 'Reference',
    'nav_menu_leads_loop' => 'Loop',
    'nav_menu_leads_group' => 'Group',

    'nav_menu_news' => 'News',
    'nav_menu_blogs' => 'Blogs',
    'nav_menu_feedback' => 'Help & Feedback',

    'nav_menu_news_blogs' => 'Blogs/News',
    'nav_menu_news_blogs_profile' => 'Profile',
    'nav_menu_news_blogs_reference' => 'Reference',
    'nav_menu_news_blogs_loop' => 'Loop',
    'nav_menu_news_blogs_group' => 'Group',

    'nav_menu_network_manage'=>'Network Administration',
    'nav_menu_network_common'=>'Network Invite',

    'nav_menu_business_tools' => 'Business Tools',
    'nav_menu_upgrade' => 'Upgrade',


    // View s_account
    's_account_lnk_sign_out' => 'Sign Out',
    's_account_lab_account_type' => 'Account Type:',
    's_account_lab_vip_level' => 'VIP Level:',
    's_account_lab_email' => 'Email:',
    's_account_lab_tel' => 'Tel:',
    's_account_lnk_upgrade' => 'Upgrade',
    's_account_lnk_edit' => 'Edit',
    's_account_lnk_change_account_email' => 'Change Account Email',
    's_account_lnk_change_password' => 'Change Password',
    's_account_lnk_setting' => 'Email Notification Settings',
    's_account_lnk_payment_setting' => 'Payment Settings',

    // View s_invite
    's_invite_lab_contacts' => 'Invite',
    's_invite_lab_connections' => 'Invite your friends to join TraDove.',

    // View s_message
    's_message_lab_message' => 'Messages :msgNum',
    's_message_lab_by' => 'By ',
    's_message_lab_invitation' => 'Invitations :invitationNum',
    's_message_lab_proposal' => 'Proposals :proposalNum',
    's_message_lab_private_discussion' => 'Private Group Discussions :discussionNum',


    // View s_network
    's_network_lab_current_network' => 'Current Network',
    's_message_lnk_enter_general_network' => 'Enter General Network',
    's_message_lab_select_your_network' => 'Select Your Private Network',
    's_message_lnk_join_network' => 'Join a Network',
    's_message_lnk_create_network' => 'Create a Network',

    // Account Type
    'account_type_' . AccountType::ID_BUYER => 'Buyer',
    'account_type_' . AccountType::ID_SELLER => 'Seller',
    'account_type_' . AccountType::ID_OTHER => 'Other',
    'account_type_' . AccountType::ID_CUSTOMER_SERVICE => 'Customer Service',
    'account_type_' . AccountType::ID_ADMIN => 'Admin',


    // VIP Level
    'vip_level_' . VIPLevel::ID_FREE => 'Free',
    'vip_level_' . VIPLevel::ID_ENTRY => 'Entry',
    'vip_level_' . VIPLevel::ID_GOLD => 'Gold',
    'vip_level_' . VIPLevel::ID_PLATINUM => 'Platinum',
    'vip_level_' . VIPLevel::ID_UNLIMITED => 'Unlimited',

    'pagination.btn_previous'   =>  'Previous',
    'pagination.btn_next'   =>  'Next',

    'btn_submit' => 'Submit',
    'btn_cancel' => 'Cancel',

    'pop_email_box_lab_title' => 'Assign :typeName Ownership',
    'pop_email_box_lab_person' => 'The person you want to assign to',
    // 'pop_email_box_lab_note' => '',

    //admin
    'blog_admin_email_twice' => 'The two email addresses do not match.',
    'blog_admin_user_not_exist' => 'The email address does not match any blog account.',
    'blog_admin_user_already_admin' => 'The user :providedEmail has had the administrator role.',
    'blog_admin_user_be_admin' => 'The user :providedEmail has been granted the administrator role successfully.',

    'lab_success' => 'Success',
    'lab_failed' => 'Failed',

    'owner' => 'Owner',
    'right_side_view_tracking_title' => 'people who viewed your profile',
    'right_side_view_tracking_more' => 'See More ...',

    'letters_left'      =>  'characters left',
);