function TChart() {
    var chart = {};

    var width = 800,
        height = 300,
        margins = {
            top: 30,
            left: 50,
            right: 30,
            bottom: 30
        },

        x,
        y,
        data = [],
        colors = d3.scale.category10(),
        svg,
        bodyG,
        line,
        timeformat,
        displayType,
        tickValues,
        xScale,
        start_date,
        end_date,
        container,
        x_axes_interval_width = 75;

    chart.render = function () {

        if (!svg) {

            if (displayType == 1) {
                tickValues = end_date.diff(start_date, 'days');
                timeformat = function () {
                    return d3.time.format("%Y/%m/%d");
                };
                width = x_axes_interval_width * tickValues;

            } else if (displayType == 2) {

                //end_date = moment(endDate).format('YYYY-MM-DD');
                var startYear = start_date.get('year');
                var startMonth = start_date.get('month');
                var endYear = end_date.get('year');
                var endMonth = end_date.get('month');
                if (parseInt(endMonth) - parseInt(startMonth) <= 3) {
                    endMonth = endMonth + 2;
                }


                //把开始时间与结束时间都设置为月初
                var newStartDate = start_date;
                var newStartDateStr = newStartDate.format('YYYY-MM') + '-01';
                start_date = moment(newStartDateStr);
                var newEndDate = end_date;
                end_date = moment(newEndDate.format('YYYY-MM') + '-01');

                width = x_axes_interval_width * (((endYear - startYear) * 12 + endMonth) - startMonth);
                tickValues = d3.time.months;

                timeformat = function () {
                    return d3.time.format("%Y/%m");
                }

            }

            svg = d3.select(container)
                .html('')
                .append("svg")
                .attr("height", height)
                .attr("width", width);

            renderAxes();

            defineBodyClip();
        }
        renderBody()
    };

    function renderAxes() {

        var axesG = svg.append("g")
            .attr("class", "axes");

        renderXAxis(axesG);

        renderYAxis(axesG);

    }

    function renderXAxis(axesG) {

        xScale = d3.time.scale()
            .domain([start_date, end_date])
            .range([0, quadrantWidth()]);

        var xAxis;
        if (displayType == 1) {
            xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(tickValues)
                .tickFormat(timeformat());

        } else {
            xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(d3.time.months, 1)
                .tickFormat(timeformat());
        }


        axesG.append("g")
            .attr("class", "x axis")
            .attr("transform", function () {
                return "translate(" + xStart() + "," + yStart() + ")";
            })
            .call(xAxis);

        d3.selectAll("g.x g.tick")
            .append("line")
            .classed("grid-line", true)
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", 0)
            .attr("y2", -quadrantHeight());

    }

    function maxYAxisValue() {
        var max = 0;
        var firstSer = data[0];
        var len = firstSer.length;
        for (var i = 0; i < len; i++) {
            if (firstSer[i].y > max) {
                max = firstSer[i].y;
            }
        }
        return max;
    }


    function renderYAxis(axesG) {

        //确定y轴高度
        var maxYvalue = (parseInt(maxYAxisValue() / 100) + 1) * 100;

        y = d3.scale.linear().domain([0, maxYvalue]);

        var yAxis = d3.svg.axis()
            .scale(y.range([quadrantHeight(), 0]))
            .orient("left");

        axesG.append("g")
            .attr("class", "y axis")
            .attr("transform", function () {
                return "translate(" + xStart() + "," + yEnd() + ")";
            })
            .call(yAxis);

        d3.selectAll("g.y g.tick")
            .append("line")
            .classed("grid-line", true)
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", quadrantWidth())
            .attr("y2", 0);

    }


    function defineBodyClip() {
        var padding = 5;

        svg.append("defs")
            .append("clipPath")
            .attr("id", "body-clip")
            .append("rect")
            .attr("x", 0 - padding)
            .attr("y", 0)
            .attr("width", quadrantWidth() + 2 * padding)
            .attr("height", quadrantHeight());

    }

    function renderBody() {

        if (!bodyG)
            bodyG = svg.append("g")
                .attr("class", "body")
                .attr("transform", "translate("
                    + xStart() + ","
                    + yEnd() + ")")
                .attr("clip-path", "url(#body-clip)");

        renderLines();

        renderDots();
    }

    function renderLines() {

        line = d3.svg.line()
            .x(function (d) {
                return xScale(getDate(d.x));
            })
            .y(function (d) {
                return y(d.y);
            });

        bodyG.selectAll("path.line")
            .data(data)
            .enter()
            .append("path")
            .style("stroke", function (d, i) {
                return colors(i);
            })
            .attr("class", "line");

        bodyG.selectAll("path.line")
            .data(data)
            .transition()
            .attr("d", function (d) {
                return line(d);
            });

    }

    function renderDots() {

        var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        data.forEach(function (list, i) {
            bodyG.selectAll("circle._" + i)
                .data(list)
                .enter()
                .append("circle")
                .attr("class", "dot _" + i)
                .on("mouseover", function (d) {
                    div.transition()
                        .duration(200)
                        .style("opacity", .9);
                    var currentDate;
                    if (displayType == 1) {
                        currentDate = moment(d.x).format('YYYY-MM-DD');
                    } else {
                        currentDate = moment(d.x).format('YYYY-MM');
                    }
                    div.html("Date:" +currentDate + "<br/>Quantity:" +  d.y)
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                })
                .on("mouseout", function (d) {
                    div.transition()
                        .duration(2000)
                        .style("opacity", 0);
                });

            bodyG.selectAll("circle._" + i)
                .data(list)
                .style("stroke", function (d) {
                    return colors(i);
                })
                .transition()
                .attr("cx", function (d) {
                    return xScale(getDate(d.x));
                })
                .attr("cy", function (d) {
                    return y(d.y);
                })
                .attr("r", 4.5);
        });

    }

    function getDate(d) {
        return moment(d);
    }

    function xStart() {
        return margins.left;
    }

    function yStart() {
        return height - margins.bottom;
    }

    function xEnd() {
        return width - margins.right;
    }

    function yEnd() {
        return margins.top;
    }

    function quadrantWidth() {
        return width - margins.left - margins.right;
    }

    function quadrantHeight() {
        return height - margins.top - margins.bottom;
    }

    chart.width = function (w) {
        if (!arguments.length) return width;
        width = w;
        return chart;
    };

    chart.height = function (h) {
        if (!arguments.length) return height;
        height = h;
        return chart;
    };

    chart.margins = function (m) {
        if (!arguments.length) return margins;
        margins = m;
        return chart;
    };

    chart.colors = function (c) {
        if (!arguments.length) return colors;
        colors = c;
        return chart;
    };

    chart.x = function (_x) {
        if (!arguments.length) return x;
        x = _x;
        return chart;
    };

    chart.y = function (_y) {
        if (!arguments.length) return y;
        y = _y;
        return chart;
    };

    chart.addSeries = function (series) {
        data.push(series);
        return chart;
    };

    chart.setDate = function (startDate, endDate) {
        start_date = moment(startDate);
        end_date = moment(endDate);
        return chart;
    };

    chart.setDisplayType = function (displayTypeV) {
        displayType = displayTypeV;
        return chart;
    };

    chart.setContainer = function (containerId) {
        container = containerId;
        return chart;
    };

    return chart;
}