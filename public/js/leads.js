function change_industry(o){
    window.location.href = base_url + "leads?i=" + $(o).val();
}

function show_more(type,lead_id){
    var url;
    if(type == 'joined'){
        url = "joined_leads_data";
    }
    if(type == "my"){
        url = base_url + "my_leads_show_more";
    }
    if(type == "all"){
        url = base_url + "leads_show_more";
    }
    if(type == "commit"){
        url = base_url + "leads_commits?posting_id=" + lead_id;
    }

    $.ajax({
        url:url,
        data:{
            start:$("#start").val(),
            industry :   $('select[name="industry"]').val()
        },
        beforeSend:function(){
           $(".left_top_readmore").unbind('click');
           $(".left_top_readmore").html(json.loading);
        },
        error:function(){
            $(".left_top_readmore").html(json.timeout);
            $(".left_top_readmore").click(function(){
                show_more(type,lead_id);
            });
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}

function do_delete(ids,loading){
    $.ajax({
        url: base_url + "leads_delete",
        data:{
            'ids': ids.join(',')
        },
        error:function(){
            layer.close( loading );
        },
        success:function(r){
            for(var i = 0; i < ids.length; i++){
                $("#lead_" + ids[i]).remove();
            }
            $('input[name="select_all"]').attr("checked", false);
            layer.close( loading );
            tradove.msg(json.del + ' ' + json.success);
        }
    });
}

function delete_all(){
    var tmp = new Array();

    $('input[name="select_one"]:checked').each(function(){
        tmp.push($(this).val());
    });

    if(tmp.length > 0){
        tradove.confirm( json.confirm, function( loading ){
            do_delete(tmp,loading);
        });
    }
}

function delete_one(id){
    tradove.confirm( json.confirm, function( loading ){
        do_delete([id], loading);
    });
}

function check_form(){
    title = $('input[name="title"]');
    if(title.val() == ''){
        layer.tips( json.subject_check, title, 3, title.offsetWidth, 1);
        title.focus();

        return false;
    }

    industry = $('#industry');
    if(!industry.val()){
        layer.tips( json.industry_check, $('#showIndustry'), 3, industry.offsetWidth, 1);

        return false;
    }

    start = $('input[name="start_time"]');
    if(start.val() == ''){
        layer.tips( json.start_date_check, start, 3, start.offsetWidth, 0);
        return false;
    }

    end = $('input[name="end_time"]');
    if(end.val() == ''){
        layer.tips( json.end_date_check, end, 3, end.offsetWidth, 0);
        return false;
    }

    content = $('textarea[name="content"]');
    if(content.val() == ''){
        layer.tips( json.content_check, content, 3, content.offsetWidth, 0);
        content.focus();

        return false;
    }

    return true;
}

function leads_search_show_more(){
    $.ajax({
        url: "leads_search_data",
        data:{
            start:$("#start").val()
        },
        beforeSend:function(){
            $(".left_top_readmore").unbind('click');
            $(".left_top_readmore").html(json.loading);
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}

function commit(){
    if(!$(".textarea470b").val())
        return;

    $("#leads_comment_form").submit();
}

function delete_comment(id){
    var i;
    $.ajax({
        url:"/leads_comment_delete/" + id,
        beforeSend:function(){
            i = layer.load();
        },
        success:function(r){
            layer.close(i);
            if(r){
                $("#leads_comment_" + id).remove();
            }else{
                tradove.msg("Delete failed.");
            }
        }
    });
}