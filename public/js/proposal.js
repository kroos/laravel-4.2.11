function compose(){
    receiver_name = $('input[name="receiver_name"]');
    receiver_id = $('input[name="receiver_id"]');
    if(receiver_id.val() == ""){
        layer.tips( json.receiver_check,  receiver_name, 3, receiver_name.width(), 0);
        return false;
    }
    subject = $('input[name="subject"]');
    if(subject.val() == ""){
        layer.tips( json.subject_check,  subject, 3, subject.width(), 0);
        return false;
    }
    content = $('textarea[name="content"]');
    if(content.val() == ""){
        layer.tips( json.content_check,  content, 3, content.width(), 0);
        return false;
    }

    $(".left_650_c2_a_r_list").showLoading();

    return true;
}

function addition(){

    content = $('textarea[name="content"]');
    if(content.val() == ""){
        layer.tips( json.content_check,  content, 3, content.width(), 0);
        return false;
    }

    return true;
}

function edit(){

    subject = $('input[name="subject"]');
    if(subject.val() == ""){
        layer.tips( json.subject_check,  subject, 3, subject.width(), 0);
        return false;
    }

    content = $('textarea[name="content"]');
    if(content.val() == ""){
        layer.tips( json.content_check,  content, 3, content.width(), 0);
        return false;
    }

    return true;
}

function deleteMulti() {
    var ids = getSelectedIds();
    if(null == ids || ids.length == 0) {
        tradove.msg("Please select one proposal first.");
    }

    deleteProposal(ids);
}

function deleteOne(id, box) {
    var ids = [id];
    deleteProposal(ids, box);
}

function deleteProposal(ids, box) {

    var ajaxUrl = '/proposal/delete';
    var successMsg = ' proposal(s) have been deleted.'
    var failMsg = 'Delete proposal(s) failed';

    $.ajax({
        url: ajaxUrl,
        dataType: 'json',
        type: 'POST',
        data: {
            "ids[]": ids
        },

        success: function (data) {
            if(data.affected > 0) {
                tradove.msg(data.affected + successMsg);

                if(box != null && box == "out") {
                    self.location = "/proposal/outbox";
                } else {
                    for(i=0; i<data.affected; i++) {
                        id = data.ids[i];
                        var div_id = 'div_' + id;
                        $('#' + div_id).remove();
                    }
                }

            } else {
                tradove.msg(failMsg);
            }
        },

        error: function (data) {
            tradove.msg(failMsg);
        }

    });
}