var _gaq = _gaq || [];
_gaq.push([ '_setAccount', 'UA-24246288-1' ]);
_gaq.push([ '_trackPageview' ]);

(function(){
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();



/**
 * Created with JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-11-22
 */

function is_valid_url(url)
{
    return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
}

String.prototype.trim = function() {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

function trim_words(theString, numWords) {
    if(null == theString || theString == "") {
        return "";
    }

    expString = theString.split(/\s+/, numWords);
    theNewString = expString.join(" ");
    return theNewString;
}


// Multi checkbox selecting
function getSelectedIds() {

    /**
     var ids = [];
     $("input[type='checkbox'][name='select_one']:checked").map(function(){
        ids.push($(this).val());
    });
     */

    // More elegant than the cods above
    var ids = $("input[type='checkbox'][name='select_one']:checked").map(function () {
        return $(this).val();
    }).get();

    return ids;
}

function getSelectedNames() {
    var cb = $("input[type='checkbox'][name='select_one']:checked").siblings("input[name='select_loop_name']");
    var names = cb.map(function () {
        return $(this).val();
    }).get();

    return  names;
}


$("div[id]").one("click", function () {
    var idString = $(this).text() + " = " + $(this).attr("id");
    $(this).text(idString);
});


function greaterThanMaxFiveWords(values) {
    var validationFlag = false;
    $(values).each(function () {
        if ($(this).val() != '') {
            var items = $(this).val().trim().split(" ");
            if (items.length > 5) {
                $(this).attr('class','input180_red');
                $(this).focus();
                validationFlag = true;
                return false;
            }else{
                $(this).attr('class','input180');
            }
        }
    });
    return validationFlag;
}

function regChangeToDefaultLogo(changeBtn,logoContainerSelector,displayNameContainerSelector,fileInputSelector){
    $(changeBtn).click(function () {
        var defaultLogo = '/imgs/photo_people.gif';
        $(logoContainerSelector).attr('src', defaultLogo + "?" + Math.random());
        $(displayNameContainerSelector).val('');
        $(fileInputSelector).val('');

        var existDisplayNameBtn = $(fileInputSelector).parent().next().find("span.upload_file");
        if(existDisplayNameBtn.length!=0){
            existDisplayNameBtn.html('');
        }

    });
}

function makeCenter(boxId)
{
    var tag = '#' + boxId;

    $(tag).css("display","block");
    $(tag).css("position","absolute");
    $(tag).css("top", Math.max(0, (($(window).height() - $(tag).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    $(tag).css("left", Math.max(0, (($(window).width() - $(tag).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
}

function pop(boxId)
{
    var tag = '#' + boxId;
    $(tag).css("display","block");
}

function hide(boxId)
{
    var tag = '#' + boxId;
    $(tag).css("display","none");
}

$(function () {

    $(document).on('click', 'input[name="select_all"]', function(){

        if ($(this).prop("checked")) {
            $('input[name="select_one"]').each(function () {
                $(this).prop("checked", true);
            });
            $('input[name="select_all"]').each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $('input[name="select_one"]').each(function () {
                $(this).prop("checked", false);
            });

            $('input[name="select_all"]').each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $("div[url]").click(function () {
        window.location.href = $(this).attr('url');
    });

    $("li[url]").click(function () {
        window.location.href = $(this).attr('url');
    });

    $("input[url]").click(function () {
        window.location.href = $(this).attr('url');
    });


    $("img[text]").mouseenter(function () {
        $.layer({
            type: 4,
            shade:[0],
            closeBtn: false,
            tips : {
                msg: $(this).attr("text"),
                follow: $(this),
                guide: 0,
                isGuide: true,
                // style: ['background-color:#E227C7; color:#fff', '#E227C7']
                style: ['background-color:#efefef; color:#666;border:5px solid #999;text-align:left;padding:10px;margin-left:-15px;', '#efefef'],
                maxWidth:200
            }
        });
    });

    $("img[text]").mouseleave(function () {
        $('div[type="tips"]').remove();
    });

    var lettersCountText = $('textarea[length]');

    if(lettersCountText.length>0){
        $.each(lettersCountText,function(n,obj) {
            var allowed = $(obj).attr("length");
            var left = allowed - $(obj).val().length;
            $("#left_letters").text(left);

            var tag = '#left_letters_' + $(obj).attr("name");
            $(tag).text(left);
        });
    }

    $(document).on('keyup', 'textarea[length]', function(){
        var allowed = $(this).attr("length");

        if (allowed == null || allowed <= 0) {
            return;
        }

        var content = $(this).val(), valLength = content.length;
        var left = allowed - valLength;

        if(left < 0) {
            left = 0;
        }

        if (valLength > allowed) {
            $(this).empty().val(content.substr(0, allowed));
        }

        $("#left_letters").text(left);

        var tag = '#left_letters_' + $(this).attr("name");
        $(tag).text(left);

    });

    $(document).on('keyup', ':text[length]', function(){
        var allowed = $(this).attr("length");

        if (allowed == null || allowed <= 0) {
            return;
        }

        var content = $(this).attr("value"), valLength = content.length;

        if (valLength > allowed) {
            $(this).attr("value", content.substr(0, allowed));
        }
    });

    $(document).on('keyup', 'textarea[max_words]', function(){
        var allowed = $(this).attr("max_words");

        if (allowed == null || allowed <= 0) {
            return;
        }

        var content = $(this).val();
        content = trim_words(content, allowed);

        $(this).val(content);
    });

    $(document).on('keyup', ':text[max_words]', function(){
        var allowed = $(this).attr("max_words");

        if (allowed == null || allowed <= 0) {
            return;
        }

        var content = $(this).val();
        content = trim_words(content, allowed);

        $(this).val(content);

    });

    //keyword search输入框处理
    $('input[key_word_search]').each(function(){
        var left = $(this).position().left + 10;
        //增加一个遮挡层
        var html = "<span style='position:absolute;left:"+left+"px;color:#BCBCBC;cursor:text;'>By Keyword Search</span>";
        //$(this).parent().css({display:"block",position:"relative"});
        $(this).before(html);
        //点击遮挡层时隐藏遮挡层并且焦点到keyword输入框上
        $(this).prev("span").bind('click',function(){
            $(this).hide();
            $(this).siblings("input[key_word_search]").focus();
        });
        //默认keyword值处理
        if($(this).val() == ""){
            $(this).prev("span").show();
        }else{
            $(this).prev("span").hide();
        }
        //keyword输入框获取焦点时隐藏遮挡层
        $(this).focus(function(){
            if(""== $(this).val()){
                $(this).prev("span").hide();
            }

        })
        //keyword输入框释放焦点时根据输入框的值决定遮挡层的显示或者隐藏
        $(this).blur(function(){
            if($(this).val() == ""){
                $(this).prev("span").show();
            }else{
                $(this).prev("span").hide();
            }
        })
    });

});

function isEmail(email) {
    if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1) {
        return true;
    }
    return false;
}

function isAlpha(charValue) {
    return ((('a' <= charValue) && (charValue <= 'z')) || (('A' <= charValue) && (charValue <= 'Z')))
}

function isValidChar(charValue){
    return ((('a' <= charValue) && (charValue <= 'z')) ||
    (('A' <= charValue) && (charValue <= 'Z')) ||
    ('-' == charValue) || ('_' == charValue) || (' ' == charValue)
    )
}

function redirect(url){
    window.location.href = url;
}

function isContainUrl(str) {
    var isContainHttp = str.indexOf('http://');
    var isContainWww = str.indexOf('www.');
    var isContainHttps = str.indexOf('https://');
    if (isContainHttp >= 0 || isContainWww >= 0 || isContainHttps >= 0) {
        return true;
    } else {
        return false;
    }
}

function getEmailDomain(email) {
    var ind = email.indexOf("@");
    return email.substr(ind+1);
}

function isUrl(url) {
    var strRegex=/^(http(s)?:\/\/)?([-_a-zA-Z0-9])+(\.[-_a-zA-Z0-9]+)+$/;
    var re = new RegExp(strRegex);
    if (re.test(url)) {
        return (true);
    } else {
        return (false);
    }
}


function isTelNum(telephone)
{
    // 国家代码(2到3位)-区号(2到3位)-电话号码(7到8位)-分机号(3位)"
    var pattern =/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
    // var pattern =/(^[0-9]{3,4}\-[0-9]{7,8}$)|(^[0-9]{7,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/;

    if(telephone != "")
    {
        if(pattern.exec(telephone))
        {
            return true;
        }
    }

    return false;

}



