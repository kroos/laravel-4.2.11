//$(function(){
//
//    function triggerIndustry(obj){
//        if(typeof obj !== 'undefined') {
//            obj.chosen({search_contains : true, no_results_text:'No results match'});
//        }
//    }
//
//    function triggerIndustryRedirect(obj,url){
//        if(typeof obj !== 'undefined'){
//            obj.bind('change', function(){
//                var u = url+ obj.val();
//                location.href = u;
//            });
//        }
//    }
//
//    var ind=$('#industry'), m=$('#ind-module').val(), p =$('#ind-append').val(), u='';
//
//    switch (m){
//        case 'news':
//            //u = '0?industry=';
//            break;
//        case 'blog':
//            break;
//        case 'lead':
//            u='leads?i=';
//            break;
//        case 'home':
//            u='home?i=';
//            break;
//        case 'company':
//            u='company_suggest?l='+p+'&i=';
//            break;
//        case 'product':
//            u='product_suggest?l='+p+'&i=';
//            break;
//        case 'search-company':
//            break;
//        case 'search-product':
//            break;
//        case 'search-user':
//            break;
//        case 'group':
//            u = '/group/suggest?i=';
//            break;
//        default :
//            break;
//    }
//
//    triggerIndustry(ind);
//
//    if(m && typeof m != 'undefined'
//        && m != 'edit-company'
//        && m != 'blog'
//        && m != 'search-user'
//        && m != 'search-company'
//        && m != 'search-product'
//        && m != 'loop'
//    ){
//        triggerIndustryRedirect(ind, u)
//    }
//
//    triggerIndustry($('#pre_industry'));
//    triggerIndustry($('#sec_industry'));
//
//
//})

var setting = {

    data:{
        simpleData:{
            enable: true,
            idKey: "id",
            pIdKey: "pId"
        }
    },
    view:{
        showLine: false,
        showIcon: false
    },

    callback:{
        onClick : clickShowNode,
        onDblClick : dblClickHideNode
    }
};

var nodes = [
    {id:101, pId:0, name:"Agriculture & Forestry Sector"},
    {id:10111, pId:101, name:"Animal Production"},
    {id:1011111, pId:10111, name:"Aquaculture"},
    {id:10112, pId:101, name:"Commercial Fishing sector"},
    {id:10113, pId:101, name:"Crop Production"},
    {id:1011312, pId:10113, name:"Grain Farming"},
    {id:102, pId:0, name:"Arts, Entertainment & Recreation Sector"},
    {id:10211, pId:102, name:"Adult Entertainment"},
    {id:103, pId:0, name:"Beverage Manufacturing"},
    {id:104, pId:0, name:"Biotechnology Product Manufacturing"},
    {id:10411, pId:104, name:"Biopharmaceuticals & Biotherapeutics Manufacturing"},
    {id:10412, pId:104, name:"Biotechnology Research Equipment Manufacturing"},
];

function clickShowNode(event, treeId, treeNode){
    var treeObj = $.fn.zTree.getZTreeObj(treeId);
    treeObj.expandNode(treeNode, true, false, true);
}

function dblClickHideNode(event, treeId, treeNode) {
    var treeObj = $.fn.zTree.getZTreeObj(treeId);
    treeObj.expandNode(treeNode, false, false, true);
};

function showMenu() {
    var showObj = $("#show"), showObjOffset = $("#show").offset();
    $("#industryTree").css({left:showObjOffset.left + "px", top:showObjOffset.top + showObj.outerHeight() + "px"}).show();//.slideDown("fast");
    //$("body").bind("mousedown", onBodyDown);
}

function hideMenu() {
    //$("#industryTree")//.fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);
}

function onBodyDown(event) {
    if (!(event.target.id == "show" || event.target.id == "industryTree" || $(event.target).parents("#industryTree").length>0)) {
        hideMenu();
    }
}

function initialZtree() {
    $.fn.zTree.init($("#treeDemo"), setting, nodes);
}

function initialSearch(is){
    if(is){
        $('#treeDemo').hide();
        $('#treeSearch').show();
        $("#treeSearch").children().filter('li').remove();
    }
    else{
        $('#treeDemo').show();
        $('#treeSearch').hide();
    }
}

//搜索到的字符串加粗
$.fn.wrapInTag = function(opts) {

    var tag = opts.tag || 'strong'
        , words = opts.words || []
        , regex = RegExp(words.join('|'), 'gi') // case insensitive
        , replacement = '<'+ tag +'>$&</'+ tag +'>';

    return this.html(function() {
        return $(this).text().replace(regex, replacement);
    });
};


function autoMatch() {
    var val = $.trim($('#search').val());
    if (val.length > 1) {
        //InitialZtree();

        initialSearch(true);
        var zTree = $.fn.zTree.getZTreeObj("treeDemo"),nodeList = zTree.getNodesByParamFuzzy("name", val), f = [], s=[];
        _.each(nodeList, function(i){f.push(i.id)});
        _.each(nodes, function(i){
            if(_.contains(f, i.id)){
                $('<li>', {
                    'id' : 's_'+ i.id,
                    'class' : 'gg',
                    'value' : i.id,
                    'text' : i.name
                }).on('click', confirmSearchIndustry).appendTo('#treeSearch');
            }
        });

        //搜索到的字符串加粗
        $('li.gg').wrapInTag({
            tag: 'strong',
            words: [val]
        });

        //将找到的nodelist节点更新至Ztree内
        //$.fn.zTree.init($("#treeDemo"), setting, s);
        //zTree.expandAll(true);

        showMenu();
    } else {
        //隐藏树
        initialSearch(false);
        hideMenu();
        initialZtree();
    }
}

//从搜索出来的行业中确定行业
function confirmSearchIndustry(){
    var that = $(this);
    console.log(that.val());
    console.log(that.text());
    console.log(that.attr('id'));

    $('#search').val(that.text());
    hideMenu();
    $('#industryTree').hide();
    $('#show').val(that.text());
}

$(document).ready(function(){
    initialZtree();
    $('#show').bind('click', showMenu);
    $("#search").bind('input', autoMatch);
});

