var linedinUploadFileLayer;

$(document).on("click", ".linkedin_upload_file_node", function () {
    linedinUploadFileLayer = $.layer({
        type: 1,
        closeBtn: 0,
        title: false,
        fix: false,
        border: [0],
        area: ['400px', 'auto'],
        page: {dom: '#linkedin_upload_pop_div'},
        offset: ['' , '40%'],
        zIndex: 1000,
        success: function () {
        }
    });
});


$(document).on("click", "#linkedin_upload_pop_div_close", function () {
    layer.close(linedinUploadFileLayer);
});




