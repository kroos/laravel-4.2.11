function uploadPlugin(para) {
    for (var i = 0; i < para.files.length; i++) {
        uploadHanler(para.files[i]);

        /*初始化文件列表*/
        var uploadedFiles = para.files[i].init ? para.files[i].init : Array();
        var filesContainer = $("#" + para.files[i].fileNameList);
        var hiddenId = para.files[i].hiddenId;
        for (var j = 0; j < uploadedFiles.length; j++) {
            var file = uploadedFiles[j];
            var fileText =file.text;
            var fileVal = file.name;

            $(filesContainer).append('<li data-hiddenId="' + hiddenId + '"><div class="filetable" data-val="' + fileVal + '">' + fileText + '<span><a class="delete_file_btn" href="javascript:void(0);"><img border="0" align="absmiddle" src="/imgs/icon/icon_delete_9px.png"></a></span></div></li>');

            var urls = new Array();
            $(filesContainer)
                .find(".filetable")
                .each(function () {
                    urls.push($(this).attr("data-val"));
                });
            $("#" +hiddenId).val(urls.join());

        }
    }
}

$(document).on("click", "a.delete_file_btn", function () {

    var filesContainer = $(this).parent().parent().parent().parent();
    var liContainer = $(this).parent().parent().parent();
    var hiddenId = $(this).parent().parent().parent().attr('data-hiddenId');

    liContainer.remove();

    var urls = new Array();

    $(filesContainer)
        .find(".filetable")
        .each(function () {
            urls.push($(this).attr("data-val"));
        });

    $("#" + hiddenId).val(urls.join());
});

function uploadHanler(para) {


    $(document).on("change", "#" + para.uploadInputid, function (e) {

        e.stopImmediatePropagation();

        var that = this;
        var url;
        if (para.type == 'image') {
            url = '/upload/picture';
        } else if (para.type == 'file') {
            url = '/upload/file';
        } else {
            return false;
        }

        var isMulitiUpload = para.isMultiple ? para.isMultiple : false;


        if (para.displayFileName == null) {
            para.displayFileName = true;
        }

        var uploadInputBtn = $("#" + para.uploadInputid);
        var uploadInputParent = $(uploadInputBtn).parent();
        var parentClone = uploadInputParent.clone();

        my_layer = layer.load(0);
        $.ajaxFileUpload(
            {
                url: url,
                secureuri: false,
                fileElementId: para.uploadInputid,
                dataType: 'json',
                success: function (data, status) {
                    layer.close(my_layer);
                    if (typeof(data.error) != 'undefined' && data.error != true) {
                        if (para.previewId != null) {
                            (function (input, imgId) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        jQuery('#' + imgId).attr('src', e.target.result);
                                    };
                                    reader.readAsDataURL(input.files[0]);
                                }
                            })(that, para.previewId);
                        }


                        //处理是否多文件上传
                        var filesContainer = $("#" + para.fileNameList);
                        if (isMulitiUpload) {

                            var fileText = uploadInputBtn.val();
                            var fileVal = data.url;


                            $(filesContainer).append('<li data-hiddenId="' + para.hiddenId + '"><div class="filetable" data-val="' + fileVal + '">' + fileText + '<span><a class="delete_file_btn" href="javascript:void(0);"><img border="0" align="absmiddle" src="/imgs/icon/icon_delete_9px.png"></a></span></div></li>');

                            var urls = new Array();
                            $(filesContainer)
                                .find(".filetable")
                                .each(function () {
                                    urls.push($(this).attr("data-val"));
                                });
                            $("#" + para.hiddenId).val(urls.join());

                        } else {
                            var fileName = uploadInputBtn.val();
                            fileName = fileName.replace(/(c:\\)*fakepath\\/i, '');
                            $(filesContainer).html('<li  data-hiddenId="' + para.hiddenId + '"  ><div class="filetable" data-val="' + data.url + '">' + fileName + '<span><a class="delete_file_btn" href="javascript:void(0);"><img border="0" align="absmiddle" src="/imgs/icon/icon_delete_9px.png"></a></span></div></li>');
                            $("#" + para.hiddenId).val(data.url);
                        }

                        para.afterUpload(data);

                    } else {
                        tradove.msg(data.msg, 5, 7);
                        $("#" + para.uploadInputid).val('');
                    }

                    layer.close(my_layer);

                },
                error: function (data, status, e) {
                    layer.close(my_layer);
                    console.log(e);
                }
            }
        );


    });
}


