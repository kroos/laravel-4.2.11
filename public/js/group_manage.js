$(function(){

    var navi = $('.left_720_c_c_l_select'),
        unNavi = $('.left_720_c_c_l_not');
        naviSelect = navi.add(unNavi),
        naviVal = Number(navi.attr('val')),
        leftSection = $('.left_720_c_c_right'),
        loadParam = {
            'loadUrl' : '/group/mgr/dis',
            'loadNavi' : 1,
            'loadType' : 0,
            'isBlock' : 0
        };


    //the first time, load discussion data
    loadDisPostingCommentData(loadParam);

    var opLoading = function(select_obj){
        for(var i=0; i<select_obj.length; i++){
            select_obj[i].showLoading();
        }
    }

    var opHideLoading = function(select_obj){
        for(var i=0; i<select_obj.length; i++){
            select_obj[i].hideLoading();
        }
    }

    var changeTabStyle = function(that, another, cls1, cls2){
        that.removeClass(cls2).addClass(cls1);
        another.removeClass(cls1).addClass(cls2);
    }

    var commonLoad = function(params, loadSection){

        $.ajax({
            type: "GET",
            url: params.loadUrl,
            data: params,
            beforeSend : function(){
                loadSection.empty();
                loadSection.showLoading();
            },
            success: function(response){
                loadSection.hideLoading();
                loadSection.append(response);

            },
            error:function(){
                loadSection.hideLoading();
            }
        })
    }

    var commonHandle = function(handleObj, select_objs, callBackUrl, callBackLoadSection){
        $.ajax({
            type: "GET",
            url: handleObj.loadUrl,
            data: handleObj,
            dataType: 'json',
            beforeSend : function(){
                opLoading(select_objs);
            },
            success: function(response){
                opHideLoading(select_objs);
                if(response.result){
                    handleObj.loadUrl = callBackUrl;
                    loadMember(handleObj,  callBackLoadSection);
                }
            },
            error:function(){
                opHideLoading(select_objs);
            }
        });
    }

    var commonHandleAction = function(that, handleUrl){

        //var that = $(this),
        var action = that.attr('val'),
            curNaviVal = Number($(".left_720_c_c_l_select").attr('val')),
            curLoadType = Number($('.left_720_c_c_r_o_select').attr('val')),
            selectIds = getSelectedIds(),
            handleObj = {
                'loadUrl' : handleUrl,
                'loadNavi' : curNaviVal,
                'loadType' : curLoadType,
                'action' : action,
                'selectIds' : selectIds
            }

        return handleObj;
    }

    var commonTabLoad = function(that, loadUrl){

        var another = that.siblings('.left_720_c_c_r_o_select'),
            curNaviVal = Number($(".left_720_c_c_l_select").attr('val')),
            curLoadType = Number(that.attr('val'));

        changeTabStyle(that, another, 'left_720_c_c_r_o_select', 'left_720_c_c_r_o_not');

        return {
            'loadUrl' : loadUrl,
            'loadNavi' : curNaviVal,
            'loadType' : curLoadType
        };

    }

    var commonSelectedObjs = function(){

        select_objs = [];
        $("input[type='checkbox'][name='select_one']:checked").map(function () {
            var that = $(this);
            select_objs.push(that.parents('.left_720_c_c_r_list'));
        });
        return select_objs;
    }

    //when click navigation bar, load differnt data
    naviSelect.bind('click', function(e){

        e.stopImmediatePropagation();

        var selectPopWin = $('#tradove_select');
        if(selectPopWin.length != 0){
            selectPopWin.hide();
        }

        //change style
        var that = $(this),
            another = that.siblings(".left_720_c_c_l_select"),
            lastNaviVal = that.attr('val');
            curNavi = Number(that.attr('val')),
            curNaviText = that.text(),

            lastNavi = $('#last_navi');

        changeTabStyle(that, another, 'left_720_c_c_l_select', 'left_720_c_c_l_not');

        lastNavi.val(lastNaviVal);
        loadParam.loadNavi = curNavi;

        switch(curNavi){

            case 1 :
                loadParam.loadType = 0;
                loadParam.isBlock = 0;
                loadDisPostingCommentData(loadParam);
                break;

            case 2 :
                loadParam.loadType = 0;
                loadParam.isBlock = 0;
                loadDisPostingCommentData(loadParam);
                break;

            case 3 :
                loadJoin({
                    loadUrl : '/group/mgr/join' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val')),
                    loadType: 0
                }, leftSection);
                break;

            case 4 :
                loadInvite({
                    loadUrl : '/group/mgr/invite' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val')),
                    loadType: 0
                }, leftSection);
                break;

            case 5 :
                loadMember({
                    loadUrl : '/group/mgr/member' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val')),
                    loadType: 0
                }, leftSection);
                break;

            case 6 :
                loadAnnounce({
                    loadUrl : '/group/mgr/announce' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val'))
                }, leftSection);
                break;

            case 7 :
                loadProfile({
                    loadUrl : '/group/mgr/profile' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val'))
                }, leftSection);
                break;

            case 8 :
                loadOwnerChange({
                    loadUrl : '/group/mgr/changeOwner' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val'))
                }, leftSection);
                break;

            case 9 :
                loadGroupDelete({
                    loadUrl : '/group/mgr/deleteGroup' ,
                    loadNavi : Number($(".left_720_c_c_l_select").attr('val'))
                }, leftSection);
                break;

            default :

                console.log("default====>"+curNavi);

        }
    });

    function isEmail(email) {
        if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1){
            return true;
        }
        return false;
    }

    /**
     *  Discussion/Posting/Comment start
     *
     */
    function loadDisPostingCommentData(params){
        var loadUrl = params.loadUrl;
        /*loadNavi = params.loadNavi,
         loadType = params.loadType,
         isBlock = params.params;*/

        $.ajax({
            type: "GET",
            url: loadUrl,
            data: params,
            beforeSend : function(){
                leftSection.empty();
                leftSection.showLoading();
            },
            success: function(response){
                leftSection.hideLoading();
                leftSection.append(response);
            },
            error:function(){
                leftSection.hideLoading();
            }
        })
    }

    function handleDisPostingCommentData(handlePara, select_obj){
        $.ajax({
            type: "GET",
            url: '/group/mgr/handle',
            data: handlePara,
            dataType: 'json',
            beforeSend : function(){
                opLoading(select_obj);
            },
            success: function(response){
                //console.log(response);
                opHideLoading(select_obj);
                if(response.result){
                    if(handlePara.loadNavi == 1 || handlePara.loadNavi == 2){
                        handlePara.loadUrl = '/group/mgr/dis';
                    }
                    loadDisPostingCommentData(handlePara);
                }
            },
            error:function(){
                opHideLoading(select_obj);
            }
        });
    }

    //click Discussion/Posting/Comment
    $(document).on("click", "div[name='mgr_tab']", function(){
        //change tab style
        var that = $(this), another = that.siblings(".left_720_c_c_r_o_select");
        changeTabStyle(that, another, 'left_720_c_c_r_o_select', 'left_720_c_c_r_o_not');

        //load data
        loadParam.loadType = that.attr('val');
        loadDisPostingCommentData(loadParam);
    });

    //click blocked/unblocked
    $(document).on("click", "#blocked, #unblocked", function(){
        loadParam.isBlock = $(this).val();
        loadDisPostingCommentData(loadParam);
    });

    $(document).on('click', 'input[name="move_content"], input[name="block_content"], input[name="recover_content"]', function(){

        var selectIds = getSelectedIds(), len = selectIds.length;

        if(len == 0){
            tradove.msg('No item selected!', 3, 2);
            return false;
        }

        var $this = $(this),
            action = $this.attr('ac'),
            handlePara = {
                'loadNavi' : Number($('.left_720_c_c_l_select').attr('val')),
                'loadType' : Number($('.left_720_c_c_r_o_select').attr('val')),
                'isBlock' : $('input[name="isBlock"][checked="checked"]').val(),
                'action' : action,
                'queryIds': selectIds
            },
            att = [],
            select_obj = [];

        $("input[type='checkbox'][name='select_one']:checked").map(function () {

            var that = $(this), arr = [that.val(), that.attr('attitude'), that.attr('comment')]
            att.push(arr);

            select_obj.push(that.parents('.left_720_c_c_r_list'));
        });

        handlePara.attitude = att;

        handleDisPostingCommentData(handlePara, select_obj);
    });
    /**
     *  Discussion/Posting/Comment end
     */



    /**
     * group join start
     */
    function loadJoin(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', 'div[name="join_tab"]', function(){
        loadJoin(commonTabLoad($(this), '/group/mgr/join'), $('#tab_content'));
    })

    function handleJoin(handleObj, select_objs){
        commonHandle(handleObj, select_objs, '/group/mgr/join', $('#tab_content'));
    }

    $(document).on('click', 'input[name="join_action"]', function(){
        var handleObj = commonHandleAction($(this), '/group/mgr/handleJoin');

        if(handleObj.selectIds.length == 0){
            tradove.msg('No item selected!', 3, 2);
            return false;
        }

        handleJoin(handleObj, commonSelectedObjs());
    });
    /**
     * group join end
     */


    /**
     * group invite start
     */
    function loadInvite(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', 'div[name="invite_tab"]', function(){
        loadInvite(commonTabLoad($(this), '/group/mgr/invite'), $('#tab_content'));
    })

    $(document).on('click', '#invite_send', function(){

        var inviteName = $('#invite_name'), nameVal = inviteName.val();

        if(!nameVal){
            layer.tips('You must select at least one user.', inviteName, {
                style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                maxWidth:185,
                closeBtn:[0,true]
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: '/group/mgr/handleInvite',
            data: {'inviteIds' : $('#invite_id').val().split(',')},
            dataType : 'json',
            beforeSend : function(){
                leftSection.showLoading();
            },
            success: function(response){
                leftSection.hideLoading();
                if(response['code'] == 0){
                    layer.msg(response['message'], 3, 3);
                }else{
                    layer.msg(response['message'], 2, 10);
                }
            },
            error:function(){
                leftSection.hideLoading();
            }
        })

    });

    $(document).on('click', '#invite_cancel', function(){
        $('#invite_name').val("");
    });

    function handWithDraw(handleObj, select_objs){
        commonHandle(handleObj, select_objs, '/group/mgr/invite', $('#tab_content'));
    }

    $(document).on('click', 'input[name="invited_withdraw"]', function(){
        var handleObj = commonHandleAction($(this), '/group/mgr/handleWithDraw');

        if(handleObj.selectIds.length == 0){
            tradove.msg('No item selected!', 3, 2);
            return false;
        }

        handWithDraw(handleObj, commonSelectedObjs());
    });

    /**
     * group invite end
     */

    /**
     * group member start
     */
    function loadMember(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', 'div[name="member_tab"]', function(){
        loadMember(commonTabLoad($(this), '/group/mgr/member'), $('#tab_content'));
    })

    function handMember(handleObj, select_objs){
        commonHandle(handleObj, select_objs, '/group/mgr/member', $('#tab_content'));
    }

    $(document).on('click', 'input[name="member_action"]', function(){
        var handleObj = commonHandleAction($(this), '/group/mgr/handleMember');

        if(handleObj.selectIds.length == 0){
            tradove.msg('No item selected!', 3, 2);
            return false;
        }

        handMember(handleObj, commonSelectedObjs());
    })
    /**
     * group member end
     */

    /**
     * group announce start
     */
    function loadAnnounce(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', '#send_announce', function(){

        var subject = $("input[name='subject']").val(), content = $("textarea[name='content']").val();
        if(!subject && !content){
            layer.msg('Subject and content can not be empty', 2, 3);
            return false;
        }

        $.ajax({
            type: "post",
            url: '/group/mgr/handleAnnounce',
            data: $("#announce_form").serialize(),
            beforeSend : function(){
                leftSection.showLoading();
            },
            success: function(response){
                leftSection.hideLoading();
                if(response == 'true'){
                    layer.msg('Group announcement has been sent successfully.', 2, 10);
                }else{
                    layer.msg('Only one announcement per week may be allowed.', 2, 3);
                }
            },
            error:function(){
                leftSection.hideLoading();
            }
        })
    })


    $(document).on('click', '#cancel_announce', function(){
        $("input[name='subject']").val("");
        $("textarea[name='content']").val("");
    })
    /**
     * group announce end
     */

    /**
     * group delete start
     */
    function loadGroupDelete(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', '#delete_group', function(){
        var i = $.layer({
            shade : true,
            //title : 'delete group',
            title : '',
            area : ['auto','auto'],
            dialog : {
                msg: 'Are you sure you want to delete this group?',
                btns : 2,
                type : 0,
                btn : ['Yes','No'],
                yes : function(){
                    layer.close(i);
                    //delete group
                    $.ajax({
                        type: "get",
                        url: '/group/mgr/handleGroupDelete',
                        dataType : 'json',
                        beforeSend : function(){
                            leftSection.showLoading();
                        },
                        success: function(response){
                            leftSection.hideLoading();
                            var ret = response.code, gid = response.gid;
                            if(ret){

                                //delete group index
                                $.get('/index/group/delete/'+gid);

                                layer.msg('Group has been deleted successfully.', 1, 10);
                                setTimeout(function(){
                                    window.location.href ='/group/joined';
                                }, 500)
                            }
                        },
                        error:function(response){
                            leftSection.hideLoading();
                        }
                    })
                },
                no : function(){
                    layer.close(i);
                }
            }
        });
    })
    /**
     * group delete end
     */

    /**
     * group chang owner start
     */
    function loadOwnerChange(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', '#member_confirm', function(e){

        e.stopImmediatePropagation();

        var memberName = $('#member_name').val();
        if(memberName == null || memberName == ""){
            tradove.msg('Please choose a group member first!', 3, 2);
            return false;
        }

        function assignGroup(){
            $.ajax({
                type: "post",
                url: '/group/mgr/handleChangeOwner',
                data : {'memberId' : $('#member_id').val()},
                dataType : 'json',
                beforeSend : function(){
                    leftSection.showLoading();
                },
                success: function(response){
                    leftSection.hideLoading();
                    if(response['code'] == '1'){
                        layer.msg(response['message'], 1, 10);
                        setTimeout(function(){
                            window.location.href ='/group/joined';
                        }, 500)
                    } else if(response['code'] == '2') {
                        layer.msg(response['message'], 1, 10);
                    } else{
                        layer.msg(response['message'], 2, 3);
                    }
                },
                error:function(){
                    leftSection.hideLoading();
                }
            });
        }

        //using select plugin, no need anymore
        /*
        //1.check email format
        if(!isEmail(email)){
            layer.tips('Your email format is not correct.', $('#member_email'), {
                style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                maxWidth:185,
                closeBtn:[0,true]
            });
            return false;
        }
        */

        //2. confirm
        var i = $.layer({
            shade : true,
            title : 'Assign Group',
            area : ['auto','auto'],
            dialog : {
                msg: 'Are you sure you want to assign the ownership to '+memberName,
                btns : 2,
                type : 0,
                btn : ['Yes','No'],
                yes : function(){
                    layer.close(i);
                    assignGroup();
                },
                no : function(){
                    layer.close(i);
                }
            }
        });

    })
    /**
     * group chang owner end
     */

    /**
     * group profile start
     */
    function loadProfile(params, loadSection){
        commonLoad(params, loadSection);
    }

    $(document).on('click', '#cancel_profile', function(){
        loadProfile({
            loadUrl : '/group/mgr/profile' ,
            loadNavi : Number($(".left_720_c_c_l_select").attr('val'))
        }, leftSection);
    })

    $(document).on('click', '#update_profile', function(){

        var updateProfile = function(){

            $.ajax({
                type: "post",
                url: '/group/mgr/updateProfile',
                data: $("#update_profile_form").serialize(),
                dataType : 'json',
                beforeSend : function(){
                    leftSection.showLoading();
                },
                success: function(response){
                    leftSection.hideLoading();
                    if(response){
                        layer.msg('Profile has been updated successfully.', 2, 10);
                    }else{
                        //layer.msg('Can only allow one announcement a week.', 2, 3);
                    }
                },
                error:function(){
                    leftSection.hideLoading();
                }
            })
        }

        /**
         *
         * @param that
         * @param thatTip 提示文字
         * @param type 提示类型
         * @param checkType 检查类型
         * @returns {boolean} true|false
         */
        function updateCheck(that, thatTip, type, checkType){

            var isConfirm = (checkType == 1 ? that.prop('checked') : Number(that.val())), isPassed = false;

            if(!isConfirm){
                thatTip.css('color', 'red').css('font-size', '10px').show();
                if(type == 1){
                    $('html,body').animate({scrollTop:that.offset().top}, 500);
                }else{
                    thatTip.show();
                }
            }

            that.change(function() {
                if (that.is(':checked') ) {
                    thatTip.hide(); that.prop('checked', true); isPassed = true;
                } else {
                    that.prop('checked', false); thatTip.show();
                }
            });


            return isPassed;
        }

        var confirmUpload = $('#confirm_upload'),confirmTip = $('#photo_confirm'),
            profile_type = $('#grp_profile_type'),type_confirm = $('#type_confirm'),
            termConfirm = $('#term_check'), termTip = $('#term_confirm');

        updateCheck(confirmUpload, confirmTip, 1, 1);
        updateCheck(termConfirm, termTip, 2, 1);
        //updateCheck(profile_type, type_confirm, 2, 0);

        //if(!confirmUpload.is(':checked') || !termConfirm.is(':checked') || !Number(profile_type.val())){
        //    return false;
        //}
        if(!confirmUpload.is(':checked') || !termConfirm.is(':checked') ){
            return false;
        }


        if(!validateFields())
        {
            return false;
        }

        updateProfile();


    })

    /**
     * group profile end
     */
});