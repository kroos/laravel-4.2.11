function invite(subject,content,receiver_id,target_id,type){

    i = null;

    if(subject == null){
        layer.tips(json.subject_check, subject, 3, subject.width(), 0);
        return;
    }

    if(content == null){
        layer.tips(json.content_check, content, 3, content.width(), 0);
        return;
    }

    loop_type = $('input[name="loop_type_s"]:checked');
    if(loop_type){
        loop_type_val = loop_type.val();
    }else{
        loop_type_val = '';
    }

    $.ajax({
        url: "/connect_add",
        method:'POST',
        data:{
            receiver_id: receiver_id,
            target_id : target_id,
            type: type,
            subject: subject,
            content: content,
            loop_type : loop_type_val
        },

        beforeSend:function(){
            i = layer.load(0);
        },

        success:function(r){
            layer.close(i);
            layer.close(tradove.index);

            if(popWinInvite != null) {
                layer.close(popWinInvite);
            }

            if( r > 0 ){
                tradove.msg(json.sent + ' ' + json.success);
            }else{
                tradove.msg(json.sent + ' ' + json.fail);
            }

        }

    });

}

$(document).ready(function(){

    var fullName = $("#current_full_name").val();

    $("#s_loop_user").click(function(){

        $(this).winpop({
            setId : 's_receiver_id',
            setName : 's_loop_user',
            dataLoadUrl:'select/not_loops',
            winLinkTitle : 'Users',
            nameFormat: 'first',
            type : 'single',

            callBack:function() {
                if($("#s_receiver_id").val() != '') {
                    var loading;
                    $.ajax({
                        url : base_url+'loop/isColleague',
                        data : {
                            'uid' : $("#s_receiver_id").val()
                        },
                        beforeSend:function(){
                            loading = layer.load(0);
                        },
                        success:function(r){
                            layer.close(loading);

                            if(r & 1){
                                $("#loop_colleague_s").show();
                            }else{
                                $("#loop_colleague_s").hide();
                            }
                            if(r & 2){
                                $("#loop_buying_selling_s").show();
                            }else{
                                $("#loop_buying_selling_s").hide();
                            }
                            if(r & 4){
                                $("#loop_partner_s").show();
                            }else{
                                $("#loop_partner_s").hide();
                            }

                            $("#loop_type_d").show();

                            var loopUser = $("#s_loop_user").val();

                            subject = fullName + " would like to invite you to join his/her loop";
                            $("input[name=s_loop_subject]").val(subject);
                            content = "Hi, \r\r"
                                + "I would like to invite you to join my loop to maintain our business relationship. Thanks.\r\r" + fullName;
                            $("textarea[name=s_loop_content]").val(content);
                        }
                    });
                }
            }
        });
    });


    $("#s_company_name").click(function(){

        $(this).winpop({
            pluginType : 'company',
            setId : 's_target_id',
            setName : 's_company_name',
            ownerId : 's_receiver_id',
            ownerName : 's_receiver_name',
            dataLoadUrl: 'select/company/not_connect',
            winLinkTitle : 'Companies',

            callBack:function() {
                if($("#s_receiver_id").val() != '') {
                    var owner = $("#s_receiver_name").val();
                    $("#s_company_owner").text(owner);

                    var company = $("#s_company_name").val();

                    subject = fullName + " would like to connect to your company";
                    $("input[name=s_company_subject]").val(subject);
                    content = "Hi, \r\r"
                        + "I would like to connect with your company and learn more about it. Thanks.\r\r"  + fullName;
                    $("textarea[name=s_company_content]").val(content);

                }
            }
        });
    });


    $("#s_product_name").click(function(){

        $(this).winpop({
            pluginType : 'company',
            setId : 's_target_id',
            setName : 's_product_name',
            ownerId : 's_receiver_id',
            ownerName : 's_receiver_name',
            dataLoadUrl: 'select/product/not_connect',
            winLinkTitle : 'Products/Service',

            callBack:function() {
                if($("#s_receiver_id").val() != '') {
                    var owner = $("#s_receiver_name").val();
                    $("#s_product_owner").text(owner);

                    var product = $("#s_product_name").val();

                    subject = fullName + " would like to connect to your Products/Services";
                    $("input[name=s_product_subject]").val(subject);
                    content = "Hi, \r\r"
                        + "I would like to connect with your Products/Services and learn more about it. Thanks.\r\r"  + fullName;

                    $("textarea[name=s_product_content]").val(content);

                }
            }
        });
    });

    $("#s_btn_loop").click(function(){

        subject = $("input[name=s_loop_subject]").val();
        content = $("textarea[name=s_loop_content]").val();
        receiver_id = $("#s_receiver_id").val();
        target_id = receiver_id;
        type = 1;

        invite(subject,content,receiver_id,target_id,type);
    });

    $("#s_btn_product").click(function() {
        subject = $("input[name=s_product_subject]").val();
        content = $("textarea[name=s_product_content]").val();
        receiver_id = $("#s_receiver_id").val();
        target_id = $("#s_target_id").val();
        type = 3;

        invite(subject,content,receiver_id,target_id,type);
    });

    $("#s_btn_company").click(function(){

        subject = $("input[name=s_company_subject]").val();
        content = $("textarea[name=s_company_content]").val();
        receiver_id = $("#s_receiver_id").val();
        target_id = $("#s_target_id").val();
        type = 4;

        invite(subject,content,receiver_id,target_id,type);
    });

});