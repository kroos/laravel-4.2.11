$(function(){

    var source = $("input[type='hidden'][name='source']").val(),
        filterCate = $('li[name="filter_type"]'),
        filter_type = $("#filter_field").val(),
        filterCateLetter = $('span[name="filter_type"]');



    if(filter_type){
        $('li[name="filter_type"][class="left_650_c_c_l179_select"]').removeClass("left_650_c_c_l179_select").addClass("left_650_c_c_l179_noselect");
        $('li[name="filter_type"][value="'+filter_type+'"]').removeClass("left_650_c_c_l179_noselect").addClass("left_650_c_c_l179_select");
    }


    /**
     * generate urls for different request
     */
    var request_url = function(from, filter, industry){
        var loop_url = '';
        if(from == 1){
            loop_url = '/loop/add/'+filter+'/'+industry+'/0';
            console.log(loop_url);
        }else if(from == 0){
            loop_url = '/loop/list/'+filter+'/0';
        }else if(from == 2){
            loop_url = '/loop/browse/'+filter+'/0/0'
        }
        return loop_url;
    }

    filterCate.click(function(){
        var $this = $(this), current_filter_type = $this.val(), filterIndustry = $('#selectIndustry').val();
        $this.removeClass("left_650_c_c_l179_noselect").addClass("left_650_c_c_l179_select");
        //console.log(request_url(source,current_filter_type));
        location.href = request_url(source,current_filter_type, filterIndustry);
        //request_url(source,current_filter_type, filterIndustry);

    });

    filterCateLetter.click(function(){

        var $this = $(this), letter = $this.attr('val'), letter_filter_type = $this.attr("value"), filterIndustry = $('#industry').val();

        if(source == 1){
            location.href = '/loop/add/'+letter_filter_type+'/'+filterIndustry+'/'+letter;
        }

        if(source == 0){
            location.href = '/loop/list/0/'+letter;
        }

        if(source == 2){
            location.href = '/loop/browse/'+letter_filter_type+'/0/'+letter;
        }

    });

    /**
     * source 
     * 0 list
     * 1 add
     * 2 browse
     */
    //loop add
    if(source == 1){
        
        $(document).on('click', '#search_loop', function(){
        
            var search_name = $('#loop_user_name').val();

            /*if(!search_name){
                layer.alert('username cannot be empty', 8);
                return false;
            }*/

            if(!search_name){

                layer.tips('keyword cannot be empty', $("#loop_user_name"), {
                    style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                    maxWidth:185,
                    closeBtn:[0,true]
                });

                return false;
            }

            $("#from_source").val(4);

            $("#"+ShowMore.containerId).empty();
            $("#recommand_number").html("");
            ShowMore.initial({
                'url' : "/loop/add/getdata/0/"+$("#from_source").val()+"/"+$("#selectIndustry").val()+"/0",
                //'showMoreId' : 'show_more_data',
                params : {
                    'q' : search_name
                }
            });
            ShowMore.addModels();

        });
        
    }
    
    if(source == 0){

        var delLoop = function(loopids,loading){

            $.get('/loop/del',{
                'loopIds' : loopids
            },function(data){
                layer.msg('You have deleted the loop', 3, 10);
                if(filter_type === undefined){
                    filter_type = 0;
                }
                location.href = '/loop/list/'+filter_type+'/0';
            }).fail(function() {
                layer.msg('Delete loop operation failed', 3, 3);
            }).always(function(){
                loading.hideLoading();
            });
        }

        $(document).on('click', "a[name='del_loop']", function(){
            
            var $this = $(this),
                loading = $this.parents(".left_650_c_c_l470_l_unit"),
                item = $this.parents(".words").siblings(".select").children("input[name='select_one']");

                loading.showLoading();
                
                var myloop = $this.siblings("input[name='loop_id']").val();
                delLoop(myloop,loading);
        });
        
        $(document).on('click', "input[name='del_all_loop']", function(){
            
            var lids = new Array(), 
                myloopIds = $("input[name='loop_id']");

            myloopIds.each(function(i,o){
                var ck = $(o).parents(".words").siblings(".select").children("input[name='select_one']");
                if($(ck).prop("checked")){
                    lids.push($(o).val());
                }
            });

            if(lids.length > 0){
                var myLoopIdString = lids.toString(),loading = $(".left_650_c_c_l470_list");
                loading.showLoading();
                delLoop(myLoopIdString,loading);
            }else{
                tradove.msg('Please choose an object first.', 3, 3);
                return false;
            }
        });

        $(document).on('click', '#search_loop', function(){

            var search_name = $('#loop_user_name').val();
            if(!search_name){

                layer.tips('keyword cannot be empty', $("#loop_user_name"), {
                    style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                    maxWidth:185,
                    closeBtn:[0,true]
                });

                return false;
            }

            $("#datalist").empty();

            var index = null;
            $.ajax({
                url : '/loop/list/search',
                data : {'q' : search_name},
                beforeSend : function(){
                    $("#datalist").showLoading();
                },
                success: function(response){
                    //console.log(response);
                    $("#datalist").append(response);
                    $("li[name='filter_type'][value='0']>span").text($("#search_total_num").val());
                    $("li[name='filter_type'][value='1']>span").text($("#search_buyer_num").val());
                    $("li[name='filter_type'][value='2']>span").text($("#search_seller_num").val());
                    $("li[name='filter_type'][value='3']>span").text($("#search_other_num").val());
                },
                complete: function(){
                    $("#datalist").hideLoading();
                }
            })

        });

        $(document).on('click', 'input[name="send_all"]', function(){
            var loopids = getSelectedIds().join(),loopnames = getSelectedNames().join();

            if(!loopids){
                tradove.msg('Please choose an object first.', 3, 3);
                return false;
            }else{
                tradove.pop_message(loopids, loopnames);
            }

        });

    }

    if(source == 2){
        $(document).on('click', '#search_loop', function(){

            var search_name = $('#loop_user_name').val();

            if(!search_name){

                layer.tips('keyword cannot be empty', $("#loop_user_name"), {
                    style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                    maxWidth:185,
                    closeBtn:[0,true]
                });

                return false;
            }

            $("#from_source").val(5);

            location.href = '/loop/browse/search/0/'+$("#selectIndustry").val()+'/0?q='+search_name;

        });
    }

});

function showLoopSetting(){
    removeLoopSetting();
    $.ajax({
        url : '/loop_setting',
        beforeSend : function(){
            $("#show_loop_setting").showLoading();
        },
        success: function(response){
            $("body").append(response);
            makeCenter('pop_loop_setting');
            // $("#show_loop_setting").hide();
        },
        complete: function(){
            $("#show_loop_setting").hideLoading();
            // $("#show_loop_setting").hide();
        }
    })
}

function removeLoopSetting(){
    $("#pop_loop_setting").remove();
    $("#show_loop_setting").show();
}

function editLoopType(obj, loopId){
    removeLoopTypeEdit();
    $.ajax({
        type: "GET",
        url : '/loop_type_edit',
        data:{
            loopId: loopId
        },
        beforeSend : function(){
            $(obj).showLoading();
        },
        success: function(response){
            $("body").append(response);
            makeCenter('pop_loop_type_edit');
        },
        complete: function(){
            $(obj).hideLoading();
        }
    });
}

function removeLoopTypeEdit(){
    $('#pop_loop_type_edit').remove();
}

function changeLoopType(obj, loopId){
    var loopType = $("input[name='loopType[]']:checked").val();
    $.ajax({
        type: "POST",
        url : '/loop_type_edit',
        data:{
            loopType: loopType,
            loopId: loopId
        },
        beforeSend : function(){
            $(obj).parent().showLoading();
        },
        success: function(response){
            removeLoopTypeEdit();
            if(response != 'failed'){
                $("#loop_"+loopId+"_relationship").text(response);
            }else{
                tradove.msg("failed", 3);
            }
        },
        complete: function(){
            $(obj).parent().hideLoading();
        }
    });
}