$(document).ready(function(){
    $("#add_posting_form").submit(function(){
        var title = $('input[name="title"]');
        if( ! title.val() ){
            layer.tips(lang.posting.posting_add_title, title, 3, title.width(), 0);
            return false;
        }
        var content = $('textarea[name="content"]');
        if( ! content.val() ){
            layer.tips(lang.posting.posting_add_content, content, 3, content.width(), 0);
            return false;
        }
        var url = $('input[name="url"]');

        if( url.val().trim() != '' && (url.val().substring(0,7) != 'http://' && url.val().substring(0,8) != 'https://') ){
            layer.tips(lang.posting.posting_add_url, url, 3, url.width(), 0);
            return false;
        }
        return true;
    });



    $(document).on('click', 'a[name="edit-post"]', function(){
        location.href = '/group/2/edit/'+$(this).attr('did');
    });

    $(document).on('click', 'a[name="to_last_page"]', function(){
        location.href = '/group/'+$(this).attr('gid')+'/postings?filter=started';
    });

    $(document).on('click', '#cancel-post-btn', function(){
        location.href = '/group/'+$('#gid').val()+'/postings?filter=started';
    });

    $(document).on('click', '#update-post-btn', function(){

        var title = $('input[name="title"]');
        if( ! title.val() ){
            layer.tips(lang.discussion.discussion_add_title, title, 3, title.width(), 0);
            return false;
        }
        var content = $('textarea[name="content"]');
        if( ! content.val() ){
            layer.tips(lang.discussion.discussion_add_content, content, 3, content.width(), 0);
            return false;
        }
        var url = $('input[name="url"]');

        if( url.val().trim() != '' && (url.val().substring(0,7) != 'http://' && url.val().substring(0,8) != 'https://') ){
            layer.tips(lang.discussion.discussion_add_url, url, 3, url.width(), 0);
            return false;
        }


        var btn = $(this);
        $.ajax({
            type: "POST",
            url: "/group/edit_discussion",
            data : $("form[name='update-post-form']").serialize(),
            dataType : 'json',
            beforeSend : function(){
                btn.showLoading();
            },
            success: function(response){
                btn.hideLoading();
                if(response.result){
                    location.href = '/group/'+response.gid+'/postings?filter=started';
                }
            },
            error:function(data){
                btn.hideLoading();
                console.error(data);
            }
        });
    });

    $(document).on('click', 'a[name="del-post"]', function(){
        var $this = $(this), loadSection = $this.closest("div.left_720_c_c_detail_list"), did = $this.attr('did'), gid = $this.attr('gid');
        var i = $.layer({
            shade : true,
            title : 'Delete Posting',
            area : ['auto','auto'],
            dialog : {
                msg: 'Are you sure you want to delete this posting?',
                btns : 2,
                type : 0,
                btn : ['Yes','No'],
                yes : function(){
                    layer.close(i);
                    //delete discussion
                    $.ajax({
                        type: "post",
                        url: '/group/delete_discussion/'+did,
                        dataType : 'json',
                        beforeSend : function(){
                            loadSection.showLoading();
                        },
                        success: function(response){
                            loadSection.hideLoading();
                            if(response){
                                layer.msg('Group has been deleted successfully.', 1, 10);
                                setTimeout(function(){
                                    location.href = '/group/'+gid+'/postings?filter=started';
                                }, 500)
                            }
                        },
                        error:function(){
                            loadSection.hideLoading();
                        }
                    })
                },
                no : function(){
                    layer.close(i);
                }
            }
        });
    });


});

function showPostingForm(){
    if($(".left_720_c_c_post").css("display")=='none'){
        $(".left_720_c_c_post").show();
    }else{
        $(".left_720_c_c_post").hide();
    }
}

function clearForm(){
    $('input[name="title"]').val('');
    $('textarea[name="content"]').val('');
    $('input[name="url"]').val('');
}

function searchPosting(gid,filter){
    var keywords = $('input[name="keywords"]').val();
    if(!keywords.trim()){
        return;
    }
    var url="/group/"+gid+"/postings?keywords="+keywords.trim();
    if(filter != ''){
        url+="&filter="+filter;
    }
    location.href = url;
}

function follow(element, postingId){
    var cc = $(element);
    $.ajax({
        type: "POST",
        url: "/follow_discussion/"+postingId,
        beforeSend : function(){
            cc.showLoading();
        },
        success: function(response){
            cc.hideLoading();
            if(response == 'success'){
                cc.replaceWith('<a href="javascript:void(0);" onclick="unFollow(this,'+postingId+');">'+lang.discussion.discussion_un_follow+'</a>');
            }else{
                tradove.msg(lang.discussion.discussion_follow_failed,10);
            }
        },
        error:function(data){
            cc.hideLoading();
            console.error(data);
        }
    });
}

function unFollow(element, postingId){
    var cc = $(element);
    $.ajax({
        type: "POST",
        url: "/un_follow_discussion/"+postingId,
        beforeSend : function(){
            cc.showLoading();
        },
        success: function(response){
            cc.hideLoading();
            if(response == 'success'){
                cc.replaceWith('<a href="javascript:void(0);" onclick="follow(this,'+postingId+');">'+lang.discussion.discussion_follow+'</a>');
            }else{
                tradove.msg(lang.discussion.discussion_un_follow_failed,10);
            }
        },
        error:function(data){
            cc.hideLoading();
            console.error(data);
        }
    });
}

function inappropriate(element,type,id){
    var cc = $(element);
    $.ajax({
        type: "POST",
        url: "/group/inappropriate",
        data: {
            "type": type,
            "id": id
        },
        beforeSend : function(){
            cc.showLoading();
        },
        success: function(response){
            cc.hideLoading();
            if(response == 'success'){
                cc.parent().empty();
            }else{
                tradove.msg(lang.discussion.discussion_inappropriate_failed,10);
            }
        },
        error:function(data){
            cc.hideLoading();
            console.error(data);
        }
    });
}

function delComment(element, id){
    var cc = $(element);
    $.ajax({
        type: "POST",
        url: "/group/del_comment",
        data: {
            "commentId": id
        },
        beforeSend : function(){
            cc.showLoading();
        },
        success: function(response){
            cc.hideLoading();
            if(response == 'success'){
                //btn
                var btnElement = $("#posting_comment_btn_"+id).remove();
                //content
                var contentElement = $("#posting_comment_content_"+id).replaceWith(lang.posting.posting_comment_del_msg);
            }else{
                tradove.msg(lang.discussion.discussion_comment_del_failed,10);
            }
        },
        error:function(data){
            cc.hideLoading();
            console.error(data);
        }
    });
}

function validComment(){
    var content = $('textarea[name="content"]');
    if( ! content.val() ){
        layer.tips(lang.posting.posting_add_comment, content, 3, content.width(), 0);
        return false;
    }
    return true;
}

function clearComment(){
    $("#comment_content").val('');
}
