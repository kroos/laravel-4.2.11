function show_more_user(start){
    $.ajax({
        url:"search_get_user",
        data:{
            start:start
        },
        beforeSend:function(){
            $(".left_top_readmore").unbind('click');
            $(".left_top_readmore").html(json.loading);
        },
        error:function(){
            $(".left_top_readmore").html(json.timeout);
            $(".left_top_readmore").click(function(){
                show_more_user(start);
            });
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}

function show_more_data(search, type, start, industry, country, diversity){
    if(type == 'product')
        url = 'search_get_product';
    if(type == 'company')
        url = 'search_get_company';
    if(type == 'group')
        url = 'search_get_group';
    if(type == 'web')
        url = 'search_get_web';

    $.ajax({
        url: url,
        data:{
            search:search,
            start:start,
            i:industry,
            c:country,
            d:diversity
        },
        beforeSend:function(){
            $(".left_top_readmore").unbind('click');
            $(".left_top_readmore").html(json.loading);
        },
        error:function(){
            $(".left_top_readmore").html(json.timeout);
            $(".left_top_readmore").click(function(){
                show_more_data(search, type);
            });
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}