(function (u) {
    var d = document, s = 'script', a = d.createElement(s), m = d.getElementsByTagName(s)[0];
    a.async = 1;
    a.src = u;
    m.parentNode.insertBefore(a, m);
})('//api.cloudsponge.com/widget/0c18da2738999563f300e6523afbdcea1c3e633b.js');

cloudspangeInvitePop = null;

$(document).on("click", "#cloudspange_invite_confirm_btn", function () {
    layer.close(cloudspangeInvitePop);
});

window.csPageOptions = {
    sources: ["gmail", "yahoo", "windowslive", "csv", "linkedin", "aol", "icloud", "outlook", "addressbook",
        "plaxo", "mail_ru", "uol", "bol", "terra", "rediff", "mail126", "mail163", "mail_yeah_net",
        "gmx", "qip_ru", "sapo", "mail", "yandex_ru"],
    beforeDisplayContacts: function (contacts, source, owner) {

        var contact, email;
        var emails = [];

        for (var i = 0; i < contacts.length; i++) {
            contact = contacts[i];
            email = contact.selectedEmail();
            if (emails.indexOf(email) < 0) {
                var name = '';
                if (contact.first_name != null && contact.first_name != '') {
                    name = contact.first_name;
                }
                emails.push(name + ':' + email);
            }
        }

        var emailStr = emails.join(',');

        if (emails.length > 0) {
            var my_layer = layer.load(0);
            $.ajax({
                url: '/home/login_invite_partner',
                type: 'POST',
                async: false,
                data: {
                    "emails": emailStr
                },
                beforeSend: function () {
                },
                success: function (data) {
                    layer.close(my_layer);

                    cloudspangeInvitePop = $.layer({
                        type: 1,
                        closeBtn: 0,
                        title: false,
                        fix: true,
                        area: ['300px', 'auto'],
                        border: [0],
                        page: {dom: '#cloudspange_invite_success_container'},
                        offset: ['', '40%'],
                        zIndex: 1000,
                        success: function () {
                        }
                    });

                },
                error: function (data) {
                    return false;
                    layer.close(my_layer);
                }
            });
        }

        return false;
    }
};


