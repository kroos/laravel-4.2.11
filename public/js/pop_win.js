(function ($) {

	var defaults = {
       pluginType : 'user',//user/company/product
       ownerId : '', // for company and product
       ownerName : '',// for company and product
       name : '',
       subject : '',
       content : '',
       loadingEl : '.pop_width500_content',//for show loading icon
       setName : '',
       nameFormat : 'all', //all/first/last
       setFormatName : '',
       setId : 'tradove_uids',
       type : 'mutil',//single/mutil
       module:'loop',//loop/group
       dataLoadUrl: 'select/loops',
       memberType: 'all',//buyer/seller/other  default all, also can specified more than one type, for example 'buyer,seller'
       winTitle : 'Address Book',
       winChoiceTitle : 'Users Selected',
       winLinkTitle : 'All Loops',
       errorMsg : 'You can choose only one person.',
       callBack:function(){}
    };
    

    //make div on the center
    jQuery.fn.center = function () {

	    this.css("position","absolute");
        this.css("z-index", "9900");
	    this.css("top", Math.max(0, (($(window).height() - this.outerHeight()) / 2) + 
	                                                $(window).scrollTop()) + "px");
	    this.css("left", Math.max(0, (($(window).width() - this.outerWidth()) / 2) + 
	                                                $(window).scrollLeft()) + "px");
	    return this;
	}

	$.fn.winpop = function(options){

		var config = $.extend({}, defaults, options), layerIndex = 0, tradove_selected_ids = [],tradove_selected_names = [], tradove_selected_names_single = [], tradove_selected_ownerId = [], tradove_selected_ownerName =[];

		var tradove_pop = function(obj){

			var popdiv = $("#tradove_select");

			if(popdiv.length == 0){
				
				var container = $("<div>",{
					"id" : "tradove_select",
					"class" : "pop_width500"
				});

				var title  = '<div class="pop_width500_title">';
					title += '  '+config.winTitle+'<span><a href="javascript:void(0)"><img id="tradove_pop_hide" src="/imgs/icon/icon_delete_12px.png" border="0"></a></span>';
				    title += '</div>';

				$(title).appendTo(container);

				var search =  '<div class="pop_500_c_search">';
					search += '  <div class="div130">Keyword Search:</div> ';
					search += '  <div class="div305"><input id="tradove_search_name" class="input300" name="" type="text"></div> ';
					search += '  <div class="div40"><a><img id="tradove_search_img" src="/imgs/icon/icon_search_big.png" border="0" align="absmiddle"></a></div> ';
					search += '</div>';
				
				var navi  = '<div class="pop_500_c_index"><a id="tradove_search_reload_all" href="javascript:void(0)">'+config.winLinkTitle+'</a></div>';

				var initlist  = '<div id="tradove_data_list" class="pop_500_c_list">';
					initlist += '</div>';

				var choice  = '<div id="tradove_choice" class="pop_500_c_list_choice">';
				    choice += '  <div class="title">'+config.winChoiceTitle+':</div>';
				    choice += '</div>';
                    choice += '<div id="tradove_choice_tip" style="display: none"></div> '


				var btn = '<div class="pop_500_c_list_btn"><input id="tradove_pop_btn" class="btn_gray_22" type="button" value="Confirm"></div>';
			
				var contents = $("<div>",{
					"class" : "pop_width500_content"
				});

				$(search).appendTo(contents);
				$(navi).appendTo(contents);
				$(initlist).appendTo(contents);
				$(choice).appendTo(contents);
				$(btn).appendTo(contents);

				contents.appendTo(container);
				container.appendTo("body");

				container.center();
				return container;
			}else{
				popdiv.center();
				popdiv.show();
				return popdiv;
			}
		};

		var clear = function(clear_type){

            //config.callBack();

			$(".pop_500_c_list_section").remove();
			
			if(clear_type == 1){
				$(".diva").remove();
				$("#tradove_search_name").val("");
				tradove_selected_ids = [];
			}

			if(layerIndex){
				layer.close(layerIndex);
			}

			$(config.loadingEl).hideLoading();
		}

		var close_win = function(obj){
			clear(1);
			//obj.hide();
            obj.remove();
		}

        /**
         * 设置返回的结果
         * @param returnIds 如果是人／产品／公司的ID
         * @param returnNames 返回的人／产品／公司的名称
         * @param returnNameSingle 如果是查询的人，根据需要的名称格式，可能返回first_name/last_name；公司和产品忽略该字段；
         * @param returnOwnerId 如果是查询公司或产品，返回该公司或产品的owner；人忽略该字段；
         * @param returnOwnerName 如果是查询公司或产品，返回该公司或产品owner的名字全称；人忽略该字段；
         */
		var set_return_value = function(returnIds, returnNames, returnNameSingle, returnOwnerId, returnOwnerName){
			var configId = config.setId;
			if(configId.length == 0 ){
				configId = 'tradove_set_ids';
			}

			var hiddenIds  = $("#"+configId), hiddenNames = $("#"+config.setName),
                hiddenNameSingle = $("#"+config.setFormatName),
                hiddenOwnerId = $("#"+config.ownerId),
                hiddenOwnerName = $("#"+config.ownerName);

			if(hiddenIds && hiddenIds.length > 0){
				hiddenIds.val("");
				hiddenIds.val(returnIds);
			}else{
				$("<input>",{
					type : 'hidden',
					id : configId,
					name : configId,
					value : returnIds
				}).appendTo(".pop_width500_content");
			}

			hiddenNames.val("");
			
			var tradove_return_names = [], tradove_return_names_single = [], tradvoe_return_owner_id =[], tradove_return_owner_name =[];
			for(var i=0; i<returnNames.length; i++){

				if(returnNames[i]){
					tradove_return_names.push(returnNames[i]);
				}
			}

            for(var i=0; i<returnNameSingle.length; i++){

                if(returnNameSingle[i]){
                    tradove_return_names_single.push(returnNameSingle[i]);
                }
            }

            for(var i=0; i<returnOwnerId.length; i++){

                if(returnOwnerId[i]){
                    tradvoe_return_owner_id.push(returnOwnerId[i]);
                }
            }

            for(var i=0; i<returnOwnerName.length; i++){

                if(returnOwnerName[i]){
                    tradove_return_owner_name.push(returnOwnerName[i]);
                }
            }


			// hiddenNames.val(tradove_return_names.toString());
			hiddenNames.val(tradove_return_names.join(", "));
            hiddenNameSingle.val(tradove_return_names_single.toString());
            hiddenOwnerId.val(tradvoe_return_owner_id.toString());
            hiddenOwnerName.val(tradove_return_owner_name.toString());

			tradove_selected_names.length = 0;
            tradove_selected_names_single.length = 0;
            tradove_selected_ownerId.length = 0;
            tradove_selected_ownerName.length = 0;
		}

		var load_data = function(params){

            //console.log(params.p);

			//$(".pop_500_c_list_section").remove();

			$.ajax({
				url : base_url + config.dataLoadUrl,
				dataType: 'json',
				data : params,
				beforeSend : function(){
					$(config.loadingEl).showLoading();
				},
				success:function(data){
                    //$(".pop_500_c_list_section").remove();
					//console.log(data);
					$(config.loadingEl).hideLoading();

                    var pType = config.pluginType;
                    console.log("pType: " + pType);
                    console.log("pop data: " + JSON.stringify(data));
                    if( pType == 'user' ){

                        for(var i=0; i<data.length; i++){

                            var	list  = '<div class="pop_500_c_list_section">';
                            list += '  <div class="pop_500_c_list_section_left"><img src="'+data[i].logo+'" border="0"></div>';
                            list += '    <div class="pop_500_c_list_section_right">';
                            list += '      <ul>';
                            list += '        <input type="hidden" name="tradove_object_id" value="'+data[i].id+'"/>';
                            list += '        <input type="hidden" name="tradove_name_first" value="'+data[i].firstName+'"/>';
                            list += '        <input type="hidden" name="tradove_name_last" value="'+data[i].lastName+'"/>';
                            list += '        <li name="tradove_name" class=""><span>'+data[i].name+'</span></li>';
                            list += '        <li name="tradove_info" class="">'+data[i].info+'</li>';
                            list += '      </ul>';
                            list += '  </div>';
                            list += '</div>';

                            $(list).appendTo("#tradove_data_list");

                        }
                    }else{

                        for(var i=0; i<data.length; i++){

                            var	list  = '<div class="pop_500_c_list_section">';
                            list += '  <div class="pop_500_c_list_section_left"><img src="'+data[i].logo+'" border="0"></div>';
                            list += '    <div class="pop_500_c_list_section_right">';
                            list += '      <ul>';
                            list += '        <input type="hidden" name="tradove_object_id" value="'+data[i].id+'"/>';
                            list += '        <input type="hidden" name="owner_id" value="'+data[i].oid+'"/>';
                            list += '        <input type="hidden" name="owner_name" value="'+data[i].oname+'"/>';
                            list += '        <li name="tradove_name" class=""><span>'+data[i].name+'</span></li>';
                            list += '        <li name="tradove_info" class="">'+data[i].info+'</li>';
                            list += '      </ul>';
                            list += '  </div>';
                            list += '</div>';

                            $(list).appendTo("#tradove_data_list");

                        }
                    }

                },
                error : function(xhr, status, error){
                	$(config.loadingEl).hideLoading();
                },
                timeout: 35000
			});

		}

		//search data
		$(document).off('click', '#tradove_search_img');
		$(document).on('click', '#tradove_search_img', function(e){
			e.stopImmediatePropagation();
			clear(0);
			var queryName = $("#tradove_search_name").val();
			if(config.pluginType=='user'){
                load_data({'q':queryName, 'memberType':config.memberType, 'p':1});
			}else{
                load_data({'q':queryName, 'p':1});
			}

		});

		$(document).on('keypress', '#tradove_search_name', function(e){
            if(e.keyCode == '13'){
                e.stopImmediatePropagation();
                clear(0);
                //load_data({'q':$("#tradove_search_name").val(), 'memberType':config.memberType, 'p':1});
                if(config.pluginType=='user'){
                    load_data({'q':$("#tradove_search_name").val(), 'memberType':config.memberType, 'p':1});
                }else{
                    load_data({'q':$("#tradove_search_name").val(), 'p':1});
                }
            }
		});
		
		$(document).off('click', '#tradove_search_reload_all');
		$(document).on('click', '#tradove_search_reload_all', function(e){
			
			e.stopImmediatePropagation();

			clear(0);

			//load_data({'memberType':config.memberType, 'p':1});
            if(config.pluginType=='user'){
                load_data({'memberType':config.memberType, 'p':1});
            }else{
                load_data({'p':1});
            }

		});
		
		$(document).off('click', '.pop_500_c_list_section');
		//add select data at the search list
		$(document).on('click', '.pop_500_c_list_section', function(e){
			
			e.stopImmediatePropagation();

			var $this = $(this),
			    tradove_name = $this.find("li[name='tradove_name']>span").text(), //name
                tradove_name_single = "",
			    tradove_select_object = $this.find("ul>input[name='tradove_object_id']"),
			    tradove_select_object_id = tradove_select_object.val(),//id
			    tradove_select_section_id = $(".diva").children("input[name='tradove_each_selected_id']"),
			    //tradove_select_section_name = $(".diva").children("label[name='tradove_each_selected_name']"),
			    tradove_id_search_index = $.inArray(tradove_select_object_id, tradove_selected_ids);

            var fFormat = config.nameFormat, fPluginType = config.pluginType;

            if(fPluginType == 'user'){

                switch (fFormat){
                    case 'all':
                        //tradove_name_single = $this.find("li[name='tradove_name']>span").text(); //fullName
                        break;
                    case 'first':
                        tradove_name_single = $this.find("input[name='tradove_name_first']").val(); //firstName
                        break;
                    case 'last':
                        tradove_name_single = $this.find("input[name='tradove_name_last']").val(); //lastName
                        break;
                    default :
                        tradove_name_single = tradove_name_single;
                        break;
                }

            }

            //如果选择的用户／产品已经选择过，将其从已选择的列表上移除
			if(tradove_select_object_id && tradove_id_search_index != -1){
				
				tradove_select_section_id.each(function(i,o){

					if($(o).val() == tradove_select_object_id){
					
						$(o).parent(".diva").remove();
						
						tradove_selected_ids.splice(tradove_id_search_index, 1);
						delete(tradove_selected_names[tradove_select_object_id]);
						delete(tradove_selected_names_single[tradove_select_object_id]);
						delete(tradove_selected_ownerId[tradove_select_object_id]);
						delete(tradove_selected_ownerName[tradove_select_object_id]);

						return false;

					}
				});
				
			}else{

                if(fPluginType == 'user'){
                    var choice =  '<div class="diva">';
                    choice += '  <input name="tradove_each_selected_id" type="hidden" value="'+tradove_select_object_id+'"/>';
                    choice += '  <span ><label name="tradove_each_selected_name">'+tradove_name+'</label>&nbsp;&nbsp;<img name="tradove_select_remove_single" src="/imgs/icon/icon_delete_9px.png" border="0" align="absmiddle"></span>';
                    choice += '</div>';

                    tradove_selected_names_single[tradove_select_object_id] = tradove_name_single;

                }else{
                    var selectOwnerId  = $this.find("input[name='owner_id']").val(), selectOwnerName = $this.find("input[name='owner_name']").val();

                    var choice =  '<div class="diva">';
                    choice += '  <input name="tradove_each_selected_id" type="hidden" value="'+tradove_select_object_id+'"/>';
                    choice += '  <input name="tradove_each_selected_ownerId" type="hidden" value="'+selectOwnerId+'"/>';
                    choice += '  <input name="tradove_each_selected_ownerName" type="hidden" value="'+selectOwnerName+'"/>';
                    choice += '  <span ><label name="tradove_each_selected_name">'+tradove_name+'</label>&nbsp;&nbsp;<img name="tradove_select_remove_single" src="/imgs/icon/icon_delete_9px.png" border="0" align="absmiddle"></span>';
                    choice += '</div>';

                    tradove_selected_ownerId[tradove_select_object_id] = selectOwnerId;
                    tradove_selected_ownerName[tradove_select_object_id] = selectOwnerName;
                }

				$(choice).appendTo("#tradove_choice");

				tradove_selected_ids.push(tradove_select_object_id);
                tradove_selected_names[tradove_select_object_id] = tradove_name;

			}

		});
		
		//remove selected data
		$(document).off('click', 'img[name="tradove_select_remove_single"]');
		$(document).on('click', 'img[name="tradove_select_remove_single"]', function(e){
			
			e.stopImmediatePropagation();

			var $this = $(this),
			    each_selected_id = $this.parent().siblings("input").val(),
			    position = tradove_selected_ids.indexOf(each_selected_id);
			
			if(position != -1){
				tradove_selected_ids.splice(position, 1);
				delete(tradove_selected_names[each_selected_id]);
				delete(tradove_selected_names_single[each_selected_id]);
				delete(tradove_selected_ownerId[each_selected_id]);
				delete(tradove_selected_ownerName[each_selected_id]);
			}

			$this.parent().parent(".diva").remove();

		});
		
		$(document).off('click', '#tradove_pop_btn');
		$(document).on('click', '#tradove_pop_btn', function(e){

			e.stopImmediatePropagation();

			var select_len = tradove_selected_ids.length;
			if(config.type == 'single' && select_len > 1){
				//layerIndex = layer.tips(config.errorMsg , this , 0, 200, 200, ['background-color:#E227C7; color:#fff', '#E227C7']);

                $('#tradove_choice_tip').show().html(config.errorMsg).css('color', 'red');

				return false;
			}

			set_return_value(tradove_selected_ids.toString(), tradove_selected_names, tradove_selected_names_single, tradove_selected_ownerId, tradove_selected_ownerName);
			close_win($("#tradove_select"));
            config.callBack();
            console.log(config);
		});

		return this.each(function(){
			var container = tradove_pop(this);
			$('#tradove_search_name').focus();
            if(config.pluginType=='user'){
                load_data({'memberType':config.memberType, 'p':1});
            }else{
                load_data({'p':1});
            }
			//load_data({'memberType':config.memberType, 'p':1});
			
			$(document).off('click', '#tradove_pop_hide');
			$(document).on('click', '#tradove_pop_hide', function(e){
				
				e.stopImmediatePropagation();
				$('#'+config.setId).val("");
				$('#'+config.setName).val("");
				$('#'+config.ownerId).val("");
				$('#'+config.ownerName).val("");

				tradove_selected_names.length = 0 ;
                tradove_selected_names_single.length = 0 ;
                tradove_selected_ownerId.length = 0;
                tradove_selected_ownerName.length = 0;

				close_win(container);

                config.callBack();
			});

            var nScrollHight = 0; //滚动距离总长(注意不是滚动条的长度)
            var nScrollTop = 0;   //滚动到的当前位置
            var nDivHight = $("#tradove_data_list").height();
            var pageCounter = 2;
            //定义滚动条位置改变时触发的事件
            $("#tradove_data_list").scroll(function(){
                nScrollHight = $(this)[0].scrollHeight;
                nScrollTop = $(this)[0].scrollTop;
                if(nScrollTop + nDivHight >= nScrollHight){
                    //alert("滚动条到底部了");
                    //1.ajax加载数据

                    //load_data({'q':$("#tradove_search_name").val(), 'memberType':config.memberType, 'p':pageCounter});
                    if(config.pluginType=='user'){
                        load_data({'q':$("#tradove_search_name").val(), 'memberType':config.memberType, 'p':pageCounter});
                    }else{
                        load_data({'q':$("#tradove_search_name").val(), 'p':pageCounter});
                    }
                    pageCounter ++;
                }
            });
		});
	}


	
})(jQuery);
