function logout(){
    location.href = base_url + 'logout';
}

function operation(o, id){
    if(typeof id == 'undefined'){
        id = getSelectedIds().join(',');
        if(!id){
            return;
        }
    }
    if(o == 'resend')
        msg = 'Are you sure you want to resend confirmation to this user?';
    if(o == 'delete')
        msg = 'Are you sure you want to delete this user?';
    if(o == 'active')
        msg = 'Are you sure you want to active this user?';
    if(o == 'approval')
        msg = 'Are you sure you want to approve this user?';
    if(o == 'reject')
        msg = 'Are you sure you want to reject this user?';
    if(o == 'public')
        msg = 'Are you sure you want to judge this user as public?';

    if(o == 'network_approval')
        msg = 'Are you sure you want to approve this user?';
    if(o == 'network_reject')
        msg = 'Are you sure you want to approve this user?';
    if(o == 'pw_delete')
        msg = 'Are you sure you want to delete this record?';
    if(o == 'pw_restore')
        msg = 'Are you sure you want to restore password?';

    tradove.confirm(msg, function(){
        from = window.location.href;
        //alert("ids: " + id);
        location.href = base_url + 'admin/operation?operation=' + o + '&ids=' + id + '&from=' + from;
    });
}

function send_test_message(){
    subject = $('input[name="subject"]');
    content = $('textarea[name="content"]');
    test_email = $('input[name="test_email"]').val();
    if(subject.val() == ''){
        layer.tips( json.subject_check, title, 3, title.offsetWidth, 1);
        subject.focus();
        return;
    }
    if(content.val() == ''){
        layer.tips( json.content_check, content, 3, content.offsetWidth, 0);
        content.focus();
        return;
    }
    if(test_email == ''){
        return;
    }

    $.ajax({
        url:base_url + 'admin/post_test_message',
        data:{
            'subject' : subject.val(),
            'content' : content.val(),
            'test_email' : test_email
        },
        method:'POST',
        success:function(r){
            if(r > 0){
                tradove.msg('Sent Successfully');
            }else{
                tradove.msg('Sent Failed');
            }
        }
    });
}

function send_system_message(){
    subject = $('input[name="subject"]');
    content = $('textarea[name="content"]');
    if(subject.val() == ''){
        layer.tips( json.subject_check, subject, 3, title.offsetWidth, 1);
        subject.focus();
        return;
    }
    if(content.val() == ''){
        layer.tips( json.content_check, content, 3, content.offsetWidth, 0);
        content.focus();
        return;
    }
    $("form")[0].submit();
}

function delete_system_message(id){
    if(typeof id == 'undefined'){
        id = getSelectedIds().join(',');
        if(!id){
            return;
        }
    }
    tradove.confirm("Are you sure to do this?", function(){
        from = window.location.href;
        location.href = base_url + 'admin/delete_system_message?ids=' + id ;
    });
}

function send_network_setting(){
    network = $('input[name="network_name"]');
    if(network.val() == ''){
        layer.tips( 'Name is empty', network, 3, network.offsetWidth, 1);
        network.focus();
        return;
    }
    company = $('input[name="company_id"]');
    if(company.val() == ''){
        layer.tips( 'Comapny ID is empty', company, 3, company.offsetWidth, 1);
        company.focus();
        return;
    }

    $('form')[0].submit();
}

function find_company(){
    company = $('input[name="company_id"]').val();
    var loading;
    if(company != ''){
        $.ajax({
            url:base_url + 'admin/find_company/' + company,
            before:function(){
//                loading = layer.load(0);
            },
            success:function(r){
//                layer.close(loading);
                $('#company_info').remove();
                $('.left_980_column').append(r);
            },
            error:function(){
//                layer.close(loading);
            }
        });
    }
}

$(function(){

    $('#setup').click(function(){
        var blogEmail = $("#email"), blogEmail2 = $("#email2"), requestUrl = '/admin/blogSetUpAdmin',loadingSection = $("#loading");

        function isEmail(email) {
            if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1){
                return true;
            }
            return false;
        }

        function blogErrorInfo(blogMessage, blogEmail){
            layer.tips(blogMessage, blogEmail, {
                style: ['background-color:#E227C7; color:#fff', '#E227C7'],
                maxWidth:185,
                closeBtn:[0,true]
            });
        }

        if(!isEmail(blogEmail.val())){
            blogErrorInfo('Your email format is not correct.', blogEmail);
            return false;
        }

        if( !isEmail(blogEmail2.val())){
            blogErrorInfo('Your email format is not correct.', blogEmail2);
            return false;
        }

        if(blogEmail.val() != blogEmail2.val()){
            blogErrorInfo('The two email addresses do not match', blogEmail);
            return false;
        }

        $.ajax({
            type: "post",
            url: requestUrl,
            data : {'email' : blogEmail.val()},
            dataType : 'json',
            beforeSend : function(){
                loadingSection.showLoading();
            },
            success: function(response){
                loadingSection.hideLoading();
                var code = response['code'], msg = response['result'];
                if(code == 0){
                    layer.msg(msg, 3, 3);
                }else{
                    layer.msg(msg, 2, 10);
                }

            },
            error:function(){
                loadingSection.hideLoading();
            }
        });

    });

    $('a[name="load_news"]').on('click', function(){
        var $this = $(this), action = $this.attr('action');
        window.location.href = '/admin/news/'+action;
    });

    $('input[name="news_action"]').on('click', function(){

        var ids = getSelectedIds(), len = ids.length, $this = $(this),
            action = $this.attr('action'),
            type = $('#news_action_type').val(),
            loading = function(){
                $("input[type='checkbox'][name='select_one']:checked").map(function () {
                    $(this).closest('.left_980_c_c_detail_list').showLoading();
                });
            },
            hideLoading = function(){
                $("input[type='checkbox'][name='select_one']:checked").map(function () {
                    $(this).closest('.left_980_c_c_detail_list').hideLoading();
                });
            };

        if(len == 0){
            layer.msg('Please select one item at least.', 3, 3);
        }

        //console.log(ids);
        $.ajax({
            type: "post",
            url: '/news/report/handle',
            data : {'ids' : ids, 'action' : action},
            dataType : 'json',
            beforeSend : function(){
                loading();
            },
            success: function(response){
                //console.log(response);
                hideLoading();
                if(response == 0){
                    layer.msg('no data was be updated', 3, 3);
                }else{
                    layer.msg('update '+response+' records', 2, 10);
                    window.location.href = '/admin/news/'+type;
                }
            },
            error:function(){
                hideLoading();
            }
        });

    });


})
