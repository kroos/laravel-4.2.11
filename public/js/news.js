$(function(){

    $('#add_comment').on('click', function(){
        var cts = $('#comment_content').val(),
            nid = $('#news_id').val(),
            loadingDiv = $(this).parents(".left_720_c_c_post");

        if(null == cts || cts == "") {
            layer.msg('Please input content before sending.', 3, 3);
            return false;
        }

        $.ajax({
            url: '/news/comments/reply',
            type: 'POST',
            data: {
                "content": cts,
                "newsId": nid
            },
            beforeSend : function(){
                loadingDiv.showLoading();
            },
            success: function (data) {
                $("#data_list").prepend(data);
                $('#comment_content').val("");
                loadingDiv.hideLoading();
                //layer.msg('Reply the discussion successfully.', 3, 10);

                var reply_number = $('#comment_num').val();
                $('#news_reply_comment_number').text("");
                $('#news_reply_comment_number').text("("+reply_number+")");
            },
            error: function (data) {
                loadingDiv.hideLoading();
            }

        });

    })

    /**
     * report news comment
     */
    $(document).on( 'click', 'a[name="news_talk"]', function(){
        var $this = $(this), cid = $this.siblings('input[name="cid"]'),
            reportType = $this.siblings('input[name="report_type"]').val(),
            commentId = cid.val(),
            hideReportIcon = function(reportType, hideObj){
                reportType == 1 ? $('#report_span').hide() : hideObj.closest('.btn').hide();
            };

        $.ajax({
            url : '/news/report',
            type : 'post',
            data : {targetId : commentId, type : reportType},
            dataType : 'json',
            beforeSend : function(){
                $this.showLoading();
            },
            success: function(data){
                $this.hideLoading();
                //console.log(data);
                hideReportIcon(reportType, $this);
            },
            complete: function(){
                $this.hideLoading();
            }
        })
    });

    $('#cancel_comment').on('click', function(){
        console.log('dfdfdfd');
        $('#comment_content').val('');
    })

    $('a[name="load_news"]').on('click', function(){
        var $this = $(this), action = $this.attr('action');
        window.location.href = '/news/comment/'+action;
    });


    $('input[name="news_action"]').on('click', function(){

        var ids = getSelectedIds(), len = ids.length, $this = $(this),
            action = $this.attr('action'),
            type = $('#news_action_type').val(),
            loading = function(){
                $("input[type='checkbox'][name='select_one']:checked").map(function () {
                    $(this).closest('.left_720_c_c_detail_mylist').showLoading();
                });
            },
            hideLoading = function(){
                $("input[type='checkbox'][name='select_one']:checked").map(function () {
                    $(this).closest('.left_720_c_c_detail_mylist').hideLoading();
                });
            };

        if(len == 0){
            layer.msg('Please select one item at least.', 3, 3);
        }

        //console.log(ids);
        $.ajax({
            type: "post",
            url: '/news/report/handle',
            data : {'ids' : ids, 'action' : action},
            dataType : 'json',
            beforeSend : function(){
                loading();
            },
            success: function(response){
                //console.log(response);
                hideLoading();
                if(response == 0){
                    layer.msg('no data was be updated', 3, 3);
                }else{
                    layer.msg('update '+response+' records', 2, 10);
                    window.location.href = '/news/comment/'+type;
                }
            },
            error:function(){
                hideLoading();
            }
        });

    });

});