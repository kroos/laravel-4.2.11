
//created by tian wei. 2013-12-05

tradove = {
    index: null,
    msg : function(msg,type,time){
        var tempType = (arguments[1] || arguments[1] == 0) ? arguments[1] : 1;
         $.layer({
            shade : false,
            area : ['auto','auto'],
            border : [3 , 0.3 , '#666', true],
            title:false,
            closeBtn:true,
            time:arguments[2] ? arguments[2] : 3,
            dialog:{
                msg:msg ,
                type:tempType,
                btns:0
            }
        });
    },
    confirm : function(msg, func){
        i = $.layer({
            shade : true,
            title: json.caution,
            area : ['auto','auto'],
            dialog : {
                msg: msg,
                btns : 2,
                type : 0,
                btn : ['Yes','No'],
                yes : function(){
                    layer.close(i);

                    loading = $.layer({
                        type : 3
                    });

                    func.call(this,loading);
                },
                no : function(){
                    layer.close(i);
                }
            }
        });
    },
    //pop invitation window
    invitation : function(type, target_id, remove){
        var callBack =function(){};
        if(arguments.length>3){
            callBack = arguments[3];
        }
        //

        //company
        if(type == 'company'){
            i = null;
            $.ajax({
                url : base_url + 'connect_company',
                method : 'POST',
                data : {
                    'id' : target_id,
                    'from' : window.location.href
                },
                beforeSend:function(){
                    i = layer.load(0);
                },
                success:function(r){
                    layer.close(i);

                    if(r == 'need confirm' ){
                        tradove.invitation(4, target_id, remove);
                    }else{
                        tradove.msg(json.connected + ' ' + json.success);

                        if(typeof remove == "string"){
                            $('#'+remove).remove();
                        }else if(typeof remove == "object"){
                            $(remove).parent().remove();
                        }
                        callBack();
                    }
                },
                error: function (data) {
                    layer.close(i);
                    tradove.msg("Join the company failed.");
                }
            });

            return;
        }

        if(type == 'group'){
            i = null;
            $.ajax({
                url: base_url + 'group/join',
                dataType: 'json',
                type: 'POST',
                data:{
                    "groupId": target_id
                },
                beforeSend:function(){
                    i = layer.load(0);
                },
                success: function (data) {
                    layer.close(i);
                    if(data.result == 'success' && data.state == 30 ) {
                        tradove.msg("You have joined the group successfully.");
                    } else if(data.result == 'success' && data.state == 0 ) {
                        tradove.msg("You have sent the joining request to the group manager.");
                    }
                    if(typeof remove == "string"){
                        $('#'+remove).remove();
                    }else if(typeof remove == "object"){
                        $(remove).parent().remove();
                    }
                    callBack();
                },
                error: function (data) {
                    layer.close(i);
                    tradove.msg("Join the group failed.");
                }
            });

            return;
        }

        i = null;

        $.ajax({
            url : base_url + 'get_invitation_form/' + type + '/' + target_id,
            beforeSend:function(){
                i = layer.load(0);
                if($("#pop_invitation_form"))
                    $("#pop_invitation_form").remove();
            },
            success:function(r){
                layer.close(i);
                if($("#pop_invitation_form")){
                    layer.close(tradove.index);
                }
                $("body").append(r);
                tradove.index = $.layer({
                    type : 1,
                    title : false,
                    border : [0],
                    area : ['450px','auto'],
                    page : {dom : '#pop_invitation_form'},
                    success : function(){
                        $('#invitation_send').click(function(){
                            tradove.sendInvitation(remove);
                        });
                    }
                });
            },
            error: function (data) {
                layer.close(i);
            }
        });
    },
    //send post
    sendInvitation : function( remove ){
        i = null;

        subject = $('input[name="invitation_subject"]');
        if(!subject.val()){
            layer.tips(json.subject_check, subject, 3, subject.width(), 0);
            return;
        }

        content = $('textarea[name="invitation_content"]');
        if(!content.val()){
            layer.tips(json.content_check, content, 3, content.width(), 0);
            return;
        }

        loop_type = $('input[name="loop_type"]:checked');
        if(loop_type){
            loop_type_val = loop_type.val();
        }else{
            loop_type_val = '';
        }

        $.ajax({
            url: base_url + "connect_add",
            method:'POST',
            data:{
                receiver_id : $('input[name="invitation_receiver_id"]').val(),
                target_id : $('input[name="invitation_target"]').val(),
                type : $('input[name="invitation_type"]').val(),
                subject : subject.val(),
                content : content.val(),
                loop_type : loop_type_val
            },
            beforeSend:function(){
                i = layer.load(0);
            },
            success:function(r){
                layer.close(i);
                layer.close(tradove.index);
                if( r > 0 ){
                    subject.val('');
                    content.val('');
                    tradove.msg(json.sent + ' ' + json.success);
                    if(typeof remove == "string"){
                        $('#'+remove).remove();
                    }else if(typeof remove == "object"){
                        $(remove).parent().remove();
                    }
                }else{
                    tradove.msg(json.sent + ' ' + json.fail);
                }
            }
        });
    },
    //pop message form
    pop_message : function(receiver_id, receiver_name, callback){

        if(!document.getElementById("pop_message_form")){
            i = null;
            $.ajax({
                url : base_url+'get_message_form',
                beforeSend:function(){
                    i = layer.load(0);
                },
                success:function(r){
                    layer.close(i);
                    $("body").append(r);
                    tradove.pop_message(receiver_id, receiver_name, callback);
                }
            });
        }else{
            tradove.index = $.layer({
                type : 1,
                title : false,
                border : [0],
                area : ['450px','auto'],
                page : {dom : '#pop_message_form'},
                success : function(){
                    $("#message_receiver_name").html(receiver_name);
                    $('input[name="message_receiver_id"]').val(receiver_id);
                    if(typeof callback == "function"){
                        callback.call(this);
                    }
                }
            });
        }
    },
    //post message
    sendMessage : function(){
        i = null;

        subject = $('input[name="message_subject"]');
        if(!subject.val()){
            layer.tips(json.subject_check, subject, 3, subject.width(), 0);
            return;
        }

        content = $('textarea[name="message_content"]');
        if(!content.val()){
            layer.tips(json.content_check, content, 3, content.width(), 0);
            return;
        }

        $.ajax({
            url: base_url + "message/send",
            method:'POST',
            data:{
                receiver_id : $('input[name="message_receiver_id"]').val(),
                subject : subject.val(),
                content : content.val(),
                link_url: $('input[name="link_url"]').val(),
                link_text: $('input[name="link_text"]').val(),
                msg_type: $("input[name='msg_type']").val(),
                target_name: $("input[name='target_name']").val()
            },

            beforeSend:function(){
                i = layer.load(0);
            },

            success:function(r){
                layer.close(i);
                layer.close(tradove.index);
                if( r > 0 ){
                    subject.val('');
                    content.val('');
                    tradove.msg(json.msg + ' ' + json.sent + ' ' + json.success);
                }else{
                    tradove.msg(json.msg + ' ' + json.sent + ' ' + json.fail);
                }
            }
        });
    }
}