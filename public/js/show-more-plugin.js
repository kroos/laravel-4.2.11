/**
 * require list:
 *  jquery-x.x.x.js
 *  layer.js
 *<pre>
 * desc: url parameter must be exist,
 *      page default set to 1,
 *      showMoreId default set to 'show_more',
 *      containerId default set to 'data_list'
 *</pre>
 * Example:
 * <pre>
 *    ShowMore.initial({
 *           url : "/industry/more_product",
 *           params : {
 *               page: 1
 *           },
 *           showMoreId : "show_more",
 *           containerId : "data_list"
 *       });
 *    ShowMore.addModels();
 * </pre>
 */
ShowMore = {
    /**
     * url for ajax request
     */
    url: "",
    /**
     * params in the url,must contain page parameter
     */
    params: {
        page: 1
    },
    /**
     * show more a link id,default show_more
     */
    showMoreId: "show_more",
    /**
     * data container id,default data_list
     */
    containerId: "data_list",
    /**
     * initial show more object
     * @param object config
     */
    initial:function( config){
        this.url = config.url;
        if(config.params){
            this.params = config.params;
        }
        if(config.showMoreId){
            this.showMoreId = config.showMoreId;
        }
        if(config.containerId){
            this.containerId = config.containerId;
        }
        $("#"+this.showMoreId).hide();
        $("#"+this.showMoreId).unbind();
        $("#"+this.showMoreId).bind("click",function(){
            ShowMore.addModels();
        });
        this.params.page = 1;
    },
    /**
     * add models to the page
     */
    addModels:function(){
        //var index = layer.load(10,0,false);
        var cc = $("#"+this.showMoreId).parent();
        if(! this.url){
            console.error("url must not be empty");
            return;
        }
        $.ajax({
            type: "GET",
            url: this.url,
            data: this.params,
            beforeSend : function(){
                cc.showLoading();
            },
            success: function(response){
                //console.log(response);
                cc.hideLoading();                
                $("#"+ShowMore.containerId).append(response);
                //layer.close(index);
                ShowMore.params.page++;
            },
            error:function(data){
                cc.hideLoading();
                console.error(data);
            }
        });
    }
}