
//type: product or company
function show_more(type){
    var url;
    if(type == "suggest_product")
        url = "product_suggest_data";

    if(type == "suggest_company")
        url = "company_suggest_data";

    data = {
        'start' : $("#start").val()
    };

    $.ajax({
        'url': url,
        'data': data,
        beforeSend:function(){
            $(".left_top_readmore").unbind("click");
            $(".left_top_readmore").html(json.loading);
        },
        error:function(){
            $(".left_top_readmore").html(json.timeout);
            $(".left_top_readmore").click(function(){
                show_more(type);
            });
        },
        success:function(response){
            $(".left_top").remove();
            $(".left_650_c_c_left470").append(response);
        }
    });
}

function list_show_more(type){
    var url;
    if(type == "product")
        url = "product_list_data";
    if(type == "company")
        url = "company_list_data";

    $.ajax({
        url: url,
        data:{
            'start' : $("#start").val()
        },
        beforeSend:function(){
            $(".left_top_readmore").unbind("click");
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}

/**
 * @param string type
 * @param array ids
 */
function do_delete(type, ids, loading){
    var url;
    if(type == "product")
        url = "product_connect_delete";
    if(type == "company")
        url = "company_connect_delete";

    $.ajax({
        url:url,
        data:{
            ids:ids.join(',')
        },
        error:function(){
            layer.close( loading );
        },
        success:function(r){
            layer.close( loading );
            console.log(r);
            if(r > 0){
                for(var i = 0; i < ids.length; i++){
                    $("#p_" + ids[i]).remove();
                }
                $('input[name="select_all"]').attr("checked", false);
            }

            total =  $("#total").html();
            $("#total").html( total - ids.length );
        }
    });
}

function connections_delete(type){

    var tmp = getSelectedIds();

    if(tmp.length > 0){
        tradove.confirm(json.confirm,function( loading ){
            do_delete(type, tmp, loading);
        });
    }

}

function delete_one(type,id){
    tradove.confirm(json.confirm,function( loading ){
        do_delete(type, [id], loading);
    });
}

function search(type, i){
    var keyword = $('input[name="connect_search_keyword"]').val();
    if(keyword == '' || keyword == 'Keyword Search'){
        if(type == 'product')
            url = 'product_connect_list';
        if(type == 'suggest_product')
            url = 'product_suggest';

        if(type == 'company')
            url = 'company_connect_list';
        if(type == 'suggest_company')
            url = 'company_suggest';

        window.location.href = url + '?i=' +i;
    }else{
        var url;

        if(type == 'product')
            url = 'product_connect_search';
        if(type == 'suggest_product')
            url = 'product_suggest_search';

        if(type == 'company')
            url = 'company_connect_search';
        if(type == 'suggest_company')
            url = 'company_suggest_search';

        window.location.href = url + '?i='+i+'&s=' + keyword;
    }
}

function search_show_more(type){
    var url;
    if(type == "product")
        url = "product_connect_search_data";
    if(type == "suggest_product")
        url = "product_suggest_search_data";

    if(type == "company")
        url = "company_connect_search_data";
    if(type == "suggest_company")
        url = "company_suggest_search_data";

    $.ajax({
        url: url,
        data:{
            'start' : $("#start").val()
        },
        beforeSend:function(){
            $(".left_top_readmore").unbind("click");
        },
        success:function(r){
            $(".left_top").remove();
            $("#list").append(r);
        }
    });
}