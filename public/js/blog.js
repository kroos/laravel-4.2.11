$(function(){

    $("a[name='blog_read_more']").on("click", function(e){

        e.stopImmediatePropagation();
        var $this = $(this), ct = $this.parent("li[name='blog_content']"), fullContents = ct.siblings("li[name='blog_content_hidden']");

        ct.hide();
        fullContents.show();
        fullContents.slideDown();

    });

    $("a[name='blog_read_more_close']").on("click", function(e){

        e.stopImmediatePropagation();
        var $this = $(this), ct = $this.parent("li[name='blog_content_hidden']"), cutContent = ct.siblings("li[name='blog_content']");

        ct.hide();
        cutContent.show();
    });

    var x = $("li[name='blog_content']").children("img");
    //console.log(x);

})