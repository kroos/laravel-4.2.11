function show_more(time, industry){
    i = layer.load();
    $.ajax({
        url:"get_alerts_and_blogs",
        data:{
            time : time,
            i : industry
        },
        timeout:1000*10,
        success:function(response){
            $(".left_top").remove();
            $("#lists").append(response);
        },
        complete:function(){
            layer.close(i);
        }
    });
}
