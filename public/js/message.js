/**
 * Created with JetBrains PhpStorm.
 * User: zhimin
 * Date: 13-11-26
 */


$(function(){

    $("input[name='btn_send']").click(function(){

        var parentId = $("input[name='parent_id']").attr('value');
        if( !parentId ) {
            parentId = 0;
        }

        var receiverId = $("input[name='receiver_id']").attr('value');
        var subject = $("input[name='subject']").val();
        var content = $("[name='content']").val();
        var link_url = $("[name='link_url']").val();
        var link_text = $("[name='link_text']").val();
        var msg_type = $("input[name='msg_type']").val();
        var target_name = $("input[name='target_name']").val();

        if( !receiverId || !subject || !content ) {
            tradove.msg("Missed some parameters");
            return false;
        }

        $.ajax({

            url:"/message/send",
            dataType: 'json',
            type: 'POST',

            data: {
                "parent_id": parentId,
                "receiver_id": receiverId,
                "subject": subject,
                "content": content,
                "link_url": link_url,
                "link_text": link_text,
                "msg_type": msg_type,
                "target_name" : target_name
            },

            success: function (data) {

                if(data > 0) {
                    tradove.msg("Message sent successfully.");
                    self.location = "/message/outbox/all";
                } else {
                    tradove.msg("Message sent failed");
                }

            },

            error: function (data) {
                tradove.msg("Message sent failed");
            }

        });

    });

    $("input[name='btn_cancel']").click(function(){

        $("input[name='parent_id']").val('0');
        $("input[name='receiver_id']").val('');
        $("input[name='receiver']").val('');
        $("input[name='subject']").val('');
        $("[name='content']").val('');

    });

    $(document).on('click', 'input[name="receiver"]', function(){
        $(this).winpop({
            setId : 'receiver_id',
            setName : 'receiver_name',
            type : 'mutil'
        });
    });


});

