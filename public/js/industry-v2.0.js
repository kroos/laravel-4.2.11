;(function($, window, document,undefined){

    //搜索到的字符串加粗
    $.fn.wrapInTag = function(opts) {
        var tag = opts.tag || 'strong', words = opts.words || [],regex = RegExp(words.join('|'), 'gi'),replacement = '<'+ tag +'>$&</'+ tag +'>';
        return this.html(function() {
            return $(this).text().replace(regex, replacement);
        });
    };

    $.fn.industry = function(options){
        var defaults = {
            'industry_div_id':'industryTree',//行业div的弹出层
            'trigger_id' : 'showIndustry',//默认触发行业div的输入框id
            'industry_id':'industry',//选中行业后,写入行业ID到此hidden项中
            'redirect_url' : '',//若这项被指定, 选择行业后进行重定向,
            'search_id' : 'searchIndustry',//触发行业搜索ID
            'callBack' : function(){} //用户回调函数
        };
        var settings = $.extend({}, defaults, options);

        return this.each(function(){
            //首先显示行业选择div
            var cls = function(){
                $('#'+settings.trigger_id).val('');
                $('#'+settings.search_id).val('');
                $('#'+settings.industry_id).val('');
            }
            var showDiv = function(){
                //清除搜索框上次搜索结果
                var s = $('#'+settings.search_id);
                if(s.val()){
                    cls();
                }
                $('#'+settings.industry_div_id).show();
                $('#'+settings.search_id).focus();
                $('.industry_result_overflow').show();
            }
            showDiv();

            //鼠标移开, div隐藏
            $('#'+settings.industry_div_id).on({
                'mouseleave' : function(){
                    $(this).hide();
                    $('.industry_search_overflow').hide();
                }
            });

            //绑定行业搜索行为
            var info = [] ;
            _.each($('.industry_result_1, .industry_result_2, .industry_result_2nd_1, .industry_result_2nd_2'), function(i){
                var o = {'id':$(i).attr('id'), 'tx':$(i).attr('vl')};
                info.push(o);
            });

            $(document).on('input', '#'+settings.search_id, function(){
                var that = $(this), val=that.val();
                if (val.length > 1) {
                    $('.industry_result_overflow').hide();
                    $('.industry_search_overflow').show();
                    $('.industry_search_overflow_1').remove();
                    _.each(info, function(i){
                        var oj = i.tx;
                        if(oj.toLowerCase().indexOf(val)>-1){
                            $('<div>',{
                                'class' : 'industry_search_overflow_1',
                                'id' : i.id,
                                'vl' : i.tx,
                                'html' : i.tx
                            }).on('click', function(){
                                var that = $(this), p = that.parent().attr('is');
                                if(typeof p !== 'undefined'){
                                    that.attr('is', 'sec');
                                }
                                //console.log(that.attr('is'));
                                confirm(that);
                            }).appendTo('.industry_search_overflow');
                        }
                    });

                    $('.industry_search_overflow_1').wrapInTag({
                        tag: 'strong',
                        words: [val]
                    });
                } else {
                    $('.industry_result_overflow').show();
                    $('.industry_search_overflow').hide();
                }
            });

            //绑定行业选中事件
            $('.industry_result_1, .industry_result_2, .industry_result_2nd_1, .industry_result_2nd_2').click(function(e){
                e.stopImmediatePropagation();
                var that = $(this);
                confirm(that);
            });

            var confirm = function(obj){
                //设置数据
                var isSec = obj.attr('is');
                //console.log('tttt');
                //console.log(isSec);
                if(typeof isSec == 'undefined'){
                    //console.log(obj.attr('vl'));
                    //console.log(obj.attr('id'));
                    $('#'+settings.trigger_id).val(obj.attr('vl'));
                    $('#'+settings.industry_id).val(obj.attr('id'));
                    $('#'+settings.industry_div_id).hide();
                }else{
                    $('#showIndustry2').val(obj.attr('vl'));
                    $('#industry2').val(obj.attr('id'));
                    $('#industryTree2').hide();
                }

                //执行回调函数
                settings.callBack();

                //执行重定向
                if(settings.redirect_url){
                    location.href = settings.redirect_url + $('#'+settings.industry_id).val();
                }
            }

            //第一级行业div的显示或隐藏
            $('.industry_result_1').on({
                'mousemove' : function(){
                    $('.industry_result_2nd').hide();
                    $('.industry_result_3nd').hide();

                    if($(this).attr('is') == 'sec'){
                        $('#p_'+$(this).attr('id')).show();
                    }else{
                        $('#s_'+$(this).attr('id')).show();
                    }
                }
            });
            $('.industry_result_2').on({
                'mousemove' : function(){
                    $('.industry_result_2nd').hide();
                    $('.industry_result_3nd').hide();
                }
            });

            //二级div鼠标移开事件
            $('.industry_result_2nd').on({
                'mouseleave' : function(){
                    //$(this).hide();
                    if(!$('.industry_result_3nd').is(':hidden')){
                        $('.industry_result_2nd').hide();
                        $('.industry_result_3nd').hide();
                    }
                }
            });

            //二级div显示或隐藏行为
            $('.industry_result_2nd_1').on({
                'mouseleave' : function(){
                    if($(this).attr('is')== 'sec'){
                        $('#x_'+$(this).attr('id')).hide();
                    }else{
                        $('#t_'+$(this).attr('id')).hide();
                    }
                },
                'mousemove' : function(){
                    if($(this).attr('is')== 'sec'){
                        $('#x_'+$(this).attr('id')).show();
                    }else{
                        $('#t_'+$(this).attr('id')).show();
                    }
                }
            });

            //三级DIV
            $('.industry_result_3nd').on({
                'mouseleave' : function(){
                    $(this).hide();
                },
                'mousemove' : function(){
                    $(this).show();
                }
            });



        });
    }

})(jQuery, window, document);;