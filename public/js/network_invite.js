$(function(){

    $("#btnCancel").on('click',function(){
        $("#first").val('');
        $("#email").val('');
    });


    function emailCheck(email, tip){

        //1.check input whether is null
        if(!email){
            tip.text('Email is required.').addClass('font_red');
            return false;
        }else{
            tip.removeClass('font_red').text('');
        }

        //2.check email format
        if (!isEmail(email)) {
            tip.text('email format is not right').addClass('font_red');
            return false;
        }else{
            tip.removeClass('font_red').text('');
        }

        return true;

    }

    function nameCheck(name, tip){
        if(!name){
            tip.text('Please enter first name and email of the person you are inviting.').addClass('font_red');
            return false;
        }else{
            tip.removeClass('font_red').text('');
        }

        return true;
    }

    $('#email').on('blur', function(){
        var that = $(this), email = $.trim(that.val()), tip = $('#emailTip');
        emailCheck(email, tip);
    });

    $('#first, #last').on('blur', function(){
        var that = $(this), name = $.trim(that.val()), tip = $('#emailTip');
        nameCheck(name, tip);
    });

    $('#btnSend').on('click', function(){
        //var email = $('#email'), emailVal = $.trim(email.val()), firstName = $.trim($('#first').val()), lastName = $.trim($('#last').val()), tip = $('#emailTip');
        var email = $('#email'), emailVal = $.trim(email.val()), firstName = $.trim($('#first').val()), tip = $('#emailTip');

        //var checkResult = emailCheck(emailVal, tip) && nameCheck(firstName, tip) && nameCheck(lastName, tip);
        var checkResult = emailCheck(emailVal, tip) && nameCheck(firstName, tip);

        if(checkResult){

            $.ajax({
                url: '/network/email/invite/check',
                type: 'POST',
                data: {"email": emailVal},
                beforeSend: function () {
                    layer.load();
                },
                success: function (data) {

                    layer.loadClose();

                    var result = $.parseJSON(data), code = result.code, returnData = result.data;
                    console.log("btnSend" + data);
                    if (code == 3 || code == 1) {
                        tradove.msg(result.msg, 5, 5);
                        //email.focus();
                        return false;
                    }else if(code == 2){
                        //站外用户,直接发送邀请邮件，邀请加入site,然后发送邀请加入network
                        //var sendData = {'email':emailVal, 'firstName': firstName, 'lastName': lastName};
                        var sendData = {'email':emailVal, 'firstName': firstName};
                        $.post('/invite/join/network/out', sendData, function(response){
                            console.log("response.code: " + response.code);
                            if(response.code == 1){
                                tradove.msg(response.msg, 0, 5);
                            }

                            return true;
                        }, 'json');

                    }else if(code == 4){
                        //站内用户，非network用户, 直接发送加入network的用户邀请
                        var sendData = {'receiverId':returnData, 'type':5, 'isSendMail': true};
                        $.post('/invite/add', sendData, function(response){

                            if(response.code == 1){
                                tradove.msg(response.msg, 1, 5);
                            }

                            return true;
                        }, 'json');
                    }else if(code == 5){
                        //站内用户, 导入的待激活用户,发送待激活邮件
                        var receiverId = returnData['invite_user_id'], networkId = returnData['invite_network_id'], sendData = {'receiverId':receiverId, 'networkId':networkId};
                        $.post('/invite/email/unactive', sendData, function(response){

                            if(response.code == 1){
                                tradove.msg(response.msg, 1, 5);
                            }

                            return true;
                        }, 'json');
                    }
                },
                error: function (data) {
                    layer.loadClose();
                }
            });
        }

    })


});